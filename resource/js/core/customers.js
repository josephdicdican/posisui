function Log(data) {
	var self = this;

	self.customer_log_id = ko.observable(-1);
	self.user_id = ko.observable(-1);
	self.customer_id = ko.observable(-1);
	self.remarks = ko.observable();
	self.date_created = ko.observable();
	self.is_unread = ko.observable();
	self.display_name = ko.observable();

	self.format_date = function() {
		var dateStr = self.date_created();
		return toDateTimeString(dateStr);
	};

	if (data) {
		self.customer_log_id(data.customer_log_id);
		self.user_id(data.user_id);
		self.customer_id(data.customer_id);
		self.remarks(data.remarks);
		self.date_created(data.date_created);
		self.is_unread(data.is_unread);
		self.display_name(data.display_name);
	};

	self.errors = ko.validation.group(self);
}

$(function() {
	function CustomerViewModel()
	{
		var self = this;

		self.customers = ko.observableArray([]);
		self.customer = ko.observable(new Customer());

		self.logs = ko.observableArray([]);
		self.log = ko.observable(new Log());

		self.search_str = ko.observable('');
		self.str_search = ko.observable('');
		self.sort_by = ko.observable('asc');
		self.sort_column = ko.observable('');

		self.no_customer = ko.computed(function() {
			return (self.customer().customer_id() == -1);
		});

		self.search_str.subscribe(function() {
			var myStr = self.search_str();

			myStr = myStr.toLowerCase();
	        myStr = myStr.replace(/\s+/g, "+");

	        self.str_search(myStr);

			self.getCustomers();
		});

		self.sortResult = function(column) {
			if(self.sort_by() != '') {
				if(self.sort_by() == 'asc') {
					self.sort_by('desc');
				} else {
					self.sort_by('asc');
				}
			}

			self.sort_column(column);
			self.getCustomers();
		};

		self.getCustomers = function() {
			var url = getRootUrl() + 'api/customers/0?query=' + self.str_search() + '&sort_column=' + self.sort_column() + '&sort_by=' + self.sort_by();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.customers.removeAll();
					self.customer(new Customer());
					$.each(data, function(index, item) {
						self.customers.push(new Customer(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getCustomer = function(customer) {
			$.ajax({
				url: getRootUrl() + 'api/customers/' + customer.customer_id(),
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					var customer = new Customer(data);

					self.customer(customer);
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});

			self.getLogs(customer.customer_id());
		};

		self.getLogs = function(id) {
			$.ajax({
				url: getRootUrl() + 'api/logs/customer/' + id,
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					/*var log = new Log(data);
					self.log(log);
					console.log(data);*/
					self.logs.removeAll();
					$.each(data, function(index, item) {
						self.logs.push(new Log(item));
					});
					console.log(ko.toJSON(self.logs()));
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.showAdd = function () {
			self.customer(new Customer());
		};

		self.addCustomer = function() {
			var json = ko.toJSON(self.customer());

			if(self.customer().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/customers/add',
					type: 'POST',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						if(data == true) {
							$('#addcustomer').modal('hide');
							self.getCustomers();
						} else if(data == -1) {
							$('#customer_exist').modal('show');
						}
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				$('#form_error').modal('show');
				self.customer().errors.showAllMessages();
			}
		};

		self.showEdit = function(customer) {
			self.customer(customer);
			$('#editcustomer').modal('show');
		};

		self.editCustomer = function() {
			var json = ko.toJSON(self.customer());

			if (self.customer().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/customers/edit/' + self.customer().customer_id(),
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						if(data == true) {
							$('#editcustomer').modal('hide');
							self.getCustomers();
						} else if(data == -1) {
							$('#customer_exist').modal('show');
						}
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				$('#form_error').modal('show');
				self.customer().errors.showAllMessages();
			}
		};

		self.deleteCustomer = function() {
			$.ajax({
				url: getRootUrl() + 'api/customers/delete/' + self.customer().customer_id(),
				type: 'DELETE',
				contentType: 'application/json',
				success: function(data) {
					console.log(data);
					$('#deletecustomer').modal('hide');
					self.getCustomers();
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getCustomers();
	}

	ko.applyBindings(new CustomerViewModel());
});