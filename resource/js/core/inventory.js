function Inventory(data)
{
	var self = this;

	self.inventory_id = ko.observable(-1);
	self.product_id = ko.observable();
	self.product_name = ko.observable();
	self.price_per_unit = ko.observable(0);
	self.stock_at_hand = ko.observable(0).extend({ required: true });
	self.max_stock_qty = ko.observable(0).extend({ required: true });
	self.min_stock_qty = ko.observable(0).extend({ required: true });
	self.reordering_qty = ko.observable(0).extend({ required: true });
	self.storeitemname = ko.observable();
	self.store_item_id = ko.observable();

	self.price_formatted = ko.computed(function() {
		var price = toMoney(Math.max(0, self.price_per_unit()));
		return 'Php ' + price;
	});

	if(data) {
		self.inventory_id(data.inventory_id);
		self.product_id(data.product_id);
		self.product_name(data.product_name);
		self.price_per_unit(data.price_per_unit);
		self.stock_at_hand(data.stock_at_hand);
		self.max_stock_qty(data.max_stock_qty);
		self.min_stock_qty(data.min_stock_qty);
		self.reordering_qty(data.reordering_qty);
		self.storeitemname(data.storeitemname);
		self.store_item_id(data.store_item_id);
	}

	self.errors = ko.validation.group(self);

	self.toJSON = function() {
		var copy = ko.toJS(self);

		delete copy.price_formatted;

		return copy;
	};
}

function Log(data)
{
	var self = this;

	self.inventory_log_id = ko.observable(-1);
	self.user_id = ko.observable(-1);
	self.display_name = ko.observable();
	self.inventory_id = ko.observable(-1);
	self.remarks = ko.observable();
	self.date_created = ko.observable();
	self.is_unread = ko.observable();

	self.format_date = function() {
		var dateStr = self.date_created();
		return toDateTimeString(dateStr);
	};

	if (data) {
		self.inventory_log_id(data.inventory_log_id);
		self.user_id(data.user_id);
		self.display_name(data.display_name);
		self.inventory_id(data.inventory_id);
		self.remarks(data.remarks);
		self.date_created(data.date_created);
		self.is_unread(data.is_unread);
	};

	self.errors = ko.validation.group(self);
}


function ReqInventory(id, name) {
	var self = this;

	self.inventory_id = ko.observable(id);
	self.stock_at_hand = ko.observable();
	self.storeitemname = ko.observable(name);
	self.max_stock_qty = ko.observable();
	self.min_stock_qty = ko.observable();
	self.reordering_qty = ko.observable();

	self.errors = ko.validation.group(self);

	self.toJSON = function() {
		var copy = ko.toJS(self);

		delete copy.storeitemname;

		return copy;
	};
}

$(function() {
	function InventoryViewModel()
	{
		var self = this;

		self.products = ko.observableArray([]);
		self.product = ko.observable(new Inventory());

		self.logs = ko.observableArray([]);
		self.log = ko.observable(new Log);

		self.storeitem = ko.observable(new StoreItem());

		self.inventory = ko.observable(new ReqInventory(0,''));
		self.is_parent = ko.observable(1);
		self.category_id = ko.observable(0);

		self.search_str = ko.observable('');
		self.str_search = ko.observable('');
		self.sort_by = ko.observable('asc');
		self.sort_column = ko.observable('');

		self.storeitem_categories = ko.observable(new StoreItemsCategories(true));

		self.no_product = ko.computed(function() {
			return self.product().inventory_id() == -1;
		});

		self.search_str.subscribe(function() {
			var myStr = self.search_str();

			myStr = myStr.toLowerCase();
	        //myStr = myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g,"");
	        myStr = myStr.replace(/\s+/g, "+");

	        self.str_search(myStr);

			self.getProducts();
		});

		self.storeitem_categories().category.subscribe(function() {
			var category_id = self.storeitem_categories().no_category() ? 0 : self.storeitem_categories().category().category_id();
			self.category_id(category_id);
			self.is_parent(0);
			self.getProducts();
		});

		self.storeitem_categories().parent_category.subscribe(function() {
			var category_id = self.storeitem_categories().no_parent_category() ? 0 : self.storeitem_categories().parent_category().category_id();
			self.category_id(category_id);
			self.is_parent(1);
			self.getProducts();
		});

		self.sortResult = function(column) {
			if(self.sort_by() != '') {
				if(self.sort_by() == 'asc') {
					self.sort_by('desc');
				} else {
					self.sort_by('asc');
				}
			}

			self.sort_column(column);
			self.getProducts();
		};

		self.getProducts = function() {
			var url = getRootUrl() + 'api/inventories/get_all?query=' + self.str_search() + '&sort_column=' + self.sort_column() + '&sort_by=' + self.sort_by() + '&filter=0&category_id=' + self.category_id() + '&is_parent=' + self.is_parent() + '&is_stockable=1';
			console.log(url);
			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.products.removeAll();
					self.product(new Inventory());
					$.each(data, function(index, item) {
						self.products.push(new Inventory(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getProduct = function(product) {
			console.log(ko.toJSON(product));
			$.ajax({
				url: getRootUrl() + 'api/inventories/get/' + product.inventory_id(),
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					console.log(data);
					var product = new Inventory(data);

					self.product(product);
					self.getLogs();
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getLogs = function() {
			$.ajax({
				url: getRootUrl() + 'api/logs/inventory/' + self.product().inventory_id(),
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					self.logs.removeAll();
					$.each(data, function(index, item) {
						self.logs.push(new Log(item));
					});
					console.log(ko.toJSON(self.logs()));
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.editProduct = function() {
			var json = ko.toJSON(self.product());

			if(self.product().errors().length == 0) {
				$.ajax({
					url:getRootUrl() + 'api/inventories/edit/' + self.product().inventory_id(),
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						$('#editproduct').modal('hide');
						self.getProducts();
					},
					error: function(xhr) {
						console.log(xhr);
					} 
				});
			} else {
				$('#form_error').modal('show');
				self.product().errors().showAllMessages();
			}
		};

		self.deleteProduct = function(product) {
			$.ajax({
				url: getRootUrl() + 'api/inventories/delete/' + self.product().inventory_id(),
				type: 'DELETE', 
				contentType: 'application/json',
				success: function(data) {
					$('#deleteproduct').modal('hide');
					self.getProducts();
					self.storeitem_categories().getStoreItems(self.category_id(), self.is_parent(), 1);
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.addInventory = function() {
			var json = ko.toJSON(self.inventory());
			
			if(self.inventory().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/inventories/add/',
					type: 'POST',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						$('#addinventory').modal('hide');
						self.getProducts();
						self.storeitem_categories().getStoreItems(self.category_id(), self.is_parent(), true);
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				$('#form_error').modal('show');
				self.inventory().errors.showAllMessages();
			}
		};

		self.getStoreItem = function(storeitem) {
			console.log(ko.toJSON(storeitem));
 
			self.inventory(new ReqInventory(storeitem.store_item_id(), storeitem.storeitemname()));
		};

		self.getProducts();
		self.storeitem_categories().getParentCategories();
	}

	ko.applyBindings(new InventoryViewModel());
});