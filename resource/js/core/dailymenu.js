function UnitMeasure(data) {
	var self = this;

	self.unit_id = ko.observable();
	self.measure_name = ko.observable().extend({required:true});
	self.storeitems_count = ko.observable();

	if(data) {
		self.unit_id(data.unit_id);
		self.measure_name(data.measure_name);
		self.storeitems_count(data.storeitems_count);
	}

	self.errors = ko.validation.group(self);

	self.toJSON = function() {
		var copy = ko.toJS(self);

		delete copy.errors;
		delete copy.storeitems_count;

		return copy;
	};
}

function ReqStoreItem() {
	var self = this;

	self.sku = ko.observable('');
	self.price_per_unit = ko.observable().extend({ required: true });
	self.unit_id = ko.observable().extend({ required: true });

	self.errors = ko.validation.group(self);

	self.toJSON = function() {
		var copy = ko.toJS(self);

		delete copy.errors;

		return copy;
	};
}

function MenuItem(data) {
	var self = this;

	self.inventory_id = ko.observable(-1);
	self.sku = ko.observable('');
	self.storeitemname = ko.observable('');
	self.stock_at_hand = ko.observable(0);
	self.price_per_unit = ko.observable(0);
	self.measure_name = ko.observable('');

	self.price_formatted = ko.computed(function() {
		return toMoney(self.price_per_unit());
	});

	if(data) {
		self.inventory_id(data.inventory_id);
		self.sku(data.sku);
		self.storeitemname(data.storeitemname);
		self.stock_at_hand(data.stock_at_hand);
		self.price_per_unit(data.price_per_unit);
		self.measure_name(data.measure_name);
	}
}

$(function() {
	function ReqProduct() {
		var self = this;

		self.product_name = ko.observable().extend({required: true});
		self.category_id = ko.observable().extend({required: true});
		self.variants = ko.observableArray([]);
		self.variant = ko.observable(new ReqStoreItem());
		self.is_stockable = ko.observable(0);

		self.add_variant = function() {
			if(self.variant().errors().length == 0) {
				self.variants.push(self.variant());
				self.variant(new ReqStoreItem());
			} else {
				$('#form_error').modal('show');
				self.variant().errors.showAllMessages();
			}
		};

		self.delete_variant = function(variant) {
			var flag = confirm('Confirm to remove ?');

			if(flag) {
				self.variants.remove(variant);
			}
		};

		self.errors = ko.validation.group(self);

		self.toJSON = function() {
			var copy = ko.toJS(self);

			delete copy.variant;
			delete copy.errors;

			return copy;
		};
	}

	function DailyMenuViewModel() {
		var self = this;

		/// properties start
			/// categories
				self.categories = ko.observableArray([]);
				self.category = ko.observable(new Category());
				self.edit_category = ko.observable(new Category());
				self.parent_categories = ko.observableArray([]);
				self.edit_sub_category = ko.observable(new SubCategory());
				self.delete_sub_category = ko.observable(new SubCategory());

			/// storeitems
				self.storeitems = ko.observableArray([]);
				self.storeitem = ko.observable(new StoreItem());
				self.unitmeasures = ko.observableArray([]);
				self.unitmeasure = ko.observable(new UnitMeasure());
				self.add_product = ko.observable(new ReqProduct());

			/// menu items
				self.menuitems = ko.observableArray([]);
				self.menuitem = ko.observable(new MenuItem());

		/// properties end

		/// helper methods start
			self.no_category = function() {
				return (self.category().category_id() == -1);
			};

			self.no_storeitem = function() {
				return (self.storeitem().store_item_id() == -1);
			};

			self.no_menuitem = function() {
				return (self.menuitem().inventory_id() == -1);
			};

			self.refresh = function() {
				self.category(new Category());
				self.storeitem(new StoreItem());
				self.getCategories();
				self.getStoreItems();
				self.getMenuItems();
			};
		/// helper methods end

		/// categories block start
			self.getParentCategories = function() {
				$.ajax({
					url: getRootUrl() + 'api/categories/get_parents/0/0',
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.parent_categories.removeAll();
						$.each(data, function(index, item) {
							self.parent_categories.push(new Category(item));
						});
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.getCategories = function() {
				$.ajax({
					url: getRootUrl() + 'api/categories/get_all/0',
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.categories.removeAll();
						self.storeitem(new StoreItem());
						$.each(data, function(index, item) {
							self.categories.push(new Category(item));
						});
						//console.log(data);
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.getCategory = function(category) {
				$.ajax({
					url: getRootUrl() + 'api/categories/' + category.category_id(),
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.category(new Category(data));
						self.storeitem(new StoreItem());
						self.getStoreItems();
						self.getMenuItems();
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.reloadCategory = function(id) {
				$.ajax({
					url: getRootUrl() + 'api/categories/' + id,
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						console.log(data);
						self.category(new Category(data));
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.editCategory = function(category) {
				self.edit_category(category);
			};

			self.addCategory = function() {
				self.getParentCategories();
				self.category(new Category());
			};

			self.deleteCategory = function() {
				var id = self.category().category_id();

				$.ajax({
					url: getRootUrl() + 'api/categories/delete/' + id,
					type: 'DELETE',
					contentType: 'application/json',
					success: function(data) {
						$('#deletecategory').modal('hide');
						self.category(new Category());
						self.getCategories();
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};
		/// category block end

		/// sub categories block start
			self.editSubCategory = function(sub) {
				self.edit_sub_category(sub);
				$('#editsubcategory').modal('show');
			};

			self.editSub = function() {
				var json = ko.toJSON(self.edit_sub_category());

				self.reloadCategory(self.edit_sub_category().parent_id());
				alert(json);
				self.edit_sub_category(new SubCategory());
			};

			self.confirmDelSubCategory = function(sub) {
				self.delete_sub_category(sub);
				$('#deletesubcategory').modal('show');
			};

			self.deleteSub = function() {
				var id = self.delete_sub_category().category_id(), parent_id = self.delete_sub_category().parent_id();

				$.ajax({
					url: getRootUrl() + 'api/categories/delete/' + id,
					type: 'DELETE',
					contentType: 'application/json',
					success: function(data) {
						$('#deletesubcategory').modal('hide');
						self.getCategories();
						self.reloadCategory(parent_id);
					},
					error: function(xhr) {
						console.log(xhr);
					}
				})
			};
		/// sub categories block end

		/// storeitems block start
			self.search_str = ko.observable('');
			self.str_search = ko.observable('');
			self.search_str.subscribe(function() {
				var myStr = self.search_str();

				myStr = myStr.toLowerCase();
		        //myStr = myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g,"");
		        myStr = myStr.replace(/\s+/g, "+");

		        self.str_search(myStr);

				self.getStoreItems();
			});

			self.getStoreItems = function() {
				var url = '';

				if(self.no_category()) {
					url = getRootUrl() + 'api/storeitems/get_all?query=' + self.str_search() + '&is_stockable=0';
				} else {
					url = getRootUrl() + 'api/storeitems/get_by_category/' + self.category().category_id() + '/0?query=' + self.str_search() + '&is_stockable=0';
				}

				$.ajax({
					url: url,
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.storeitems.removeAll();
						$.each(data, function(index, item) {
							self.storeitems.push(new StoreItem(item));
						});
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.getStoreItem = function(storeitem) {
				self.storeitem(storeitem);
			};

			self.showAddProduct = function() {
				self.add_product(new ReqProduct());
				self.add_product().category_id(self.category().category_id());
				$('#addproduct').show(); $('#app').hide();
			};

			self.cancelAddProduct = function() {
				self.add_product(new ReqProduct());
				$('#app').show(); $('#addproduct').hide();
			};

			self.addProduct = function() {
				if(self.add_product().errors().length == 0 && self.add_product().variants().length > 0) {
					var json = ko.toJSON(self.add_product());

					$.ajax({
						url: getRootUrl() + 'api/storeitems/add',
						type: 'POST',
						contentType: 'application/json',
						data: json,
						success: function(data) {
							console.log(data);
							$('#addproduct').hide();
							$('#app').show();
							self.add_product(new ReqProduct());
							self.getCategories();
							self.getStoreItems();
						},
						error: function(xhr) {
							console.log(xhr);
						}
					});
					console.log(json);
				} else {
					$('#form_error').modal('show');
					self.add_product().errors.showAllMessages();
				}
			};

			self.deleteStoreItem = function() {
				var url = getRootUrl() + 'api/storeitems/delete/' + self.storeitem().store_item_id() + '/' + self.storeitem().product_id();

				$.ajax({
					url: url,
					type: 'DELETE',
					contentType: 'application/json',
					success: function(data) {
						console.log(data);
						$('#deletestoreitem').modal('hide');
						self.getCategories();
						self.getStoreItems();
						self.storeitem(new StoreItem());
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.editStoreItem = function() {
				var url = getRootUrl() + 'api/storeitems/edit/' + self.storeitem().store_item_id(),
					json = ko.toJSON(self.storeitem());

				$.ajax({
					url: url,
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						console.log(data);
						$('#editstoreitem').modal('hide');
						self.getStoreItems();
						self.getCategories();
					},
					error: function(xhr) {
						console.log(xhr);
					}
				})
			};

			self.cancelEditStoreItem = function() {
				$('#editstoreitem').modal('hide');
				self.getStoreItems();
				self.storeitem(new StoreItem());
			};

			self.removeStoreItemImage = function() {
				var url = getRootUrl() + 'api/storeitems/remove_image/' + self.storeitem().image_id();

				$.ajax({
					url: url,
					type: 'DELETE',
					contentType: 'application/json',
					success: function(data) {
						$('#confirmstoreitemimageremove').modal('hide'); $('#editstoreitemimage').modal('hide');
						self.getStoreItems();
						self.storeitem(new StoreItem());
					}, 
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};
		/// storeitems block end

		/// unit measures block start
			self.getUnitMeasures = function() {
				var url = getRootUrl() + 'api/unitmeasures/get_all';
				$.get(url, function(data) {
					self.unitmeasures.removeAll();
					$.each(data, function(index, item) {
						self.unitmeasures.push(new UnitMeasure(item));
					});
				}).fail(function(data, status, xhr) {
					console.log(status)
				});
			};

			self.showEditUnitMeasure = function(unitmeasure) {
				self.unitmeasure(unitmeasure);
				$('#editunitmeasure').modal('show');
			};

			self.editUnitMeasure = function() {
				var json = ko.toJSON(self.unitmeasure());
				$.ajax({
					url: getRootUrl() + 'api/unitmeasures/edit',
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						$('#editunitmeasure').modal('hide');
						self.unitmeasure(new UnitMeasure());
					},
					error: function(xhr) {
						console.log(xhr);
					}
				})
			};

			self.showAddUnitMeasure = function() {
				self.unitmeasure(new UnitMeasure());
				$('#addunitmeasure').modal('show');
			};

			self.addUnitMeasure = function() {
				var json = ko.toJSON(self.unitmeasure());
				if(self.unitmeasure().errors().length == 0) {
					$.ajax({
						url: getRootUrl() + 'api/unitmeasures/add',
						type: 'POST',
						contentType: 'application/json',
						data: json,
						success: function(data) {
							console.log(data);
							self.unitmeasure(new UnitMeasure());
							self.getUnitMeasures();
							$('#addunitmeasure').modal('hide');
						},
						error: function(xhr) {
							console.log(xhr);
						}
					});
				} else {
					$('#form_error').modal('show');
					self.unitmeasure().errors.showAllMessages();
				}
			};
		/// unit measures block end

		/// menu items block start
			self.getMenuItems = function() {
				var category_id = (self.no_category()) ? 0 : self.category().category_id();
				var url = getRootUrl() + 'api/inventories/get_all?is_stockable=0&is_parent=0&category_id=' + category_id;

				$.ajax({
					url: url,
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.menuitems.removeAll();
						$.each(data, function(index, item) {
							self.menuitems.push(new MenuItem(item));
						});
						console.log(data);
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.getMenuItem = function(menuitem) {
				var url = getRootUrl() + 'api/inventories/get/' + menuitem.inventory_id();

				$.ajax({
					url: url,
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.menuitem(new MenuItem(data));
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.editMenuItem = function() {
				var json = ko.toJSON(self.menuitem()), url = getRootUrl() + 'api/inventories/edit_menu_item/' + self.menuitem().inventory_id();

				$.ajax({
					url: url,
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						console.log(data);
						$('#editmenuitem').modal('hide');
						self.getMenuItems();
						self.menuitem(new MenuItem());
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};

			self.cancelEditMenuItem = function() {
				$('#editmenuitem').modal('hide');
				self.getMenuItems();
				self.menuitem(new MenuItem());
			};

			self.resetDailyMenu = function() {
				var url = getRootUrl() + 'api/inventories/reset_daily_menu';

				$.ajax({
					url: url,
					type: 'DELETE',
					contentType: 'application/json',
					success: function(data) {
						self.category(new Category());
						self.getMenuItems();
						$('#confirmresetdailymenu').modal('hide');
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			}
		/// menu items block end

			self.showDailyMenu = function() {
				$('#app').show(); $('#dailymenu-setup').hide();
			};

			self.showDailyMenuSetup = function() {
				$('#dailymenu-setup').show(); $('#app').hide();
			};

		/// initialization
			self.getCategories();
			self.getParentCategories();
			self.getStoreItems();
			self.getUnitMeasures();
			self.getMenuItems();
			self.showDailyMenu();
	}

	ko.applyBindings(new DailyMenuViewModel());
	$('#addproduct').hide();
});