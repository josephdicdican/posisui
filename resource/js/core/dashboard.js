$(function() {
	function DashboardViewModel()
	{
		var self = this;

		self.orders = ko.observableArray([]);
		self.order = ko.observable(new Order());

		self.getOrders = function() {
			var url = getRootUrl() + 'api/pos/get_all/20';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.orders.removeAll();
					$.each(data, function(index, item) {
						self.orders.push(new Order(item));
					});
					console.log(data);
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.transactions = ko.observableArray([]);
		self.overall_total_items = ko.observable(0);
		self.overall_total_sales = ko.observable(0);

		self.overall_total_sales_formatted = ko.computed(function() {
			return toMoney(self.overall_total_sales());
		})

		self.getTransactions = function() {
			var url = getRootUrl() + 'api/pos/get_orders/1/1?admin_access=true';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					console.log(data);
					self.transactions.removeAll();
					$.each(data, function(index, item) {
						self.transactions.push(new Order(item));
						self.overall_total_items(self.overall_total_items() + parseFloat(item.total_items));
						self.overall_total_sales(self.overall_total_sales() + parseFloat(item.total_amount));
					});
					console.log(data);
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getOrders();
		self.getTransactions();
	}

	ko.applyBindings(new DashboardViewModel(), document.getElementById('dashboard'));
});