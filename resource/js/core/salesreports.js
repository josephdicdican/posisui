$(function() {
	function SalesReportsViewModel() {
		var self = this;

		self.transactions = ko.observableArray([]);
		self.transaction = ko.observable(new Order());
		self.overall_total_items = ko.observable(0);
		self.overall_total_sales = ko.observable(0);

		self.overall_total_sales_formatted = ko.computed(function() {
			return toMoney(self.overall_total_sales());
		})
		self.has_transaction = ko.computed(function() {
			return (self.transaction().order_id() != -1);
		});

		self.getTransactions = function() {
			var url = getRootUrl() + 'api/pos/get_orders/1/1';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.transactions.removeAll();
					$.each(data, function(index, item) {
						self.transactions.push(new Order(item));
						self.overall_total_items(self.overall_total_items() + parseFloat(item.total_items));
						self.overall_total_sales(self.overall_total_sales() + parseFloat(item.total_amount));
					});
					console.log(data);
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getTransactionLines = function(transaction) {
			var url = getRootUrl() + 'api/pos/get_orderlines/' + transaction.order_id();
			self.transaction(transaction);

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.transaction().orderlines.removeAll();
					$.each(data, function(index, item) {
						var line = new Orderline();
						line.init_sales(item);
						self.transaction().orderlines.push(line);
					});
					console.log(ko.toJSON(self.transaction().orderlines()));
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.printSales = function() {
			$('.transaction-lines').removeAttr('id');
			$('.transactions').attr('id', 'print-content')
			window.print();
		};

		self.printTransaction = function() {
			$('.transactions').removeAttr('id');
			$('.transaction-lines').attr('id', 'print-content')
			window.print();
		};

		self.getTransactions();
	}

	ko.applyBindings(new SalesReportsViewModel());
});