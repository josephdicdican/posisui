function UserRole(role_id, role_name)
{
	var self = this;

	self.role_id = ko.observable(role_id);
	self.role_name = ko.observable(role_name);
} 

function UserEdit() {
	var self = this;

	self.user_id = ko.observable();
	self.display_name = ko.observable().extend({ required: true, minLength: 2, maxLength: 20});
	self.username = ko.observable().extend({ required: true, minLength: 6, maxLength: 20});
	self.role_id = ko.observable().extend({ required: { message: "Please specify user role."}});

	self.init = function(user) {
		self.user_id(user.user_id());
		self.display_name(user.display_name());
		self.username(user.username());
		self.role_id(user.role_id());
	};

	self.errors = ko.validation.group(self);
}

function User(data) 
{
	var self = this;

	self.user_id = ko.observable(-1);
	self.display_name = ko.observable().extend({ required: true, minLength: 2, maxLength: 20});
	self.username = ko.observable().extend({ required: true, minLength: 6, maxLength: 20});
	self.password = ko.observable('').extend({ required: true});

	self.confirm_pass = ko.observable().extend({ validation: { validator: mustEqual, message: "Passwords do not match", params: self.password } });

	self.date_created = ko.observable();
	self.date_modified = ko.observable();

	self.role_id = ko.observable().extend({ required: { message: "Please specify user role."}});
	self.role_name = ko.observable();

	self.is_admin = ko.computed(function() {
		return (self.role_id() == 1);
	});

	self.is_logged = ko.computed(function() {
		var logged_id = document.getElementById('logged_id').value;
		return logged_id == self.user_id();
	});

	self.comp_role_name = ko.computed(function() {
		var roles = [null, 'admin', 'staff'];

		return roles[self.role_id()];
	});

	if(data) {
		self.user_id(data.user_id);
		self.display_name(data.display_name);
		self.username(data.username);
		self.password(data.password);
		self.role_id(data.role_id);
		self.role_name(data.role_name);
	}

	self.init_view = function (data) {
		self.date_created(data.date_created);
		self.date_modified(data.date_modified);
	};

	self.errors = ko.validation.group(self);
}

function Log(data)
{
	var self = this;

	self.user_log_id = ko.observable(-1);
	self.user_id = ko.observable(-1);
	self.display_name = ko.observable();
	self.user2log_id = ko.observable(-1);
	self.remarks = ko.observable();
	self.date_created = ko.observable();

	self.format_date = function() {
		var dateStr = self.date_created();
		return toDateTimeString(dateStr);
	};
	
	if (data) {
		self.user_log_id(data.user_log_id);
		self.user_id(data.user_id);
		self.display_name(data.display_name);
		self.user2log_id(data.user2log_id);
		self.remarks(data.remarks);
		self.date_created(data.date_created);
	};
	self.errors = ko.validation.group(self);
}

$(function() {
	function UserViewModel() 
	{
		var self = this;

		self.users = ko.observableArray([]);
		self.user = ko.observable(new User());
		self.userEdit = ko.observable(new UserEdit());

		self.logs = ko.observableArray([]);
		self.log = ko.observable(new Log());

		self.search_str = ko.observable('');
		self.str_search = ko.observable('');
		self.sort_by = ko.observable('asc');
		self.sort_column = ko.observable('');
		
		self.userroles = ko.observableArray([
			new UserRole(1, 'admin'),
			new UserRole(2, 'staff')
		]);

		self.no_user = ko.computed(function() {
			return (self.user().user_id() == -1);
		});

		self.status = ko.observable(new Status(null, ''));

		self.search_str.subscribe(function() {
			var myStr = self.search_str();

			myStr = myStr.toLowerCase();
	        //myStr = myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g,"");
	        myStr = myStr.replace(/\s+/g, "+");

	        self.str_search(myStr);

			self.getUsers();
		});

		self.sortResult = function(column) {
			if(self.sort_by() != '') {
				if(self.sort_by() == 'asc') {
					self.sort_by('desc');
				} else {
					self.sort_by('asc');
				}
			}

			self.sort_column(column);
			self.getUsers();
		};


		self.getUsers = function() {
			var url = getRootUrl() + 'api/users/0?query=' + self.str_search() + '&sort_column=' + self.sort_column() + '&sort_by=' + self.sort_by();
			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.users.removeAll();
					self.user(new User());
					$.each(data, function(index, item) {
						self.users.push(new User(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getUser = function(user) {
			$.ajax({
				url: getRootUrl() + 'api/users/' + user.user_id(),
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					var user = new User(data);
					user.init_view(data);

					self.user(user);
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
			self.getLogs(user.user_id());
		};

		self.getLogs = function(id) {
			$.ajax({
				url: getRootUrl() + 'api/logs/user/' + id,
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					self.logs.removeAll();
					$.each(data, function(index, item) {
						self.logs.push(new Log(item));
					});
					//console.log(ko.toJSON(self.logs()));
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.showAdd = function () {
			self.user(new User());
		};

		self.addUser = function() {
			var json = ko.toJSON(self.user());

			if(self.user().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/users/add',
					type: 'POST',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						if(data == true) {
							$('#adduser').modal('hide');
							self.getUsers();
						} else if(data == -1) {
							$('#username_exist').modal('show');
						}
						console.log(data);
					},
					error: function(xhr) {
						console.log(xhr);
					}
				}); 
			} else {
				$('#form_error').modal('show');
				self.user().errors.showAllMessages();
			}
		};

		self.showEdit = function(user) {
			var userEdit = new UserEdit();
			userEdit.init(user);

			self.userEdit(userEdit);
			$('#edituser').modal('show');
		};

		self.editUser = function() {
			var json = ko.toJSON(self.userEdit());

			if(self.userEdit().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/users/update/' + self.userEdit().user_id(),
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						console.log(data);
						$('#edituser').modal('hide');
						self.getUsers();
					},
					error: function(xhr) {
						console.log('yow');
						console.log(xhr);
					}
				});
			} else {
				$('#form_error').modal('show');
				self.userEdit().errors.showAllMessages();
			}
		};

		self.deleteUser = function() {
			$.ajax({
				url: getRootUrl() + 'api/users/delete/' + self.user().user_id(),
				type: 'DELETE',
				contentType: 'application/json',
				success: function(data) {
					console.log(data);
					$('#deleteuser').modal('hide');
					self.getUsers();
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getUsers();
	}

	ko.applyBindings(new UserViewModel());
});