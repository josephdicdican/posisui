function Crumb(id, name) {
	var self = this;

	self.category_id = ko.observable(id);
	self.category_name = ko.observable(name);
}

$(function() {
	function PosViewModel() {
		var self = this;

	/// categories
		self.categories = ko.observableArray([]);
		self.category = ko.observable(new Category());
		self.category_crumbs = ko.observableArray([]);

	/// storeitems
		self.storeitems = ko.observableArray([]);
		self.storeitem = ko.observable(new StoreItem());
		self.is_stockable = ko.observable(1);

	/// orders and orderlines properties
		self.orders = ko.observableArray([]);
		self.order = ko.observable(new Order());
		self.orderline = ko.observable(new Orderline());
		self.sku = ko.observable('');
		self.sales_today = ko.observableArray([]); // orders checkout today
		self.overall_sales = ko.observable(0); // holds sum of orders' totalamount
		self.overall_items = ko.observable(0); // holds sum of orders' total_items
		self.order_hold = ko.observable();

		self.overall_sales_formatted = ko.computed(function() {
			return 'Php ' + toMoney(self.overall_sales());
		});

	/// customers properties
		self.customers = ko.observableArray([]);
		self.customer = ko.observable(new Customer());
		self.selected_customer = ko.observable(new Customer());
		self.search_str = ko.observable('');
		self.str_search = ko.observable('');
		self.sort_by = ko.observable('asc');
		self.sort_column = ko.observable('');

		self.no_customer = ko.computed(function() {
			return (self.customer().customer_id() == -1);
		});
		self.no_selected_customer = ko.computed(function() {
			return (self.selected_customer().customer_id() == -1);
		});

		self.search_str.subscribe(function() {
			var myStr = self.search_str();

			myStr = myStr.toLowerCase();
	        myStr = myStr.replace(/\s+/g, "+");

	        self.str_search(myStr);

			self.getCustomers();
		});

	/// categories and storeitems
		self.no_category = function() {
			return (self.category().category_id() == -1);
		};

		self.is_parent = function() {
			var parent_id = parseFloat(self.category().parent_id());
			return (parent_id == 0);
		};

		self.getParentCategories = function(is_stockable) {
			self.is_stockable(is_stockable);
			$.ajax({
				url: getRootUrl() + 'api/categories/get_parents/0/' + is_stockable ,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.category_crumbs.removeAll();
					self.category(new Category());
					self.getStoreItems(is_stockable);
					self.categories.removeAll();
					$.each(data, function(index, item) {
						self.categories.push(new Category(item));
					});
					$('#product-categories').show(); $('#onhold-orders').hide();
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getCategory = function(category) {
			var id = category.category_id();
			self.getCategoryPerform(id);
		};

		self.getCategoryPerform = function(id) {
			$.ajax({
				url: getRootUrl() + 'api/categories/' + id,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.category_crumbs.removeAll();
					self.category_crumbs.push(new Crumb(self.category().category_id(), self.category().category_name()));

					self.category(new Category(data));
					self.storeitem(new StoreItem());
					self.getStoreItems();

					if(self.is_parent() == true) {
						self.category_crumbs.removeAll();
					} 

					self.category_crumbs.push(new Crumb(self.category().category_id(), self.category().category_name()));

					console.log(self.category_crumbs());
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.most_sold = ko.observable(0);
		self.getMostSoldItems  = function() {
			self.most_sold(1);
			self.getParentCategories(self.is_stockable());
			self.category(new Category());
			self.getStoreItems(self.is_stockable());
		};

		self.getAllItems = function(is_stockable) {
			self.most_sold(0);
			self.getParentCategories(is_stockable);
		};

		self.getStoreItems = function(is_stockable) {
			if(self.no_category()) {
				$.ajax({
					url: getRootUrl() + 'api/storeitems/get_all?most_sold=' + self.most_sold() + '&is_stockable=' + is_stockable,
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.storeitems.removeAll();
						$.each(data, function(index, item) {
							self.storeitems.push(new StoreItem(item));
						});
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				var url = getRootUrl() + 'api/storeitems/get_by_category/' + self.category().category_id() + '/0?most_sold=' + self.most_sold() + '&is_stockable=' + is_stockable;
				$.ajax({
					url: url,
					type: 'GET',
					contentType: 'application/json',
					success: function(data) {
						self.storeitems.removeAll();
						$.each(data, function(index, item) {
							self.storeitems.push(new StoreItem(item));
						});
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			}
		};

	/// orders and orderlines methods
		self.no_orderline = ko.computed(function() {
			return (typeof self.orderline().store_item_id() == "undefined" && self.orderline().store_item_id() != -1);
		});

		self.addOrderline = function(storeitem) {
			self.storeitem(storeitem);
			var orderline = new Orderline();
			orderline.init_add(storeitem);

			self.add_orderline(orderline, storeitem);
		};

		self.saveOrderline = function() {
			self.add_orderline(self.orderline(), self.storeitem());
			self.orderline(new Orderline());
			self.sku('');
			$('#scan').modal('hide');
			$('#add_item').focus();
		};

		self.add_orderline = function(orderline, storeitem) {
			if(self.order().orderlines().length > 0) {
				var exist = false;

				$.each(self.order().orderlines(), function(index, item) {
					if(item.store_item_id() == storeitem.store_item_id()) {
						exist = true;
						item.quantity(Math.max(0, parseFloat(item.quantity())) + 1);
					}
				});

				if(exist == false) {
					self.order().orderlines.push(orderline)
				}
			} else {
				self.order().orderlines.push(orderline)
			}

			$('#add_item').focus();
		};

		self.confirmDeleteOrderline = function(orderline) {
			self.orderline(orderline);
		};

		self.removeOrderline = function() {
			if(self.order().new_order()) {
				self.order().orderlines.remove(self.orderline());
			} else {
				self.removeOrderlineHold();
			}

			self.orderline(new Orderline());
			$('#deleteitem').modal('hide');
		};

		self.removeOrderlineHold = function() {
			$.each(self.order().orderlines(), function(index, item) {
				if(item.line_id() == self.orderline().line_id()) {
					self.order().orderlines()[index].quantity(-1);
				} else if(item.line_id() == -1) {
					self.order().orderlines.remove(function(line) {
						return line.store_item_id() == item.store_item_id();
					});
				}
			});
		};

		self.scanStoreItem = function() {
			$.ajax({
				url: getRootUrl() + 'api/storeitems/get_storeitem_sku/' + self.sku(),
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.orderline(new Orderline());
					if(data) {
						var orderline = new Orderline();
						self.storeitem(new StoreItem(data));

						orderline.init_add(self.storeitem());
						self.orderline(orderline);

						if(!self.storeitem().no_stock()) {
							self.saveOrderline();
							$('#scan').modal('hide');
						} else {
							$('#scan').modal('show');
						}
					}
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.cancelScan = function() {
			self.orderline(new Orderline());
			self.sku('');
			$('#scan').modal('hide');
			$('#add_item').focus();
		};

		self.cancelOrder = function() {
			self.order(new Order());
			self.sku('');
			$('#cancelorder').modal('hide');
			$('#add_item').focus();
		}; 

		self.holdOrder = function() {
			var json = ko.toJSON(self.order());

			if(self.order().new_order()) { // hold new order 
				var url = getRootUrl() + 'api/pos/add_order/', request_type = 'POST';
			} else { // hold existing order or update hold
				var url = getRootUrl() + 'api/pos/update_order/', request_type = 'PUT';
			}

			$.ajax({
				url: url,
				type: request_type,
				contentType: 'application/json',
				data: json,
				success: function(data) {
					console.log(data);
					if(data) {
						self.order(new Order());
						$('#holdorder').modal('hide');
					}
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.checkoutOrder = function() {
			self.order().is_checkout(1);
			var json = ko.toJSON(self.order()), url = '', request_type = 'POST';
			
			if(self.order().new_order()) {
				url = getRootUrl() + 'api/pos/add_order/';
			} else {
				url = getRootUrl() + 'api/pos/update_order/';
				request_type = 'PUT';
			}
			
			$.ajax({
				url: url,
				type: request_type,
				contentType: 'application/json',
				data: json,
				success: function(data) {
					if(data) {
						$('#printreceipt').modal('show');
					}
					
					self.selected_customer(new Customer());
					self.getSales();
					$('#product-categories').show(); $('#onhold-orders').hide();
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.printReceipt = function() {
			// $('#no-print').hide();
			window.print();
			self.cancelPrint();
		};

		self.cancelPrint = function() {
			self.order(new Order());
			$('#no-print').show();
			$('#printreceipt').modal('hide');
			$('#tender-cash').val(0);
			$('#cashpayment').modal('hide');
			$('#onhold-orders').hide();
			$('product-categories').show();
			self.getStoreItems();
			self.getOrders();
			self.getParentCategories();
		};

		self.getOrders = function() {
			$.ajax({
				url: getRootUrl() + 'api/pos/get_orders/0/0',
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.orders.removeAll();
					$('#product-categories').hide(); $('#onhold-orders').show();
					$.each(data, function(index, item) {
						self.orders.push(new Order(item));
					});
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getOrderlines = function(order) {
			self.order(new Order());
			self.order(order);
			var url = getRootUrl() + 'api/pos/get_orderlines/' + self.order().order_id();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.order().orderlines.removeAll();
					$.each(data, function(index, item) {
						var line = new Orderline(), storeitem = new StoreItem();
						line.init(item); 
						storeitem.store_item_id(item.store_item_id);

						self.add_orderline(line, storeitem);
					});
				}
			});
		};

		self.confirmDiscard = function(order) {
			$('#discardorder').modal('show');
			self.order_hold(order);
		};

		self.discardOrder = function() {
			var url = getRootUrl() + 'api/pos/discard_order/' + self.order_hold().order_id();

			$.ajax({
				url: url,
				type: 'DELETE',
				contentType: 'application/json',
				success: function(data) {
					if(data) {
						$('#discardorder').modal('hide');
						self.order_hold(null);
						self.getOrders();
					}
				},
				error: function(xhr) {
					console.log(xhr);
				}
			})
		};

		self.getSales = function() { // get orders checkout today
			var url = getRootUrl() + 'api/pos/get_orders/1/1';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.sales_today.removeAll();
					$.each(data, function(index, item) {
						self.sales_today.push(new Order(item));
						self.overall_items(self.overall_items() + parseFloat(item.total_items));
						self.overall_sales(self.overall_sales() + parseFloat(item.total_amount));
					});
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.discardExpiredOnholdOrders = function() {
			var url = getRootUrl() + 'api/pos/delete_expired_onhold_orders/';

			$.ajax({
				url: url,
				type: 'DELETE',
				contentType: 'application/json',
				success: function(data) {
					self.getOrders();
					if(self.orders().length <= 0) {
						self.getAllItems();
					}

					console.log(data);
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};
			
	/// customers methods 
		self.sortResult = function(column) {
			if(self.sort_by() != '') {
				if(self.sort_by() == 'asc') {
					self.sort_by('desc');
				} else {
					self.sort_by('asc');
				}
			}

			self.sort_column(column);
			self.getCustomers();
		};

		self.getCustomers = function() {
			var url = getRootUrl() + 'api/customers/0?query=' + self.str_search() + '&sort_column=' + self.sort_column() + '&sort_by=' + self.sort_by();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.customers.removeAll();
					self.customer(new Customer());
					$.each(data, function(index, item) {
						self.customers.push(new Customer(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getCustomer = function(customer) {
			$.ajax({
				url: getRootUrl() + 'api/customers/' + customer.customer_id(),
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					var customer = new Customer(data);

					self.customer(customer);
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.showSelectCustomer = function() {
			$('#select-customer').show(); $('#pos').hide();
		};

		self.selectCustomer = function() {
			self.selected_customer(self.customer());
			self.order().customer_id(self.selected_customer().customer_id());
			$('#pos').show(); $('#select-customer').hide();
		};

		self.removeSelectedCustomer = function() {
			self.selected_customer(new Customer());
			self.customer(new Customer());
			self.order().customer_id(0);
			$('#pos').show(); $('#select-customer').hide();
		};

		self.cancelSelectCustomer = function() {
			$('#pos').show(); $('#select-customer').hide();
		};

		self.showAdd = function () {
			self.customer(new Customer());
		};

		self.addCustomer = function() {
			var json = ko.toJSON(self.customer());

			if(self.customer().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/customers/add',
					type: 'POST',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						if(data == true) {
							$('#addcustomer').modal('hide');
							self.getCustomers();
						} else if(data == -1) {
							$('#customer_exist').modal('show');
						}
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				$('#form_error').modal('show');
				self.customer().errors.showAllMessages();
			}
		};

		self.getCustomers();
		self.getOrders();
		self.getStoreItems();
		self.getSales();
		self.getParentCategories();
	}

	var posVM = new PosViewModel();
	var onholdMaxTimeout = 30000;

	setInterval(function() { // interval each 30 seconds 
		var count = posVM.orders().length;
		
		if(count > 0) {
			var options =  {
			    content: '<i class="fa fa-warning"></i> Onhold orders older than 10 minutes will now be deleted.', // text of the snackbar
			    style: "alert alert-warning", // add a custom class to your snackbar
			    timeout: 5000, // time in milliseconds after the snackbar autohides, 0 is disabled
			    htmlAllowed: true, // allows HTML as content value
			    onClose: function(){ 
			    	posVM.discardExpiredOnholdOrders();
			    } // callback called when the snackbar gets closed.
			};

			$.snackbar(options);
		}
	}, onholdMaxTimeout);

	ko.applyBindings(posVM);
	$('#add_item').focus();
	$('#onhold-orders').hide();
	$('#product-categories').show();
	$('#select-customer').hide();
});
