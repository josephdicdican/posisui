function Log(data)
{
	var self = this;

	self.supplier_log_id = ko.observable(-1);
	self.user_id = ko.observable(-1);
	self.display_name = ko.observable();
	self.supplier_id = ko.observable();
	self.remarks = ko.observable();
	self.date_created = ko.observable();

	self.format_date = function() {
		var dateStr = self.date_created();
		return toDateTimeString(dateStr);
	};

	if (data) {
		self.supplier_log_id(data.supplier_log_id);
		self.user_id(data.user_id);
		self.display_name(data.display_name);
		self.supplier_id(data.supplier_id);
		self.remarks(data.remarks);
		self.date_created(data.date_created);
	};

	self.errors = ko.validation.group(self);
}

$(function() {
	function SupplierViewModel()
	{
		var self = this;

		self.suppliers = ko.observableArray([]);
		self.supplier = ko.observable(new Supplier());

		self.logs = ko.observableArray([]);
		self.log = ko.observable(new Log());

		self.search_str = ko.observable('');
		self.str_search = ko.observable('');
		self.sort_by = ko.observable('asc');
		self.sort_column = ko.observable('');

		self.no_supplier = ko.computed(function() {
			return self.supplier().supplier_id() == -1;
		});

		self.search_str.subscribe(function() {
			var myStr = self.search_str();

			myStr = myStr.toLowerCase();
	        //myStr = myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g,"");
	        myStr = myStr.replace(/\s+/g, "+");

	        self.str_search(myStr);

			self.getSuppliers();
		});

		self.sortResult = function(column) {
			if(self.sort_by() != '') {
				if(self.sort_by() == 'asc') {
					self.sort_by('desc');
				} else {
					self.sort_by('asc');
				}
			}

			self.sort_column(column);
			self.getSuppliers();
		};

		self.getSuppliers = function() {
			var url = getRootUrl() + 'api/suppliers/0?query=' + self.str_search() + '&sort_column=' + self.sort_column() + '&sort_by=' + self.sort_by();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.suppliers.removeAll();
					self.supplier(new Supplier());
					$.each(data, function(index, item) {
						self.suppliers.push(new Supplier(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getSupplier = function(supplier) {
			$.ajax({
				url: getRootUrl() + 'api/suppliers/' + supplier.supplier_id(),
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					var supplier = new Supplier(data);

					self.supplier(supplier);
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
			self.getLogs(supplier.supplier_id());
		};

		self.getLogs = function(id) {
			$.ajax({
				url: getRootUrl() + 'api/logs/supplier/' + id,
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					self.logs.removeAll();
					$.each(data, function(index, item) {
						self.logs.push(new Log(item));
					});
					console.log(ko.toJSON(self.logs()));
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.showAdd = function () {
			self.supplier(new Supplier());
		};

		self.addSupplier = function() {
			var json = ko.toJSON(self.supplier());

			if(self.supplier().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/suppliers/add',
					type: 'POST',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						if(data == true) {
							$('#addsupplier').modal('hide');
							self.getSuppliers();
						} else if(data == -1) {
							$('#supplier_exist').modal('show');
						}
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				$('#form_error').modal('show');
				self.supplier().errors.showAllMessages();
			}
		};

		self.editSupplier = function() {
			var json = ko.toJSON(self.supplier());

			if(self.supplier().errors().length == 0) {
				$.ajax({
					url: getRootUrl() + 'api/suppliers/edit/' + self.supplier().supplier_id(),
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						if(data == true) {
							$('#editsupplier').modal('hide');
							self.getSuppliers();
						} else if(data == -1) {
							$('#supplier_exist').modal('show');
						}
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				$('#form_error').modal('show');
				self.supplier().errors.showAllMessages();
			}
		};

		self.deleteSupplier = function(supplier) {
			$.ajax({
				url: getRootUrl() + 'api/suppliers/delete/' + supplier.supplier_id(),
				type: 'DELETE',
				contentType: 'application/json',
				success: function(data) {
					$('#deletesupplier').modal('hide');
					self.getSuppliers();
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getSuppliers();
	}

	ko.applyBindings(new SupplierViewModel());
});