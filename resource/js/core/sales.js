function Interval(data, interval_type) {
	var self = this;

	self.interval = ko.observable(0);
	self.items = ko.observable(0);
	self.sales = ko.observable(0);
	self.interval_type = ko.observable(interval_type);

	if(data) {
		self.interval(data.interval);
		self.items(data.items);
		self.sales(data.sales);
	}
}

function TransactionSale(data) {
	var self = this;

	self.order_id = ko.observable(-1);
	self.date_of_order = ko.observable();
	self.items = ko.observable(0);
	self.sales = ko.observable(0);

	if(data) {
		self.order_id(data.order_id);
		self.date_of_order(data.date_of_order);
		self.items(data.items);
		self.sales(data.sales);
	}
}

function StoreItemSale(data, total) {
	var self = this;

	self.sku = ko.observable(-1);
	self.storeitemname = ko.observable();
	self.items = ko.observable(0);
	self.sales = ko.observable(0);
	self.percent = ko.computed(function() {
		return Math.floor((parseFloat(self.items()) / total) * 100);
	});

	if(data) {
		self.sku(data.sku);
		self.storeitemname(data.storeitemname);
		self.items(data.items);
		self.sales(data.sales);
	}
}

function StoreItemChartItem(y, legendText, legend, total) {
	var self = this;

	self.y = ko.observable(y);
	self.legendText = ko.observable(legendText);
	self.label = ko.observable(legend);
	/*self.x = ko.computed(function() {
		return self.y() / total;
	});*/
}

$(function() {
	function SalesReportViewModel() {
		var self = this;

		self.intervals = ko.observableArray([]);
		self.interval = ko.observable(0);

		self.transaction_sales = ko.observableArray([]);
		self.transaction_sale = ko.observable();

		self.storeitem_sales = ko.observableArray([]);
		self.storeitem_sale = ko.observable();
		self.storeitem_chart_items = ko.observableArray([]);

		self.interval_type = ko.observable('week');
		self._interval = ko.observable(new Interval(false, ''));

		self.view = ko.observable('Transactions Sales');

		self.save_export_url = ko.computed(function() {
			var url = getRootUrl() + 'api/reports/';
			
			if(self.view() == 'Transactions Sales') {
				url += 'transaction_sales/';
			} else {
				url += 'storeitems_sales/';
			}

			url += self.interval_type() + '/' + self.interval() + '/true';

			return url;
		});

		self.viewTransactions = function() {
			self.view('Transactions Sales');
			$('#transactions').show(); $('#storeitems').hide();
		};

		self.viewStoreItems = function() {
			self.view('Store Items Sales');
			$('#storeitems').show(); $('#transactions').hide();
		};

		self.showList = function() {
			$('.sales-list').show(); $('.sales-chart').hide();
		};

		self.showChart = function() {
			$('.sales-chart').show(); $('.sales-list').hide();
		};
		
		self.setIntervalType = function(interval_type) {
			self.interval_type(interval_type);
			self.getIntervals();
		};

		self.getIntervals = function() {
			var url = getRootUrl() + 'api/sales/get_intervals/' + self.interval_type();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.intervals.removeAll();
					$.each(data, function(index, item) {
						self.intervals.push(new Interval(item, self.interval_type()));
					});
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getTransactionSales = function() {
			var url = getRootUrl() + 'api/sales/transaction_sales/' + self.interval_type() + '/' + self.interval();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.transaction_sales.removeAll();
					$.each(data, function(index, item) {
						self.transaction_sales.push(new TransactionSale(item));
					});
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getStoreItemsSales = function() {
			var url = getRootUrl() + 'api/sales/storeitems_sales/' + self.interval_type() + '/' + self.interval();
			var data_points = [];

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.storeitem_sales.removeAll();
					$.each(data, function(index, item) {
						var storeitem = new StoreItemSale(item, self._interval().items());
						self.storeitem_sales.push(storeitem);
					});
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getInterval = function(interval) {
			self.interval(interval.interval());
			self._interval(interval);
			self.getTransactionSales();
			self.getStoreItemsSales();
		};

		self.getIntervals();
		self.getTransactionSales();
		self.viewTransactions();
	}

	ko.applyBindings(new SalesReportViewModel(), document.getElementById('sales_reports'));
	$('#storeitems').hide();
	$('.sales-chart').hide();
});