function Interval(data, interval_type) {
	var self = this;

	self.interval = ko.observable(0);
	self.items = ko.observable(0);
	self.sales = ko.observable(0);
	self.interval_type = ko.observable(interval_type);

	if(data) {
		self.interval(data.interval);
		self.items(data.items);
		self.sales(data.sales);
	}
}

function SalesForecast(data) {
	var self = this;

	self.category_name = ko.observable('');
	self.sold_items = ko.observable(0);
	self.total_sales = ko.observable(0);
	self.week = ko.observable();
	self.month = ko.observable();
	self.year = ko.observable();

	self.category_id = ko.observable(-1);
	self.store_item_id = ko.observable(-1);
	self.sku = ko.observable(-1);
	self.storeitemname = ko.observable('');

	self.total_sales_formatted = ko.computed(function() {
		return toMoney(self.total_sales());
	});

	self.init_category = function(data) {
		self.category_name(data.category_name);
		self.sold_items(data.sold_items);
		self.total_sales(data.total_sales);
		self.week(data.week);
		self.month(data.month);
		self.year(data.year);
	}

	self.init_storeitem = function(data) {
		self.init_category(data);
		self.category_id(data.category_id);
		self.store_item_id(data.store_item_id);
		self.sku(data.sku);
		self.storeitemname(data.storeitemname);
	};
}

$(function() {
	function SalesForecastViewModel() {
		var self = this;

		self.intervals = ko.observableArray([]);
		self.interval = ko.observable(0);

		self.interval_type = ko.observable('week');
		self._interval = ko.observable(new Interval(false, ''));

		self.is_stockable = ko.observable(1);
		self.group_by = ko.observable('store_item_id');
		self.sales = ko.observableArray([]);

		self.is_week = ko.computed(function() {
			return self.interval_type() == 'week';
		});	

		self.is_month = ko.computed(function() {
			return self.interval_type() == 'month';
		});	

		self.is_year = ko.computed(function() {
			return self.interval_type() == 'year';
		});	

		self.is_storeitems = ko.computed(function() {
			return self.group_by() == 'store_item_id';
		});

		self.is_consumable = ko.computed(function() {
			return self.is_stockable() == 0;
		});

		self.setIsStockable = function(is_stockable) {
			self.is_stockable(is_stockable);
			self.getSales();
		};

		self.setGroupBy = function(by) {
			self.group_by(by);
			self.getSales();
		};

		self.setIntervalType = function(interval_type) {
			self.interval_type(interval_type);
			self.getSales();
		};

		self.getSales = function() {
			var url = getRootUrl() + 'api/salesforecast/sales/' + self.is_stockable() + '/' + self.interval_type() + '/' + self.group_by();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.sales.removeAll();
					$.each(data, function(index, item) {
						var forecast = new SalesForecast();

						if(self.is_storeitems()) {
							forecast.init_storeitem(item);
						} else {
							forecast.init_category(item);
						}

						self.sales.push(forecast);
					});
				},
				error: function(xhr) {
					console.log(xhr);
				} 
			});
		};

		self.getSales();
	}

	ko.applyBindings(new SalesForecastViewModel());
});