function Interval(data, interval_type) {
	var self = this;

	self.interval = ko.observable(0);
	self.items = ko.observable(0);
	self.sales = ko.observable(0);
	self.interval_type = ko.observable(interval_type);

	if(data) {
		self.interval(data.interval);
		self.items(data.items);
		self.sales(data.sales);
	}
}

function Item() {
	var self = this;

	self.store_item_id = ko.observable(-1);
	self.storeitemname = ko.observable('');
	self.sku = ko.observable('');

	self.category_id = ko.observable(-1);
	self.category_name = ko.observable('');

	self.forecasts = ko.observableArray([]);
	self.forecast_project = ko.observable();

	self.init_storeitem = function(data) {
		self.store_item_id(data.store_item_id);
		self.storeitemname(data.storeitemname);
		self.sku(data.sku);
	};

	self.init_category = function(data) {
		self.category_id(data.category_id);
		self.category_name(data.category_name);
	};
}

function SalesForecast(data) {
	var self = this;

	self.category_name = ko.observable('');
	self.sold_items = ko.observable(0);
	self.total_sales = ko.observable(0);
	self.week = ko.observable();
	self.month = ko.observable();
	self.year = ko.observable();

	self.category_id = ko.observable(-1);
	self.store_item_id = ko.observable(-1);
	self.sku = ko.observable(-1);
	self.storeitemname = ko.observable('');

	self.week_beginning = ko.observable();
	self.week_end = ko.observable();

	self.month_beginning = ko.observable();

	self.growth = ko.observable(0);
	self.decline = ko.observable(0);
	self.items_projection = ko.observable(0);
	self.projection = ko.observable(0);

	self.projection_formatted = ko.computed(function() {
		return parseFloat(self.projection()).toFixed(2);
	});

	self.items_projection_formatted = ko.computed(function() {
		return parseFloat(self.items_projection()).toFixed(2);
	});

	self.growth_percent = ko.computed(function() {
		return parseFloat(self.growth() * 100).toFixed(1);
	});
	self.decline_percent = ko.computed(function() {
		return parseFloat(self.decline() * 100).toFixed(1);
	});

	self.no_action = ko.computed(function() {
		return self.growth() == self.decline();
	});

	self.grew = ko.computed(function() {
		return self.growth() > self.decline();
	});

	self.declined = ko.computed(function() {
		return self.growth() < self.decline();
	});

	self.total_sales_formatted = ko.computed(function() {
		return toMoney(self.total_sales());
	});

	self.init_category = function(data) {
		self.category_name(data.category_name);
		self.sold_items(data.sold_items);
		self.total_sales(data.total_sales);
		self.week(data.week);
		self.month(data.month);
		self.year(data.year);
	}

	self.init_storeitem = function(data) {
		self.init_category(data);
		self.category_id(data.category_id);
		self.store_item_id(data.store_item_id);
		self.sku(data.sku);
		self.storeitemname(data.storeitemname);
	};

	self.init_dates = function(data, interval_type) {
		if(interval_type == 'week') {
			self.week_beginning(data.week_beginning);
			self.week_end(data.week_end);
		} else if(interval_type == 'month') {
			self.month_beginning(data.month_beginning);
		}

		self.growth(data.growth);
		self.decline(data.decline);
		self.items_projection(data.items_projection);
		self.projection(data.projection);
	};
}

$(function() {
	function SalesForecastViewModel() {
		var self = this;

		self.intervals = ko.observableArray([]);
		self.interval = ko.observable(0);

		self.interval_type = ko.observable('week');
		self._interval = ko.observable(new Interval(false, ''));

		self.is_stockable = ko.observable(1);
		self.group_by = ko.observable('store_item_id');
		self.sales = ko.observableArray([]);

		self.items = ko.observableArray([]);

		self.is_week = ko.computed(function() {
			return self.interval_type() == 'week';
		});	

		self.is_month = ko.computed(function() {
			return self.interval_type() == 'month';
		});	

		self.is_year = ko.computed(function() {
			return self.interval_type() == 'year';
		});	

		self.is_storeitems = ko.computed(function() {
			return self.group_by() == 'store_item_id';
		});

		self.is_consumable = ko.computed(function() {
			return self.is_stockable() == 0;
		});

		self.setIsStockable = function(is_stockable) {
			self.is_stockable(is_stockable);
			self.getItems();
		};

		self.setGroupBy = function(by) {
			self.group_by(by);
			self.getItems();
		};

		self.setIntervalType = function(interval_type) {
			self.interval_type(interval_type);
			self.getItems();
		};

		/*self.getSales = function() {
			var url = getRootUrl() + 'api/salesforecast/forecast/' + self.is_stockable() + '/' + self.interval_type() + '/' + self.group_by();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.sales.removeAll();
					$.each(data, function(index, item) {
						var forecast = new SalesForecast();

						if(self.is_storeitems()) {
							forecast.init_storeitem(item);
						} else {
							forecast.init_category(item);
						}

						forecast.init_dates(item, self.interval_type());

						self.sales.push(forecast);
					});
				},
				error: function(xhr) {
					console.log(xhr);
				} 
			});
		};*/

		self.getItems = function() {
			var url = getRootUrl() + 'api/salesforecast/distinct_items/' + self.is_stockable() + '/' + self.group_by() + '/' + self.interval_type();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.items.removeAll();
					$.each(data, function(index, row) {
						var item = new Item(), id = 0;

						if(self.is_storeitems()) {
							id = row.store_item_id;
							item.init_storeitem(row);
						} else {
							id = row.category_id;
							item.init_category(row);
						}

						item.forecasts.removeAll();
						$.each(row.forecasts, function(i, f_item) {
							var forecast = new SalesForecast();

							if(self.is_storeitems()) {
								forecast.init_storeitem(f_item);
							} else {
								forecast.init_category(f_item);
							}

							forecast.init_dates(f_item, self.interval_type());

							if(i == 0) {
								item.forecast_project(forecast);
							}

							item.forecasts.push(forecast);
						});

						self.items.push(item);
					});

					console.log(ko.toJSON(self.items()));
				},
				error: function(xhr) {
					console.log(xhr);
				} 
			});
		};

		self.getItems();
	}

	ko.applyBindings(new SalesForecastViewModel());
});