function Inventory(data)
{
	var self = this;

	self.inventory_id = ko.observable(-1);
	self.product_id = ko.observable();
	self.product_name = ko.observable();
	self.price_per_unit = ko.observable(0);
	self.stock_at_hand = ko.observable(0).extend({ required: true });
	self.max_stock_qty = ko.observable().extend({ required: true });
	self.min_stock_qty = ko.observable().extend({ required: true });
	self.reordering_qty = ko.observable().extend({ required: true });
	self.storeitemname = ko.observable();
	self.store_item_id = ko.observable();

	if(data) {
		self.inventory_id(data.inventory_id);
		self.product_id(data.product_id);
		self.product_name(data.product_name);
		self.price_per_unit(parseFloat(data.price_per_unit));
		self.stock_at_hand(parseFloat(data.stock_at_hand));
		self.max_stock_qty(parseFloat(data.max_stock_qty));
		self.min_stock_qty(parseFloat(data.min_stock_qty));
		self.reordering_qty(parseFloat(data.reordering_qty));
		self.storeitemname(data.storeitemname);
		self.store_item_id(data.store_item_id);
	}

	self.color = ko.observable();
	self.status = ko.computed(function() {
		var status = "";

		if(self.stock_at_hand() > self.max_stock_qty()) {
			status = "Over Stock";
			self.color('label bg-blue');
		} else if(self.stock_at_hand() == self.max_stock_qty()) {
			status = "Full Stock";
			self.color('label bg-green');
		} else if(self.stock_at_hand() <= self.reordering_qty() && self.stock_at_hand() <= self.min_stock_qty() && self.stock_at_hand() > 0) {
			status = "Almost out of stock";
			self.color('label bg-orange');
		} else if(self.stock_at_hand() <= self.reordering_qty() && self.stock_at_hand() > 0) {
			status = "For reordering";
			self.color('label bg-yellow');
		} else if(self.stock_at_hand() == 0) {
			status = "Out of stock";
			self.color('label bg-red-active');
		}

		return status;
	});

	self.errors = ko.validation.group(self);

	self.toJSON = function() {
		var copy = ko.toJS(self);

		delete copy.status;
		delete copy.color;
		return copy;
	};
}

function InventoryFilter(code, name) {
	var self = this;

	self.code = ko.observable(code);
	self.name = ko.observable(name);
}

var filters = [ new InventoryFilter(0, 'Filter items'), new InventoryFilter(1, 'Restocking items'), new InventoryFilter(2, 'Out of stock items'), new InventoryFilter(3, 'Overstock items')];

$(function() {
	function InventoryViewModel()
	{
		var self = this;

		self.products = ko.observableArray([]);
		self.product = ko.observable(new Inventory());

		self.filters = ko.observableArray(filters);
		self.selected_filter = ko.observable(0);

		self.search_str = ko.observable('');
		self.str_search = ko.observable('');
		self.sort_by = ko.observable('asc');
		self.sort_column = ko.observable('');

		self.is_parent = ko.observable(1);
		self.category_id = ko.observable(0);

		self.storeitem_categories = ko.observable(new StoreItemsCategories(false));

		self.no_product = ko.computed(function() {
			return self.product().inventory_id() == -1;
		});

		self.selected_filter.subscribe(function() {
			self.getProducts();
		});

		self.storeitem_categories().category.subscribe(function() {
			var category_id = self.storeitem_categories().no_category() ? 0 : self.storeitem_categories().category().category_id();
			self.category_id(category_id);
			self.is_parent(0);
			self.getProducts();
		});

		self.storeitem_categories().parent_category.subscribe(function() {
			var category_id = self.storeitem_categories().no_parent_category() ? 0 : self.storeitem_categories().parent_category().category_id();
			self.category_id(category_id);
			self.is_parent(1);
			self.getProducts();
		});

		self.sortResult = function(column) {
			if(self.sort_by() != '') {
				if(self.sort_by() == 'asc') {
					self.sort_by('desc');
				} else {
					self.sort_by('asc');
				}
			}

			self.sort_column(column);
			self.getProducts();
		};

		self.save_export_url = ko.computed(function() {
			var url = getRootUrl() + 'api/reports/inventories?query=' + self.str_search() + '&sort_column=' + self.sort_column() + '&sort_by=' + self.sort_by() + '&filter=' + self.selected_filter() + '&category_id=' + self.category_id() + '&is_parent=' + self.is_parent();
		
			return url;
		});

		self.getProducts = function() {
			var url = getRootUrl() + 'api/inventories/0?query=' + self.str_search() + '&sort_column=' + self.sort_column() + '&sort_by=' + self.sort_by() + '&filter=' + self.selected_filter() + '&category_id=' + self.category_id() + '&is_parent=' + self.is_parent();

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.products.removeAll();
					self.product(new Inventory());
					$.each(data, function(index, item) {
						self.products.push(new Inventory(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.printReport = function() {
			window.print();
		};

		self.getProducts();
	}

	ko.applyBindings(new InventoryViewModel(), document.getElementById("inventory-report"));
});