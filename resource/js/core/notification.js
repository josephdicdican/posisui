function Log(data) {
	var self = this;

	self.order_id = ko.observable(-1);
	self.firstname = ko.observable();
	self.lastname = ko.observable();
	self.remarks = ko.observable();
	self.date_created = ko.observable();
	self.is_unread = ko.observable();
	self.user = ko.observable();
	self.display_name = ko.observable();
	self.username = ko.observable();
	self.storeitemname = ko.observable();

	if (data) {
		self.order_id(data.order_id);
		self.firstname(data.firstname);
		self.lastname(data.lastname);
		self.remarks(data.remarks);
		self.date_created(data.date_created);
		self.is_unread(data.is_unread);
		self.user(data.user);
		self.display_name(data.display_name);
		self.username(data.username);
		self.storeitemname(data.storeitemname);
	};

	self.errors = ko.validation.group(self);
}

$(function() {
	function NotificationViewModel()
	{
		var self = this;

		self.customerlogs = ko.observableArray([]);
		self.customerlog = ko.observable(new Log);

		self.userlogs = ko.observableArray([]);
		self.userlog = ko.observable(new Log);

		self.inventorylogs = ko.observableArray([]);
		self.inventorylog = ko.observable(new Log);

		self.orderlogs = ko.observableArray([]);
		self.orderlog = ko.observable(new Log);

		self.getCustomerLogs = function() {
			var url = getRootUrl() + 'api/logs/customer_logs';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.customerlogs.removeAll();
					self.customerlog(new Log());
					$.each(data, function(index, item) {
						self.customerlogs.push(new Log(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getUserLogs = function() {
			var url = getRootUrl() + 'api/logs/user_logs';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.userlogs.removeAll();
					self.userlog(new Log());
					$.each(data, function(index, item) {
						self.userlogs.push(new Log(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getOrderLogs = function() {
			var url = getRootUrl() + 'api/logs/order_logs';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.orderlogs.removeAll();
					self.orderlog(new Log());
					$.each(data, function(index, item) {
						self.orderlogs.push(new Log(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getInventoryLogs = function() {
			var url = getRootUrl() + 'api/logs/inventory_logs';

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.inventorylogs.removeAll();
					self.inventorylog(new Log());
					$.each(data, function(index, item) {
						self.inventorylogs.push(new Log(item));
					});
				},
				error: function (xhr) {
					console.log(xhr);
				}
			});
		};

		self.getCustomerLogs();
		self.getUserLogs();
		self.getOrderLogs();
		self.getInventoryLogs();
	}

	ko.applyBindings(new NotificationViewModel());
});