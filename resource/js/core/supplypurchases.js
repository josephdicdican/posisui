function AuthSupplyPurchaseDelete() {
    var self = this;

    self.password = ko.observable().extend({ required: true});
    self.supply_purchase_id = ko.observable(-1);

    self.errors = ko.validation.group(self);

    self.toJSON = function() {
    	var copy = ko.toJS(self);

    	delete copy.errors;

    	return copy;
    };
}

function SupplyPurchase(data) {
	var self = this;

	self.supply_purchase_id = ko.observable(-1);
	self.date_of_purchase = ko.observable(new Date());
	self.date_created = ko.observable(new Date());
	self.is_confirmed = ko.observable(false);
	self.total_qty = ko.observable(0);
	self.supplypurchaselines = ko.observableArray([]); // accepts array of SupplyPurchaseLine
	self.errors = ko.validation.group(self);


	self.confirmed = ko.computed(function() {
		return (self.is_confirmed() == 1);
	});

	self.format_date = function(dateStr) {
		return toDateTimeString(dateStr);
	};

	self.totalCost = function() {
		var total = 0;

		if(self.supplypurchaselines().length > 0) {
			$.each(self.supplypurchaselines(), function(index, item) {
				total += item.purchase_qty() * item.purchase_price_per_unit();
			});
		}

		return toMoney(total);
	};

	self.totalQty = function() {
		var total = 0;

		if(self.supplypurchaselines().length > 0) {
			$.each(self.supplypurchaselines(), function(index, item) {
				total += parseFloat(item.purchase_qty());
			});
		}

		return total;
	};

	self.valid = ko.computed(function() {
		var errors = self.errors().length;

		$.each(self.supplypurchaselines(), function(index, item) {
			errors += item.errors().length;
		});

		return (errors == 0 && self.supplypurchaselines().length > 0);
	});

	if(data) {
		self.supply_purchase_id(data.supply_purchase_id);
		self.date_of_purchase(data.date_of_purchase);
		self.date_created(data.date_created);
		self.is_confirmed((data.is_confirmed == '1') ? true : false);
		self.total_qty(data.total_qty);
	}

	self.toJSON = function() {
		var copy = ko.toJS(self);

		delete copy.confirmed;
		delete copy.format_total_cost;
		delete copy.valid;
		delete copy.errors;

		return copy;
	};
}

function SupplyPurchaseLine(data) {
	var self = this;

	self.purchase_line_id = ko.observable();
	self.supply_purchase_id = ko.observable(-1);
	self.store_item_id = ko.observable();
	self.purchase_qty = ko.observable().extend({ required: true, min: 1 });
	self.purchase_price_per_unit = ko.observable().extend({ required: true, min: 0.25, pattern : { message : 'Must be real money value', params : moneyRgx } });
	self.storeitemname = ko.observable();
	self.is_confirmed = ko.observable(false);

	self.confirmed = ko.computed(function() {
		return self.is_confirmed();
	});

	self.formatted_purchase_price_per_unit = ko.computed(function() {
		var price = toMoney(self.purchase_price_per_unit())
		return 'Php ' + price;
	});

	self.subtotal = ko.computed(function() {
		var subtotal = toMoney(self.purchase_qty() * self.purchase_price_per_unit());
		return 'Php ' + subtotal;
	});

	self.removed = ko.computed(function() {
		return (self.supply_purchase_id() == -1);
	});

	if(data) {
		self.purchase_line_id(data.purchase_line_id);
		self.supply_purchase_id(data.supply_purchase_id);
		self.store_item_id(data.store_item_id);
		self.purchase_qty(data.purchase_qty);
		self.purchase_price_per_unit(data.purchase_price_per_unit);
		self.storeitemname(data.storeitemname);
		self.is_confirmed(data.is_confirmed ? true : false);
	}

	self.init_add = function(data) {
		self.store_item_id(data.store_item_id());
		self.storeitemname(data.storeitemname());
		self.purchase_qty(1);
		self.purchase_price_per_unit(data.price_per_unit());
	};

	self.init_edit = function(data, supply_purchase_id) {
		self.purchase_line_id(-1);
		self.supply_purchase_id(supply_purchase_id);
		self.store_item_id(data.store_item_id());
		self.storeitemname(data.storeitemname());
		self.purchase_qty(1);
		self.purchase_price_per_unit(data.price_per_unit());
	};

	self.toJSON = function() {
		var copy = ko.toJS(self);

		delete copy.storeitemname;
		delete copy.subtotal;
		delete copy.formatted_purchase_price_per_unit;
		delete copy.is_confirmed;
		delete copy.confirmed;
		delete copy.removed;
		delete copy.errors;

		return copy;
	};

	self.errors = ko.validation.group(self);
}

function FilterView(node, value) {
	var self = this;

	self.node = ko.observable(node);
	self.nodeVal = ko.observable(value);
} 

var filters = [new FilterView('Filter purchases ...', ''), new FilterView('Onhold Purchases', 0), new FilterView('Added Purchases', 1)];

$(function() {
	function SupplyPurchaseViewModel() {
		var self = this;

		self.categories = ko.observableArray([]);
		self.parent_categories = ko.observableArray([]);
		self.parent_category = ko.observable();
		self.category = ko.observable();

		self.storeitems = ko.observableArray([]);
		self.storeitem = ko.observable();

		self.supplypurchases = ko.observableArray([]);
		self.supplypurchase = ko.observable(new SupplyPurchase());
		self.supplypurchaselines = ko.observableArray([]);
		self.supplypurchaseline = ko.observable();

		self.filters = ko.observableArray(filters);
		self.selectedFilter = ko.observable();
		self.filterGet = ko.observable(null);

		var d = new Date(), date = new Date(d.getFullYear() - 1, d.getMonth(), 1);
		self.min_date = ko.observable(date);

	/// Categories & StoreItems block start
		self.no_parent_category = ko.computed(function() {
			return self.parent_category() == null;
		});

		self.no_category = ko.computed(function() {
			return self.category() == null;
		});

		self.parent_category.subscribe(function() {
			var id = null;

			if(self.no_parent_category() == false) {
				id = self.parent_category().category_id();
			}

			self.getStoreItems(id, 1);
			self.getCategories();
		});

		self.category.subscribe(function() {
			var id = null;

			if(self.no_category() == false) {
				id = self.category().category_id();
			}

			self.getStoreItems(id, 0);
		});

		self.getParentCategories = function() {
			$.ajax({
				url: getRootUrl() + 'api/categories/get_parents/',
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.parent_categories.removeAll();
					$.each(data, function(index, item) {
						self.parent_categories.push(new Category(item));
					});
					//console.log(ko.toJSON(self.parent_categories()));
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getCategories = function() {
			var parent_id = (self.no_parent_category()) ? 0 : self.parent_category().category_id() ;
			$.ajax({
				url: getRootUrl() + 'api/categories/get_parents/' + parent_id,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.categories.removeAll();
					$.each(data, function(index, item) {
						self.categories.push(new Category(item));
					});
					//console.log(data);
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.getStoreItems = function(id, parent) {
			var url = getRootUrl() + 'api/storeitems/get_all'
			if(id != null) {
				url = getRootUrl() + 'api/storeitems/get_by_category/' + id + '/' + parent;
			}

			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.storeitems.removeAll();
					$.each(data, function(index, item) {
						self.storeitems.push(new StoreItem(item));
					});
					//console.log(ko.toJSON(self.storeitems()));
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};
	/// Categories & StoreItems block end

	/// Supply Purchases block start
		self.no_supplypurchase = ko.computed(function() {
			return self.supplypurchase().supply_purchase_id() == -1;
		});

		self.getSupplyPurchases = function() {
			$('#newpurchase, #editpurchase').hide(); $('#purchases').show(); $('#promptcanceladd').modal('hide');
			var url = getRootUrl() + 'api/supplypurchases/purchases?confirmed=' + self.filterGet();
			
			$.ajax({
				url: url,
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.supplypurchases.removeAll();
					$.each(data, function(index, item) {
						self.supplypurchases.push(new SupplyPurchase(item));
					});
				},
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

		self.selectedFilter.subscribe(function() {
			self.filterGet(self.selectedFilter().nodeVal());
			self.getSupplyPurchases();
		});

		self.getSupplyPurchase = function(supplypurchase) {
			self.supplypurchase(supplypurchase);
			self.getSupplyPurchaseLines();
		};

		self.getSupplyPurchaseLines = function() {
			$.ajax({
				url: getRootUrl() + 'api/supplypurchases/purchaselines/' + self.supplypurchase().supply_purchase_id(),
				type: 'GET',
				contentType: 'application/json',
				success: function(data) {
					self.supplypurchaselines.removeAll();
					$.each(data, function(index, item) {
						self.supplypurchaselines.push(new SupplyPurchaseLine(item));
					});
					self.supplypurchase().supplypurchaselines(self.supplypurchaselines());
				}, 
				error: function(xhr) {
					console.log(xhr);
				}
			});
		};

	  	/// Supply purchase adding start
			self.addPurchaseLine = function(storeitem) {
				var purchaseline = new SupplyPurchaseLine();
				purchaseline.init_add(storeitem);

				if(self.supplypurchase().supplypurchaselines().length > 0) {
					var exist = false; // flag to know if exist
					
					$.each(self.supplypurchase().supplypurchaselines(), function(index, item) {
						//console.log(ko.toJSON(item));
						if(item.store_item_id() == purchaseline.store_item_id()) {
							exist = true; // set true if found
							item.purchase_qty(item.purchase_qty() + purchaseline.purchase_qty());
						} 
					});

					if(exist == false) { // purchase line new or not yet exist then push
						self.supplypurchase().supplypurchaselines.unshift(purchaseline);
					}
				} else {
					self.supplypurchase().supplypurchaselines.unshift(purchaseline);
				}
			};

			self.removePurchaseLine = function(purchaseline) {
				var flag = confirm('Confirm to remove ' + purchaseline.storeitemname() + ' ?');

				if(flag) {
					self.supplypurchase().supplypurchaselines.remove(purchaseline);
				}
			};

			self.showAddSupplyPurchase = function() {
				self.supplypurchase(new SupplyPurchase());
				$('#newpurchase').show(); $('#purchases').hide();
			};

			self.cancelAddSupplyPurchase = function() {
				if(self.supplypurchase().supplypurchaselines().length > 0) {
					$('#promptcanceladd').modal('show');
				} else {
					self.getSupplyPurchases();
				}
			};

			self.addSupplyPurchase = function() {
				var data = ko.toJSON(self.supplypurchase());

				$.ajax({
					url: getRootUrl() + 'api/supplypurchases/add_supply_purchase',
					type: 'POST',
					contentType: 'application/json',
					data: data,
					success: function(data) {
						console.log(data);
						self.supplypurchase(new SupplyPurchase());
	 					self.getSupplyPurchases();
	 					$('#promptsaveconfirm').modal('hide');
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};
		/// Supply purchase adding end

		/// Supply purchase editing start
			self.showEditSupplyPurchase = function() {
				$('#editpurchase').show(); $('#purchases').hide();
			};

			self.addPurchaseLineEdit = function(storeitem) {
				var purchaseline = new SupplyPurchaseLine();
				purchaseline.init_edit(storeitem, self.supplypurchase().supply_purchase_id());

				if(self.supplypurchase().supplypurchaselines().length > 0) {
					var exist = false; // flag to know if exist
					
					$.each(self.supplypurchase().supplypurchaselines(), function(index, item) {
						console.log(ko.toJSON(item));
						if(item.store_item_id() == purchaseline.store_item_id()) {
							exist = true; // set true if found
							item.purchase_qty(parseFloat(item.purchase_qty()) + parseFloat(purchaseline.purchase_qty()));
						} 
					});

					if(exist == false) { // purchase line new or not yet exist then push
						self.supplypurchase().supplypurchaselines.unshift(purchaseline);
					}
				} else {
					self.supplypurchase().supplypurchaselines.unshift(purchaseline);
				}
			};

			self.removePurchaseLineEdit = function(purchaseline) {
				var flag = confirm('This will be deleted after saving. Confirm to remove ' + purchaseline.storeitemname() + ' ?');

				if(flag) {
					purchaseline.supply_purchase_id(-1);
				}
			};

			self.editSupplyPurchase = function() {
				var json = ko.toJSON(self.supplypurchase());
				var url = getRootUrl() + 'api/SupplyPurchases/edit_supply_purchase';

				$.ajax({
					url: url,
					type: 'PUT',
					contentType: 'application/json',
					data: json,
					success: function(data) {
						console.log(data);
						self.getSupplyPurchases();
						$('#prompteditconfirm').modal('hide');
						self.getSupplyPurchaseLines();
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			};
		/// Supply purchase editing end

		/// Supply purchase delete start
			self.auth_supplypurchase_del = ko.observable(new AuthSupplyPurchaseDelete());
			self.showAuthSupplyPurchaseDelete = function() {
				self.auth_supplypurchase_del(new AuthSupplyPurchaseDelete());
				self.auth_supplypurchase_del().supply_purchase_id(self.supplypurchase().supply_purchase_id());
				$('#sp_del_note').html('');
				$('#authsupplypurchasedelete').modal('show');
			};

			self.deleteSupplyPurchase = function() {
				var note = $('#sp_del_note');
				note.addClass('text-danger');

				if(self.auth_supplypurchase_del().errors().length == 0) {
					var json = ko.toJSON(self.auth_supplypurchase_del());
					var url = getRootUrl() + 'api/supplypurchases/delete_supply_purchase';
					console.log(json);
					$.ajax({
						url: url,
						type: 'DELETE',
						contentType: 'application/json',
						data: json,
						beforeSend: function() {
							note.html('Removing supply purchase please wait...');
						},
						success: function(data) {
							if(data) {
								note.html('');
								$('#authsupplypurchasedelete').modal('hide');
								self.getSupplyPurchases();
								self.getSupplyPurchaseLines();
							} else {
								note.html('Removing supply purchase failed. Invalid password.');
							}
						},
						error: function(xhr) {
							console.log(xhr);
						}
					});
				} else {
					self.auth_supplypurchase_del().errors.showAllMessages();
				}				
			};
		/// Supply purchase delete end

	/// Supply Purchase block end

		self.getParentCategories();
		self.getCategories();
		self.getSupplyPurchases();
		self.getStoreItems(null, 0);
	}

	ko.applyBindings(new SupplyPurchaseViewModel());
	$('#newpurchase').hide(); $('#editpurchase').hide();
});