function StoreItemsCategories(flag_inventory) {
	var self = this;

	self.categories = ko.observableArray([]);
	self.parent_categories = ko.observableArray([]);
	self.parent_category = ko.observable();
	self.category = ko.observable();

	self.storeitems = ko.observableArray([]);
	self.storeitem = ko.observable();

	self.is_inventory = ko.observable(flag_inventory);

/// Categories & StoreItems block start
	self.no_parent_category = ko.computed(function() {
		return self.parent_category() == null;
	});

	self.no_category = ko.computed(function() {
		return self.category() == null;
	});

	self.parent_category.subscribe(function() {
		var id = null;

		if(self.no_parent_category() == false) {
			id = self.parent_category().category_id();
		}

		self.getStoreItems(id, 1, self.is_inventory());
		self.getCategories();
	});

	self.category.subscribe(function() {
		var id = null;

		if(self.no_category() == false) {
			id = self.category().category_id();
		}

		self.getStoreItems(id, 0, self.is_inventory());
	});

	self.getParentCategories = function() {
		$.ajax({
			url: getRootUrl() + 'api/categories/get_parents/',
			type: 'GET',
			contentType: 'application/json',
			success: function(data) {
				self.parent_categories.removeAll();
				$.each(data, function(index, item) {
					self.parent_categories.push(new Category(item));
				});
				console.log(ko.toJSON(self.parent_categories()));
			},
			error: function(xhr) {
				console.log(xhr);
			}
		});
	};

	self.getCategories = function() {
		var parent_id = (self.no_parent_category()) ? 0 : self.parent_category().category_id() ;
		$.ajax({
			url: getRootUrl() + 'api/categories/get_parents/' + parent_id,
			type: 'GET',
			contentType: 'application/json',
			success: function(data) {
				self.categories.removeAll();
				$.each(data, function(index, item) {
					self.categories.push(new Category(item));
				});
				//console.log(data);
			},
			error: function(xhr) {
				console.log(xhr);
			}
		});
	};

	self.getStoreItems = function(id, parent, is_inventory) {
		var url = getRootUrl() + 'api/storeitems/get_all?is_inventory=' + self.is_inventory();
		if(id != null) {
			url = getRootUrl() + 'api/storeitems/get_by_category/' + id + '/' + parent + '?is_inventory=' + self.is_inventory();
		}
		console.log(url);

		$.ajax({
			url: url,
			type: 'GET',
			contentType: 'application/json',
			success: function(data) {
				self.storeitems.removeAll();
				$.each(data, function(index, item) {
					self.storeitems.push(new StoreItem(item));
				});
			},
			error: function(xhr) {
				console.log(xhr);
			}
		});
	};
/// Categories & StoreItems block end

	self.getParentCategories();
	self.getCategories();
	self.getStoreItems();
}