function getRootUrl() 
{
	return window.location.origin + '/posisui/';
}

function Status(isOK, message) 
{
	var self = this;

	self.isOK = ko.observable(isOK);
	self.message = ko.observable(message);
}

/*ko.validation.rules['mustEqual'] = {
    validator: function (val, otherVal) {
        return val === otherVal;
    },
    message: 'The field must equal {0}'
};
ko.validation.registerExtenders();*/


var mustEqual = function (val, other) {
    return val == other;
};

ko.validation.rules.pattern.message = 'Invalid.';
ko.validation.init({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null
});

ko.bindingHandlers.datepicker = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var $el = $(element);
        
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {};
        $el.datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function() {
            var observable = valueAccessor();
            observable($el.datepicker("getDate"));
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $el.datepicker("destroy");
        });

    },
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            $el = $(element),
            current = $el.datepicker("getDate");
        
        if (value - current !== 0) {
            $el.datepicker("setDate", value);   
        }
    }
};

ko.bindingHandlers.timepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize timepicker with some optional options
        var options = allBindingsAccessor().timepickerOptions || {},
            input = $(element).timepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "time-change", function (event, time) {
            var observable = valueAccessor(),
                current = ko.utils.unwrapObservable(observable);
            
            if (current - time !== 0) {
                observable(time);
            }
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).timepicker("destroy");
        });
    },

    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            // calling timepicker() on an element already initialized will
            // return a TimePicker object
            instance = $(element).timepicker();

        if (value - instance.getTime() !== 0) {
            instance.setTime(value);
        }
    }
};

var vat = 0.12;

function toMoney(num) {
    var numFormatted = parseFloat(num).toFixed(2);

    return numFormatted;
}

function toDateTimeString(dateString) {
    var date = new Date(dateString);

    return date.toLocaleString();
}

function toTimeString(dateString) {
    var d = new Date(dateString);
    d = d.toLocaleTimeString().replace(/:\d+ /, ' ');

    return d;
}

function SubCategory(data) {
    var self = this;

    self.category_id = ko.observable(-1);
    self.parent_id = ko.observable();
    self.category_name = ko.observable();
    self.image_id = ko.observable();
    self.image = ko.observable();
    self.image_type = ko.observable();
    self.products_count = ko.observable();

    self.no_image = ko.computed(function() {
        return (self.image() === null);
    });

    self.removable = ko.computed(function() {
        return self.products_count() <= 0;
    });

    self.img_src = ko.computed(function() {
        var src = getRootUrl() + 'uploads/images/default.png';

        if(!self.no_image()) {
            src = getRootUrl() + 'uploads/images/' + self.image();
        }

        return src;
    });

    self.is_parent = ko.computed(function() {
        return self.parent_id() == null;
    });

    if(data) {
        self.category_id(data.category_id);
        self.parent_id(data.parent_id);
        self.category_name(data.category_name);
        self.image_id(data.image_id);
        self.image(data.image);
        self.image_type(data.image_type);
        self.products_count(data.products_count);
    }

    self.toJSON = function() {
        var copy = ko.toJS(this);

        delete copy.img_src;

        return copy;
    };
}

function Category(data) {
    var self = this;

    self.category_id = ko.observable(-1);
    self.parent_id = ko.observable(0);
    self.category_name = ko.observable();
    self.image_id = ko.observable();
    self.image = ko.observable();
    self.image_type = ko.observable();
    self.products_count = ko.observable();
    self.nested = ko.observable(false);

    self.no_image = ko.computed(function() {
        return (self.image() === null);
    });

    self.removable = ko.computed(function() {
        return self.products_count() <= 0 && self.sub_categories().length <= 0;
    });

    self.is_parent = ko.computed(function() {
        return self.parent_id() == null;
    });

    self.img_src = ko.computed(function() {
        var src = getRootUrl() + 'uploads/images/default.png';

        if(!self.no_image()) {
            src = getRootUrl() + 'uploads/images/' + self.image();
        }

        return src;
    });

    self.sub_categories = ko.observableArray([]);

    if(data) {
        self.category_id(data.category_id);
        self.parent_id(data.parent_id);
        self.category_name(data.category_name);
        self.image_id(data.image_id);
        self.image(data.image);
        self.image_type(data.image_type);
        self.products_count(data.products_count);

        if(data.sub_categories instanceof Array) {
            $.each(data.sub_categories, function(index, item) {
                self.sub_categories.push(new SubCategory(item));
            });
        }
    }

    self.toJSON = function() {
        var copy = ko.toJS(self);

        delete copy.img_src;
        delete copy.no_image;

        return copy;
    };
}

function StoreItem(data) {
    var self = this;

    self.store_item_id = ko.observable(-1);
    self.sku = ko.observable();
    self.price_per_unit = ko.observable(0);
    self.product_id = ko.observable();
    self.product_name = ko.observable();
    self.unit_id = ko.observable();
    self.measure_name = ko.observable();
    self.category_id = ko.observable();
    self.category_name = ko.observable();
    self.parent_id = ko.observable();
    self.storeitemname = ko.observable();
    self.inventory_id = ko.observable();
    self.stock_at_hand = ko.observable();
    self.reordering_qty = ko.observable();
    self.max_stock_qty = ko.observable();
    self.min_stock_qty = ko.observable();
    self.image_id = ko.observable(-1);
    self.image_type = ko.observable('');
    self.image = ko.observable('');

    self.price_formatted = ko.computed(function() {
        return "Php " + parseFloat(self.price_per_unit()).toFixed(2);
    });

    self.removable = ko.computed(function() {
        return (self.inventory_id() == null);
    });

    self.no_stock = ko.computed(function() {
        return (self.stock_at_hand() == null || self.stock_at_hand() == 0);
    });

    self.for_reordering = ko.computed(function() {
        return (self.stock_at_hand() <= self.reordering_qty() && self.stock_at_hand() > 0);
    }); 

    self.no_image = ko.computed(function() {
        return (self.image() === null);
    });

    self.img_src = ko.computed(function() {
        var src = getRootUrl() + 'uploads/images/default.png';

        if(!self.no_image()) {
            src = getRootUrl() + 'uploads/images/' + self.image();
        }

        return src;
    });

    if(data) {
        self.store_item_id(data.store_item_id);
        self.sku(data.sku);
        self.price_per_unit(data.price_per_unit);
        self.product_id(data.product_id);
        self.product_name(data.product_name);
        self.unit_id(data.unit_id);
        self.measure_name(data.measure_name);
        self.category_id(data.category_id);
        self.category_name(data.category_name);
        self.parent_id(data.parent_id);
        self.storeitemname(data.storeitemname);
        self.inventory_id(data.inventory_id);
        self.stock_at_hand(data.stock_at_hand);
        self.reordering_qty(data.reordering_qty);
        self.max_stock_qty(data.max_stock_qty);
        self.min_stock_qty(data.min_stock_qty);
        self.image_id(data.image_id);
        self.image(data.image);
        self.image_type(data.image_type);
    }

    self.toJSON = function() {
        var copy = ko.toJS(this);

        delete copy.storeitemname;
        delete copy.image_id;
        delete copy.image;
        delete copy.image_type;
        delete copy.no_image;
        delete copy.img_src;

        return copy;
    };
}

function Customer(data) {
    var self = this;

    self.customer_id = ko.observable(-1);
    self.firstname = ko.observable().extend({ required: true, minLength: 2, maxLength: 20});
    self.lastname = ko.observable().extend({ required: true, minLength: 2, maxLength: 20});
    self.address = ko.observable().extend({ required: true });
    self.email = ko.observable().extend({ required: true, email: true });
    self.gender = ko.observable('1').extend({ required: true });

    self.is_senior = ko.observable(false);
    self.card_no = ko.observable();

    self.fullname = ko.computed(function() {
        return self.firstname() + " " + self.lastname();
    });

    self.isSenior = ko.computed(function() {
        return self.is_senior();
    });

    if (data) {
        self.customer_id(data.customer_id);
        self.firstname(data.firstname);
        self.lastname(data.lastname);
        self.address(data.address);
        self.email(data.email);
        self.gender(data.gender);
        self.is_senior((data.is_senior != null && data.is_senior == 1));
        self.card_no(data.card_no);
    };

    self.errors = ko.validation.group(self);
}

function Supplier(data) {
    var self = this;

    self.supplier_id = ko.observable(-1);
    self.supplier_name = ko.observable().extend({ required: true, minLength: 2, maxLength: 20 });
    self.supplier_contact = ko.observable().extend({ required: false, minLength: 11, maxLength: 11, email: false });
    self.supplier_email = ko.observable().extend({ required: true, email: true });

    if(data) {
        self.supplier_id(data.supplier_id);
        self.supplier_name(data.supplier_name);
        self.supplier_contact(data.supplier_contact);
        self.supplier_email(data.supplier_email);
    }

    self.errors = ko.validation.group(self);
}

function Order(data) {
    var self = this;

    self.order_id = ko.observable(-1);
    self.date_of_order = ko.observable();
    self.is_checkout = ko.observable(0);
    self.is_discounted = ko.observable(0);
    self.total_items = ko.observable(0);
    self.totalamount = ko.observable(0);
    self.customer_id = ko.observable(0);

    self.total_amount_formatted = ko.computed(function() {
        return toMoney(self.totalamount());
    });

    self.format_date = ko.computed(function() {
        return toDateTimeString(self.date_of_order());
    });

    self.is_check_out = ko.computed(function() {
        return (self.is_checkout() == 1);
    });

    if(data) {
        self.order_id(data.order_id);
        self.date_of_order(data.date_of_order);
        self.is_checkout(data.is_checkout);
        self.is_discounted(data.is_discounted);
        self.total_items(data.total_items);
        self.totalamount(data.total_amount);
    }

    self.orderlines = ko.observableArray([]);
    self.total_amount = ko.computed(function() {
        var total_amount = 0;

        $.each(self.orderlines(), function(index, item) {
            if(item.quantity() != -1) {
                total_amount += parseFloat(item.quantity() * item.price_per_unit());
            }
        });

        return toMoney(total_amount);
    });

    self.format_date = function(dateStr) {
        return toDateTimeString(dateStr);
    };

    self.format_time = ko.computed(function() {
        return toTimeString(self.date_of_order());
    });

    self.vat = ko.computed(function() {
        var vat = Math.max(0, self.total_amount() * 0.12);
        return toMoney(vat);
    });

    self.vatable_amount = ko.computed(function() {
        var vatable = Math.max(0, self.total_amount() - self.vat());
        return toMoney(vatable);
    });

    self.valid_order = ko.computed(function() {
        var errors = 0;

        $.each(self.orderlines(), function(index, item) {
            if(!item.valid() && self.new_order()) {
                errors += 1;
            }
        });

        return (errors == 0 && self.orderlines().length > 0);
    });

    self.new_order = ko.computed(function() {
        return self.order_id() == -1;
    });

    self.tender_amount = ko.observable(0);
    
    self.change = ko.computed(function() {
        var change = Math.max(0, parseFloat(self.tender_amount() - self.total_amount()));

        return toMoney(change);
    });
}

function Orderline() {
    var self = this;

    self.line_id = ko.observable(-1);
    self.order_id = ko.observable(-1);
    self.store_item_id = ko.observable(-1);
    self.stock_at_hand = ko.observable(0);
    self.quantity = ko.observable(1).extend({required: true, min: 1});

    self.storeitemname = ko.observable('');
    self.price_per_unit = ko.observable();

    self.errors = ko.validation.group(self);

    self.price_per_unit_formatted = ko.computed(function() {
        return toMoney(self.price_per_unit());
    });

    self.quantity_valid = ko.computed(function() {
        return self.quantity() <= self.stock_at_hand();
    });

    self.hold_removed = ko.computed(function() {
        return self.quantity() == -1 && self.order_id() != -1;
    });

    self.init_add = function(storeitem) {
        self.store_item_id(storeitem.store_item_id());
        self.storeitemname(storeitem.storeitemname());
        self.price_per_unit(parseFloat(storeitem.price_per_unit()));
        self.quantity(1); 
        self.stock_at_hand(parseFloat(storeitem.stock_at_hand()));
    };

    self.init = function(data) {
        var stock = parseFloat(data.stock_at_hand);
        self.line_id(data.line_id);
        self.order_id(data.order_id);
        self.store_item_id(data.store_item_id);
        self.stock_at_hand(isNaN(stock) ? 0 : stock);
        self.quantity((self.stock_at_hand() == 0) ? -1 : parseFloat(data.quantity));
        self.storeitemname(data.storeitemname);
        self.price_per_unit(parseFloat(data.price_per_unit));
    };

    self.init_sales = function(data) {
        var stock = parseFloat(data.stock_at_hand);
        self.line_id(data.line_id);
        self.order_id(data.order_id);
        self.store_item_id(data.store_item_id);
        self.stock_at_hand(isNaN(stock) ? 0 : stock);
        self.quantity(parseFloat(data.quantity));
        self.storeitemname(data.storeitemname);
        self.price_per_unit(parseFloat(data.price_per_unit));
    };

    self.subtotal = ko.computed(function() {
        var total = 0;
        
        if(self.quantity() > 0) {
            total = parseFloat(self.quantity() * self.price_per_unit());
        }

        return toMoney(total);
    });

    self.valid = ko.computed(function() {
        return self.quantity_valid() && self.errors().length == 0;
    });

    self.toJSON = function() {
        var copy = ko.toJS(self);

        delete copy.storeitemname;
        delete copy.price_per_unit;
        delete copy.price_per_unit_formatted;
        delete copy.subtotal;
        delete copy.quantity_valid;
        delete copy.valid;
        delete copy.stock_at_hand;
        delete copy.errors;
        delete copy.hold_removed;

        return copy;
    };
}

var bodyClass = (localStorage.getItem('nav_state') == 'close') ? ' sidebar-collapse' : '';
var body = document.body;
    body.className = body.className + bodyClass;

function test() {
    if(localStorage.getItem('nav_state') == 'open') {
        body.className = body.className + bodyClass;
        localStorage.setItem('nav_state', 'close');
    } else {
        body.className.replace( /(?:^|\s)sidebar-collapse(?!\S)/g , '' );
        localStorage.setItem('nav_state', 'open');
    }
}

var urlRgx = '^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$', 
    moneyRgx = '^[0-9]{1,6}(.[0-9]{1,2})?$', 
    realNumRgx = '^[1-9]{1,6}(.[0-9]{1,2})?$',
    zipCodeRgx = '^[0-9]{4,6}$';
// .extend({ required: true, pattern : { message : 'Please input real money value', params : moneyRgx } });

(function ($) {
    $.fn.initial = function (options) {

        // Defining Colors
        var colors = ["#1abc9c", "#16a085", "#f1c40f", "#f39c12", "#2ecc71", "#27ae60", "#e67e22", "#d35400", "#3498db", "#2980b9", "#e74c3c", "#c0392b", "#9b59b6", "#8e44ad", "#bdc3c7", "#34495e", "#2c3e50", "#95a5a6", "#7f8c8d", "#ec87bf", "#d870ad", "#f69785", "#9ba37e", "#b49255", "#b49255", "#a94136"];

        return this.each(function () {

            var e = $(this);
            var settings = $.extend({
                // Default settings
                name: '',
                seed: 0,
                charCount: 1,
                textColor: '#ffffff',
                height: 100,
                width: 100,
                fontSize: 60,
                fontWeight: 400,
                fontFamily: 'HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica, Arial,Lucida Grande, sans-serif',
                radius: 0
            }, options);

            // overriding from data attributes
            settings = $.extend(settings, e.data());

            // making the text object
            var c = settings.name.substr(0, settings.charCount).toUpperCase();
            var cobj = $('<text text-anchor="middle"></text>').attr({
                'y': '50%',
                'x': '50%',
                'dy' : '0.35em',
                'pointer-events':'auto',
                'fill': settings.textColor,
                'font-family': settings.fontFamily
            }).html(c).css({
                'font-weight': settings.fontWeight,
                'font-size': settings.fontSize+'px',
            });

            var colorIndex = Math.floor((c.charCodeAt(0) + settings.seed) % colors.length);
            //colorIndex = Math.floor((Math.random() * colors.length));

            var svg = $('<svg></svg>').attr({
                'xmlns': 'http://www.w3.org/2000/svg',
                'pointer-events':'none',
                'width': settings.width,
                'height': settings.height
            }).css({
                'background-color': colors[colorIndex],
                'width': settings.width+'px',
                'height': settings.height+'px',
                'border-radius': settings.radius+'px',
                '-moz-border-radius': settings.radius+'px'
            });

            svg.append(cobj);
           // svg.append(group);
            var svgHtml = window.btoa(unescape(encodeURIComponent($('<div>').append(svg.clone()).html())));

            e.attr("src", 'data:image/svg+xml;base64,' + svgHtml);

        })
    };

}(jQuery));

$(function() {
    var time = $('#today');

    setInterval(function() {
        var t = new Date();
        var hhmmss = t.toLocaleString().split(',')[1];
        time.html(t.toDateString() + hhmmss);
    }, 1000);

    var quickNav = $('#quick-nav'), quickNavTog = $('#quick-nav-tog'), 
    btnShowQuickNav = $('#show-quick-nav'), btnHideQuickNav = $('#hide-quick-nav');

    btnHideQuickNav.click(function() {
        quickNav.hide(); quickNavTog.show();
    });

    btnShowQuickNav.click(function() {
        quickNav.show(); quickNavTog.hide();
    });

    quickNavTog.hide();
});

