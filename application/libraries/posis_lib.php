<?php

class Posis_lib {
	public function __construct() 
	{
		$this->CI  =& get_instance();
	}

	private $CI; 

	/**	
	* Checks session and redirect to proper page
	*/
	public function check_session() 
	{
		if($this->is_not_logged()) {
			$this->CI->session->set_flashdata('log', 'Trying to access a page on PoSIS while not logged in. Please log in to continue.');
			redirect(base_url('main/index'));
		}
	}

	public function admin_only() 
	{
		if($this->is_not_admin()) {
			redirect(base_url('error/unauthorized'));
		}
	}

	public function api_check($data, $grant = false) 
	{
		if($this->is_not_logged())	{
			$data = array('status' => Rest::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized');
		} else if ($this->is_not_admin() && $grant == false) {
			$data = array('status' => Rest::HTTP_UNAUTHORIZED, 'message' => 'Admin access only');
		} else if(empty($data)) {
			$data = array();
		}

		return $data;
	}

	public function api_is_processed($id, $process) 
	{
		$log = array();
		switch($process) {
			case 'add':
				$log['status'] = Rest::HTTP_CREATED;
				$log['message']['ok'] = 'Added succuessfully';
				$log['message']['notOK'] = 'Adding failed';
				break;

			case 'update':
				$log['status'] = Rest::HTTP_OK;
				$log['message']['ok'] = 'Updated succuessfully';
				$log['message']['notOK'] = 'Updating failed';
				break;
		}

		if($id == -1) {
			$data = array('status' => Rest::HTTP_BAD_REQUEST, 'message' => $log['message']['notOK']);
		} else {
			$data = array('status' => $log['status'], 'message' => $log['message']['ok']);
		}

		return $this->to_json($data);
	}

	public function api_is_deleted($id) {
		if($id == -1) {
			$data = array('status' => Rest::HTTP_BAD_REQUEST, 'message' => 'Deleting failed');
		} else {
			$data = array('status' => Rest::HTTP_OK, 'message' => 'Deleted successfully');
		}

		return $this->to_json($data);
	}

	public function to_json($data) 
	{
		$this->CI->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function date_to_sql($date) 
	{
		$dt = explode('T', $date);
		$time = explode('.', $dt[1]);
		$sql_date = $dt[0].' '.$time[0];

		return $sql_date;
	}

	private function is_not_logged() 
	{
		return ($this->CI->session->userdata('user_id') == '');
	}

	private function is_not_admin() 
	{
		return ($this->CI->session->userdata('is_admin') == false);
	}

	public function convert_to_csv($input_array, $output_file_name, $delimiter)
	{
	    /** open raw memory as file, no need for temp files */
	    $temp_memory = fopen('php://memory', 'w');
	    /** loop through array */
	    foreach ($input_array as $line) {
	        /** default php csv handler **/
	        fputcsv($temp_memory, $line, $delimiter);
	    }
	    /** rewrind the "file" with the csv lines **/
	    fseek($temp_memory, 0);
	    /** modify header to be downloadable csv file **/
	    header('Content-Type: application/csv');
	    header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
	    /** Send file to browser for download */
	    fpassthru($temp_memory);
	}

	/** Array to convert to csv */
	// $array_to_csv = Array(Array(12566, 'Enmanuel', 'Corvo'), Array(56544, 'John', 'Doe'), Array(78550, 'Mark', 'Smith'));
	// convert_to_csv($array_to_csv, 'report.csv', ',');
}