			<!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="header-pane col-md-12 z-depth-1">
                    <div class="title-pane col-lg-5 col-md-5 col-sm-12 col-xs-12 pull-left">
                        <h3>
                            Daily Menu &emsp;
                            <div class="btn-group">
                                <a data-bind="click: $root.showDailyMenu" href="#" class="btn btn-default btn-flat btn-sm">Items</a>
                                <a data-bind="click: $root.showDailyMenuSetup" href="#" class="btn btn-default btn-flat btn-sm">Setup</a>
                            </div>
                        </h3>
                    </div>
                </section>
                <section class="content">
                    <!-- Add product start -->
                    <div id="addproduct" class="row body-content">
                        <div class="">
                            <div class="box box-success box-solid">
                                <div class="box-header">
                                    <h4>
                                        Add Store Items to <span data-bind="text: category().category_name"></span>
                                        <a data-bind="click: $root.cancelAddProduct" href="#" class="close">&times;</a>
                                    </h4>
                                </div>
                                <div class="box-body">
                                    <form data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }, with: add_product' action="#" method="post" class="row">
                                        <input data-bind="value: category_id" type="hidden" />
                                        <div class="col-xs-3">
                                            <h4>Item</h4>
                                            <div class="form-group">
                                                <input data-bind="textInput: product_name" type="text" class="form-control" placeholder="Item Name" />
                                            </div>
                                             <div class="pull-right">
                                                <a data-bind="click: $root.cancelAddProduct" href="#" class="btn btn-danger"><i class="fa fa-times"></i> DISCARD</a>
                                                <button data-bind="click: (variants().length > 0) ? $root.addProduct : null, attr: { disabled: variants().length <= 0 }" type="button" class="btn btn-success"><i class="fa fa-save"></i> SAVE</button>
                                            </div>
                                        </div>
                                        <div data-bind="with: variant" class="col-xs-3">
                                            <h4>New Variant</h4>
                                            <div class="form-group">
                                                <input data-bind="textInput: sku" type="text" class="form-control" name="sku" placeholder="SKU" />
                                            </div>
                                            <div class="form-group">
                                                <input data-bind="textInput: price_per_unit" type="number" class="form-control" name="price" min="0" placeholder="e.g. 25.05" />
                                            </div>
                                            <div class="form-group">
                                                <select data-bind="options: $root.unitmeasures, optionsText: 'measure_name', optionsValue: 'unit_id', optionsCaption: 'Choose unit', value: unit_id" class="form-control"></select>
                                            </div>
                                            <div class="form-group">
                                                <a data-bind="click: $root.showAddUnitMeasure" href="#" class="btn btn-default btn-sm"><i class="fa fa-toggle-on"></i> New Unit</a>
                                                <a data-bind="click: $parent.add_variant" href="#" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Add Variant</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <h4>Variants</h4>
                                            <table class="table table-condensed table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SKU</th>
                                                        <th>Unit</th>
                                                        <th>Price/Unit</th>
                                                        <th class="text-center">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: variants">
                                                    <tr>
                                                        <td>
                                                            <input data-bind="textInput: sku" type="text" />
                                                        </td>
                                                        <td>
                                                            <select data-bind="options: $root.unitmeasures, optionsText: 'measure_name', optionsValue: 'unit_id', optionsCaption: 'Choose unit', value: unit_id"></select>
                                                        </td>
                                                        <td>
                                                            <input data-bind="textInput: price_per_unit" type="text" />
                                                        </td>
                                                        <td class="text-center">
                                                            <a data-bind="click: $parent.delete_variant" href="#" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <br />
                        </div>
                    </div><!-- Add product end -->

                    <!-- Daily Menu Setup start -->
                    <div id="dailymenu-setup" class="row body-content">
                        <div class="z-depth-1">
                            <div class="horizon">
                                <div class="form-inline dt-bootstrap">
                                    <div class="col-md-8 col-sm-12 col-xs-12 detailed-col">
                                        <div class="col-md-4 side-category">
                                            <div class="">
                                                <div class="box-header with-border">
                                                    <div class="">
                                                        <h4 class="pull-left">
                                                            <a data-bind="click: $root.refresh" href="#">Menu</a>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="box-body slimscroll no-padding">
                                                    <ul class="nav category" data-bind="foreach: categories">
                                                        <li class="">
                                                            <a href="#" data-bind="click: $root.getCategory">
                                                                <img class="img-circle" data-bind="attr: { src: img_src, alt: category_name }" class="img-boxed" width="80" height="80" />
                                                                <span data-bind="text: category_name"></span>
                                                                <span class="pull-right text-success">&darr;</span>
                                                            </a>
                                                         </li>
                                                        <ul class="sub-nav treeview-menu" data-bind="foreach: sub_categories">
                                                            <li class="treeview">
                                                                <a href="#" data-bind="click: $root.getCategory">
                                                                    <span data-bind="text: category_name"></span>
                                                                    <span data-bind="text: products_count" class="badge bg-green pull-right"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </ul>
                                                    <!-- <p data-bind="text: ko.toJSON(categories)"></p> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 no-padding">
                                            <div class="slimscroll-head">
                                                <h4 class="pad10">
                                                    Menu Items
                                                    <span class="pull-right">
                                                        <a href="#" class="btn btn-danger btn-flat btn-sm" data-toggle="modal" data-target="#confirmresetdailymenu">Reset Daily Menu Counts</a>
                                                    </span>
                                                </h4>
                                            </div>
                                            <div class="slimscroll">
                                                <table class="table table-hover store-items">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 60%;">Item Name</th>
                                                            <th style="width: 30%;" class="text-center">Counts</th>
                                                            <th style="width: 10%;" class="text-center"><i class="fa fa-ellipsis-h"></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: menuitems">
                                                        <tr data-bind="click: $root.getMenuItem">
                                                            <td style="width: 60%;">
                                                                <span data-bind="text: storeitemname"></span>
                                                            </td>
                                                            <td style="width: 30%;" class="text-center">
                                                                <span data-bind="text: stock_at_hand"></span>
                                                            </td>
                                                            <td style="width: 10%" class="text-center">
                                                                <div>
                                                                    <button data-bind="clickBubble: false" type="button" class="btn btn-sm text-primary" data-toggle="modal" data-target="#editmenuitem"><i class="fa fa-pencil"></i></button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot data-bind="visible: menuitems().length <= 0">
                                                        <tr>
                                                            <td class="danger" colspan="4">
                                                                No menu items retrieved
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12" id="details">
                                        <h4>
                                            <p class="pull-left">Menu Item Info</p>
                                            <a data-bind="click: $root.showDailyMenu" href="#" class="pull-right"><i class="fa fa-times"></i></a>
                                        </h4>

                                        <div data-bind="visible: !no_menuitem(), with: menuitem" class="info pull-left">
                                           <ul class="li-list">
                                                <li class="li-item">
                                                    <div class="li-left">Item Name</div>
                                                    <div class="li-body" data-bind="text: storeitemname"></div>
                                                </li>
                                                <li class="li-item">
                                                    <div class="li-left">Price per <span data-bind="text: measure_name"></span></div>
                                                    <div class="li-body" data-bind="text: price_formatted"></div>
                                                </li>
                                                <li class="li-item">
                                                    <div class="li-left">Stock at Hand</div>
                                                    <div class="li-body" data-bind="text: stock_at_hand"></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!-- col-md-4 -->
                                </div>
                            </div>
                        </div>
                    </div><!-- Daily Menu Setup end -->

                    <!-- App start -->
                    <div id="app" class="row body-content">
                        <div class="">
                            <div class="z-depth-1">
                                <div class="horizon">
                                    <div class="form-inline dt-bootstrap">
                                        <div class="col-xs-12 col-sm-6 col-md-3 side-category">
                                            <div class="">
                                            	<div class="box-header with-border">
                                            		<div class="">
                                                        <h4 class="pull-left">
                                                            <a data-bind="click: $root.refresh" href="#">Menu</a>
                                                            <div class="btn-group">
                                                                <a data-bind="click: $root.addCategory" href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addcategory"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </h4>
                                            		</div>
                                            	</div>
                                                <div class="box-body slimscroll no-padding">
                                                    <ul class="nav category" data-bind="foreach: categories">
                                                    	<li class="">
                                                            <a href="#" data-bind="click: $root.getCategory">
                                                                <img class="img-circle" data-bind="attr: { src: img_src, alt: category_name }" class="img-boxed" width="80" height="80" />
                                                                <span data-bind="text: category_name"></span>
                                                                <span class="pull-right text-success">&darr;</span>
                                                            </a>
                                                         </li>
                                                        <ul class="sub-nav treeview-menu" data-bind="foreach: sub_categories">
                                                            <li class="treeview">
                                                                <a href="#" data-bind="click: $root.getCategory">
                                                                    <span data-bind="text: category_name"></span>
                                                                    <span data-bind="text: products_count" class="badge bg-green pull-right"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </ul>
                                                    <!-- <p data-bind="text: ko.toJSON(categories)"></p> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-9">
                                            <div class="">
                                            	<div class="box-header with-border">
                                            		<div class="">
                                            			<h4 data-bind="visible: !no_category()" class="pull-left">
                                            				<span data-bind="text: category().category_name"></span>
                                            				<div class="btn-group">
                                                                <a data-bind="click: $root.showAddProduct" href="#" class="btn btn-default btn-flat btn-sm">
                                                                    <i class="fa fa-plus"></i> add item
                                                                </a>
                					                            <a href="#" class="btn btn-default btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                					                              <span class="caret"></span>
                					                            </a>
                					                            <ul class="dropdown-menu" style="margin:0;">
                					                              <li data-target="#editcategory" data-toggle="modal"><a href="#"><span class="text-primary"><i class="fa fa-pencil"></i> Edit</span></a></li>
                					                              <li data-bind="visible : category().removable()" data-target="#deletecategory" data-toggle="modal"><a href="#" class="text-danger"><span class="text-danger"><i class="fa fa-trash-o"></i> Remove</a></span></li>
                					                              <li data-bind="visible : !category().removable()" class="disabled"><a href="#" class="text-danger"><span class="text-danger"><i class="fa fa-trash-o"></i> Remove</a></span></li>
                					                            </ul>
                					                        </div>
                                            			</h4>
                                            			<h3 data-bind="visible: no_category()" class="text-muted pull-left">Please choose a menu</h3>
                                                        <form data-bind="submit: $root.getStoreItems" class="navbar-form pull-right" role="search">
                                                            <div class="form-group">
                                                                <input data-bind="textInput: search_str" type="text" class="form-control exp-search" placeholder="Search">
                                                            </div>
                                                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                        </form>
                                            		</div>
                                            	</div>
                                                <div class="box-body">
                                                    <div class="">
                                                    	<div class="col-xs-12 col-sm-12 col-md-8 no-padding">
                                                    		<div class="box box-success">
                								                <div class="box-body pad10">
                								                  	<div class="slimscroll no-padding">
                                                                        <!-- <div data-bind="visible: category().is_parent()" style="margin: 10px 0;"><span class="badge bg-info">Sub Categories</span></div>
                                                                        <table data-bind="visible: category().is_parent(), with: category" class="table table-hover sub-category">
                                                                            <tbody data-bind="foreach: sub_categories">
                                                                                <tr data-bind="click: $root.getCategory">
                                                                                  <td style="width: 10%">
                                                                                    <img data-bind="visible: !no_image(), attr: { src: img_src, alt: category_name }" width="30" height="30" />
                                                                                    <i data-bind="visible: no_image()" class="fa fa-file-image-o fa-2x"></i>
                                                                                  </td>
                                                                                  <td>
                                                                                    <span data-bind="text: category_name"></span>
                                                                                  </td>
                                                                                  <td style="width: 30%">
                                                                                    <div class="text-right">
                                                                                        <button data-bind="click: $root.editSubCategory, clickBubble: false" type="button" class="btn btn-sm text-primary"><i class="fa fa-pencil"></i></button>
                                                                                        <button data-bind="click: removable() ? $root.confirmDelSubCategory : null, clickBubble: false, attr: { title: removable() ? null : 'Delete not allowed', disabled: !removable() }" type="button" class="btn btn-sm text-danger"><i class="fa fa-times"></i></button>
                                                                                    </div>
                                                                                  </td>
                                                                                </tr>
                                                                            </tbody>
                                                                            <tfoot data-bind="visible: sub_categories().length <= 0">
                                                                                <tr>
                                                                                    <td class="danger" colspan="4">
                                                                                        No sub menu
                                                                                    </td>
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table> -->

                                                                        <div data-bind="visible: storeitems().length >= 0" style="margin: 10px 0;"><span class="badge bg-success">Store items</span></div>
                                                                        <table class="table table-hover store-items">
                                                                            <tbody data-bind="foreach: storeitems">
                                                                                <tr data-bind="click: $root.getStoreItem">
                                                                                  <td style="width: 10%">
                                                                                    <a data-bind="visible: !no_image()" href="#" data-toggle="modal" data-target="#editstoreitemimage"><img data-bind="attr: { src: img_src, alt: storeitemname }" width="30" height="30" /></a>
                                                                                    <a data-bind="visible: no_image()" href="#" data-toggle="modal" data-target="#addstoreitemimage"><i class="fa fa-file-image-o fa-2x"></i></a>
                                                                                  </td>
                                                                                  <td style="width: 40%;"><span data-bind="text: storeitemname"></span></td>
                                                                                  <td style="width: 20%;" class="text-right"><span data-bind="text: price_formatted"></span></td>
                                                                                  <td style="width: 30%">
                                                                                    <div class="text-right">
                                                                                        <button data-bind="clickBubble: false" type="button" class="btn btn-sm text-primary" data-toggle="modal" data-target="#editstoreitem"><i class="fa fa-pencil"></i></button>
                                                                                        <button data-bind="clickBubble: false" type="button" class="btn btn-sm text-danger" data-toggle="modal" data-target="#deletestoreitem"><i class="fa fa-times"></i></button>
                                                                                    </div>
                                                                                  </td>
                                                                                </tr>
                                                                            </tbody>
                                                                            <tfoot data-bind="visible: storeitems().length <= 0">
                                                                                <tr>
                                                                                    <td class="danger" colspan="4">
                                                                                        No menu items yet
                                                                                    </td>
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                								                </div>
                								                <!-- /.box-body -->
                								            </div>
                                                    	</div>

                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-xs-4">
                                                            <div class="">
                                                                <div data-bind="visible: !no_category(), with: category" class="">
                                                                    <div class="box box-solid box-success">
                                                                        <div class="box-body">
                                                                            <img data-bind="attr: { src: img_src, alt: category_name }" class="img-boxed" style="width: 100%;" />
                                                                            <h4>
                                                                                <span data-bind="text: category_name"></span>
                                                                            </h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-bind="visible: !no_storeitem(), with: storeitem">
                                                                    <div class="box box-solid box-success">
                                                                        <div class="box-body">
                                                                            <h4 data-bind="text: storeitemname" class="text-left"></h4>
                                                                            <table style="width: 100%;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td rowspan="2" style="vertical-align: center;">
                                                                                            <img data-bind="attr: { src: img_src, alt: storeitemname }" class="img-boxed" style="width: 80%;" />
                                                                                        </td>
                                                                                        <td class="text-right"><span data-bind="text: sku" class="barcode"></span></td>
                                                                                    </tr>
                                                                                    <tr><td class="text-right"><span data-bind="text: price_formatted"></span></td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="box box-success box-solid">
                                                                <div class="box-header">
                                                                    Existing unit measures
                                                                </div>
                                                                <div class="box-body">
                                                                    <div data-bind="foreach: unitmeasures">
                                                                        <span class="btn btn-xs">
                                                                            <span data-bind="text: measure_name"></span>&nbsp;
                                                                            <a data-bind="click: $root.showEditUnitMeasure" href="#"><i class="fa fa-pencil"></i></a>
                                                                        </span>&nbsp;
                                                                    </div>
                                                                </div>
                                                                <div class="box-footer">
                                                                    <div class="pull-right">
                                                                        <a data-bind="click: $root.showAddUnitMeasure" href="#" class="btn btn-default btn-sm"><i class="fa fa-toggle-on"></i> New Unit</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div> <!-- col-xs-12 -->
                    </div><!-- App end --> 

                </section>  <!-- content -->
            </div><!-- content-wrapper -->

        <!-- Sub categories pane start -->
            <div data-bind="with: edit_sub_category" class="modal fade modal-edit" id="editsubcategory" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit <span data-bind="text: category_name"></span></h4>
                        </div>
                        <div class="modal-body">
                            <form action="<?php echo base_url('dailymenu/index'); ?>" enctype="multipart/form-data" method="post" class="form-horizontal">
                            	<input data-bind="value: category_id" type="hidden" name="category_id" />
                                <input data-bind="value: image_id" type="hidden" name="image_id" />
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input data-bind="textInput: category_name" type="text" name="category_name" class="form-control" placeholder="Category Name" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-sm-12">
                                        <p>
                                            <img data-bind="attr: { src: img_src }" width="80">
                                        </p>
                                        <!-- add image -->
                                        <label for="image_upload"  data-bind="visible: no_image()">
                                            <i class="fa fa-upload" style="margin-right:5px;"></i><span >Add Image</span>
                                        </label>
                                        <!-- change image -->
                                        <label for="image_upload"  data-bind="visible: !no_image()">
                                            <i class="fa fa-times" style="margin-right:5px;"></i><span >Change Image</span>
                                        </label>
                                        <input type="file" name="userfile" id="image_upload" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" name="process" value="update_category" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: delete_sub_category" class="modal fade modal-delete" id="deletesubcategory" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
            	<div class="modal-dialog modal-sm" role="document">
            		<div class="modal-content text-center">
            			<div class="modal-header">
            				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            				<h4>Remove <span data-bind="text: category_name"></span> ? </h4>
            			</div>
            			<div class="modal-body">
			              	<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                            <button data-bind="click: $root.deleteSub" type="button" class="btn btn-danger btn-sm"><i class="fa fa-check"></i> Delete</button>
            			</div>
            		</div>
            	</div>
            </div><!-- Sub categories pane end -->

        <!-- Categories pane start -->
            <div data-bind="with: category" class="modal fade modal-add" id="addcategory" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">New Menu</h4>
                        </div>
                        <div class="modal-body">
                            <form action="<?php echo base_url('dailymenu/index'); ?>" enctype="multipart/form-data" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="category_name" placeholder="Menu Name" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-sm-12">
                                        <label for="img_upload">
                                            <i class="fa fa-upload" style="margin-right:5px;"></i><span >Add Image</span>
                                        </label>
                                		<input type="file" name="userfile" id="img_upload" class="form-control" />
                                	</div>
                                </div>
                                <div class="form-group">
                                	<div class="col-sm-12">
                                		<label>
                                			<input data-bind="checked: nested" type="checkbox" name="nested" />
                                			Nested
                                		</label>
                                	</div>
                                </div>
                                <div data-bind="visible: nested" class="form-group">
                                    <div class="col-sm-12">
                                        <select data-bind="attr: { required: nested() }, options: $root.parent_categories, optionsText: 'category_name', optionsValue: 'category_id', value: parent_id, optionsCaption: 'Choose parent menu ...'" name="parent_id"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" name="process" value="add_category" class="btn btn-success pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: category" class="modal fade modal-edit" id="editcategory" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit <span data-bind="text: category_name"></span></h4>
                        </div>
                        <div class="modal-body">
                            <form action="<?php echo base_url('dailymenu/index'); ?>" enctype="multipart/form-data" method="post" class="form-horizontal">
                            	<input data-bind="value: category_id" type="hidden" name="category_id" />
                            	<input data-bind="value: image_id" type="hidden" name="image_id" />
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input data-bind="textInput: category_name" type="text" name="category_name" class="form-control" placeholder="Category Name" />
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-sm-12">
                                        <p>
                                            <img data-bind="attr: { src: img_src }" width="80">
                                        </p>
                                        <!-- add image -->
                                        <label for="image_upload"  data-bind="visible: no_image()">
                                            <i class="fa fa-upload" style="margin-right:5px;"></i><span >Add Image</span>
                                        </label>
                                        <!-- change image -->
                                        <label for="image_upload"  data-bind="visible: !no_image()">
                                            <i class="fa fa-times" style="margin-right:5px;"></i><span >Change Image</span>
                                        </label>
                                        <!-- hidden input type -->
                                        <input type="file" name="userfile" id="image_upload" class="form-control" />
                                	</div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" name="process" value="update_category" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: category" class="modal fade modal-delete" id="deletecategory" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
            	<div class="modal-dialog modal-sm" role="document">
            		<div class="modal-content text-center">
            			<div class="modal-header">
            				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            				<h4>Remove <span data-bind="text: category_name"></span> ? </h4>
            			</div>
            			<div class="modal-body">
			              	<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                            <button data-bind="click: $root.deleteCategory" type="button" class="btn btn-danger btn-sm"><i class="fa fa-check"></i> Delete</button>
                        </div>
            		</div>
            	</div>
            </div><!-- Categories pane end -->

        <!-- Unit measures pane start -->
            <div data-bind="with: unitmeasure" class="modal fade modal-add" id="addunitmeasure" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">New Unit Measure</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind="submit: $root.addUnitMeasure" action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input data-bind="textInput: measure_name" type="text" class="form-control" placeholder="Unit Measure" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-success pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: unitmeasure" class="modal fade modal-edit" id="editunitmeasure" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit Unit Measure</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind="submit: $root.editUnitMeasure" action="#" method="post" class="form-horizontal">
                                <input data-bind="value: unit_id" type="hidden" />
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input data-bind="textInput: measure_name" type="text" class="form-control" placeholder="Unit Measure" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- Unit measures pane end -->

        <!-- Store Items pane start -->
            <div data-bind="with: storeitem" class="modal fade modal-delete" id="deletestoreitem" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content text-center">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4>Remove <span data-bind="text: storeitemname"></span> ? </h4>
                        </div>
                        <div class="modal-body">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                            <button data-bind="click: $root.deleteStoreItem" type="button" class="btn btn-danger btn-sm"><i class="fa fa-check"></i> Delete</button>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: storeitem" class="modal fade modal-edit" id="editstoreitem" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit <span data-bind="text: storeitemname"></span></h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind="submit: $root.editStoreItem" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>Price per <span data-bind="text: measure_name"></span> (in peso)</label>
                                        <input data-bind="textInput: price_per_unit" type="text" class="form-control" placeholder="e.g. 20" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button data-bind="click: $root.cancelEditStoreItem" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: storeitem" class="modal fade modal-edit" id="addstoreitemimage" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Add image for Item #<span data-bind="text: store_item_id"></span></h4>
                        </div>
                        <div class="modal-body">
                            <form action="<?php echo base_url('dailymenu/add_storeitem_image'); ?>" enctype="multipart/form-data" method="post" class="form-horizontal">
                                <input data-bind="value: store_item_id" type="hidden" name="store_item_id" />
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span data-bind="text: storeitemname"></span><br />
                                        <span data-bind="text: sku"></span><br />
                                        <img data-bind="attr: { src: img_src, alt: storeitemname }" width="100" height="100" /><br />
                                        <!-- add image -->
                                        <label for="storeitem_image">
                                            <i class="fa fa-upload" style="margin-right:5px;"></i><span >Add Image</span>
                                        </label>
                                        <input type="file" name="userfile" id="storeitem_image" class="form-control" required="" accept="image/*" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: storeitem" class="modal fade modal-edit" id="editstoreitemimage" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit image for Item #<span data-bind="text: store_item_id"></span></h4>
                        </div>
                        <div class="modal-body">
                            <form action="<?php echo base_url('dailymenu/edit_storeitem_image'); ?>" enctype="multipart/form-data" method="post" class="form-horizontal">
                                <input data-bind="value: store_item_id" type="hidden" name="store_item_id" />
                                <input data-bind="value: image_id" type="hidden" name="image_id" />
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span data-bind="text: storeitemname"></span><br />
                                        <span data-bind="text: sku"></span><br />
                                        <img data-bind="attr: { src: img_src, alt: storeitemname }" width="100" height="100" /><br />
                                        <!-- change image -->
                                        <label for="storeitem_image">
                                            <i class="fa fa-pencil" style="margin-right:5px;"></i><span >Change Image</span>&emsp;
                                            <a href="#" class="text-danger" data-toggle="modal" data-target="#confirmstoreitemimageremove"><i class="fa fa-times"></i> remove</a>
                                        </label>
                                        <input type="file" name="userfile" id="storeitem_image" class="form-control" required="" accept="image/*" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: storeitem" class="modal fade modal-delete" id="confirmstoreitemimageremove" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Item # <span data-bind="text: store_item_id"></span></h4>
                        </div>
                        <div class="modal-body text-center">
                            <img data-bind="attr: { src: img_src, alt: storeitemname }" style="width: 50%;" /><br /><br />
                            <p>
                                Confirm to remove image of 
                                <span data-bind="text: storeitemname"></span>?
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                            <button data-bind="click: $root.removeStoreItemImage" type="button" class="btn btn-danger pull-right">Yes</button>
                        </div>
                    </div>
                </div>
            </div><!-- Store Items pane end -->

        <!-- Menu Item pane start -->
            <div data-bind="with: menuitem" class="modal fade modal-edit" id="editmenuitem" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit <span data-bind="text: storeitemname"></span></h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind="submit: $root.editMenuItem" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>Today's Number of <span data-bind="text: measure_name"></span></label>
                                        <input data-bind="textInput: stock_at_hand" type="number" class="form-control" placeholder="e.g. 20" min="0" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button data-bind="click: $root.cancelEditMenuItem" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-bind="with: storeitem" class="modal fade modal-delete" id="confirmresetdailymenu" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Confirm Reset Daily Menu</span></h4>
                        </div>
                        <div class="modal-body">
                            Are you sure to reset daily menu counts?
                            <p class="text-muted">All current daily menu counts will be set to zero (0) if confirm.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                            <button data-bind="click: $root.resetDailyMenu" type="button" class="btn btn-danger pull-right">Yes</button>
                        </div>
                    </div>
                </div>
            </div><!-- Menu Item pane end -->

            <div class="modal fade modal-delete" id="form_error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h4>Please check your submission.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <script id="customMessageTemplate" type="text/html">
                <em class="text-warning" data-bind='validationMessage: field'></em>
            </script>
