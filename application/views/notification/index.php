<div id="dashboard" class="content-wrapper">
    <section class="content page">
        <div class="row">
            <div class="col-md-12">
                <h3 class="box-title"><center>Notifications</center></h3>
                <div class="box box-success">
                    <div class="box-body slimscroll" style="height:490px;">
                        <h4 data-bind="visible: customerlogs().length > 0"><i>Customer logs</i></h4><hr/>
                        <div data-bind="foreach: customerlogs">
                            <div class="alert alert-success">
                                <a style="text-decoration:none;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span data-bind="text: firstname"></span> <span data-bind="text: lastname"></span> was <b><span data-bind="text: remarks"></span></b> by <span data-bind="text: display_name"></span> on <span data-bind="text: date_created"></span>
                            </div>
                        </div>

                        <br/>
                        <h4 data-bind="visible: userlogs().length > 0"><i>User logs</i></h4><hr/>
                        <div data-bind="foreach: userlogs">
                            <div class="alert alert-success">
                                <a style="text-decoration:none;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span data-bind="text: username"></span> was <b><span data-bind="text: remarks"></span></b> by <span data-bind="text: user"></span> on <span data-bind="text: date_created"></span><br/>
                            </div>
                        </div>

                        <br/>
                        <h4 data-bind="visible: inventorylogs().length > 0"><i>Inventory logs</i></h4><hr/>
                        <div data-bind="foreach: inventorylogs">
                            <div class="alert alert-success">
                                <a style="text-decoration:none;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span data-bind="text: storeitemname"></span> was <b><span data-bind="text: remarks"></span></b> by <span data-bind="text: user"></span> on <span data-bind="text: date_created"></span><br/>
                            </div>
                        </div>

                        <br/>
                        <h4 data-bind="visible: orderlogs().length > 0"><i>Order logs</i></h4><hr/>
                        <div data-bind="foreach: orderlogs">
                            <div class="alert alert-success">
                                <a style="text-decoration:none;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span data-bind="text: order_id"></span> was <b><span data-bind="text: remarks"></span></b> by <span data-bind="text: user"></span> on <span data-bind="text: date_created"></span><br/>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div> <!-- row -->
    </section> <!-- .content -->
</div><!-- /.content-wrapper -->