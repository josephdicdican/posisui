
<div class="login-box">
        <div class="login-logo">
            <a href="<?php echo base_url(); ?>">
              <img src="<?php echo base_url('resource/images/logo.png');?>" alt="R's Food Avenue Logo" />
            </a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Create New Password</p>
            <form action="<?php echo base_url('Auth/forgot_password'); ?>" method="POST" role="form">
            
                <input type="hidden" name="user_id" value="<?php echo $user['user_id']; ?>">
                    <div class="form-group has-feedback">
                    <span class="form-control-feedback" ><i class="fa fa-lock"></i></span>
                    <input type="password" size="20" id="password" name="password" class="form-control m-wrap placeholder-no-fix" placeholder="Password" value="" required="required"/>
                </div>
                <div class="form-group has-feedback">
                    <span class="form-control-feedback" ><i class="fa fa-lock"></i></span>
                    <input type="password" size="20" id="confirm_password" name="confirm_password" class="form-control m-wrap placeholder-no-fix" placeholder="Confirm Password" value="" required="required" onkeyup="checkPasswordMatch();"/>
                </div>
                <div class="registrationFormAlert" id="divCheckPasswordMatch">
                </div>
                <div class="form-group has-feedback">
                    <button type="submit" value="resetpassword" name="process" class="btn btn-success btn-block btn-flat"><i class="fa fa-sign-in"></i> Submit</button>
                </div>
                <?php if(isset($error));?>
                <?php echo($error);?>

                <?php if(isset($alert)){?>
                <div class="alert alert-danger"><?php echo($alert);?></div>
                <?php }?>
            </form>
        </div>
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->