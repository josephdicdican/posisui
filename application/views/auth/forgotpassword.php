
<div class="login-box">
        <div class="login-logo">
            <a href="<?php echo base_url(); ?>">
              <img src="<?php echo base_url('resource/images/logo.png');?>" alt="R's Food Avenue Logo" />
            </a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Forgot Password</p>
            <form action="<?php echo base_url('Auth/forgot_password'); ?>" method="POST" role="form">
                <div class="form-group has-feedback">
                    <input type="text" size="20" id="username" name="username" class="form-control" placeholder="Username" value="" required="required"/>
                    <span class="form-control-feedback"><i class="fa fa-user"></i></span>
                </div>
                <div class="form-group has-feedback">
                    <button type="submit" value="forgotpassword" name="process" class="btn btn-success btn-block btn-flat"><i class="fa fa-sign-in"></i> Submit</button>
                </div>
                <div class="form-group has-feedback">
                   <a href="<?php echo base_url('Auth/login');?>">Sign-in</a><br>
               </div>
                <?php if(isset($error)){?>
                    <div class="alert alert-danger"><?php echo($error);?></div>
                <?php }?>
                
            </form>
        </div>
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->