
<div class="login-box">
        <div class="login-logo">
            <a href="<?php echo base_url(); ?>">
              <img src="<?php echo base_url('resource/images/logo.png');?>" alt="R's Food Avenue Logo" />
            </a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
        	<p class="error_msg"><?php echo $error; ?></p>
            <p class="login-box-msg">Please login</p>
            <form action="<?php echo base_url('Auth/login'); ?>" method="POST" role="form">
                <div class="form-group has-feedback">
                    <input type="text" size="20" id="username" name="username" class="form-control" placeholder="Username" value="" required="required"/>
                    <span class="form-control-feedback"><i class="fa fa-user"></i></span>
                </div>
                <div class="form-group has-feedback">
                    <span class="form-control-feedback" ><i class="fa fa-lock"></i></span>
                    <input type="password" size="20" id="password" name="password" class="form-control m-wrap placeholder-no-fix" placeholder="Password" value="" required="required"/>
                </div>

                <div class="form-group has-feedback">
                    <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button>
                </div>
                <div class="form-group has-feedback">
                    <a href="<?php echo base_url('Auth/forgot_password');?>">I forgot my password</a><br>
                </div>
            </form>
        </div>
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->