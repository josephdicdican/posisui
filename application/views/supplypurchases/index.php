            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="header-pane col-md-12 z-depth-1">
                    <div class="title-pane col-lg-5 col-md-5 col-sm-12 col-xs-12 pull-left">
                        <h3>
                            Supply Purchases&emsp;
                            <a data-bind="click: $root.showAddSupplyPurchase" href="#" class="btn btn-success"><i class="fa fa-plus-circle"></i> New Purchase</a>
                        </h3>
                    </div>
                </section>
                <section class="content">
                    <div class="row body-content">
                        <div class="">
                            <div class="z-depth-2">
                                <div class="horizon">
                                    <!-- Supply Purchase Add start -->
                                    <div data-bind='validationOptions: { messageTemplate: "errMsgTemplate" }' id="newpurchase" class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h4>
                                                    New Supply Purchase&emsp;
                                                    <a data-bind="attr: { disabled: !supplypurchase().valid(), 'data-target': supplypurchase().valid() ? '#promptsaveconfirm':'#', title: !supplypurchase().valid() ? 'It seems somethings wrong, please check your inputs.':'' }" href="#" data-toggle="modal" data-target="#" class="btn btn-success">Save</a>
                                                    <a data-bind="click: $root.cancelAddSupplyPurchase" href="#" class="btn btn-default">Discard</a>
                                                    <a data-bind="click: $root.cancelAddSupplyPurchase" href="#" class="close">&times;</a>&emsp;
                                                    Date of Purchase: 
                                                    <label>
                                                        <input data-bind="datepicker: supplypurchase().date_of_purchase, datepickerOptions: { minDate: min_date(), dateFormat: 'mm-dd-yy' }"  type="text" class="form-control text-center" />
                                                    </label>&emsp;
                                                    <label data-bind="attr: { title: supplypurchase().confirmed() ? 'All quantities will be added in the inventory' : 'If confirmed, quantities will be added to the inventory' }" data-toggle="tooltip" data-placement="top" title="If confirmed, purchase quantities will be added to inventory">
                                                        <input data-bind="checked: supplypurchase().is_confirmed" type="checkbox" />Confirm
                                                    </label>&emsp;
                                                    <!-- <a href="#" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Option to assign supplier">assign supplier</a> -->
                                                </h4>
                                            </div>
                                            <div data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }' class="box-body">
                                                <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                                    <div class="col-md-4 side-category">
                                                        <div class="box-header with-border">
                                                            <div class="">
                                                                <h4>
                                                                    <span>Store Items</span>&emsp;&emsp;&emsp;
                                                                    <select data-bind="options: parent_categories, optionsText:'category_name', optionsCaption:'Select Category', value: parent_category" class="form-control"></select>
                                                                    <select data-bind="options: categories, optionsText:'category_name', optionsCaption:'Select Sub Category', value: category" class="form-control"></select>
                                                                    <!-- <label>
                                                                        <input type="text" class="inline form-control input-md" />
                                                                        <a href="#"><i class="fa fa-search"></i></a>
                                                                    </label> -->
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <div class="box-body slimscroll no-padding">
                                                            <table class="table table-hover purchase">
                                                                <tbody data-bind="foreach: storeitems">
                                                                    <tr data-bind="click: $root.addPurchaseLine" role="row">
                                                                        <td><span data-bind="text: sku"></span></td>
                                                                        <td><span data-bind="text: storeitemname"></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 no-padding">
                                                        <div class="box-header">
                                                            <h4 class="text-success">
                                                                Purchase Lines 
                                                                <span class="pull-right">
                                                                    <span class="label label-success"><span data-bind="text: supplypurchase().supplypurchaselines().length"></span> items</span>
                                                                    <span class="label label-default">Php <span data-bind="text: supplypurchase().totalCost()"></span></span>
                                                                    <span class="label label-default"><span data-bind="text: supplypurchase().totalQty()"></span> qty</span>
                                                                </span>
                                                            </h4>
                                                        </div>
                                                        <div class="slimscroll-head">
                                                            <table class="table"style="margin-bottom:0;">
                                                                <thead>
                                                                    <tr role="row">
                                                                        <th style="width: 20%">Item</th>
                                                                        <th style="width: 20%" data-toggle="tooltip" data-placement="top" title="Purchasing price / unit">Price <small>(per unit)</small></th>
                                                                        <th class="" style="width: 20%">Quantity</th>
                                                                        <th style="width: 10%">Subtotal</th>
                                                                        <th style="width: 10%" class="text-center">Action</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        <div class="slimscroll">
                                                            <table class="table table-hover">
                                                                <tbody data-bind="foreach: supplypurchase().supplypurchaselines">
                                                                    <tr role="row">
                                                                        <td style="width: 20%"><span data-bind="text: storeitemname"></span></td>
                                                                        <td style="width: 20%">
                                                                            <input data-bind="textInput: purchase_price_per_unit" style="width:60px; text-align:center;" type="number" min="0" />
                                                                        </td>
                                                                        <td style="width: 20%" class="">
                                                                            <input data-bind="textInput: purchase_qty" style="width:60px; text-align:center;" type="number" min="1" />
                                                                        </td>
                                                                        <td style="width: 10%"><span data-bind="text: subtotal"></span></td>
                                                                        <td style="width: 10%" class="text-center">
                                                                            <a data-bind="click: $root.removePurchaseLine" href="#" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!-- <pre data-bind="text: ko.toJSON(supplypurchase)"></pre>  -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Supply Purchase Add end -->
                                    <!-- Supply Purchase Edit start -->
                                    <div data-bind='validationOptions: { messageTemplate: "errMsgTemplate" }' id="editpurchase" class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h4>
                                                    Edit Supply Purchase # <span data-bind="text: supplypurchase().supply_purchase_id"></span>&emsp;
                                                    <a data-bind="attr: { disabled: !supplypurchase().valid(), 'data-target': supplypurchase().valid() ? '#prompteditconfirm':'#', title: !supplypurchase().valid() ? 'It seems somethings wrong, please check your inputs.':'' }" href="#" class="btn btn-success" data-toggle="modal" data-target="#">Save</a>
                                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#promptcanceledit">Discard</a>
                                                    <a href="#" class="close">&times;</a>&emsp;
                                                    Date of Purchase: 
                                                    <label><span data-bind="text: supplypurchase().format_date(supplypurchase().date_of_purchase())"></span>
                                                        <!-- <input data-bind="datepicker: supplypurchase().date_of_purchase, datepickerOptions: { minDate: min_date(), dateFormat: 'mm-dd-yy' }"  type="text" class="form-control text-center" /> -->
                                                    </label>&emsp;
                                                    <label data-bind="attr: { title: supplypurchase().confirmed() ? 'All quantities will be added in the inventory' : 'If confirmed, quantities will be added to the inventory' }" data-toggle="tooltip" data-placement="top" title="If confirmed, purchase quantities will be added to inventory">
                                                        <input data-bind="checked: supplypurchase().is_confirmed" type="checkbox" />
                                                        Confirm
                                                    </label>&emsp;
                                                    <a href="#" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Option to assign supplier">assign supplier</a>
                                                </h4>
                                            </div>
                                            <div data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }' class="box-body">
                                                <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                                    <div class="col-md-4 side-category">
                                                        <div class="box-header with-border">
                                                            <div class="">
                                                                <h4>
                                                                    <span>Store Items</span>&emsp;&emsp;&emsp;
                                                                    <label>
                                                                        <input type="text" class="inline form-control input-md" />
                                                                        <a href="#"><i class="fa fa-search"></i></a>
                                                                    </label>
                                                                </h4>
                                                                <div>
                                                                    <div class="col-xs-6 no-padding">
                                                                        <select data-bind="options: parent_categories, optionsText:'category_name', optionsCaption:'Select Category', value: parent_category"></select>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <select data-bind="options: categories, optionsText:'category_name', optionsCaption:'Select Sub Category', value: category"></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="box-body slimscroll no-padding">
                                                            <table class="table table-hover purchase">
                                                                <tbody data-bind="foreach: storeitems">
                                                                    <tr data-bind="click: $root.addPurchaseLineEdit" role="row">
                                                                        <td><span data-bind="text: sku"></span></td>
                                                                        <td><span data-bind="text: storeitemname"></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 no-padding">
                                                        <div class="box-header">
                                                            <h3 class="text-success">
                                                                Purchase Lines 
                                                                <span class="pull-right">
                                                                    <span class="label label-success"><span data-bind="text: supplypurchase().supplypurchaselines().length"></span> items</span>
                                                                    <span class="label label-default">Php <span data-bind="text: supplypurchase().totalCost()"></span></span>
                                                                    <span class="label label-default"><span data-bind="text: supplypurchase().totalQty()"></span> qty</span>
                                                                </span>
                                                            </h3>
                                                        </div>
                                                        <div class="slimscroll-head">
                                                            <table class="table"style="margin-bottom:0;">
                                                                <thead>
                                                                    <tr role="row">
                                                                        <th style="width: 20%">Item</th>
                                                                        <th style="width: 20%" data-toggle="tooltip" data-placement="top" title="Purchasing price / unit">Price <small>(per unit)</small></th>
                                                                        <th style="width: 20%">Quantity</th>
                                                                        <th style="width: 10%">Subtotal</th>
                                                                        <th style="width: 10%" class="text-center">Action</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        <div class="slimscroll">
                                                            <table class="table table-hover">
                                                                <tbody data-bind="foreach: supplypurchase().supplypurchaselines">
                                                                    <tr data-bind="visible: !removed()" role="row">
                                                                        <td style="width: 20%"><span data-bind="text: storeitemname"></span></td>
                                                                        <td style="width: 20%"><input data-bind="textInput: purchase_price_per_unit" style="width:60px; text-align:center;" type="number" min="0" /></td>
                                                                        <td style="width: 20%">
                                                                            <input data-bind="textInput: purchase_qty" style="width:60px; text-align:center;" type="number" min="1" />
                                                                        </td>
                                                                        <td style="width: 10%"><span data-bind="text: subtotal"></span></td>
                                                                        <td style="width: 10%" class="text-center">
                                                                            <a data-bind="click: $root.removePurchaseLineEdit" href="#" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!-- <pre data-bind="text: ko.toJSON(supplypurchase)"></pre> --> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Supply Purchase Edit end -->
                                    <!-- Main Panel Supply Purchases start -->
                                    <div id="purchases" class="form-inline dt-bootstrap">
                                        <div class="col-md-12 col-sm-12 col-xs-12 no-padding" id="origin">
                                            <!-- Supply Purchases List Pane start -->
                                            <div class="col-md-4 side-category">
                                                <div class="box-header with-border">
                                                    <div class="">
                                                        <h4 class="pull-left">
                                                            <span>Purchases</span>
                                                            <select data-bind="options: filters, optionsText: 'node', value: selectedFilter"></select>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="slimscroll-head">
                                                    <table class="table" style="margin-bottom:0;">
                                                        <thead>
                                                            <tr role="row">
                                                                <th style="width: 50%">Date</th>
                                                                <th style="width: 30%" class="text-center">Purchase Qty</th>
                                                                <th class="text-center" style="width: 20%">Status</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div class="box-body slimscroll no-padding">
                                                    <table class="table table-hover purchase">
                                                        <tbody data-bind="foreach: supplypurchases">
                                                            <tr data-bind="click: $root.getSupplyPurchase" role="row">
                                                                <td style="width: 50%"><span data-bind="text: format_date(date_created())"></span></td>
                                                                <td style="width: 30%" class="text-center"><span data-bind="text: total_qty()"></span></td>
                                                                <td style="width: 20%" class="text-center">
                                                                    <button data-bind="visible: confirmed()" type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Added Purchase"><i class="fa fa-check"></i></button>
                                                                    <button data-bind="visible: !confirmed()" type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Onhold Purchase"><i class="fa fa-minus"></i></button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- Supply Purchases List Pane end -->

                                            <!-- Supply Purchases Line Pane start -->
                                            <div class="col-md-8 no-padding">
                                                <div class="box-header">
                                                    <h4 class="text-success">
                                                        <span data-bind="visible: no_supplypurchase()">Click a supply purchase</span>
                                                        <span data-bind="visible: !no_supplypurchase()">
                                                            Supply Purchase #<span data-bind="text: supplypurchase().supply_purchase_id"></span>&nbsp;
                                                            <span data-bind="visible: supplypurchase().confirmed()" class="badge bg-green"><i class="fa fa-check"></i> confirmed</span>
                                                            <span data-bind="visible: !supplypurchase().confirmed()">
                                                                <span class="badge bg-red"><i class="fa fa-times"></i> not confirm</span>&nbsp;&nbsp;
                                                                <a data-bind="click: $root.showEditSupplyPurchase" href="#" class="btn btn-primary"><i class="fa fa-pencil"></i> edit</a>
                                                                <a data-bind="click: $root.showAuthSupplyPurchaseDelete" href="#" class="btn btn-danger"><i class="fa fa-times"></i> remove</a>
                                                            </span>
                                                        </span> 
                                                        <span class="pull-right">
                                                            <span class="label label-success"><span data-bind="text: supplypurchase().supplypurchaselines().length"></span> items</span>
                                                            <span class="label label-default">Php <span data-bind="text: supplypurchase().totalCost()"></span></span>
                                                            <span class="label label-default"><span data-bind="text: supplypurchase().totalQty()"></span> qty</span>
                                                        </span>
                                                    </h4>
                                                </div>
                                                <div class="slimscroll-head">
                                                    <table class="table"style="margin-bottom:0;">
                                                        <thead>
                                                            <tr role="row">
                                                                <th style="width: 20%">Item</th>
                                                                <th style="width: 20%" data-toggle="tooltip" data-placement="top" title="Purchasing price / unit">Price <small>(per unit)</small></th>
                                                                <th class="text-center" style="width: 20%">Quantity</th>
                                                                <th style="width: 20%">Subtotal</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div class="slimscroll">
                                                    <table class="table table-hover">
                                                        <tbody data-bind="foreach: supplypurchaselines">
                                                            <tr role="row">
                                                                <td style="width: 20%"><span data-bind="text: storeitemname"></span></td>
                                                                <td style="width: 20%"><span data-bind="text: formatted_purchase_price_per_unit"></span></td>
                                                                <td style="width: 20%" class="text-center"><span data-bind="text: purchase_qty"></span></td>
                                                                <td style="width: 20%"><span data-bind="text: subtotal"></span></td>
                                                            </tr>
                                                            <!-- <tr role="row">
                                                                <td style="width: 50%">Coke 1 Litre</td>
                                                                <td style="width: 30%">P25.00 / unit</td>
                                                                <td style="width: 20%" class="text-center"><input class="form-control" style="height:20px; width:70px; text-align:center;" name="quantity[]" type="text" value="1"></td>
                                                            </tr> -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- Supply Purchases Line Pane end -->
                                        </div>
                                    </div>
                                    <!-- Main Panel Supply Purchases end -->
                                </div><!-- /.box-body -->
                            </div>
                        <div> <!-- col-xs-12 -->
                    </div> <!-- row -->
                </section>  <!-- content -->
            </div><!-- content-wrapper -->

            <div class="modal fade modal-delete" id="form_error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h4>Please check your submission.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-delete" id="promptcanceladd" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Confirm to Discard</h4>
                        </div>
                        <div class="modal-body">
                            <p><b>This action will erase all the purchases you just entered.</b> You can click save for later revisions or confirm to discard entry.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default pull-left" data-dismiss="modal" aria-label="Close">Cancel</a>
                            <a data-bind="click: $root.getSupplyPurchases" href="#" class="btn btn-danger">Confirm</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade modal-delete" id="promptsaveconfirm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Confirm to Save</h4>
                        </div>
                        <div class="modal-body">
                            <p data-bind="visible: supplypurchase().valid()">Click <b>save</b> to conclude supply purchase entries or cancel to continue.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default pull-left" data-dismiss="modal" aria-label="Close">Cancel</a>
                            <a data-bind="click: supplypurchase().valid() ? $root.addSupplyPurchase : null, attr: { disabled: !supplypurchase().valid() }" href="#" class="btn btn-danger">Save</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-delete" id="prompteditconfirm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Confirm to Save</h4>
                        </div>
                        <div class="modal-body">
                            <p data-bind="visible: supplypurchase().valid()">Click <b>save</b> to conclude supply purchase updates or cancel to continue</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default pull-left" data-dismiss="modal" aria-label="Close">Cancel</a>
                            <a data-bind="click: supplypurchase().valid() ? $root.editSupplyPurchase : null, attr: { disabled: !supplypurchase().valid() }" href="#" class="btn btn-danger">Save</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade modal-delete" id="promptcanceledit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Confirm to Discard</h4>
                        </div>
                        <div class="modal-body">
                            <p><b>This action will erase all the updates you just entered.</b> You can click save for later revisions or confirm to discard entry.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default pull-left" data-dismiss="modal" aria-label="Close">Cancel</a>
                            <a data-bind="click: $root.getSupplyPurchases" href="#" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Confirm</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-delete" id="authsupplypurchasedelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div data-bind='validationOptions: { messageTemplate: "errMsgTemplate" }' class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Admin authentication</h4>
                        </div>
                        <div class="modal-body">
                            <p>Attempt to delete Supply Purchase #<span data-bind="text: supplypurchase().supply_purchase_id"></span>.</p>
                            <p class="text-muted">Please provide your password to delete supply purchase.</p>
                            <input data-bind="textInput: auth_supplypurchase_del().password" type="password" class="form-control" />
                            <p id="sp_del_note"></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default pull-left" data-dismiss="modal" aria-label="Close">Cancel</a>
                            <a data-bind="click: $root.deleteSupplyPurchase" href="#" class="btn btn-danger">Confirm</a>
                        </div>
                    </div>
                </div>
            </div>

            <script id="customMessageTemplate" type="text/html">
                <br>
                <em class="text-warning" data-bind='validationMessage: field'></em>
            </script>
            <script id="errMsgTemplate" type="text/html">
                <em class="text-warning" data-bind='validationMessage: field'></em>
            </script>
