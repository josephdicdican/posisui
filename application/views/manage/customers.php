<!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="header-pane col-md-12 z-depth-1">
                <div class="title-pane col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-left">
                    <h3><a data-bind="click: $root.getCustomers" href="#" data-toggle="tooltip" data-placement="bottom" title="Click to refresh">Customers</a></h3>
                </div>
                <div class="quick-nav col-lg-8 col-md-8 col-sm-10 col-xs-10 pull-left">
                    <form data-bind="submit: $root.getCustomers" class="navbar-form" role="search">
                        <button data-bind="click: $root.showAdd" type="button" class="btn btn-success pull-left" data-toggle="modal" data-target="#addcustomer" ><i class="fa fa-5px fa-plus-circle"></i> Add Customer</button>
                        <div class="form-group pull-left">
                            <input data-bind="textInput: search_str" autocomplete="off" type="text" class="form-control exp-search" placeholder="Search" />
                        </div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        <span data-bind="visible: search_str()" class="search_result">Search Result for <span data-bind="text: search_str" class="text-info"></span> </span>
                        <!-- <span data-bind="text: sort_column"></span> <span data-bind="text: sort_by"></span> -->
                    </form>
                </div>
                <div class="icon_gen">
                    <a href="#details" class="btn btn-default btn-flat show_info selected" data-original-title="Show Details" data-toggle="tooltip" data-placement="left" title="Show Details"><i class="fa fa-info-circle"></i></a>
                </div>
            </section>
            <section class="content">
                <div class="row body-content">
                    <div class="">
                        <div class="z-depth-2">
                            <div class="no-padding horizon">
                                <div class="form-inline dt-bootstrap">
                                    <div class="col-md-8 col-sm-12 col-xs-12 detailed-col" id="origin">
                                        <div class="slimscroll-head">
                                            <table class="table" style="margin-bottom:0;">
                                                <thead>
                                                    <tr role="row">
                                                        <th data-bind="click: $root.sortResult.bind($data, 'fullname')" style="width: 25%;">
                                                            <span data-toggle="tooltip" data-placement="top" title="Sort by customer name">Customer Name</span>
                                                            <span class="pull-right">
                                                                <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'fullname'" href="#"><i class="fa fa-angle-up"></i></a>
                                                                <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'fullname'" href="#"><i class="fa fa-angle-down"></i></a>
                                                            </span>
                                                        </th>
                                                        <th data-bind="click: $root.sortResult.bind($data, 'email')" style="width: 30%;">
                                                            <span data-toggle="tooltip" data-placement="top" title="Sort by email">Email</span>
                                                            <span class="pull-right">
                                                                <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'email'" href="#"><i class="fa fa-angle-up"></i></a>
                                                                <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'email'" href="#"><i class="fa fa-angle-down"></i></a>
                                                            </span>
                                                        </th>
                                                        <th style="width: 20%;" title="Senior Citizen">SC</th>
                                                        <th class="text-center" style="width: 20%;">Action</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="slimscroll">
                                            <table class="table table-hover">
                                                <tbody data-bind="foreach: customers">
                                                    <tr data-bind="click: $root.getCustomer" role="row" class="odd show_info">
                                                        <td style="width: 25%;" data-bind="text: fullname"></td>
                                                        <td style="width: 30%;" data-bind="text: email"></td>
                                                        <td style="width: 20%;">
                                                            <span data-bind="visible: isSenior()"><i class="fa fa-check"></i></span>
                                                            <span data-bind="visible: !isSenior()"><i class="fa fa-minus"></i></span>
                                                        </td>
                                                        <td class="text-center" style="width: 20%;">
                                                            <button data-bind="click: $root.showEdit, clickBubble: false" type="button" class="btn btn-sm btn-primary bg-success" style="border:none;" id="edit" data-toggle="modal" data-target="#editcustomer"><i class="fa fa-edit"></i></button>
                                                            <button type="button" class="tip btn btn-sm btn-danger bg-danger" style="border:none;" id="delete" data-toggle="modal" data-target="#deletecustomer"><i class="fa fa-trash-o"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div data-bind="visible: customers().length == 0" class="pad10">
                                                <div class="alert alert-danger">No customers to display.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12" id="details">
                                        <h4>
                                            <p class="pull-left">Customer Info</p>
                                            <button type="button" class="close pull-right" data-dismiss="alert">&times;</button>
                                        </h4>
                                        <div class="info pull-left">
                                            <ul class="nav nav-tabs nav-justified">
                                              <li class="active"><a data-toggle="tab" href="#detail">Details</a></li>
                                              <li><a data-toggle="tab" href="#act">Activity</a></li>
                                            </ul>
                                            <div data-bind="visible: !no_customer()" class="tab-content">
                                                <div data-bind="with: customer" id="detail" class="tab-pane fade in active">
                                                    <br />
                                                    <ul class="li-list">
                                                      <li class="li-item">
                                                        <div class="li-left">Name</div>
                                                        <div class="li-body"><span data-bind="text: fullname"></span> </div>
                                                      </li>
                                                      <li class="li-item">
                                                        <div class="li-left">Email</div>
                                                        <div class="li-body"><span data-bind="text: email"></span><br /></div>
                                                      </li>
                                                      <li class="li-item">
                                                        <div class="li-left">Address</div>
                                                        <div class="li-body"><span data-bind="text: address"></span><br /></div>
                                                      </li>
                                                      <li class="li-item">
                                                        <div class="li-left">SC</div>
                                                        <div class="li-body">
                                                            <p data-bind="visible: isSenior()">
                                                                <i class="fa fa-check"></i> <span data-bind="text: card_no"></span>
                                                            </p>
                                                            <p data-bind="visible: !isSenior()">
                                                                <i class="fa fa-minus"></i>
                                                            </p>
                                                        </div>
                                                      </li>
                                                    </ul>
                                                    <hr />
                                                </div>
                                                <div id="act" class="tab-pane fade">
                                                    <br />
                                                    <ul class="li-list pad10">
                                                        <li data-bind="foreach: logs" class="li-item">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <span data-bind="text: remarks"></span> by 
                                                                    <b><span data-bind="text: display_name"></span></b> on
                                                                    <!-- <span data-bind="text: date_created"></span> -->
                                                                    <span data-bind="text: format_date()"></span>
                                                                </div>
                                                            </div>
                                                        </li>                                                       
                                                    </ul>
                                                    <hr />
                                                </div>
                                                <p class="text-right">
                                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editcustomer"><i class="fa fa-pencil"></i> Edit</a>
                                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deletecustomer"><i class="fa fa-times"></i> Delete</a>
                                                </p>
                                            </div>
                                            <div data-bind="visible: no_customer()" class="tab-content pad10">
                                                <p class="alert alert-info">Choose one record to display info</p>
                                            </div>
                                        </div>
                                    </div> <!-- col-md-4 details-->
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    <div> <!-- col-xs-12 -->
                </div> <!-- row -->
            </section>  <!-- content -->
        </div> <!-- content-wrapper -->

        <!-- Edit Customer Modal -->
            <div data-bind="with: customer" class="modal fade modal-edit" id="editcustomer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit <span data-bind="text: fullname"></span> Info</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind= 'validationOptions: { messageTemplate: "customMessageTemplate" }, submit: $root.editCustomer' action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 ">First Name:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: firstname" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Last Name:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: lastname" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3">Address:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: address" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3">Email</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: email" type="email" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Gender</label>
                                    <div class="col-sm-9">
                                        <label for="r_male"><input data-bind="checked: gender" type="radio" id="r_male" name="gender" checked value="1" /> Male</label>
                                        <label for="r_female"><input data-bind="checked: gender" type="radio" id="r_female" name="gender" value="0" /> Female</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Is Senior Cetizen?</label>
                                    <div class="col-sm-9">
                                        <input data-bind="checked: is_senior" type="checkbox" />
                                        <p data-bind="visible: isSenior()">
                                            <br />
                                            <label>Please input Card No</label>
                                            <input data-bind="textInput: card_no, attr: { required: isSenior() }" class="form-control" />
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button data-bind="click: $root.getCustomers" type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- edit modal -->

            <!-- Add Customer Modal -->
            <div data-bind="with: customer" class="modal fade modal-add" id="addcustomer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="mySmallModalLabel">Add Customer</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }, submit: $root.addCustomer' action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 ">First Name:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: firstname" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Last Name:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: lastname" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3">Address:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: address" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3">Email</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: email" type="email" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Gender</label>
                                    <div class="col-sm-9">
                                        <label for="r_male"><input data-bind="checked: gender" type="radio" id="r_male" name="gender" checked value="1" /> Male</label>
                                        <label for="r_female"><input data-bind="checked: gender" type="radio" id="r_female" name="gender" value="0" /> Female</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Senior Citizen?</label>
                                    <div class="col-sm-9">
                                        <input data-bind="checked: is_senior" type="checkbox" />
                                        <p data-bind="visible: isSenior()">
                                            <br />
                                            <label>Please input Card No</label>
                                            <input data-bind="textInput: card_no, attr: { required: isSenior() }" class="form-control" />
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                                        <button type="submit" class="btn btn-success pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- add modal -->

            <!-- Delete Customer Modal -->
            <div data-bind="with: customer" class="modal fade modal-delete" id="deletecustomer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="mySmallModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body text-center">
                            <p>Are you sure to delete Customer <span data-bind="text: fullname"></span>?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                            <button data-bind="click: $root.deleteCustomer"  type="button" class="btn btn-danger">Confirm</button>
                        </div>
                    </div>
                </div>
            </div> <!-- delete customer modal -->

            <div class="modal fade modal-delete" id="form_error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h4>Please check your submission.</h4>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="modal fade modal-delete" id="customer_exist" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h4>Cannot save customer details.</h4>
                            <p>Customer already exists. Please try another one.</p>
                        </div>
                    </div>
                </div>
            </div>

            <script id="customMessageTemplate" type="text/html">
                <em class="text-warning" data-bind='validationMessage: field'></em>
            </script>