<!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="header-pane col-md-12 z-depth-1">
                <div class="title-pane col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-left">
                    <h3><a data-bind="click: $root.getSuppliers" href="#" data-toggle="tooltip" data-placement="bottom" title="Click to refresh">Suppliers</a></h3>
                </div>
                <div class="quick-nav col-lg-8 col-md-8 col-sm-10 col-xs-10 pull-left">
                    <form data-bind="submit: $root.getSuppliers" class="navbar-form" role="search">
                        <button data-bind="click: $root.showAdd" type="button" class="btn btn-success pull-left" data-toggle="modal" data-target="#addsupplier"><i class="fa fa-plus"></i> Add Supplier</button>
                        <div class="form-group pull-left">
                            <input data-bind="textInput: search_str" autocomplete="off" type="text" class="form-control exp-search" placeholder="Search" />
                        </div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        <span data-bind="visible: search_str()" class="search_result">Search Result for <span data-bind="text: search_str" class="text-info"></span> </span>
                    </form>
                </div>
                <div class="icon_gen">
                    <a href="#details" class="btn btn-default btn-flat show_info selected" data-original-title="Show Details" data-toggle="tooltip" data-placement="left" title="Show Details"><i class="fa fa-info-circle"></i></a>
                </div>
            </section>
            <section class="content">
                <div class="row body-content">
                    <div class="">
                        <div class="z-depth-2">
                            <div class="horizon">
                                <div class="form-inline dt-bootstrap">
                                    <div class="col-md-8 col-sm-12 col-xs-12 detailed-col" id="origin">
                                        <div class="slimscroll-head">
                                            <table class="table" style="margin-bottom:0;">
                                                <thead>
                                                    <tr role="row">
                                                        <th data-bind="click: $root.sortResult.bind($data, 'supplier_name')" style="width: 30%;">
                                                            <span data-toggle="tooltip" data-placement="top" title="Sort by supplier name">Supplier Name</span>
                                                            <span class="pull-right">
                                                                <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'supplier_name'" href="#"><i class="fa fa-angle-up"></i></a>
                                                                <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'supplier_name'" href="#"><i class="fa fa-angle-down"></i></a>
                                                            </span>
                                                        </th>
                                                        <th style="width: 20%;">Contact Details</th>
                                                        <th data-bind="click: $root.sortResult.bind($data, 'supplier_email')" style="width: 30%;">
                                                            <span data-toggle="tooltip" data-placement="top" title="Sort by supplier email">Contact Email</span>
                                                            <span class="pull-right">
                                                                <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'supplier_email'" href="#"><i class="fa fa-angle-up"></i></a>
                                                                <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'supplier_email'" href="#"><i class="fa fa-angle-down"></i></a>
                                                            </span>
                                                        </th>
                                                        <th class="text-center" style="width: 20%;">Action</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="slimscroll">
                                            <table class="table table-hover">
                                                <tbody data-bind="foreach: suppliers">
                                                    <tr data-bind="click: $root.getSupplier" role="row" class="odd show_info" >
                                                        <td data-bind="text: supplier_name" style="width: 30%;"></td>
                                                        <td data-bind="text: supplier_contact" style="width: 20%;"></td>
                                                        <td data-bind="text: supplier_email" style="width: 30%;"></td>
                                                        <td class="text-center" style="width: 20%;" >
                                                            <button type="button" class="btn btn-sm btn-primary bg-success" style="border:none;" id="edit" data-toggle="modal" data-target="#editsupplier"><i class="fa fa-edit"></i></button>
                                                            <button type="button" class="btn btn-sm btn-danger bg-danger" style="border:none;" data-toggle="modal" data-target="#deletesupplier"><i class="fa fa-trash-o"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div data-bind="visible: suppliers().length == 0" class="pad10">
                                                <div class="alert alert-danger">No suppliers to display.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12" id="details">
                                        <h4>
                                            <p class="pull-left">Supplier Info</p>
                                            <button type="button" class="close pull-right" data-dismiss="alert">&times;</button>
                                        </h4>

                                        <div class="info pull-left">
                                            <ul class="nav nav-tabs nav-justified">
                                              <li class="active"><a data-toggle="tab" href="#detail">Details</a></li>
                                              <li><a data-toggle="tab" href="#act">Activity</a></li>
                                            </ul>
                                            <div data-bind="visible: !no_supplier()" class="tab-content">
                                                <div data-bind="with: supplier" id="detail" class="tab-pane fade in active">
                                                    <br />
                                                    <ul class="li-list">
                                                        <li class="li-item">
                                                            <div class="li-left">Name of Store</div>
                                                            <div class="li-body" data-bind="text: supplier_name"></div>
                                                        </li>
                                                        <li class="li-item">
                                                            <div class="li-left">Contact Name</div>
                                                            <div class="li-body" data-bind="text: supplier_email"></div>
                                                        </li>
                                                        <li class="li-item">
                                                            <div class="li-left">Phone</div>
                                                            <div class="li-body" data-bind="text: supplier_contact"></div>
                                                        </li>
                                                    </ul>
                                                    <hr />
                                                </div>
                                                <div id="act" class="tab-pane fade">
                                                    <br />
                                                    <ul class="li-list pad10">
                                                        <li data-bind="foreach: logs" class="li-item">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <span data-bind="text: remarks"></span> by 
                                                                    <b><span data-bind="text: display_name"></span></b> on
                                                                    <!-- <span data-bind="text: date_created"></span> -->
                                                                    <span data-bind="text: format_date()"></span>
                                                                </div>
                                                            </div>
                                                        </li>  
                                                    </ul>
                                                    <hr />
                                                </div>
                                                <p class="text-right">
                                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editsupplier"><i class="fa fa-pencil"></i> Edit</a>
                                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deletesupplier"><i class="fa fa-times"></i> Delete</a>
                                                </p>
                                            </div>
                                            <div data-bind="visible: no_supplier()" class="tab-content pad10">
                                                 <p class="alert alert-info">Choose one record to display info</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    <div> <!-- col-xs-12 -->
                </div> <!-- row -->
            </section>  <!-- content -->
        </div> <!-- content-wrapper -->

        <!-- Confirm delete Supplier Modal -->
            <div data-bind="with: supplier" class="modal fade modal-delete" id="deletesupplier" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="mySmallModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body text-center">
                            <p>Are you sure to delete <span data-bind="text: supplier_name"></span> ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                            <button data-bind="click: $root.deleteSupplier" type="button" class="btn btn-danger">Confirm</button>
                        </div>
                    </div>
                </div>
            </div> <!-- Confirm delete modal -->

        <!-- Edit Supplier Modal -->
            <div data-bind="with: supplier" class="modal fade modal-edit" id="editsupplier" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit Supplier</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }, submit: $root.editSupplier' action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3">Supplier Name</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: supplier_name" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Phone</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: supplier_contact" type="number" min="1" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Email</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: supplier_email"  type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- edit modal -->

            <!-- Add supplier Supplier Modal -->
            <div data-bind="with: supplier" class="modal fade modal-add" id="addsupplier" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Add Supplier</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }, submit: $root.addSupplier' action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3">Supplier Name:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: supplier_name" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Phone:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: supplier_contact" type="number" min="1" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Email:</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: supplier_email" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                                        <button type="submit" class="btn btn-success pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- Add supplier modal -->

            <div class="modal fade modal-delete" id="form_error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h4>Please check your submission.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-delete" id="supplier_exist" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h4>Cannot add supplier.</h4>
                            <p>Supplier already exists. Please try another one.</p>
                        </div>
                    </div>
                </div>
            </div>

            <script id="customMessageTemplate" type="text/html">
                <em class="text-warning" data-bind='validationMessage: field'></em>
            </script>