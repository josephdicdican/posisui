<!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- <section class="content-header">
                    <h6>A
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </h6>
                </section> -->

                <section class="content page">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary info-box">
                                <div class="panel-heading bg-light-blue">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <i class="fa fa-shopping-cart fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <h3>Sales</h3>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                         </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-danger info-box">
                                <div class="panel-heading bg-red">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <i class="fa fa-barcode fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <h3>Products</h3>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-success info-box">
                                <div class="panel-heading bg-green">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <i class="fa fa-folder-open fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <h3>Inventory</h3>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-warning info-box">
                                <div class="panel-heading bg-yellow">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <i class="fa fa-bar-chart-o fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <h3>Report</h3>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div><!-- /.row --> <!-- big nav-tabs -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Monthly Report</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <div class="btn-group">
                                            <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="box box-success">
                                                <div class="box-header with-border">
                                                    <p class="text-center"><strong>Sales: October 2015</strong></p>
                                                </div><!-- /.box-header -->
                                                <div class="chart">
                                                    <!-- Sales Chart Canvas -->
                                                    <canvas id="salesChart" height="180"></canvas>

                                                </div><!-- /.chart-responsive -->
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="box box-success">
                                                <div class="box-header with-border">
                                                    <p class="text-center"><strong>To Products: October 2015</strong></p>
                                                </div><!-- /.box-header -->
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="chart-responsive">
                                                                <canvas id="pieChart" width="100%"></canvas>
                                                            </div><!-- ./chart-responsive -->
                                                        </div><!-- /.col -->
                                                        <div class="col-md-4">
                                                            <ul class="chart-legend clearfix">
                                                                <li><i class="fa fa-circle-o text-red"></i> Coca-Cola</li>
                                                                <li><i class="fa fa-circle-o text-green"></i> Sparkle</li>
                                                                <li><i class="fa fa-circle-o text-yellow"></i> Royal</li>
                                                                <li><i class="fa fa-circle-o text-aqua"></i> C2 Solo</li>
                                                                <li><i class="fa fa-circle-o text-light-blue"></i> Nestea</li>
                                                                <li><i class="fa fa-circle-o text-gray"></i> RC-Cola</li>
                                                            </ul>
                                                        </div><!-- /.col -->
                                                    </div><!-- /.row -->
                                                    <div class="box-footer no-padding">
                                                        <ul class="nav nav-pills nav-stacked">
                                                            <li><a href="#">Coca-Cola <span class="pull-right text-red"> <i class="fa fa-angle-down"></i> 12%</span></a></li>
                                                            <li><a href="#">Sparkle <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a></li>
                                                            <li><a href="#">Royal <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                                                            <li><a href="#">C2 Solo <span class="pull-right text-red"> <i class="fa fa-angle-down"></i> 12%</span></a></li>
                                                            <li><a href="#">Nestea <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a></li>
                                                            <li><a href="#">RC-Cola <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                                                        </ul>
                                                    </div><!-- /.footer -->
                                                </div><!-- /.box-body -->
                                            </div><!-- /.box -->
                                        </div> <!-- col -->
                                    </div> <!-- row -->
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div> <!-- row -->

                    <div class="row">
                        <div class="col-md-8">
                            <!-- TABLE: LATEST ORDERS -->
                            <div class="box box-success">
                                <div class="box-header with-border bg-success">
                                    <h3 class="box-title">Transaction Panel</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table class="table no-margin table table-striped">
                                          <thead>
                                            <tr>
                                              <th>Order ID</th>
                                              <th>Item</th>
                                              <th>Order Date</th>
                                              <th>Order Time</th>
                                              <th>Amount (Php)</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                                <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                                <td>Call of Duty IV</td>
                                                <td>10/21/2013</td>
                                                <td>03:20 PM</td>
                                                <td>P 5663.54</td>
                                            </tr>
                                            <tr>
                                                <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                                <td>Samsung Smart TV</td>
                                                <td>10/21/2013</td>
                                                <td>03:20 PM</td>
                                                <td>P 5663.54</td>
                                            </tr>
                                            <tr>
                                                <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                                <td>iPhone 6 Plus</td>
                                                <td>10/21/2013</td>
                                                <td>03:20 PM</td>
                                                <td>P 5663.54</td>
                                            </tr>
                                            <tr>
                                                <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                                <td>Samsung Smart TV</td>
                                                <td>10/21/2013</td>
                                                <td>03:20 PM</td>
                                                <td>P 5663.54</td>
                                            </tr>
                                            <tr>
                                                <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                                <td>Samsung Smart TV</td>
                                                <td>10/21/2013</td>
                                                <td>03:20 PM</td>
                                                <td>P 5663.54</td>
                                            </tr>
                                            <tr>
                                                <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                                <td>iPhone 6 Plus</td>
                                                <td>10/21/2013</td>
                                                <td>03:20 PM</td>
                                                <td>P 5663.54</td>
                                            </tr>
                                            <tr>
                                                <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                                <td>Call of Duty IV</td>
                                                <td>10/21/2013</td>
                                                <td>03:20 PM</td>
                                                <td>P 5663.54</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div><!-- /.box-body -->
                                <div class="box-footer clearfix">
                                    <a href="javascript::;" class="btn btn-sm btn-primary btn-flat pull-right">View All Transaction</a>
                                </div><!-- /.box-footer -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->

                        <div class="col-md-4">
                            <!-- TABLE: LATEST ORDERS -->
                            <div class="box box-success">
                                <div class="box-header with-border bg-success">
                                    <h3 class="box-title">Recently Added Products</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="products-list product-list-in-box">
                                        <li class="item">
                                            <div class="product-img">
                                                <img src="resource/images/default-50x50.gif" alt="Product Image" />
                                            </div>
                                            <div class="product-info">
                                                <a href="javascript::;" class="product-title">Samsung TV <span class="label label-warning pull-right">$1800</span></a>
                                                <span class="product-description">Samsung 32" 1080p 60Hz LED Smart HDTV.</span>
                                            </div>
                                        </li><!-- /.item -->
                                        <li class="item">
                                            <div class="product-img">
                                                <img src="resource/images/default-50x50.gif" alt="Product Image" />
                                            </div>
                                            <div class="product-info">
                                                <a href="javascript::;" class="product-title">Bicycle <span class="label label-info pull-right">$700</span></a>
                                                <span class="product-description">26" Mongoose Dolomite Men's 7-speed, Navy Blue.</span>
                                            </div>
                                        </li><!-- /.item -->
                                        <li class="item">
                                            <div class="product-img">
                                                <img src="resource/images/default-50x50.gif" alt="Product Image" />
                                            </div>
                                            <div class="product-info">
                                                <a href="javascript::;" class="product-title">Xbox One <span class="label label-danger pull-right">$350</span></a>
                                                <span class="product-description">Xbox One Console Bundle with Halo Master Chief Collection.</span>
                                            </div>
                                        </li><!-- /.item -->
                                        <li class="item">
                                            <div class="product-img">
                                                <img src="resource/images/default-50x50.gif" alt="Product Image" />
                                            </div>
                                            <div class="product-info">
                                                <a href="javascript::;" class="product-title">PlayStation 4 <span class="label label-success pull-right">$399</span></a>
                                                <span class="product-description">PlayStation 4 500GB Console (PS4)</span>
                                            </div>
                                        </li><!-- /.item -->
                                    </ul>
                                </div><!-- /.box-body -->
                                <div class="box-footer text-center">
                                    <a href="javascript::;" class="btn btn-sm btn-primary btn-flat pull-right">View All Recently Added Products</a>
                                </div><!-- /.box-footer -->

                            </div><!-- /.box -->
                        </div><!-- /.col -->

                    </div> <!-- row -->

                </section> <!-- .content -->
            </div><!-- /.content-wrapper -->