<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <span class="logo-mini"><img src="<?php echo base_url('resource'); ?>/images/small-logo.png"></span>
        <span class="logo-lg"><img src="<?php echo base_url('resource'); ?>/images/logo.png"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" onclick="test()">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
                <li><a href="#" class="event-none"><span id="today"></span></a></li>
                <li><a href="#" class="event-none"><span><?php echo $this->session->userdata('display_name'); ?></span></a></li>
                <li><a href="<?php echo base_url('notifications/') ?>"><i class="fa fa-th"></i><span class="label label-warning"></span></a></li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img data-name="<?php echo $this->session->userdata('display_name'); ?>" class="user-image profile" alt="User Image" />

                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img data-name="<?php echo $this->session->userdata('display_name'); ?>" class="img-circle profile" alt="User Image" />
                            <p><?php echo $this->session->userdata('username'); ?><small><?php echo $this->session->userdata('role_name'); ?></small></p>
                            <p style="margin-top:10px;"><a href="<?php echo base_url('auth/logout'); ?>" class="btn btn-success"><i class="fa fa-sign-in"></i> Sign out</a></p>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

