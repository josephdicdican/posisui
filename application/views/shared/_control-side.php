    <!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#tab_home" data-toggle="tab"><i class="fa fa-th"></i></a></li>
        <li><a href="#tab_shop" data-toggle="tab"><i class="fa fa-shopping-cart"></i><span class="label label-success">4</span></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="tab_home">
            <h3 class="control-sidebar-heading">Title</h3>
            <ul class="control-sidebar-menu">
            <li>
                <div class="box-body">
                    <a class="btn btn-primary btn-app" href="<?php echo base_url('manage/users'); ?>">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                    </a>
                    <a class="btn btn-app" href="<?php echo base_url('manage/customers'); ?>">
                        <i class="fa fa-users"></i>
                        <span>Customers</span>
                    </a>
                    <a class="btn btn-app" href="<?php echo base_url('manage/suppliers'); ?>">
                        <i class="fa fa-users"></i>
                        <span>Supplier</span>
                    </a>
                    <a class="btn btn-app" href="<?php echo base_url('inventory/'); ?>">
                        <i class="fa fa-shopping-cart"></i>
                        <span>Inventory</span>
                    </a>
                    <a class="btn btn-primary btn-app" href="<?php echo base_url('supplypurchase/'); ?>">
                        <i class="fa fa-barcode"></i>
                        <span>Purchases</span>
                    </a>
                    <a class="btn btn-app" href="<?php echo base_url('productscategories/'); ?>">
                        <i class="fa fa-barcode"></i>
                        <span>Products</span>
                    </a>
                    <a class="btn btn-app" href="<?php echo base_url('sales/'); ?>">
                        <i class="fa fa-shopping-cart"></i>
                        <span>Sales</span>
                    </a>
                    <a class="btn btn-app" href="<?php echo base_url('reports/'); ?>">
                        <i class="fa fa-line-chart"></i>
                        <span>Reports</span>
                    </a>
                </div>
            </li>
            </ul><!-- /.control-sidebar-menu -->
        </div><!-- /.tab-pane -->

        <!-- Home tab content -->
        <div class="tab-pane" id="tab_shop">
            <h3 class="control-sidebar-heading">Onhold Orders</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-green"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-red"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-light-blue"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-yellow"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-green"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-red"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-light-blue"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-yellow"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Customer Name</h4>
                            <p>23rd September 2015 01:51 PM</p>
                        </div>
                    </a>
                </li>
            </ul><!-- /.control-sidebar-menu -->
        </div><!-- /.tab-pane -->
    </div>
</aside><!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>