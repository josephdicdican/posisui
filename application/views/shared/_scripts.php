    <!-- jQuery 2.1.4 -->
    <script type="text/javascript" src="<?php echo base_url('resource/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/plugins/fastclick/fastclick.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script type="text/javascript" src="<?php echo base_url('resource/js/app.min.js'); ?>"></script>
    <!-- SlimScroll 1.3.0 -->
    <script type="text/javascript" src="<?php echo base_url('resource/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- SnackbarJS, find it in /dist -->
    <script src="<?php echo base_url('resource/js'); ?>/snackbar.min.js"></script>
    <?php
        if ($view == 'main/index') {?>
            <script type="text/javascript" src="<?php echo base_url('resource/js/main/custom.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url('resource/js/main/slick.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url('resource/js/main/wow.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url('resource/js/main/jquery.min.js'); ?>"></script>
            <script>
                $(document).scroll(function () {
                      var y = $(this).scrollTop();
                      if (y > 100) {
                          $('#scroll').show();
                      } else {
                          $('#scroll').hide();
                      }
                  });
              </script>
       <?php } ?>

<?php if($view == 'main/dashboard') { ?>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script type="text/javascript" src="<?php echo base_url('resource/js/dashboard2.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/plugins/morris/morris.js'); ?>"></script>-->

    <!-- Custom Js -->
    <script type="text/javascript" src="<?php echo base_url('resource/js/custom-scripts.js'); ?>"></script>
<?php } ?>
    <!-- Metis Menu Js -->
    <!--<script type="text/javascript" src="<?php echo base_url('resource/js/jquery.metisMenu.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/js/canvasjs.min.js'); ?>"></script>-->

    <script type="text/javascript" src="<?php echo base_url('resource/js/knockout-3.3.0.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/js/knockout.validation.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/js/admin.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/js/core/storeitemscategories.js'); ?>"></script>
<?php if($view == 'supplypurchases/index') { ?>
    <script type="text/javascript" src="<?php echo base_url('resource/plugins/jQueryUI/jquery-ui.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('resource/js/jquery.timepicker.js'); ?>"></script>
<?php } ?>
<?php if(isset($corejs)) { ?>
   <?php foreach($corejs as $js) { ?>
    <script type="text/javascript" src="<?php echo base_url('resource/js/'.$js); ?>"></script>
   <?php } ?>
<?php } ?>

<script>
    $('.show_info').click(function(){
        $('#details').addClass('col-md-4 col-sm-12 col-xs-12');
        $('#details').addClass('open');
        $('#details').removeClass('hidden');
        $('#origin').removeClass('col-md-12');
        $('#origin').addClass('col-md-8 col-sm-12 col-xs-12');
        $('#details').show();


        $('.show_info').removeClass('selected');
        $(this).addClass('selected');
    });

    $('.close').click(function(){
        $('#origin').removeClass('col-md-8 col-sm-12 col-xs-12');
        $('#origin').addClass('col-md-12 col-sm-12 col-xs-12');
        $('#details').hide();
        $('.show_info').removeClass('selected');
    });

    $('.purchase tr').click(function(){
        $('.purchase tr').removeClass('selected');
        $(this).addClass('selected');
    });

    $('.profile').initial(); 
</script>
