<?php
$sidenavs = array(
        array('icon' => 'fa fa-dashboard', 'link' => base_url('pos'), 'linkName' => 'POS', 'is_dropdown' => false, 'is_active' => ($view == 'POS/pos')),
    );

if($this->session->userdata['is_admin']) {
    $sidenavs = array(
        array('icon' => 'fa fa-dashboard', 'link' => base_url('main/dashboard'), 'linkName' => 'Dashboard', 'is_dropdown' => false, 'is_active' => ($view == 'main/dashboard')),
        array('icon' => 'fa fa-users', 'link' => base_url('manage/users'), 'linkName' => 'Users', 'is_dropdown' => false, 'is_active' => ($view == 'manage/users')),
        array('icon' => 'fa fa-users', 'link' => base_url('manage/customers'), 'linkName' => 'Customers', 'is_dropdown' => false, 'is_active' => ($view == 'manage/customers')),
        array('icon' => 'fa fa-users', 'link' => base_url('manage/suppliers'), 'linkName' => 'Suppliers', 'is_dropdown' => false, 'is_active' => ($view == 'manage/suppliers')),
        
        array('icon' => 'fa fa-th', 'link' => '#', 'linkName' => 'Stockable', 'is_dropdown' => true, 
            'is_active' => ($view == 'inventory/products' || $view == 'supplypurchases/index' || $view == 'productscategories/index'), 
            'childNavs' => array(
                    array('icon' => 'fa fa-th', 'link' => base_url('inventory/index'), 'linkName' => 'Inventory', 'is_dropdown' => false, 'is_active' => ($view == 'inventory/products')),
                    array('icon' => 'fa fa-cart-plus', 'link' => base_url('supplypurchase/index'), 'linkName' => 'Supply Purchases', 'is_dropdown' => false, 'is_active' => ($view == 'supplypurchases/index')),
                    array('icon' => 'fa fa-barcode', 'link' => base_url('productscategories/index'), 'linkName' => 'Products & Categories', 'is_dropdown' => false, 'is_active' => ($view == 'productscategories/index'))
                )
            ),

        array('icon' => 'fa fa-cart-plus', 'link' => '#', 'linkName' => 'Consumable', 'is_dropdown' => true, 
            'is_active' => ($view == 'dailymenu/index'), 
            
            'childNavs' => array(
                    array('icon' => 'fa fa-calendar', 'link' => base_url('dailymenu/index'), 'linkName' => 'Daily Menu', 'is_dropdown' => false, 'is_active' => ($view == 'dailymenu/index')),
                )
            ),

        array('icon' => 'fa fa-line-chart', 'link' => '#', 'linkName' => 'Reports', 'is_dropdown' => true, 
            'is_active' => ($view == 'reports/daily_report' || $view == 'reports/sales_forecast' || $view == 'reports/sales_reports' || $view == 'reports/inventory'), 
            'childNavs' => array(
                    array('icon' => 'fa fa-calendar-check-o', 'link' => base_url('reports'), 'linkName' => 'End of Day Report', 'is_dropdown' => false, 'is_active' => ($view == 'reports/daily_report')),
                    array('icon' => 'fa fa-bar-chart', 'link' => base_url('reports/sales_reports'), 'linkName' => 'Sales Reports', 'is_dropdown' => false, 'is_active' => ($view == 'reports/sales_reports')),
                    array('icon' => 'fa fa-line-chart', 'link' => base_url('reports/sales_forecast'), 'linkName' => 'Sales Forecast', 'is_dropdown' => false, 'is_active' => ($view == 'reports/sales_forecast')),
                    array('icon' => 'fa fa-list', 'link' => base_url('reports/inventory'), 'linkName' => 'Inventory Report', 'is_dropdown' => false, 'is_active' => ($view == 'reports/inventory'))  
                )
            ) 
    );
} 
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <?php foreach($sidenavs as $nav) { ?>
                <li class="<?php ($nav['is_dropdown']) ? 'treeview' : '' ; ?> <?php echo ($nav['is_active']) ? 'active' : ''; ?>">
                    <a href="<?php echo $nav['link']?>">
                        <i class="<?php echo $nav['icon']?>"></i> <span><?php echo $nav['linkName']?></span>
                        <?php if($nav['is_dropdown']) { ?>
                            <i class="fa fa-angle-left pull-right"></i>
                        <?php } ?>
                    </a>
                    <?php if($nav['is_dropdown']) { ?>
                        <ul class="treeview-menu">
                            <?php foreach($nav['childNavs'] as $child) { ?>
                                <li class="treeview <?php echo ($child['is_active']) ? 'active' : ''; ?>">
                                    <a href="<?php echo $child['link']; ?>">
                                        <i class="<?php echo $child['icon']; ?>"></i> <span><?php echo $child['linkName']?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </section>
<!-- /.sidebar -->
</aside>