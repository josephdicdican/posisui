<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Posis &ndash; <?php echo isset($title)?$title:''; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<?php $this->load->view('shared/_head'); ?>
</head>
<?php
	$bodyClass = '';

	switch($view) {
		case 'auth/index';
			$bodyClass = 'login-page';
			break;

		case 'auth/forgotpassword';
			$bodyClass = 'login-page';
			break;

		case 'auth/resetpassword';
			$bodyClass = 'login-page';
			break;

		case 'main/index':
			$bodyClass = 'landing skin-green';
			break;

		default:
			$bodyClass = 'skin-green sidebar-mini sidebar-collapse'; //sidebar-collapse
			break;
	}
?>
<body class="<?php echo $bodyClass; ?>">
	<div class="wrapper">
		<?php
			if($show_build) {
				$this->load->view('shared/_header');
				$this->load->view('shared/_sidebar');
			}
		?>
		<?php $this->load->view($view); ?>
		<?php
			if($show_build) {
				$this->load->view('shared/_footer');
			}
		?>
	</div>
	<?php $this->load->view('shared/_scripts'); ?>
</body>
</html>