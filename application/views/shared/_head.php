
<?php if($view == 'supplypurchases/index') { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/jquery-ui.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/jquery.timepicker.css'); ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/bootstrap/css/bootstrap.css'); ?>" />
<!-- Font Awesome Icons -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/font-awesome/css/font-awesome.min.css'); ?>" />
<!-- Ionicons -->
<!-- <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" /> -->

<!-- core CSS of SnackbarJS, find it in /dist -->
<link href="<?php echo base_url('resource/css'); ?>/snackbar.min.css" rel="stylesheet">
<!-- the default theme of SnackbarJS, find it in /themes-css -->
<link href="<?php echo base_url('resource/css'); ?>/material.css" rel="stylesheet">

<!-- Theme style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/styles.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/skins/_all-skins.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/custom.css'); ?>" />

<?php
	if ($view == 'main/index') { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/main/green-theme.css'); ?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/main/slick.css'); ?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/main/mainstyle.css'); ?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/animate.css'); ?>" />
<?php } else if ($view == 'POS/pos') { ?>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/barcode/barcode.css'); ?>"/>