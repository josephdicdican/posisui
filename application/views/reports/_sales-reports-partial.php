<div class="box" id="sales_reports">
    <div class="box-header">
        <h3>
            Sales Report&emsp; 
            <div class="btn-group">
                <a data-bind="click: $root.viewTransactions" href="#" class="btn btn-default btn-flat btn-sm">
                    <i class="fa fa-shopping-cart"></i> Transactions Sales
                </a>
                <a data-bind="click: $root.viewStoreItems" href="#" class="btn btn-default btn-flat btn-sm">
                    <i class="fa fa-barcode"></i> Store Items Sales
                </a>
            </div>
            <span class="pull-right">
                <a data-bind="click: $root.printReport" href="#" data-toggle="tooltip" data-placement="bottom" title="Print report"><i class="fa fa-print"></i></a>
                <a data-bind="attr: { href: save_export_url }" data-toggle="tooltip" data-placement="bottom" title="Save report"><i class="fa fa-save"></i></a>&emsp;
            </span>
        </h3>
    </div>
    <div class="box-body horizon">
        <div id="print-content" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="col-md-3 detailed-col" id="origin">
                <div class="slimscroll-head">
                    <div class="box">
                        <div class="box-header">
                            Intervals:
                            <div class="btn-group">
                                <a data-bind="click: $root.setIntervalType.bind($data, 'week')" href="#" class="btn btn-default btn-flat btn-sm">Week</a>&emsp;
                                <a data-bind="click: $root.setIntervalType.bind($data, 'month')" href="#" class="btn btn-default btn-flat btn-sm">Month</a>&emsp;
                                <a data-bind="click: $root.setIntervalType.bind($data, 'year')" href="#" class="btn btn-default btn-flat btn-sm">Year</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slimscroll">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th><span data-bind="text: interval_type"></span> #</th>
                                <th>Items</th>
                                <th>Sales</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: intervals">
                            <tr data-bind="click: $root.getInterval">
                                <td><span data-bind="text: interval"></span></td>
                                <td><span data-bind="text: items"></span></td>
                                <td><span data-bind="text: sales"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-9 detailed-col" id="origin">
                <div id="transactions">
                    <div class="slimscroll-head">
                        <div class="box">
                            <div class="box-header">
                                Transactions Sales
                            </div>
                        </div>
                    </div>
                    <div class="slimscroll">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Order #</th>
                                    <th>Date</th>
                                    <th class="text-center">Items</th>
                                    <th class="text-right">Sales</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: transaction_sales">
                                <tr>
                                    <td><span data-bind="text: order_id"></span></td>
                                    <td><span data-bind="text: date_of_order"></span></td>
                                    <td class="text-center"><span data-bind="text: items"></span></td>
                                    <td class="text-right"><span data-bind="text: sales"></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <div data-bind="visible: transaction_sales().length == 0" class="pad10">
                            <div class="text-danger">No record retrieved.</div>
                        </div>
                    </div>
                </div>
                <div id="storeitems">
                    <div class="slimscroll-head">
                        <div class="box">
                            <div class="box-header">
                                Store Items Sales
                                <div class="btn-group">
                                    <a data-bind="click: $root.showList" href="#" class="btn btn-default btn-flat btn-sm">
                                        <i class="fa fa-list"></i> List
                                    </a>
                                    <a data-bind="click: $root.showChart" href="#" class="btn btn-default btn-flat btn-sm">
                                        <i class="fa fa-th"></i> Grid
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slimscroll">
                        <table class="table table-condensed sales-list">
                            <thead>
                                <tr>
                                    <th>SKU</th>
                                    <th>Store Item</th>
                                    <th class="text-center">Sold</th>
                                    <th class="text-right">Sales</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: storeitem_sales">
                                <tr>
                                    <td><span data-bind="text: sku"></span></td>
                                    <td><span data-bind="text: storeitemname"></span></td>
                                    <td class="text-center"><span data-bind="text: items"></span></td>
                                    <td class="text-right"><span data-bind="text: sales"></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <div data-bind="visible: storeitem_sales().length == 0" class="pad10">
                            <div class="text-danger">No record retrieved.</div>
                        </div>
                        <div id="storeItemsSalesChart" class="sales-chart">
                            <div data-bind="foreach: storeitem_sales" class="pad10 row">
                                <div class="col-xs-2">
                                    <span data-bind="text: storeitemname"></span> <br />
                                    (<span data-bind="text: items"></span> out of <span data-bind="text: $parent._interval().items"></span>) - <span data-bind="text: percent">%</span>
                                    <div class="progress">
                                        <div data-bind="attr: { 'aria-valuenow': percent}, style: { width: percent }" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"><span data-bind="text: percent"></span>% </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- col-md-4 -->
    </div><!-- /.box-body -->
</div>