        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->load->view('reports/_quick_nav'); ?> 
                        <div class="col-xs-12" id="print-content">
                            <?php $this->load->view('reports/_sales-reports-partial'); ?>
                        </div>
                </div> <!-- row -->
            </section><!-- content -->
        </div> <!-- content-wrapper