                        <div class="col-sm-12 no-print">
                            <div id="quick-nav" class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="panel panel-danger info-box hoverable">
                                        <div class="panel-heading bg-red">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="fa fa-calendar fa-5x"></i>
                                                </div>
                                                <div class="col-xs-8 text-right">
                                                    <h3>End of Day <br />Report</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="<?php echo base_url('reports/'); ?>">
                                            <div class="panel-footer">
                                                <span class="pull-left">See More ...</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="panel panel-success info-box hoverable">
                                        <div class="panel-heading bg-green">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="fa fa-bar-chart fa-5x"></i>
                                                </div>
                                                <div class="col-xs-8 text-right">
                                                    <h3>Sales <br />Reports</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="<?php echo base_url('reports/sales_reports'); ?>">
                                            <div class="panel-footer">
                                                <span class="pull-left">See More ...</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="panel panel-primary info-box hoverable">
                                        <div class="panel-heading bg-blue">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="fa fa-area-chart fa-5x"></i>
                                                </div>
                                                <div class="col-xs-8 text-right">
                                                    <h3>Sales <br />Forecast</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="<?php echo base_url('reports/sales_forecast'); ?>">
                                            <div class="panel-footer">
                                                <span class="pull-left">See More ...</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="panel panel-warning info-box hoverable">
                                        <div class="panel-heading bg-yellow">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="fa fa-bars fa-5x"></i>
                                                </div>
                                                <div class="col-xs-8 text-right">
                                                    <h3>Inventory <br />Reports</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="<?php echo base_url('reports/inventory'); ?>">
                                            <div class="panel-footer">
                                                <span class="pull-left">See More ...</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-right">
                                    <a id="hide-quick-nav" href="#" class="">Hide Quick Nav <i class="fa fa-angle-up"></i></a>
                                </div>
                            </div>
                            <div id="quick-nav-tog" class="row">
                                <div class="col-sm-12">
                                    <a id="show-quick-nav" href="#" class=" pull-right">Show Quick Nav <i class="fa fa-angle-down"></i></a>
                                </div>
                            </div>
                        </div> 