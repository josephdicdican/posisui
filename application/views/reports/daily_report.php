<!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="content">
                <div class="row">
                    <div class="col-xs-12 no-padding">
                        <?php $this->load->view('reports/_quick_nav'); ?>
                        <div class="col-xs-12 no-padding">
                            <!-- Transactions List Pane start -->
                            <div class="col-md-5 side-category transactions">
                                <div class="box-header with-border">
                                    <div class="">
                                        <h3>
                                            <span>Today's Transactions</span>
                                            <span class="pull-right no-print">
                                                <a href="#"><i data-bind="click: $root.printSales" class="fa fa-print"></i></a>
                                                <a href="<?php echo base_url('api/reports/export_today_sale_csv'); ?>" target="_blank"><i class="fa fa-save"></i></a>
                                            </span>
                                        </h3>
                                    </div>
                                </div>
                                <div class="slimscroll-head">
                                    <table class="table" style="margin-bottom:0;">
                                        <thead>
                                            <tr role="row">
                                                <th style="width: 5%;">#</th>
                                                <th style="width: 40%;">Time</th>
                                                <th class="text-center" style="width: 25%;">Total Items</th>
                                                <th class="text-right" style="width: 25%;">Total Sales</th>
                                                <th style="width: 5%;"><i class="fa fa-ellipsis-h"></i></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="box-body slimscroll no-padding" style="height: 400px;">
                                    <table class="table table-hover">
                                        <tbody data-bind="foreach: transactions">
                                            <tr data-bind="click: $root.getTransactionLines">
                                                <td style="width: 5%;"><span data-bind="text: order_id"></span></td>
                                                <td style="width: 40%;"><span data-bind="text: format_time"></span></td>
                                                <td class="text-center" style="width: 25%;"><span data-bind="text: total_items"></span></td>
                                                <td class="text-right" style="width: 25%;"><span data-bind="text: total_amount_formatted"></span></td>
                                                <td style="width: 5%;">
                                                    <a href="#" data-toggle="modal" data-target="#transaction-menu"><i class="fa fa-ellipsis-h"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div data-bind="visible: transactions().length == 0" class="pad10">
                                        <div class="alert alert-danger">No transactions yet today.</div>
                                    </div>
                                </div>
                                 <div class="pos-logs">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><b>Total Transactions: </b><big data-bind="text: transactions().length"></big>&emsp;</td>
                                                <td><b>Total Quantity: </b><big data-bind="text: overall_total_items"></big>&emsp;</td>
                                                <td><b>Total Sales: </b><big data-bind="text: overall_total_sales_formatted"></big></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- Transactions List Pane end -->

                            <!-- Transaction line Line Pane start -->
                            <div class="col-md-7 no-padding transaction-lines">
                                <div class="box-header">
                                    <h3>
                                        <span data-bind="visible: has_transaction()">Transaction #<span data-bind="text: transaction().order_id"></span></span> Lines
                                    </h3>
                                </div>
                                <div class="slimscroll-head">
                                    <table class="table" style="margin-bottom:0;">
                                        <thead>
                                            <tr role="row">
                                                <th style="width: 30%;">Item</th>
                                                <th style="width: 20%;">Price <small>(per unit)</small></th>
                                                <th class="text-center" style="width: 20%;">Quantity</th>
                                                <th class="text-right" style="width: 20%;">Subtotal</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="slimscroll">
                                    <table class="table table-hover">
                                        <tbody data-bind="foreach: transaction().orderlines">
                                            <tr role="row">
                                               <td style="width: 30%;"><span data-bind="text: storeitemname"></span></td>
                                               <td style="width: 20%;"><span data-bind="text: price_per_unit_formatted"></span></td>
                                               <td class="text-center" style="width: 20%;"><span data-bind="text: quantity"></span></td>
                                               <td class="text-right" style="width: 20%;"><span data-bind="text: subtotal"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div data-bind="visible: transaction().orderlines().length == 0" class="pad10">
                                        <div class="alert alert-danger">No items to display.</div>
                                    </div>
                                </div>
                                <div data-bind="visible: has_transaction()" class="pos-logs">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><big>Today's Sale</big>&emsp;&emsp;</td>
                                                <td><b>Total Items: </b><big data-bind="text: transaction().orderlines().length"></big>&emsp;</td>
                                                <td><b>Total Quantity: </b><big data-bind="text: transaction().total_items"></big>&emsp;</td>
                                                <td><b>Total Sales: </b><big data-bind="text: transaction().total_amount_formatted"></big></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- Transaction line Line Pane start -->
                        </div>
                    </div>
                </div> <!-- row -->
            </section><!-- content -->
        </div> <!-- content-wrapper-->

        <div class="modal fade modal-add" id="transaction-menu" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Transaction # <span data-bind="text: transaction().order_id"></span> options</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <a data-bind="click: $root.printTransaction" class="btn btn-app">
                                <i class="fa fa-print"></i> Print
                            </a>
                            <a class="btn btn-app">
                                <i class="fa fa-save"></i> Save
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-default" data-dismiss="modal" aria-label="Close">Cancel</a>
                        <!-- <a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Confirm</a> -->
                    </div>
                </div>
            </div>
        </div>