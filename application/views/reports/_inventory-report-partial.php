<div class="box" id="inventory-report">
    <div class="box-header">
        <h3><a data-bind="click: $root.getProducts" href="#">Inventory Report</a>&emsp;
            <select data-bind="options: filters, optionsText: 'name', optionsValue: 'code', value: selected_filter" class="form-control" style="width: 200px; display: inline;"></select>&emsp;
            <select data-bind="options: storeitem_categories().parent_categories, optionsText: 'category_name', optionsCaption: 'Select Category ...', value: storeitem_categories().parent_category" class="form-control" style="width: 200px; display: inline;"></select>&emsp;
            <select data-bind="options: storeitem_categories().categories, optionsText: 'category_name', optionsCaption: 'Select Sub-category ...', value: storeitem_categories().category" class="form-control" style="width: 200px; display: inline;"></select>
            <span class="pull-right">
                <a data-bind="click: $root.printReport" href="#" data-toggle="tooltip" data-placement="bottom" title="Print report"><i class="fa fa-print"></i></a>
                <a data-bind="attr: { href: save_export_url }" data-toggle="tooltip" data-placement="bottom" title="Save report"><i class="fa fa-save"></i></a>&emsp;
            </span>
        </h3>
    </div>
    <div class="box-body horizon">
        <div id="print-content" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="col-md-12 detailed-col" id="origin">
                <div class="slimscroll-head">
                    <table class="table" role="grid" aria-describedby="example1_info" style="margin-bottom:0;">
                        <thead>
                            <tr role="row">
                                <th data-bind="click: $root.sortResult.bind($data, 'storeitemname')" style="width: 25%">
                                    <span data-toggle="tooltip" data-placement="right" title="Sort by inventory item">Item</span>
                                    <span class="pull-right">
                                        <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'storeitemname'" href="#"><i class="fa fa-angle-up"></i></a>
                                        <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'storeitemname'" href="#"><i class="fa fa-angle-down"></i></a>
                                    </span>
                                </th>
                                <th data-bind="click: $root.sortResult.bind($data, 'stock_at_hand')" class="text-center" style="width: 15%">
                                    <span data-toggle="tooltip" data-placement="top" title="Sort by stock at hand">Stock at Hand</span>
                                    <span class="pull-right">
                                        <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'stock_at_hand'" href="#"><i class="fa fa-angle-up"></i></a>
                                        <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'stock_at_hand'" href="#"><i class="fa fa-angle-down"></i></a>
                                    </span>
                                </th>
                                <th data-bind="click: $root.sortResult.bind($data, 'reordering_qty')" class="text-center" style="width: 15%">
                                    <span data-toggle="tooltip" data-placement="top" title="Sort by reordering qty">Reordering Qty</span>
                                    <span class="pull-right">
                                        <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'reordering_qty'" href="#"><i class="fa fa-angle-up"></i></a>
                                        <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'reordering_qty'" href="#"><i class="fa fa-angle-down"></i></a>
                                    </span>
                                </th>
                                <th data-bind="click: $root.sortResult.bind($data, 'max_stock_qty')" class="text-center" style="width: 15%">
                                    <span data-toggle="tooltip" data-placement="top" title="Sort by maximum qty">Maximum Qty</span>
                                    <span class="pull-right">
                                        <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'max_stock_qty'" href="#"><i class="fa fa-angle-up"></i></a>
                                        <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'max_stock_qty'" href="#"><i class="fa fa-angle-down"></i></a>
                                    </span>
                                </th>
                                <th data-bind="click: $root.sortResult.bind($data, 'min_stock_qty')" class="text-center" style="width: 15%">
                                    <span data-toggle="tooltip" data-placement="top" title="Sort by minimum qty">Minimum Qty</span>
                                    <span class="pull-right">
                                        <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'min_stock_qty'" href="#"><i class="fa fa-angle-up"></i></a>
                                        <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'min_stock_qty'" href="#"><i class="fa fa-angle-down"></i></a>
                                    </span>
                                </th>
                                <th style="width: 15%">Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="slimscroll">
                    <table class="table table-hover table-condensed dataTable">
                        <tbody data-bind="foreach: products">
                            <tr>
                                <td data-bind="text: storeitemname" style="width: 25%"></td>
                                <td data-bind="text: stock_at_hand" class="text-center" style="width: 15%"></td>
                                <td data-bind="text: reordering_qty" class="text-center" style="width: 15%"></td>
                                <td data-bind="text: max_stock_qty" class="text-center" style="width: 15%"></td>
                                <td data-bind="text: min_stock_qty" class="text-center" style="width: 15%"></td>
                                <td style="width: 15%"><span data-bind="text: status, attr: { class: color }"></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <div data-bind="visible: products().length == 0" class="pad10">
                        <div class="alert alert-danger">No items to display.</div>
                    </div>
                </div>
            </div>
        </div> <!-- col-md-4 -->
    </div><!-- /.box-body -->
</div>