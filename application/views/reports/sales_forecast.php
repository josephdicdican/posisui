            <div class="content-wrapper">
                <section class="content page">
                    <div class="row">
                        <div class="col-xs-12 no-padding">
                            <?php $this->load->view('reports/_quick_nav'); ?>
                            <div class="col-xs-12 no-padding">
                                <!-- Forecast Filter Pane start -->
                                <div class="col-md-3 side-category">
                                    <div class="box-header with-border">
                                        <div class="">
                                            <h4>
                                                <span>Sales Forecast</span>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="slimscroll-head pad10 text-right">
                                        <span class="pull-left">Intervals</span>
                                        <div class="btn-group">
                                            <a data-bind="click: $root.setIntervalType.bind($data, 'week'), css: is_week() ? 'active' : null" href="#" class="btn btn-default btn-flat btn-sm">Week</a>&emsp;
                                            <a data-bind="click: $root.setIntervalType.bind($data, 'month'), css: is_month() ? 'active' : null" href="#" class="btn btn-default btn-flat btn-sm">Month</a>&emsp;
                                            <a data-bind="click: $root.setIntervalType.bind($data, 'year'), css: is_year() ? 'active' : null" href="#" class="btn btn-default btn-flat btn-sm">Year</a>
                                        </div>
                                    </div>
                                    <div class="slimscroll-head pad10 text-right">
                                         <span class="pull-left">Type:</span>
                                        <div class="btn-group">
                                            <a data-bind="click: $root.setIsStockable.bind($data, 1), css: !is_consumable() ? 'active' : null" href="#" class="btn btn-default btn-flat btn-sm">Stockable</a>&emsp;
                                            <a data-bind="click: $root.setIsStockable.bind($data, 0), css: is_consumable() ? 'active' : null" href="#" class="btn btn-default btn-flat btn-sm">Consumable</a>&emsp;
                                        </div>
                                    </div>
                                    <div class="slimscroll-head pad10 text-right">
                                         <span class="pull-left">View By:</span>
                                        <div class="btn-group">
                                            <a data-bind="click: $root.setGroupBy.bind($data, 'store_item_id'), css: is_storeitems() ? 'active' : null " href="#" class="btn btn-default btn-flat btn-sm">Store Items</a>&emsp;
                                            <a data-bind="click: $root.setGroupBy.bind($data, 'category_id'), css: !is_storeitems() ? 'active' : null" href="#" class="btn btn-default btn-flat btn-sm">Category</a>&emsp;
                                        </div>
                                    </div>
                                </div><!-- Forecast Filter Pane end -->

                                <!-- Forecast Line Pane start -->
                                <div class="col-md-9 no-padding">
                                    <div class="slimscroll-head">
                                    </div>
                                    <div class="slimscroll" style="padding: 0 10px 0 0;">
                                        <table data-bind="visible: !is_storeitems()" class="table table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">
                                                        Category/Menu
                                                        <small class="text-muted">
                                                            (<span data-bind="text: interval_type"></span>, sold items, sales, sale action)
                                                        </small>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody data-bind="foreach: items">
                                                <tr>
                                                    <td style="width: 30%;">
                                                        <strong data-bind="text: category_name"></strong><br />
                                                        <p data-bind="with: forecast_project" class="bg-success pad10">
                                                            <span class="text-italic">Next <span data-bind="text:$root.interval_type"></span>'s projections</span><br /> 
                                                            Items Sales: <strong data-bind="text: items_projection_formatted"></strong><br />
                                                            Sales: <strong>Php <span data-bind="text: projection_formatted"></span></strong>
                                                        </p>
                                                    </td>
                                                    <td style="width: 70%;">
                                                        <table class="table table-condensed no-padding">
                                                            <tbody data-bind="foreach: forecasts">
                                                                <tr>
                                                                    <td style="width: 30%;">
                                                                        <span data-bind="visible: $root.is_week()"><span data-bind="text: week_beginning"></span> &mdash; <span data-bind="text: week_end"></span></span>
                                                                        <span data-bind="visible: $root.is_month()"><span data-bind="text: month_beginning"></span></span>
                                                                        <span data-bind="visible: $root.is_year()"><span data-bind="text: year"></span></span>
                                                                    </td>
                                                                    <td style="width: 20%;" class="text-center">
                                                                        <span data-bind="text: sold_items"></span>
                                                                    </td>
                                                                    <td style="width: 30%;" class="text-right">
                                                                        <span data-bind="text: total_sales_formatted"></span>
                                                                    </td>
                                                                    <td style="width: 20%;" class="text-center">
                                                                        <span data-bind="visible: grew()">
                                                                            <i class="fa fa-angle-up text-success"></i> <span data-bind="text: growth_percent"></span>%
                                                                        </span>
                                                                        <span data-bind="visible: declined()">
                                                                            <i class="fa fa-angle-down text-danger"></i> <span data-bind="text: decline_percent"></span>%
                                                                        </span>
                                                                        <span data-bind="visible: no_action()" class="text-warning">
                                                                            &mdash;
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table data-bind="visible: is_storeitems()" class="table table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">
                                                        Store Item
                                                        <small class="text-muted">
                                                            (<span data-bind="text: interval_type"></span>, sold items, sales, sale action)
                                                        </small>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody data-bind="foreach: items">
                                                <tr>
                                                    <td style="width: 30%;">
                                                        <strong data-bind="text: storeitemname"></strong><br />
                                                        <span data-bind="text: sku"></span><br />
                                                        <p data-bind="with: forecast_project" class="bg-success pad10">
                                                            <span class="text-italic">Next <span data-bind="text: $root.interval_type"></span>'s projections</span><br /> 
                                                            Items Sales: <strong data-bind="text: items_projection_formatted"></strong><br />
                                                            Sales: <strong>Php <span data-bind="text: projection_formatted"></span></strong>
                                                        </p>
                                                    </td>
                                                    <td style="width: 70%;">
                                                        <table class="table table-condensed no-padding">
                                                            <tbody data-bind="foreach: forecasts">
                                                                <tr>
                                                                    <td style="width: 30%;">
                                                                        <span data-bind="visible: $root.is_week()"><span data-bind="text: week_beginning"></span> &mdash; <span data-bind="text: week_end"></span></span>
                                                                        <span data-bind="visible: $root.is_month()"><span data-bind="text: month_beginning"></span></span>
                                                                        <span data-bind="visible: $root.is_year()"><span data-bind="text: year"></span></span>
                                                                    </td>
                                                                    <td style="width: 20%;" class="text-center">
                                                                        <span data-bind="text: sold_items"></span>
                                                                    </td>
                                                                    <td style="width: 30%;" class="text-right">
                                                                        <span data-bind="text: total_sales_formatted"></span>
                                                                    </td>
                                                                    <td style="width: 20%;" class="text-center">
                                                                        <span data-bind="visible: grew()">
                                                                            <i class="fa fa-angle-up text-success"></i> <span data-bind="text: growth_percent"></span>%
                                                                        </span>
                                                                        <span data-bind="visible: declined()">
                                                                            <i class="fa fa-angle-down text-danger"></i> <span data-bind="text: decline_percent"></span>%
                                                                        </span>
                                                                        <span data-bind="visible: no_action()" class="text-warning">
                                                                            &mdash;
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- Forecast Line Pane start -->
                            </div>
                        </div>
                    </div> <!-- box -->
                </section><!-- /.content -->
            </div><!-- /.content-wrapper