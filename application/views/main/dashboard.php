            <!-- Content Wrapper. Contains page content -->
            <div id="dashboard" class="content-wrapper">
                <section class="content page">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="z-depth-2">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <div class="panel panel-primary info-box hoverable">
                                            <div class="panel-heading bg-light-blue">
                                                <div class="row">
                                                    <div class="col-xs-5">
                                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-7 text-right">
                                                        <h3>Sales</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('reports/') ?>">
                                                <div class="panel-footer">
                                                    <span class="pull-left">View Details</span>
                                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </a>
                                        </div>
                                     </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <div class="panel panel-danger info-box hoverable">
                                            <div class="panel-heading bg-red">
                                                <div class="row">
                                                    <div class="col-xs-5">
                                                        <i class="fa fa-barcode fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-7 text-right">
                                                        <h3>Store Items</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('Productscategories/index'); ?>">
                                                <div class="panel-footer">
                                                    <span class="pull-left">View Details</span>
                                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <div class="panel panel-success info-box hoverable">
                                            <div class="panel-heading bg-green">
                                                <div class="row">
                                                    <div class="col-xs-5">
                                                        <i class="fa fa-folder-open fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-7 text-right">
                                                        <h3>Inventory</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('Inventory/index'); ?>">
                                                <div class="panel-footer">
                                                    <span class="pull-left">View Details</span>
                                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <div class="panel panel-warning info-box hoverable">
                                            <div class="panel-heading bg-yellow">
                                                <div class="row">
                                                    <div class="col-xs-5">
                                                        <i class="fa fa-bar-chart-o fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-7 text-right">
                                                        <h3>Report</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('reports/') ?>">
                                                <div class="panel-footer">
                                                    <span class="pull-left">View Details</span>
                                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div><!-- /.row --> <!-- big nav-tabs -->
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <!-- TABLE: LATEST ORDERS -->
                            <div class="box box-success">
                                <div class="box-header with-border bg-success">
                                    <h3 class="box-title">Transaction Panel</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body slimscroll">
                                    <div class="table-responsive">
                                        <table data-bind="click: $root.getOrders" class="table no-margin table table-striped">
                                          <thead>
                                            <tr>
                                              <th>Order ID</th>
                                              <th>Date of Order</th>
                                              <th class="text-center">Total Items</th>
                                              <th class="text-right">Total Amount</th>
                                              <th class="text-center">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody data-bind="foreach: orders">
                                            <tr>
                                                <td data-bind="text: order_id" style="width: 20%"></td>
                                                <td data-bind="text: date_of_order" style="width: 20%"></td>
                                                <td data-bind="text: total_items" style="width: 20%" class="text-center"></td>
                                                <td data-bind="text: total_amount_formatted" style="width: 20%" class="text-right"></td>
                                                <td style="width: 20%" class="text-center">
                                                    <span data-bind="visible: is_check_out()" class="label bg-green">checkout</span>
                                                    <span data-bind="visible: !is_check_out()" class="label bg-yellow">hold</span>
                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div><!-- /.box-body -->
                                <div class="box-footer clearfix">
                                    <a href="<?php echo base_url('reports/sales_reports'); ?>" class="btn btn-sm btn-primary btn-flat pull-right">View All Transaction</a>
                                </div><!-- /.box-footer -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <div class="pos-logs">
                                <h4>Today's Sales</h4>
                                <table>
                                    <tbody>
                                        <tr><td><b>Total Transactions: </b><big data-bind="text: transactions().length" class="pull-right"></big>&emsp;</td><tr/>
                                        <tr><td><b>Total Items Sold: </b><big data-bind="text: overall_total_items" class="pull-right"></big>&emsp;</td><tr/>
                                        <tr><td><b>Total Sales: </b><big data-bind="text: overall_total_sales_formatted" class="pull-right"></big></td><tr/>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> <!-- row -->
                </section> <!-- .content -->
            </div><!-- /.content-wrapper -->