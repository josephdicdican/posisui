 <!-- Start header section -->
    <header id="header">
        <div class="header-inner">
            <!-- Header image -->
            <img src="<?php echo base_url('resource'); ?>/images/download.jpg" alt="img">
            <div class="overlay-green">
                <div class="header-content">
                    <div class="container content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <div class="row">
                                        <!-- Start logo area -->
                                    <div class="logo-area">
                                        <!-- Image based logo -->
                                        <span class="image wow animated zoomIn"><a class="logo" href="index.html"><img src="<?php echo base_url('resource'); ?>/images/logo.png" alt="logo"></a></span>
                                        <!-- Text based logo -->
                                        <!-- <a class="logo" href="index.html">App Landy.</a> -->
                                    </div>
                                    <!-- End logo area -->
                                        <div class="header-title">
                                            <h1 class="wow animated bounceInLeft">
                                              <span class="pos">Point of Sale</span>
                                              <span>Inventory</span>
                                            </h1>
                                            <p class="tag">Fast. Accurate. Efficient</p>
                                            <p class="sign-in">
                                          <a href="#featured" class="">&darr;</a>
                                            <a href="<?php echo base_url('Auth/login'); ?>" class="next-login wow animated bounceInRight">Sign in &rarr;</a>
                                          </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
  <!-- End header section -->


  <!-- Start features section -->
  <section id="featured">
    <div class="featured-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="title-area wow fadeInUp">
              <h2 class="title">Point of Sale <span>Inventory</span></h2>
              <p>Take your business to the next level! Track your sales, create reports in just few clicks and secure a storage of every customer transaction with Point of Sale Inventory System.</p>
            </div>
          </div>
          <!-- Start download app content -->
          <div class="col-md-12">
            <div class="featured-content">
              <!-- Start single download -->
              <a class="single-desc wow fadeInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s" href="#fast">
                <div class="feature-content">
                  <p>Fast</p>
                </div>
              </a>
              <!-- Start single download -->
              <a class="single-desc wow fadeInLeft" data-wow-duration="0.75s" data-wow-delay="0.75s" href="#accurate">
                <div class="feature-content">
                  <p>Accurate</p>
                </div>
              </a>
              <!-- Start single download -->
              <a class="single-desc wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s" href="#efficient">
                <div class="feature-content">
                  <p>Efficient</p>
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-12">
          <div class="features-area">
            <div class="row">
              <!-- Start features img -->
              <div class="col-md-offset-1 col-md-10">
                <div class="feature-img wow fadeInUp">
                  <img src="<?php echo base_url('resource'); ?>/images/pos-tab.png" alt="POS in tablet">
                </div>
              </div>
              <!-- End features img -->
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End features section -->

<!-- start pricing table -->
  <section id="pricing-table">
        <div class="container">
            <div class="row">
                <div class="container features-info">
                    <section id="fast" class="pos-sec wow animated bounceInLeft">
                        <div class="feature-info-col col-md-7 col-md-offset-4">
                            <h2 class="feature-title">Fast</h2>
                            <p>End those long lines during the busiest time of your sales day. With POSIS, satisfy your customers with much faster check-out process and avoid lost of sales due to long waits at the same time.</p>
                        </div>
                    </section>
                    <section id="accurate" class="pos-sec wow animated bounceInRight">
                        <div class="feature-info-col col-md-7 col-md-offset-1">
                            <h2 class="feature-title pull-right">Accurate</h2>
                            <p class="mid">Since every computation will now be done automatically, accounting processes will be simplified and mistakes from staff will be avoided.</p>
                        </div>
                    </section>
                    <section id="efficient" class="pos-sec wow animated bounceInLeft">
                        <div class="feature-info-col col-md-7 col-md-offset-4">
                            <h2 class="feature-title">Efficient</h2>
                            <p>With POSIS workload will be lighten up making the management and it's staff more productive and EFFICIENT. Improved efficiency means higher customer satisfaction and higher sales!</p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
  </section>
  <!-- End pricing table -->

  <!-- Start how it works -->
  <section id="howit-works">
    <div class="howit-works-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="title-area wow fadeInLeft">
              <h2 class="title">How it <span>Works</span></h2>
              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
            </div>
          </div>
          <div class="col-md-12">
            <div class="howit-works-video">
              <iframe src="https://player.vimeo.com/video/33790882" width="428" height="276" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End how it works -->

  <!-- Start footer -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="footer-social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
          </div>
          <div class="footer-text">
            <p>&copy; Alrights reserved 2016. <a href="">R's Food Avenue</a></p>
            <p>
              <a href="#">POS</a>
              <a href="#">IS</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End -->
  <a href="#" id="scroll" title="Scroll to Top" style="display: none;"><i class="fa fa-chevron-up" alt="Top"></i><span></span></a>