<!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="header-pane col-md-12 z-depth-1">
                <div class="title-pane col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-left">
                    <h3><a data-bind="click: $root.getProducts" href="#">Inventory</a></h3>
                </div>
                <div class="quick-nav col-lg-8 col-md-8 col-sm-10 col-xs-10 pull-left">
                    <form data-bind="submit: $root.getProducts" class="navbar-form" role="search">
                        <!-- <button data-bind="click: $root.showAdd" type="button" class="btn btn-success pull-left" data-toggle="modal" data-target="#addsupplier"><i class="fa fa-plus"></i> Add Supplier</button> -->
                        <div class="form-group pull-left">
                            <input data-bind="textInput: search_str" autocomplete="off" type="text" class="form-control exp-search" placeholder="Search" />
                        </div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        <span data-bind="visible: search_str()" class="search_result">Search Result for <span data-bind="text: search_str" class="text-info"></span> </span>
                    </form>
                </div>
                <div class="icon_gen">
                    <a href="#details" class="btn btn-default btn-flat show_info selected" data-original-title="Show Details" data-toggle="tooltip" data-placement="left" title="Show Details"><i class="fa fa-info-circle"></i></a>
                </div>
            </section>
            <section class="content">
                <div class="row body-content">
                    <div class="">
                        <div class="z-depth-2">
                            <div class="horizon">
                                <div class="form-inline dt-bootstrap">
                                    <div class="col-md-8 col-sm-12 col-xs-12 detailed-col" id="origin">
                                        <div class="col-md-4 side-category">
                                            <div class="box-header with-border">
                                                <div class="">
                                                    <h4 class="pull-left">
                                                        <span>Store Items</span>
                                                        <select data-bind="options: storeitem_categories().parent_categories, optionsText: 'category_name', optionsCaption: 'Select Category ...', value: storeitem_categories().parent_category"></select>
                                                        <select data-bind="options: storeitem_categories().categories, optionsText: 'category_name', optionsCaption: 'Select Sub-category ...', value: storeitem_categories().category"></select>
                                                    </h4>
                                                </div>
                                            </div>

                                            <div class="box-body slimscroll no-padding">
                                                <ul class="nav category">
                                                    <div data-bind="foreach: storeitem_categories().storeitems">
                                                        <li data-bind="click: $root.getStoreItem" role="row" data-toggle="modal" data-target="#addinventory">
                                                            <a href="#">
                                                                <!-- <img class="img-circle" class="img-boxed" width="200" height="200" /> -->
                                                                <span data-bind="text: storeitemname"></span>
                                                            </a>                                        
                                                        </li>
                                                    </div>
                                                    <div data-bind="visible: storeitem_categories().storeitems().length == 0">
                                                        <br/><span style="color: red;"><center><b>No result/s found</center></b></span>
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-8 no-padding">
                                            <div class="slimscroll-head">
                                                <table class="table"style="margin-bottom:0;">
                                                    <thead>
                                                        <tr role="row">
                                                            <th data-bind="click: $root.sortResult.bind($data, 'storeitemname')" style="width: 25%;">
                                                                <span data-toggle="tooltip" data-placement="top" title="Sort by inventory item">Inventory Item</span>
                                                                <span class="pull-right">
                                                                    <a data-bind="visible: sort_by() == 'asc' && sort_column() == 'storeitemname'" href="#"><i class="fa fa-angle-up"></i></a>
                                                                    <a data-bind="visible: sort_by() == 'desc' && sort_column() == 'storeitemname'" href="#"><i class="fa fa-angle-down"></i></a>
                                                                </span>
                                                            </th>
                                                            <th style="width: 20%">Price/Unit</th>
                                                            <th class="text-center" style="width: 20%">Stock At Hand</th>
                                                            <th class="text-center" style="width: 20%">Action</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="slimscroll">
                                                <table class="table table-hover">
                                                    <tbody data-bind="foreach: products">
                                                        <tr data-bind="click: $root.getProduct" role="row" class="odd show_info">
                                                            <td data-bind="text: storeitemname" style="width: 25%"></td>
                                                            <td data-bind="text: price_formatted" style="width: 20%"></td>
                                                            <td data-bind="text: stock_at_hand" style="width: 20%" class="text-center"></td>
                                                            <td class="text-center" style="width: 20%">
                                                                <button type="button" class="btn btn-sm btn-primary bg-success" style="border:none;" id="edit" data-toggle="modal" data-target="#editproduct"><i class="fa fa-edit"></i></button>
                                                                <button type="button" class="btn btn-sm btn-danger bg-danger" style="border:none;" data-toggle="modal" data-target="#deleteproduct"><i class="fa fa-trash-o"></i></button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div data-bind="visible: products().length == 0" class="pad10">
                                                    <div class="alert alert-danger">No result/s found.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12" id="details">
                                        <h4>
                                            <p class="pull-left">Product Info</p>
                                            <button type="button" class="close pull-right" data-dismiss="alert">&times;</button>
                                        </h4>

                                        <div class="info pull-left">
                                            <ul class="nav nav-tabs nav-justified">
                                              <li class="active"><a data-toggle="tab" href="#detail">Details</a></li>
                                              <li><a data-toggle="tab" href="#act">Activity</a></li>
                                            </ul>
                                            <div data-bind="visible: !no_product()" class="tab-content">
                                                <div data-bind="with: product" id="detail" class="tab-pane fade in active">
                                                    <br />
                                                    <ul class="li-list">
                                                        <li class="li-item">
                                                            <div class="li-left">Product Name</div>
                                                            <div class="li-body" data-bind="text: storeitemname"></div>
                                                        </li>
                                                        <li class="li-item">
                                                            <div class="li-left">Price</div>
                                                            <div class="li-body" data-bind="text: price_formatted"></div>
                                                        </li>
                                                        <li class="li-item">
                                                            <div class="li-left">Stock at Hand</div>
                                                            <div class="li-body" data-bind="text: stock_at_hand"></div>
                                                        </li>
                                                        <li class="li-item">
                                                            <div class="li-left">Maximum Quantity</div>
                                                            <div class="li-body" data-bind="text: max_stock_qty"></div>
                                                        </li>
                                                        <li class="li-item">
                                                            <div class="li-left">Minimum Quantity</div>
                                                            <div class="li-body" data-bind="text: min_stock_qty"></div>
                                                        </li>
                                                        <li class="li-item">
                                                            <div class="li-left">Reordering Point</div>
                                                            <div class="li-body" data-bind="text: reordering_qty"></div>
                                                        </li>
                                                    </ul>
                                                    <hr />
                                                </div>
                                                <div id="act" class="tab-pane fade">
                                                    <br />
                                                    <ul class="li-list">
                                                        <li data-bind="foreach: logs" class="li-item">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <span data-bind="text: remarks"></span> by 
                                                                    <b><span data-bind="text: display_name"></span></b> on
                                                                    <!-- <span data-bind="text: date_created"></span> -->
                                                                    <span data-bind="text: format_date()"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <hr />
                                                </div>
                                                <p class="text-right">
                                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editproduct"><i class="fa fa-pencil"></i> Edit</a>
                                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteproduct"><i class="fa fa-times"></i> Delete</a>
                                                </p>
                                            </div>
                                            <div data-bind="visible: no_product()" class="tab-content pad10">
                                                <p class="alert alert-info">Choose one record to display info</p>
                                            </div>
                                        </div>
                                    </div> <!-- col-md-4 -->
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    <div> <!-- col-xs-12 -->
                </div> <!-- row -->
            </section>  <!-- content -->
        </div> <!-- content-wrapper -->

        <!-- Confirm delete Product Modal -->
            <div data-bind="with: product" class="modal fade modal-delete" id="deleteproduct" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="mySmallModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body text-center">
                            <p>Are you sure to delete <span data-bind="text: product_name"></span> ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                            <button data-bind="click: $root.deleteProduct" type="button" class="btn btn-danger">Confirm</button>
                        </div>
                    </div>
                </div>
            </div> <!-- Confirm delete modal -->

        <!-- Edit Product Modal -->
            <div data-bind="with: product" class="modal fade modal-edit" id="editproduct" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Edit Product</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }, submit: $root.editProduct' action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3">Store Item</label>
                                    <div class="col-sm-9">
                                        <div data-bind="text: storeitemname"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Stock at hand</label>
                                    <div class="col-sm-9">
                                        <div data-bind="text: stock_at_hand"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Maximum Quantity</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: max_stock_qty"  type="number" min="1" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Minimum Quantity</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: min_stock_qty"  type="number" min="1" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Reordering Point</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: reordering_qty"  type="number" min="1" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- edit modal -->  

            <div class="modal fade modal-delete" id="form_error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h4>Please check your submission.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Add inventory Modal -->
            <div data-bind="with: inventory" class="modal fade modal-add" id="addinventory" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Add to Inventory</h4>
                        </div>
                        <div class="modal-body">
                            <form data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }, submit: $root.addInventory' action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3">Store Item</label>
                                    <div class="col-sm-5">
                                        <input data-bind="value: inventory_id" type="hidden"/>
                                        <div data-bind="text: storeitemname" type="text" class="form-control"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Stock at hand</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: stock_at_hand" type="number" min="1" class="form-control" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 ">Maximum Quantity</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: max_stock_qty, attr: { min: reordering_qty() }" type="number" min="1"  class="form-control" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3">Minimum Quantity</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: min_stock_qty, attr: { max: reordering_qty(), min: 0 }" type="number" min="1" class="form-control" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3">Reordering Point</label>
                                    <div class="col-sm-9">
                                        <input data-bind="textInput: reordering_qty, attr: { max: max_stock_qty(), min: min_stock_qty() }" type="number" min="1"  class="form-control" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                                        <button type="submit" class="btn btn-success pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- Add inventory modal -->

        <script id="customMessageTemplate" type="text/html">
            <em class="text-warning" data-bind='validationMessage: field'></em>
        </script>