<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content pos">
				<div class="row" id="no-print">
					<div class="">
						<div class="z-depth-1">
							<div id="pos" class="horizon">
								<!-- order and orderlines -->
								<div data-bind='validationOptions: { messageTemplate: "errMsgTemplate" }' class="col-md-7 orderline no-padding">
									<div class="">
										<h4>
											<span data-bind="visible: order().new_order()">New Order</span>
											<span data-bind="visible: !order().new_order()">Order #<span data-bind="text: order().order_id()"></span></span>
											<a data-bind="visible: !no_selected_customer()" href="#" class="pull-right">
												Customer: <span data-bind="click: $root.showSelectCustomer, text: selected_customer().fullname"></span>
											</a>
										</h4>
										<div class="list-item-head">
											<table class="table" style="margin-bottom:0;">
												<thead>
													<tr role="row" class="success">
														<th style="width:40%;">Product</th>
														<th style="width:15%;" class="text-right">Price</th>
														<th style="width: 15%;" class="text-center">Quantity</th>
														<th style="width:15%;" class="text-right">Subtotal</th>
														<th style="width:15%;" class="text-center">Action</th>
													</tr>
												</thead>
											</table>
										</div>
										<div class="list-item-content">
											<table class="table table-hover" role="grid">
												<tbody data-bind="foreach: order().orderlines">
													<tr data-bind="visible: !hold_removed()">
														<td style="width:40%;"><span data-bind="text: storeitemname"></span></td>
														<td style="width:15%;" class="text-right"><span data-bind="text: price_per_unit_formatted"></span></td>
														<td style="width:15%;" class="text-left">
															<input data-bind="textInput: quantity, attr: { disabled: hold_removed() }" class="form-control" style="text-align:center; font-size: 18px;" type="number" min="1" />
															<span data-bind="visible: !quantity_valid()"><em class="text-warning">Must not exceed <span data-bind="text: stock_at_hand"></span></em></span>
														</td>
														<td style="width:15%;" class="text-right"><span data-bind="text: subtotal"></span></td>
														<td style="width:15%;" class="text-center">
															<button data-bind="click: $root.confirmDeleteOrderline" type="button" class="tip btn btn-danger btn-sm" id="delete" data-toggle="modal" data-target="#deleteitem"><i class="fa fa-times"></i></button>
														</td>
													</tr>
												</tbody>
											</table>
											<!-- <pre data-bind="text: ko.toJSON(order)"></pre> -->
											<div data-bind="visible: order().orderlines().length == 0" class="pad10 text-center">
												<div class="alert alert-danger">No orderlines yet.</div>
											</div>
										</div>

										<div class="totaldiv">
											<table class="table table-striped">
												<tr class="success">
													<td colspan="2" style="font-size: 24px;">Total Payable</td>
													<td class="text-right" colspan="2"><span data-bind="text: order().total_amount" style="font-size: 24px;" id="total"></span></td>
												</tr>
											</table>
										</div>

										<div class="col-xs-12 text-center" style="padding:0 8px;">
											<form data-bind="submit: $root.scanStoreItem" class="">
												<div class="form-group col-xs-9" style="margin-bottom:5px; padding: 0 5px 0 0;">
													<input data-bind="textInput: sku" type="text" id="add_item" class="form-control" placeholder="Enter barcode" required />
												</div>
												<div class="col-xs-3 no-padding">
													<button type="submit" class="btn btn-success btn-block btn-flat">Scan</button>
												</div>
											</form>
										</div>

										<div id="botbuttons" class="col-xs-12 text-center" style="padding:0 8px;">
											<div class="">
												<div class="col-xs-6" style="padding: 0 5px 0 0;">
													<div class="btn-group-vertical btn-block">
														<button type="button" class="btn btn-warning btn-block btn-flat" id="suspend" data-toggle="modal" data-target="#holdorder">Hold</button>
													</div>
												</div>
												<div class="col-xs-6" style="padding: 0;">
													<div class="btn-group-vertical btn-block">
														<button type="button" class="btn btn-danger btn-block btn-flat" id="reset" data-toggle="modal" data-target="#cancelorder">Cancel</button>
													</div>
												</div>
												<div class="col-xs-6" style="padding: 0 5px 0 0; margin-top:5px;">
													<a data-bind="click: $root.showSelectCustomer" href="#" class="btn btn-success btn-block btn-flat">Customer</a>
												</div>
												<div class="col-xs-6" style="padding:0; margin-top:5px;">
													<button type="button" class="btn btn-success btn-block btn-flat" data-toggle="modal" data-target="#cashpayment">Payment</button>
												</div>
											</div>
										</div>
									</div>
								</div> <!-- end order and orderlines -->
								<!-- onhold orders, categories and storeitems -->
								<div class="col-md-5 no-padding">
									<section class="content-header">
										<div>
											<ul class="">
												<li class="inline"><a data-bind="click: $root.getAllItems.bind($data, 0)" href="#" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-inbox"></i> Today's Menu</a></li>
												<li class="inline"><a data-bind="click: $root.getOrders" href="#" class="btn btn-warning btn-flat btn-sm"><i class="fa fa-inbox"></i> Onhold Orders</a></li>
											</ul>
											<ul class="">
												<li class="inline"><a data-bind="click: $root.getAllItems.bind($data, 1)" href="#" class="btn btn-success btn-flat btn-sm"><i class="fa fa-th"></i> Categories</a></li>
												<li class="inline"><a data-bind="click: $root.getMostSoldItems" href="#" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-line-chart"></i> Most Sold Items</a></li>
											</ul>
											<ol data-bind="foreach: category_crumbs" class="breadcrumb no-margin">
												<li data-bind="visible: ($index() == 0)">
													<a data-bind="click: $root.getCategory" href="#" class="btn btn-default btn-flat btn-sm"><i class="fa fa-folder"></i> <span data-bind="text: category_name"></span></a>
												</li>
												<li data-bind="visible: ($index() == 1)"><a href="#" class="btn btn-default btn-flat btn-sm"><i class="fa fa-folder"></i> <span data-bind="text: category_name"></span></a></li>
											</ol>
										</div>
									</section>
									<!-- Product Categories Pane -->
									<div id="product-categories" class="box-body no-padding">
										<div class="box box-success">
											<div class="box-body no-padding">
												<div class="slimscroll" style="height: 70vh;">
													<table data-bind="visible: most_sold() != 1 " class="table table-hover">
														<tbody data-bind="visible: no_category(), foreach: categories"> 
															<tr data-bind="click: $root.getCategory">
																<td>
																	<a href="#">
																		<img data-bind="attr: { src: img_src }" width="50" height="50">&emsp;
																		<span data-bind="text: category_name"></span>
																	</a>
																</td>
															</tr>
														</tbody>
														<tbody data-bind="visible: !no_category(), foreach: category().sub_categories"> 
															<tr data-bind="click: $root.getCategory">
																<td>
																	<a href="#">
																		<img data-bind="attr: { src: img_src }" width="50" height="50">&emsp;
																		<span data-bind="text: category_name"></span>
																	</a>
																</td>
															</tr>
														</tbody>
													</table>
													<table class="table table-hover">
														<tbody data-bind="foreach: storeitems">
															<tr data-bind="click: no_stock() ? null : $root.addOrderline" class="pointer">
																<td style="width: 20%;">
																	<img data-bind="attr: { src: img_src, alt: storeitemname }" width="50" height="50">&emsp;
																</td>
																<td style="width: 80%;">
																	<span data-bind="text: storeitemname"></span>
																	<span data-bind="visible: !no_stock()" title="available">&emsp;<span data-bind="text: stock_at_hand" class="label bg-green"></span></span>
																	<span data-bind="visible: no_stock()">&emsp;<span class="label bg-red">out of order</span></span>
																	<br />
																	<span data-bind="text: sku"></span> &emsp;
																	<span data-bind="text: price_formatted" class="label bg-blue"></span>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<!-- /.box-body -->
										</div>
									</div><!-- Product Categories Pane end -->
									<!-- Onhold Orders Pane -->
									<div id="onhold-orders" class="box-body no-padding">
										<!-- <code data-bind="text: ko.toJSON(orders)"></code> -->
										<div class="box-body no-padding">
											<div class="slimscroll">
												<table class="table table-hover">
													<thead>
														<tr>
															<th>Order #</th>
															<th>Date</th>
														</tr>
													</thead>
													<tbody data-bind="foreach: orders"> 
														<tr class="pointer">
															<td style="width: 5%;"><span data-bind="text: order_id"></span></td>
															<td><span data-bind="text: format_time"></span></td>
															<td>
																<a data-bind="click: $root.getOrderlines" href="#" class="btn btn-success btn-sm">continue</a>
																<a data-bind="click: $root.confirmDiscard" href="#" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
															</td>
														</tr>
													</tbody>
												</table>
												<div data-bind="visible: orders().length == 0" class="pad10"><div class="alert alert-danger">No onhold order yet.</div></div>
											</div>
										</div>
									</div><!-- Onhold Orders Pane end -->
								</div><!-- end of onhold orders, categories and storeitems -->
								<div class="col-md-12 pos-logs">
									<table>
										<tbody>
											<tr>
												<td><big>Today's POS</big>&emsp;&emsp;</td>
												<td><b>Staff Assigned:</b> <big><?php echo $this->session->userdata('username'); ?>&emsp;</big></td>
												<td><b>Total Transactions: </b><big data-bind="text: sales_today().length"></big>&emsp;</td>
												<td><b>Total Items: </b><big data-bind="text: overall_items"></big>&emsp;</td>
												<td><b>Total Sales: </b><big data-bind="text: overall_sales_formatted"></big></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div id="select-customer" class="horizon">
								<div class="col-md-12">
									<div class="box	">
										<div class="box-header">
											<h4>
												<span data-bind="visible: no_customer()">Select Customer</span>
												<span data-bind="visible: !no_customer()">
													Selected: 
													<span data-bind="text: customer().fullname"></span>
													<div class="btn-group">
														<a data-bind="click: $root.selectCustomer" href="#" class="btn btn-success btn-flat btn-sm">select</a>
														<a data-bind="visible: !no_selected_customer(), click: $root.removeSelectedCustomer" href="#" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-times"></i></a>
														<a data-bind="click: $root.cancelSelectCustomer" href="#" class="btn btn-default btn-flat btn-sm">cancel</a>
													</div>
												</span>
												<span class="pull-right">
													<button data-bind="click: $root.showAdd" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addcustomer" >
														<i class="fa fa-5px fa-plus-circle"></i> New Customer
													</button>&emsp;
													<button data-bind="click: $root.cancelSelectCustomer" type="button" class="close">&times;</button>
												</span>
											</h4>
										</div>
										<div class="box-body no-padding">
											<div class="slimscroll">
												<table class="table table-condensed table-hover">
													<tbody data-bind="foreach: customers">
														<tr data-bind="click: $root.getCustomer">
															<td><span data-bind="text: fullname"></span</td>
															<td><span data-bind="text: email"></span></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Payment Modal -->
				<div data-bind="visible: order().valid_order()" class="modal fade" id="cashpayment" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header" id="no-print">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="mySmallModalLabel">Order Checkout</h4>
							</div>
							<div data-bind="visible: order().valid_order()" class="modal-body">
								<div class="row">
									<div id="printContent" class="col-xs-8">
										<div class="no-padding">
											<div class="head-receipt text-center" style="display:none;">
												<div id="logo" class="text-center">
													<img src="<?php echo base_url('resource'); ?>/images/logo.png">
												</div>
												<h4>R's Food Avenue</h4>
												<p>Mobolo Cebu City</p>
												<p>Tel. No. <span>236-1234</span></p>
												<h4>OFFICIAL RECEIPT</h4>
												<table class="table text-left">
													<tbody>
														<tr>
															<td style="width:20%;" class="text-left">OR No.</td>
															<td>01234567890</td>
														</tr>
														<tr>
															<td style="width:20%;" class="text-left">Staff:</td>
															<td><?php echo $this->session->userdata('display_name'); ?></td>
														</tr>
														<tr>
															<td style="width:20%;" class="text-left">Customer:</td>
															<td>
																<span data-bind="visible: no_selected_customer()">1</span>
																<span data-bind="visible: !no_selected_customer(), text: selected_customer().fullname"></span>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<table class="table table-condensed" role="grid">
												<thead>
													<tr role="row" class="success">
														<th style="width:40%;">Product</th>
														<th style="width:15%;" class="text-right">Price</th>
														<th style="width: 15%;" class="text-center">Quantity</th>
														<th style="width:15%;" class="text-right">Subtotal</th>
													</tr>
												</thead>
												<tbody data-bind="foreach: order().orderlines">
													<tr data-bind="visible: !hold_removed()">
														<td style="width:40%;"><span data-bind="text: storeitemname"></span></td>
														<td style="width:15%;" class="text-right"><span data-bind="text: price_per_unit_formatted"></span></td>
														<td style="width:15%;" class="text-center">
															<span data-bind="text: quantity"></span>
														</td>
														<td style="width:15%;" class="text-right"><span data-bind="text: subtotal"></span></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="totaldiv">
											<table class="table table-condensed">
												<tr class="success">
													<td colspan="2" style="font-size: 18px;">Total Php</td>
													<td class="text-right" colspan="2"><span data-bind="text: order().total_amount" style="font-size: 18px;" id="total"></span></td>
												</tr>
												<tr>
													<td colspan="2">VAT 12%</td>
													<td class="text-right" colspan="2"><span data-bind="text: order().vat"></span></td>
												</tr>
												<tr>
													<td colspan="2">Vatable Amount</td>
													<td class="text-right" colspan="2"><span data-bind="text: order().vatable_amount"></span></td>
												</tr>
												<tr>
													<td colspan="2">Tendered Amount</td>
													<td class="text-right" colspan="2"><span data-bind="text: order().tender_amount"></span></td>
												</tr>
												<tr>
													<td colspan="2">Change</td>
													<td class="text-right" colspan="2"><span data-bind="text: order().change"></span></td>
												</tr>
											</table>
										</div>
										<div class="foot-receipt text-center" style="display:none;">
											<p>Customer Copy</p>
											<p>Thank you. Come Again!</p>
											<h4>Issued on: 09/14/2012 12:00 PM</h4>
										</div>
									</div>
									<div class="col-xs-4" id="no-print">
										<form data-bind="visible: order().valid_order(), submit: $root.checkoutOrder" action="#" method="post">
											<div class="form-group">
												<label>Input Cash</label>
												<input id="tender-cash" data-bind="textInput: order().tender_amount, attr: { min: order().total_amount, title: 'Insufficient amount entered' }" type="number" class="form-control" required />
											</div>
											<div class="form-group text-right">
												<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Back</button>
												<button type="submit" class="btn btn-success">Checkout</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div data-bind="visible: !order().valid_order()" class="modal-body">
								<h3 class="text-center" style="color:red;">Error! Please check order to continue.</h3>
							</div>
						</div>
					</div>
				</div> <!-- payment modal -->

				<!-- Print receipt -->
				<div class="modal fade modal-delete" id="printreceipt" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title text-center" id="mySmallModalLabel">Print receipt?</h4>
							</div>
							<div class="modal-footer">
								<button data-bind="click: $root.cancelPrint" type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
								<button data-bind="click: $root.printReceipt" type="button" class="btn btn-danger">Yes</button>
							</div>
						</div>
					</div>
				</div> <!-- Print receipt end -->

				<!-- Scan Modal -->
				<div class="modal fade" id="scan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button data-bind="click: $root.cancelScan" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title text-center" id="mySmallModalLabel">Orderline</h4>
							</div>
							<div data-bind='validationOptions: { messageTemplate: "errMsgTemplate" }' class="modal-body">
								<h2 data-bind="visible: no_orderline()" class="text-center text-danger">Sorry!<br /> Item not found.</h2>
								<div data-bind="visible: storeitem().no_stock() && !no_orderline()">
									<h2 class="text-center text-danger">Sorry!</h2> 
									<h4 class="text-center">
										<span data-bind="text: storeitem().storeitemname"></span> is out of order for now.<br />
									</h4>
								</div>
								<form data-bind="visible: !no_orderline() && !storeitem().no_stock(), submit: $root.saveOrderline" action="#" method="post">
									<h3 data-bind="text: storeitem().sku"></h3>
									<h2 data-bind="text: orderline().storeitemname"></h2><br />
									<div class="form-group">
										<label>Unit Price</label>
										<input data-bind="value: orderline().price_per_unit" type="text" class="form-control" disabled />
									</div>
									<div class="form-group">
										<label>Quantity</label>
										<input data-bind="textInput: orderline().quantity" class="form-control" />
										<span data-bind="visible: !orderline().quantity_valid()"><em class="text-warning">Must not exceed <span data-bind="text: orderline().stock_at_hand"></span></em></span>
									</div>
									<!-- <code data-bind="text: ko.toJSON(storeitem)"></code> -->
									<hr />
									<div data-bind="visible: !no_orderline()" class="form-group text-right">
										<button data-bind="click: $root.cancelScan" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Back</button>
										<button data-bind="attr: { disabled: !orderline().valid(), title: !orderline().valid() ? 'Invalid quantity' : '' }" type="submit" class="btn btn-success">Add</button>
									</div>
								</form>
							</div>
							<div data-bind="visible: no_orderline()" class="modal-footer">
								<button data-bind="click: $root.cancelScan" type="button" class="btn btn-danger " data-dismiss="modal">Back</button>
							</div>
						</div>
					</div>
				</div> <!-- scan modal -->

				<!-- Hold Order -->
				<div class="modal fade modal-delete" id="holdorder" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title text-center" id="mySmallModalLabel">Confirm Hold Order</h4>
							</div>
							<div data-bind="visible: !order().valid_order()" class="modal-body">
								<p data-bind="visible: order().orderlines().length > 0" class="text-danger"><b>Something's not right.</b> Please check the order to continue.</p>
								<p data-bind="visible: order().orderlines().length == 0">No orderlines yet. Please add at least 1.</p>
							</div>
							<div data-bind="visible: !order().valid_order()" class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
							</div>
							<div data-bind="visible: order().valid_order()" class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
								<button data-bind="click: $root.holdOrder" type="button" class="btn btn-danger">Hold</button>
							</div>
						</div>
					</div>
				</div> <!-- Hold Order end -->

				<!-- Confirm Cancel Order -->
				<div class="modal fade modal-delete" id="cancelorder" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title text-center" id="mySmallModalLabel">Confirm Discard Order</h4>
							</div>
							<div class="modal-body">
								<p data-bind="visible: order().orderlines().length > 0" class="text-danger"><big>Warning.</big> This action will discard your current orderlines. Confirm to continue.</p>
								<p data-bind="visible: order().orderlines().length == 0">No orderline yet. Please add at least 1.</p>
							</div>
							<div data-bind="visible: order().orderlines().length > 0" class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
								<button data-bind="click: $root.cancelOrder" type="button" class="btn btn-danger">Discard</button>
							</div>
							<div data-bind="visible: order().orderlines().length == 0" class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
							</div>
						</div>
					</div>
				</div> <!-- Confirm Cancel Order end -->

				<!-- Confirm Cancel Order -->
				<div class="modal fade modal-delete" id="discardorder" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title text-center" id="mySmallModalLabel">Confirm Discard Order</h4>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
								<button data-bind="click: $root.discardOrder" type="button" class="btn btn-danger">Discard</button>
							</div>
						</div>
					</div>
				</div> <!-- Confirm Cancel Order end -->

				<!-- delete item from orderline -->
				<div class="modal fade modal-delete" id="deleteitem" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title text-center" id="mySmallModalLabel">Remove <span data-bind="text: orderline().storeitemname"></span> ?</h4>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
								<button data-bind="click: $root.removeOrderline" type="button" class="btn btn-danger">Remove</button>
							</div>
						</div>
					</div>
				</div> <!-- Delete item from orderline -->

				<!-- Add Customer Modal -->
	            <div data-bind="with: customer" class="modal fade modal-add" id="addcustomer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-keyboard="false" data-backdrop="static">
	                <div class="modal-dialog " role="document">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                            <h4 class="modal-title text-center" id="mySmallModalLabel">Add Customer</h4>
	                        </div>
	                        <div class="modal-body">
	                            <form data-bind='validationOptions: { messageTemplate: "customMessageTemplate" }, submit: $root.addCustomer' action="#" method="post" class="form-horizontal">
	                                <div class="form-group">
	                                    <label class="col-sm-3 ">First Name:</label>
	                                    <div class="col-sm-9">
	                                        <input data-bind="textInput: firstname" type="text" class="form-control" />
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                    <label class="col-sm-3 ">Last Name:</label>
	                                    <div class="col-sm-9">
	                                        <input data-bind="textInput: lastname" type="text" class="form-control" />
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                    <label class="col-sm-3">Address:</label>
	                                    <div class="col-sm-9">
	                                        <input data-bind="textInput: address" type="text" class="form-control" />
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                    <label class="col-sm-3">Email</label>
	                                    <div class="col-sm-9">
	                                        <input data-bind="textInput: email" type="email" class="form-control" />
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                    <label class="col-sm-3 ">Gender</label>
	                                    <div class="col-sm-9">
	                                        <label for="r_male"><input data-bind="checked: gender" type="radio" id="r_male" name="gender" checked value="1" /> Male</label>
	                                        <label for="r_female"><input data-bind="checked: gender" type="radio" id="r_female" name="gender" value="0" /> Female</label>
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                    <label class="col-sm-3 ">Senior Citizen?</label>
	                                    <div class="col-sm-9">
	                                        <input data-bind="checked: is_senior" type="checkbox" />
	                                        <p data-bind="visible: isSenior()">
	                                            <br />
	                                            <label>Please input Card No</label>
	                                            <input data-bind="textInput: card_no, attr: { required: isSenior() }" class="form-control" />
	                                        </p>
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                    <div class="col-md-12">
	                                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
	                                        <button type="submit" class="btn btn-success pull-right">Save</button>
	                                    </div>
	                                </div>
	                            </form>
	                        </div>
	                    </div>
	                </div>
	            </div> <!-- add modal -->

	            <div class="modal fade modal-delete" id="customer_exist" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
	                <div class="modal-dialog modal-sm" role="document">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fa fa-exclamation-circle"></i> Error</h4>
	                        </div>
	                        <div class="modal-body text-center">
	                            <h4>Cannot save customer details.</h4>
	                            <p>Customer already exists. Please try another one.</p>
	                        </div>
	                    </div>
	                </div>
	            </div>
			</section>

		</div>
		 <script id="customMessageTemplate" type="text/html">
			<br>
			<em class="text-warning" data-bind='validationMessage: field'></em>
		</script>
		<script id="errMsgTemplate" type="text/html">
			<em class="text-warning" data-bind='validationMessage: field'></em>
		</script>