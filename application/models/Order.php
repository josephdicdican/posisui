 <?php

class Order extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	/**
	* Get list of orders with is_checkout condition (optional default 0)
	* to get onhold orders is_checkout value = 1
	* @param $is_checkout int
	* @param $range int 1 = TODAY, 2 = WEEK, 3 = MONTH
	* @param $date_from string
	* @param $date_to string
	* @return array
	*/
	public function get_orders($is_checkout = 0, $range = 0, $date_from = null, $date_to = null, $admin_access = false) 
	{
		$this->db->select()->from('orders_transactions_list')->where('is_checkout', $is_checkout);
		
		switch($range) {
			case 1: // get orders TODAY 
				$this->db->where('DATE(date_of_order) = CURDATE()', null, false);
				break;

			case 2: // get orders WEEK mysql >> DATE_SUB(NOW(), INTERVAL 1 WEEK)
				$this->db->where('date_of_order > DATE_SUB(NOW(), INTERVAL 1 WEEK)', null, false);
				break;

			case 3:  // get orders MONTH mysql >> DATE_SUB(NOW(), INTERVAL 1 MONTH)
				$this->db->where('date_of_order > DATE_SUB(NOW(), INTERVAL 1 MONTH)', null, false);	

			case 0:
			default:
				break;
		}

		$query = $this->db->get();


		return $query->result_array();
	}

	/**
	* Gets orderlines by order_id passed
	* @param $order_id int
	* @return array
	*/
	public function get_orderlines($order_id)
	{
		$this->db->select()->from('orderlines_list')->where('order_id', $order_id);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_all_transaction($limit = 0)
	{
		$this->db->select()->from('orders_transactions_list');
		$this->db->order_by('date_of_order', 'DESC');
		if($limit > 0) {
			$this->db->limit($limit);
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	* Adds order and corresponding orderlines of it, discount info included if has
	* if discount_id not 0 discount info will be added
	* @param $order array
	* @param $orderlines array
	* @param $discount_id int default 0
	*/
	public function add_order($order, $orderlines, $discount_id = 0, $customer_id = 0)
	{
		$this->db->trans_start();
		
		$this->db->insert('orders', $order);
		$order_id = $this->db->insert_id();

		$lines = $this->add_orderlines($order_id, $orderlines, $order['is_checkout']);

		if($discount_id > 0) {
			$this->add_order_discount($order_id, $discount_id);
		}

		if($customer_id != 0) {
			$customer = array(
					'order_id' => $order_id,
					'customer_id' => $customer_id
				);
			$this->add_customer($customer);
		}

		$this->add_order_log($order, $order_id, $discount_id);

		$this->db->trans_complete();

		return $lines;
	}
		/**
		* Adds orderlines of order with order_id passed
		* @param $order_id int
		* @param $orderlines array
		* @param $is_checkout int 
		*/
		private function add_orderlines($order_id, $orderlines, $is_checkout = 0) 
		{
			$lines = 0;

			if($order_id && empty($orderlines) == false) {
				foreach($orderlines as $line) {
					$line['order_id'] = $order_id;
					$this->add_orderline($line);
					
					if($is_checkout) {
						$this->update_inventory($line);
					}
					
					$lines++;
				}
			}

			return $lines;
		}

		/**
		* Add customer to an order
		* @param $customer array
		* @return int
		*/
		private function add_customer($customer) {
			$this->db->insert('ordercustomers', $customer);	

			return $this->db->insert_id();
		}

	/**
	* Updates order and orderlines below it
	* Adds discount if has or update or remove
	* @param $order_id int
	* @param $order array
	* @param $orderlines array 
	* @param $discount_id int
	* @return boolean
	*/
	public function update_order($order_id, $order, $orderlines, $discount_id = 0) 
	{
		$this->db->trans_start();

		$this->db->where('order_id', $order_id);
		$this->db->update('orders', $order);

		$flag = $this->process_orderlines($order_id, $orderlines, $order['is_checkout']);

		$this->add_order_log($order, $order_id, $discount_id);

		$this->db->trans_complete();

		return $flag;
	}

		/**
		* Process each orderline accordingly (either add, update or delete) based on conditions
		* Add orderline if line_id = -1, Update orderline if line_id > 0 && quantity > 0, Delete orderline if quantity = -1
		* Updates inventory if is_checkout 1 (only applies to add or update)
		* @param $order_id int
		* @param $orderlines array
		* @param $is_checkout int
		*/
		private function process_orderlines($order_id, $orderlines, $is_checkout = 0)
		{
			if($order_id && empty($orderlines) == false) {
				foreach($orderlines as $line) {
					$flag_inventory_update = true;
					$line['order_id'] = $order_id;

					if(floatval($line['line_id']) == -1) { // Add orderline if line_id = -1
						unset($line['line_id']);
						$this->add_orderline($line);
					} else if(floatval($line['line_id']) > 0 && floatval($line['quantity']) > 0) { // Update orderline if line_id > 0 && quantity > 0
						$this->update_orderline($line);
					} else if(floatval($line['quantity']) == -1) { // Delete orderline if quantity = -1
						$this->delete_orderline($line['line_id']);
					}

					if($flag_inventory_update) { // updates inventory
						$this->update_inventory($line);
					}
				}
			}

			return true;
		}

	public function discard_order($order_id) 
	{
		$this->db->where('order_id', $order_id);
		$this->db->delete('orders');

		return $this->db->affected_rows();
	}
	
	public function delete_expired_onhold_orders()
	{
		$this->db->where('date_of_order < (NOW() - INTERVAL 10 MINUTE)', null, false);
		$this->db->where('is_checkout', 0);
		$this->db->delete('orders');

		return $this->db->affected_rows();
	}

	/**
	* Add an orderline to orderlines table
	* @param $orderline array
	*/
	private function add_orderline($orderline)
	{
		$this->db->insert('orderlines', $orderline);
	}

	/**
	* Updates an orderline from orderlines table
	* @param $orderline array
	*/
	private function update_orderline($orderline) 
	{
		$this->db->where('line_id', $orderline['line_id']);
		$this->db->update('orderlines', $orderline);
	}

	/**
	* Deletes an orderline from orderlines table by line_id passed
	* @param $Line_id int
	*/
	private function delete_orderline($line_id)
	{
		$this->db->where('line_id', $line_id);
		$this->db->delete('orderlines');
	}

	/**
	* Adds order discount info to discountedorders table
	* @param $order_id int
	* @param $discount_id int
	*/
	private function add_order_discount($order_id, $discount_id)
	{
		$discounted_order = array('order_id' => $order_id, 'discount_id' => $discount_id);
		$this->db->insert('discountedorders', $discounted_order);
	}

	/**
	* Adds order log of every order either hold or checked out
	* @param $order array
	* @param $order_id int
	* @param $discount_id int
	*/
	private function add_order_log($order, $order_id, $discount_id = 0) 
	{
		$order_log = array(
				'user_id' => $this->session->userdata('user_id'),
				'order_id' => $order_id
			);

		$remarks = 'Hold order ' . $order_id;

		if($order['is_checkout']) {
			$discount_remarks = ($order['is_discounted']) ? ' with discount of Discount #' . $discount_id : '';
			$remarks = 'Checked out order ' . $order_id . $discount_remarks;
		} 

		$order_log['remarks'] = $remarks;

		$this->db->insert('orderlogs', $order_log);
	}

	/**
	* Updates stock_at_hand of inventories table with orderline's quantity 
	* @param $orderline array
	*/
	private function update_inventory($orderline) 
	{
		$this->db->set('stock_at_hand', 'stock_at_hand-'.$orderline['quantity'], false);
		$this->db->where('inventory_id', $orderline['store_item_id']);
		$this->db->update('inventories');
	}
}