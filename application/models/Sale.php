<?php

class Sale extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	public function get_totals() 
	{
		$this->db->select('count(distinct order_id) as transactions, sum(quantity) as items, truncate(sum(quantity * price_per_unit),2) as sales');
		$this->db->from('storeitems_sales');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_intervals($interval_type = 'week')
	{
		$fields = ''.$interval_type.' as interval, sum(quantity) as items, truncate(sum(quantity * price_per_unit),2) as sales';
		$this->db->select($fields);
		$this->db->from('storeitems_sales')->distinct();
		$this->db->group_by($interval_type);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_transaction_sales($interval_type = 0, $interval = 0, $is_report = false)
	{
		$fields = 'order_id, date_of_order, sum(quantity) as items, truncate(sum(quantity * price_per_unit),2) as sales, week, month, year';
		if($is_report) {
			$fields = 'order_id, date_of_order, sum(quantity) as items, truncate(sum(quantity * price_per_unit),2) as sales';
		}
		
		if($interval_type) {
			$this->db->where($interval_type, $interval);
		}

		$this->db->select($fields)->from('storeitems_sales');
		$this->db->group_by('order_id');
		$this->db->order_by('date_of_order', 'DESC');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_storeitems_sales($interval_type = 0, $interval = 0, $is_report = false)
	{
		$fields = 'store_item_id, sku, storeitemname, sum(quantity) as items, truncate(sum(quantity * price_per_unit),2) as sales, date_of_order, week, month, year';
		if($is_report) {
			$fields = 'store_item_id, sku, storeitemname, sum(quantity) as items, truncate(sum(quantity * price_per_unit),2) as sales';
		}
		
		if($interval_type) {
			$this->db->where($interval_type, $interval);
		}

		$this->db->select($fields)->from('storeitems_sales');
		$this->db->group_by('store_item_id');
		$this->db->order_by('date_of_order', 'DESC');
		$query = $this->db->get();

		return $query->result_array();
	}
}