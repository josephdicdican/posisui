<?php

class Account extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	private $table = 'users_vw';

	/**
	* Get a user by username and password
	* @param $username string
	* @param $password string
	* @return array (user instance)
	*/
	public function login($username, $password)
	{
		$data = array('username' => $username, 'password' => $password);
		$this->db->from($this->table)->where($data);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	public function check_user_exist($username)
	{
		$this->db->select()->from("users");
		$this->db->where('username', $username);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	public function change_password($password, $user_id)
	{
	
		$pw1=sha1($password);

 		$this->db->trans_start();

		$this->db->where('user_id',$user_id);
		$this->db->set('password', $pw1);
		$this->db->update('users');
		$this->db->trans_complete();

		return $this->db->affected_rows();
		//-------------
	}
	
}