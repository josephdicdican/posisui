<?php

class Customer extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	private $table = 'customers';

	/**
	* Gets all the customers in record
	* @param none
	* @return array (array of customers)
	*/
	public function get_all_customers($search_str = null, $sort_column = null, $sort_by = null) 
	{
		$this->db->select()->from('customers_list');

		if($search_str) {
			$this->db->or_like('firstname', $search_str, '%', true);
			$this->db->or_like('lastname', $search_str, '%', true);
			$this->db->or_like('email', $search_str, '%', true);
		}

		if($sort_column != null) {
			if($sort_column == 'fullname') {
				$this->db->order_by('firstname', $sort_by);
				$this->db->order_by('lastname', $sort_by);
			} else {
				$this->db->order_by($sort_column, $sort_by);
			}
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	* Gets a customer with the customer_id passed
	* @param $customer_id int
	* @return array (customer instance)
	*/
	public function get_customer($customer_id)
	{
		$this->db->from('customers_list')->where('customer_id', $customer_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	/**
	* Adds new customer in customers table and insert customer's card_number  
	* in SeniorCitizens table if customer is a senior citizen
	* @param $customer array
	* @return int
	*/
	public function add_customer($customer, $senior)
	{
		$this->db->trans_start();
		$flag = false;

		if($this->check_customer_exist($customer['firstname'], $customer['lastname']) == 0) {
			$flag = true;

			$this->db->insert($this->table, $customer);
			$id = $this->db->insert_id();
			$user_id = $this->session->userdata('user_id');

			if ($id != 0) {
				$logs = array(
						'user_id' => $user_id,
						'customer_id' => $id,
						'remarks' => "Added",
						'is_unread' => 1
					);
				$this->db->insert('customerlogs', $logs);

				if ($customer['is_senior']) {
					$senior['customer_id'] = $id;
					$this->db->insert('seniorcitizens', $senior);
				}
			}
		} else {
			$flag = -1;
		}
		
		$this->db->trans_complete();

		return $flag;
	}

	public function check_customer_exist($firstname, $lastname) {
		$condition = array(
				'firstname' => $firstname,
				'lastname'  => $lastname
			);

		$this->db->select()->from($this->table)->where($condition);
		$query = $this->db->get();

		return $query->num_rows();
	}

	/**
	* Updates customer details in customers table
	* @param $customer array
	* @return int
	*/
	public function update_customer($customer, $senior)
	{
		$id = $customer['customer_id'];
		$this->db->trans_start();
		$flag = false;

		if($this->check_customer_exist($customer['firstname'], $customer['lastname']) == 0) {
			$flag = true;

			$this->db->where('customer_id',$id);
			$this->db->update($this->table, $customer);

			$row = $this->_get_senior($id);
			$user_id = $this->session->userdata('user_id');

			if(is_array($row)) {
				if($customer['is_senior']) {
					$this->db->where('customer_id', $id);
					$this->db->update('seniorcitizens', $senior);
				} else {
					$this->_delete_senior($id);
				}
			} else {
				if($customer['is_senior']) {
					$senior['customer_id'] = $id;
					$this->db->insert('seniorcitizens', $senior);
				}
			}

			if ($id != 0) {
				$logs = array(
						'user_id' => $user_id,
						'customer_id' => $id,
						'remarks' => "Updated",
						'is_unread' => 1
					);
				$this->db->insert('customerlogs', $logs);
			}
		} else {
			$flag = -1;
		}

		$this->db->trans_complete();

		// return array($customer, $senior);
		return $flag;
	}

	private function _get_senior($customer_id) 
	{
		$this->db->from('seniorcitizens')->where('customer_id', $customer_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	private function _delete_senior($customer_id) 
	{
		$this->db->where('customer_id', $customer_id);
		$this->db->delete('seniorcitizens');

		return $customer_id;
	}

	/**
	* Deletes customer in customers table 
	* @param $customer_id
	* @return int
	*/
	public function delete_customer($customer_id)
	{
		$cond = array('customer_id' => $customer_id);
		$this->db->trans_start();

		$this->db->delete($this->table, $cond);
		$this->db->delete('seniorcitizens', $cond);
		
		$this->db->trans_complete();

		return $this->db->affected_rows();
	}
}