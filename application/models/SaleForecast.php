<?php

class SaleForecast extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function overall_sales($interval_type = 'week')
	{

	}

	public function get_distinct_intervals($interval_type = 'week') 
	{
		$fields = '' . $interval_type;

		$fields .= $this->get_interval_addon_fields($interval_type);

		$this->db->distinct()->select($fields)->from('storeitems_sales')->order_by($interval_type, 'DESC');
		$query = $this->db->get();

		return $query->result_array();
	}
/*
SELECT
	distinct store_item_id, storeitemname, sku
	from storeitems_sales
	where is_stockable = 0
	order by store_item_id; 
*/
	public function get_distinct_items($is_stockable = 1, $item_type = 'store_item_id') {
		$fields = 'store_item_id, storeitemname, sku';
		
		if($item_type == 'category_id') {
			$fields = 'category_id, category_name';
		}

		$this->db->distinct()->select($fields)->from('storeitems_sales');
		$this->db->where('is_stockable', $is_stockable)->order_by($item_type);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_sales($is_stockable = 1, $interval_type = 'week', $group_by = 'category_id', $id = 0)
	{
		$fields = 'category_id, category_name, SUM(quantity) as sold_items, SUM(quantity * price_per_unit) as total_sales, week, month, year';
		
		if($group_by == 'store_item_id') {
			$fields = 'store_item_id, storeitemname, sku, category_id, category_name, SUM(quantity) as sold_items, SUM(quantity * price_per_unit) as total_sales, week, month, year';
		}

		$fields .= $this->get_interval_addon_fields($interval_type);
		$cond = array('is_stockable' => $is_stockable);

		if($id > 0) {
			$cond[$group_by] = $id;
		}

		$this->db->select($fields)->from('storeitems_sales')->where($cond);
		$this->db->group_by($group_by.', '.$interval_type);
		$this->db->order_by($group_by); 	
		$query = $this->db->get();

		return $query->result_array();
	}

	private function get_interval_addon_fields($interval_type)
	{
		$fields = '';

		switch ($interval_type) {
			case 'week':
				$fields = ' , FROM_DAYS(TO_DAYS(date_of_order) -MOD(TO_DAYS(date_of_order) -2, 7)) AS week_beginning, FROM_DAYS(TO_DAYS(date_of_order) -MOD(TO_DAYS(date_of_order) -1, 7)) AS week_end,';
				break;

			case 'month':
				$fields = ' , DATE(DATE_FORMAT(date_of_order, "%Y-%m-01")) AS month_beginning';
				break;
			
			default:
				# code...
				break;
		}

		return $fields;
	}

/*
SELECT 
	SUM(quantity) as sold_items,
	SUM(quantity * price_per_unit) as total_sales
	from storeitems_sales;

SELECT 
	category_name,
	SUM(quantity) as consumable_sold_items,
	SUM(quantity * price_per_unit) as total_sales,
	week, month, year
	from storeitems_sales 
	where is_stockable = 0 
	group by category_id, week;

SELECT 
	category_name,
	SUM(quantity) as stockable_sold_items,
	SUM(quantity * price_per_unit) as total_sales,
	week, month, year
	from storeitems_sales 
	where is_stockable = 1
	group by category_id, week;
*/
}