<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	private $table = 'categories_list';

	/**
	* Gets list of categories by $parent_id in categories_list view
	* By default gets all parent categories
	* @param $parent_id int
	* @return array
	*/
	public function get_categories($parent_id = 0, $is_stockable = 1)
	{
		$this->db->where('parent_id', $parent_id);
		$this->db->where('is_stockable', $is_stockable);
		$this->db->from($this->table);
		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	* Gets categories and its corresponding sub_categories
	* @return array
	*/
	public function get_categories_sub($is_stockable = 1) 
	{
		$categories = array();
		$list = $this->get_categories(0, $is_stockable); // gets all parent categories

		if(is_array($list)) { // if is array list 
			foreach($list as $item) { // traverse each category and query its sub_categories
				$item['sub_categories'] = $this->get_categories($item['category_id'], $is_stockable);
				$categories[] = $item;
			}
		}

		return $categories;
	}

	/**
	* Gets a category with its sub categories if has
	* @param $category_id int
	* @return array
	*/
	public function get_category($category_id)
	{
		$this->db->where('category_id', $category_id);
		$this->db->from($this->table);
		$query = $this->db->get();

		$category = $query->first_row('array');

		if(is_array($category)) {
			$category['sub_categories'] = $this->get_categories($category['category_id']);
		}

		return $category;
	}

	/**
	* Adds a category in the categories table adding image if has
	* @param $category array
	* @param $image boolean or array
	* @return boolean
	*/
	public function add_category($category, $image) 
	{
		$this->db->insert('categories', $category);
		$category_id = $this->db->insert_id();

		if($image) {
			$image_id = $this->add_image($image);
			$category_image = array(
					'category_id' => $category_id,
					'image_id' => $image_id
				);

			$this->add_category_image($category_image);
		}
	}

	/**
	* Adds an image in the images table and returns inserted id
	* @param $args array
	* @return int
	*/
	private function add_image($args) 
	{
		$this->db->insert('images', $args);

		return $this->db->insert_id();
	}

	/**
	* Adds a category image to categoryimages table
	* @param $args array
	* @return int
	*/
	private function add_category_image($args) 
	{
		$this->db->insert('categoryimages', $args);

		return $this->db->insert_id();
	}

	/**
	* Deletes a category by id
	* Deletes image file from directory if has
	* @param $category_id int
	* @return int
	*/
	public function delete_category($category_id) 
	{
		$image = $this->get_category_image($category_id);

		if($image) {
			$image_data = $this->get_image($image['image_id']);
			$this->delete_image_file($image_data);
			$this->delete_image($image['image_id']);
		}

		$this->db->where('category_id', $category_id);
		$this->db->delete('categories');

		return $this->db->affected_rows();
	}

	/**
	* Updates category from the parameters passed
	* Deletes old image if $image_id not null and $image array not null
	* Adds new image record if $image_id null and $image array not null
	* @param $category_id int
	* @param $category array
	* @param $image_id int
	* @param $image array
	*/
	public function update_category($category_id, $category, $image_id, $image) 
	{
		$this->db->trans_start();
		$this->db->where('category_id', $category_id);
		$this->db->update('categories', $category);

		if($image && $image_id) { // if both have values, replace image with new image
			$image_data = $this->get_image($image_id);
			$this->delete_image_file($image_data); // delete old image from directory

			$this->update_image($image_id, $image);
		} else if ($image && $image_id == null) { // if $image has but $image_id null, add new image 
			$id = $this->add_image($image);

			$category_image = array(
					'category_id' => $category_id,
					'image_id' => $id
				);

			$this->add_category_image($category_image);
		} 

		$this->db->trans_complete();
	}
 	
	/**
	* Gets category image by category id passed
	* @param $category_id int
	* @return array
	*/
	private function get_category_image($category_id)
	{
		$this->db->select()->from('categoryimages')->where('category_id', $category_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	private function get_image($image_id)
	{
		$this->db->select()->from('images')->where('image_id', $image_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	private function update_image($image_id, $image) 
	{
		$this->db->where('image_id', $image_id);
		$this->db->update('images', $image);
	}

	private function delete_image($image_id) 
	{
		$this->db->where('image_id', $image_id);
		$this->db->delete('images');
	}

	private function delete_image_file($image) 
	{
		$file_with_path = $_SERVER['DOCUMENT_ROOT'] .'/posisui/uploads/images/' . $image['image'];

		if(file_exists($file_with_path)) {
			unlink($file_with_path);
		}
	}
}