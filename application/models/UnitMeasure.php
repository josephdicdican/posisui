<?php

class UnitMeasure extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	private $table = 'unitmeasures';

	public function get_unit_measures()
	{
		$this->db->select()->from('unitmeasure_storeitems');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_unit_measure($unit_id)
	{
		$this->db->select()->from($this->table)->where('unit_id', $unit_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	public function add_unit_measure($unit_measure)
	{
		$this->db->insert($this->table, $unit_measure);

		return $this->db->insert_id();
	}

	public function edit_unit_measure($unit_id, $args)
	{
		$this->db->where('unit_id', $unit_id);
		$this->db->update($this->table, $args);

		return $this->db->affected_rows();
	}
}