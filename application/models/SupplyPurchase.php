<?php

class SupplyPurchase extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	/**
	* Gets all supply purchases (optional filter with $is_confirmed true | false)
	* @param $is_confirmed boolean 
	* @return array
	*/
	public function get_supply_purchases($is_confirmed = null)
	{
		$this->db->select()->from('supplypurchase_list');

		if($is_confirmed != '') {
			$this->db->where('is_confirmed', $is_confirmed);
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	* Gets supply purchase lines by $supply_purchase_id
	* @param $supply_purchase_id int
	* @return array
	*/
	public function get_supply_purchase_lines($supply_purchase_id) 
	{
		$this->db->select()->from('supplypurchaselines_list')->where('supply_purchase_id', $supply_purchase_id);
		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	* Adds supply purchase and purchase lines
	* @param $supply_purchase array
	* @param $purchase_lines array
	*/
	public function add_supply_purchase($supply_purchase, $purchase_lines = array()) 
	{
		$this->db->trans_start();

		$this->db->insert('supplypurchases', $supply_purchase);
		$supply_purchase_id = $this->db->insert_id();

		$this->_add_purchase_lines($purchase_lines, $supply_purchase_id, $supply_purchase['is_confirmed']);

		$this->db->trans_complete();
	}

		/**
		* Adds batch of $purchase_lines with $supply_purchase_id and $is_confirmed
		* @param $purchase_lines array
		* @param $supply_purchase_id int
		* @param $is_confirmed boolean
		*/
		public function _add_purchase_lines($purchase_lines, $supply_purchase_id, $is_confirmed = false) 
		{
			$this->db->trans_start();

			if($purchase_lines) {
				foreach($purchase_lines as $line) {
					$line['supply_purchase_id'] = $supply_purchase_id;

					$this->_add_purchase_line($line, $is_confirmed);
				}
			}

			$this->db->trans_complete();
		}

			/**
			* Adds a purchase line record with is_confirmed as flag if true to either to update or add new inventory
			* @param $line array
			* @param $is_confirmed boolean
			*/
			public function _add_purchase_line($line, $is_confirmed)
			{
				$this->db->trans_start();

				$this->db->insert('supplypurchaselines', $line);

				if($is_confirmed) { // if $is_confirmed true, update or add inventory
					$this->_inventory_update($line);
				}

				$this->db->trans_complete();
			}

	/**
	* Updates supply purchase and its supply purchase lines
	* @param $supply_purchase_id int
	* @param $supply_purchase array
	* @param $supplypurchaselines array
	* @return boolean
	*/
	public function update_supply_purchase($supply_purchase_id, $supply_purchase, $supplypurchaselines)
	{
		$this->db->where('supply_purchase_id', $supply_purchase_id);
		$this->db->update('supplypurchases', $supply_purchase);

		$this->_update_purchase_lines($supplypurchaselines, $supply_purchase['is_confirmed']);

		return true;
	}

		/**
		* Updates the supply purchase lines
		* @param $supplypurchaselines array
		* @param $is_confirmed boolean
		*/
		public function _update_purchase_lines($supplypurchaselines = array(), $is_confirmed = false) 
		{
			if($supplypurchaselines) {
				foreach($supplypurchaselines as $line) {
					$this->_update_purchase_line($line, $is_confirmed);
				}	
			}
		}

			/**
			* Updates the record of purchase line from array, 
			* add new purchase line if new, remove purchase line, update purchase line
			* @param $purchaseline array
			* @param $is_confirmed boolean
			*/
			private function _update_purchase_line($purchaseline, $is_confirmed = false) 
			{
				$inventory_update = false;
				if($purchaseline['purchase_line_id'] == -1 && $purchaseline['supply_purchase_id'] != -1) { // add new purchase line
					$inventory_update = true;
					unset($purchaseline['purchase_line_id']);
					$this->db->insert('supplypurchaselines', $purchaseline);
				} else if($purchaseline['purchase_line_id'] != -1 && $purchaseline['supply_purchase_id'] != -1) { // update purchase line
					$inventory_update = true;
					$this->db->where('purchase_line_id', $purchaseline['purchase_line_id']);
					unset($purchaseline['purchase_line_id']);
					$this->db->update('supplypurchaselines', $purchaseline);
				} else if($purchaseline['purchase_line_id'] != -1 && $purchaseline['supply_purchase_id'] == -1) { // remove purchase line
					$this->db->where('purchase_line_id', $purchaseline['purchase_line_id']);
					$this->db->delete('supplypurchaselines');
				}

				if($is_confirmed && $inventory_update) { // updates inventory if confirmed and inventory_update true
					$this->_inventory_update($purchaseline);
				}
			}

	public function delete_supply_purchase($supply_purchase_id, $user_id, $password) 
	{
		$row = false;
		$this->db->select()->from('users')->where(array('user_id' => $user_id, 'password' => $password));
		$query = $this->db->get();

		if(empty($query->first_row('array')) == false) {
			$this->db->where('supply_purchase_id', $supply_purchase_id);
			$this->db->delete('supplypurchases');
			$row = $this->db->affected_rows();
		}

		return $row;
	}
//// Helper methods start
	/**
	* Updates inventory from purchase line passed
	* @param $line array
	*/
	public function _inventory_update($line) 
	{
		if($this->_check_inventory_exist($line['store_item_id'])) { // if exist, update stock_at_hand of inventory
			$this->_update_inventory($line['store_item_id'], $line['purchase_qty']);
		} else { // else add new inventory with the stock_at_hand set
			$new_inventory = array('inventory_id' => $line['store_item_id'], 'stock_at_hand' => $line['purchase_qty'], 'max_stock_qty' => 0, 'min_stock_qty' => 0, 'reordering_qty' => 0); // inventory for edition
			$this->_add_inventory($new_inventory);
		}
	}

	/**
	* Checks if inventory exist by inventory id passed
	* @param $inventory_id int
	* @return array
	*/
	public function _check_inventory_exist($inventory_id) 
	{
		$this->db->select()->from('inventories')->where('inventory_id', $inventory_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	/**
	* Updates inventory to inventories table
	* @param $inventory_id int
	* @param $stock_to_add int
	* @return int
	*/
	public function _update_inventory($inventory_id, $stock_to_add = 0)
	{
		$this->db->where('inventory_id', $inventory_id);
		$this->db->set('stock_at_hand', 'stock_at_hand + ' . $stock_to_add, false);
		$this->db->update('inventories');

		return $this->db->affected_rows();
	}

	/**
	* Add inventory to inventories table
	* @param $inventory array
	* @return int
	*/
	public function _add_inventory($inventory) 
	{
		$this->db->insert('inventories', $inventory);

		return $this->db->insert_id();
	}
/// Helper methods end
}