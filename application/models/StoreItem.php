<?php

class StoreItem extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	private $table = 'storeitems_list';

	public function get_storeitems_list($is_inventory = false, $search_str = null, $most_sold = 0, $is_stockable = 1)
	{
		$this->db->select()->from($this->table);
		$this->db->where('is_stockable', $is_stockable);

		if($is_inventory) {
			$this->db->where('inventory_id', NULL);
		}

		if($search_str) {
			$this->db->like('storeitemname', $search_str);
		}

		if($most_sold == 1) {
			$this->db->where('lines !=', 0);
			$this->db->where('stock_at_hand !=', 0);
			$this->db->order_by('lines', 'DESC');
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_storeitems($is_stockable = 1)
	{
		$this->db->select()->from($this->table)->where('inventory_id', NULL);
		$this->db->where('is_stockable', $is_stockable);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_storeitem($item_id)
	{
		$this->db->select()->from($this->table)->where('store_item_id', $item_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	public function get_storeitem_sku($sku)
	{
		$this->db->select()->from($this->table)->where('sku', $sku);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	/**
	* Gets list of storeitems by category by id passed with option to get list by parent_id or category_id
	* @param $category_id int
	* @param $is_parent boolean (default false)
	* @param $is_inventory boolean (default false)
	* @return array
	*/
	public function get_storeitems_by_category($category_id, $is_parent = false, $is_inventory = false, $search_str = null, $most_sold = 0)
	{
		$cond = ($is_parent) ? 'parent_id' : 'category_id' ;
		$this->db->select()->from($this->table);
		$this->db->where($cond, $category_id);

		if($is_inventory) {
			$this->db->where('inventory_id', NULL);
		}

		if($search_str) {
			$this->db->like('storeitemname', $search_str);
		}

		if($most_sold == 1) {
			$this->db->where('lines!=', '0');
			$this->db->order_by('lines', 'DESC');
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	public function add_store_items($req, $is_stockable = 1)
	{
		$this->db->trans_start();

		$this->db->insert('products', $req['product']);
		$product_id = $this->db->insert_id();

		$req['productInCategory']['product_id'] = $product_id;
		$this->add_productincategory($req['productInCategory']);

		$this->add_store_items_by_array($req['storeitems'], $product_id, $is_stockable);

		$this->db->trans_complete();

		return true;
	}

	private function add_store_items_by_array($reqArr, $product_id, $is_stockable)
	{
		if($reqArr) {
			$this->db->trans_start();

			foreach($reqArr as $item) { // adding product_id to each record
				$item['product_id'] = $product_id;
				$this->db->insert('storeitems', $item);
				$store_item_id = $this->db->insert_id();

				if($is_stockable == 0 && $store_item_id) {
					$inventory = array(
							'inventory_id' => $store_item_id,
							'stock_at_hand' => 0,
							'max_stock_qty' => 0,
							'min_stock_qty' => 0,
							'reordering_qty' => 0
						);

					$this->add_inventory($inventory);
				}
			}

			$this->db->trans_complete();
		}
	}

	private function add_inventory($inventory) 
	{
		$this->db->insert('inventories', $inventory);
	}

	private function add_productincategory($req)
	{
		$this->db->insert('productsincategories', $req);

		return $this->db->insert_id();
	}

	public function delete_store_item($store_item_id, $product_id) 
	{
		if(($this->count_storeitem_by_product_id($product_id) <= 1)) {
			$this->delete_product($product_id);
		}

		$this->db->where('store_item_id', $store_item_id);
		$this->db->delete('storeitems');

		return $this->db->affected_rows();
	}

	private function count_storeitem_by_product_id($product_id)
	{
		$this->db->trans_start();
		$this->db->select()->from('storeitems');
		$this->db->where('product_id', $product_id);

		$count = $this->db->count_all_results();
		$this->db->trans_complete();

		return $count;
	}

	private function delete_product($product_id) 
	{
		$this->db->where('product_id', $product_id);
		$this->db->delete('products');

		return $this->db->affected_rows();
	}

	public function edit_store_item($store_item_id, $storeitem)
	{
		$this->db->where('store_item_id', $store_item_id);
		$this->db->update('storeitems', $storeitem);

		return $this->db->affected_rows();
	}

	public function add_storeitem_image($store_item_id, $image)
	{
		$image_id = $this->add_image($image);
		$storeitem_image = array(
				'store_item_id' => $store_item_id,
				'image_id' => $image_id
			);

		$this->db->insert('storeitemimages', $storeitem_image);
		return $this->db->insert_id();
	}

	public function edit_storeitem_image($store_item_id, $image, $image_id)
	{
		$image_data = $this->get_image($image_id);
		$this->delete_image_file($image_data);

		$this->db->where('image_id', $image_id);
		$this->db->update('images', $image);

		return $this->db->affected_rows();
	}

	private function get_image($image_id)
	{
		$this->db->select()->from('images')->where('image_id', $image_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	private function add_image($image)
	{
		$this->db->insert('images', $image);
		return $this->db->insert_id();
	}

	public function delete_image($image_id)
	{
		$image_data = $this->get_image($image_id);
		$this->delete_image_file($image_data);
		$this->db->where('image_id', $image_id);
		$this->db->delete('images');
	}

	private function delete_image_file($image) 
	{
		$file_with_path = $_SERVER['DOCUMENT_ROOT'] .'/posisui/uploads/images/' . $image['image'];

		if(file_exists($file_with_path)) {
			unlink($file_with_path);
		}
	}
}