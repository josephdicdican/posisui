<?php

class Inventory extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	private $table = "inventories_list";

	public function get_all_products($search_str = null, $sort_column = null, $sort_by = null, $filter = 0, $category_id = 0, $is_parent = false, $is_report = false, $is_stockable = 1)
	{
		$fields = 'inventory_id, sku, storeitemname,  stock_at_hand, reordering_qty, max_stock_qty, min_stock_qty, price_per_unit, product_id, product_name, storeitemname';

		if($is_report) {
			$fields = 'sku, storeitemname, stock_at_hand, reordering_qty, max_stock_qty, min_stock_qty';
		}

		if($is_stockable == 0 && $is_report == false) {
			$fields = 'inventory_id, sku, storeitemname, stock_at_hand';
		}

		$this->db->select($fields)->from($this->table);

		if($search_str) {
			$this->db->like('storeitemname', $search_str, '%', true);
		}

		$this->db->where('is_stockable', $is_stockable);

		switch($filter) {
			case 1: // filter get reordering items
				$this->db->where('stock_at_hand < reordering_qty', null, false);
				break;

			case 2: // filter get out of stock items
				$this->db->where('stock_at_hand = 0', null, false);
				break;

			case 3: // filter get over stock items
				$this->db->where('stock_at_hand > max_stock_qty', null, false);
				break;

			case 0:
			default:
				break;
		}

		if($sort_column != null) {
			$this->db->order_by($sort_column, $sort_by);
		}

		if($category_id > 0) {
			$category_field = ($is_parent) ? 'parent_id' : 'category_id';
			$this->db->where($category_field, $category_id);
		}

		$query = $this->db->get();
		//pre_print_r($this->db->last_query());

		return $query->result_array();
	}

	public function get_product($inventory_id)
	{
		$this->db->from($this->table)->where('inventory_id', $inventory_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	public function update_inventory($inventory_id, $product)
	{
		$this->db->trans_start();

		$this->db->from('inventories')->where('inventory_id', $inventory_id);
		$this->db->update('inventories', $product);

		$user_id = $this->session->userdata('user_id');

		if ($inventory_id != 0) {
			$logs = array(
					'user_id' => $user_id,
					'inventory_id' => $inventory_id,
					'remarks' => "Updated",
					'is_unread' => 1
				);
			$this->db->insert('inventorylogs', $logs);
		}
		$this->db->trans_complete();

		return $inventory_id;
	}

	public function delete_inventory($inventory_id)
	{
		$this->db->where('inventory_id', $inventory_id);
		$this->db->delete('inventories');

		return true;
	}

	public function add_inventory($product)
	{
		$this->db->trans_start();
		$this->db->insert('inventories', $product);
		
		$inventory_id = $product['inventory_id'];
		$user_id = $this->session->userdata('user_id');

		if ($inventory_id != 0) {
			$logs = array(
					'user_id' => $user_id,
					'inventory_id' => $inventory_id,
					'remarks' => "Added",
					'is_unread' => 1
				);
			$this->db->insert('inventorylogs', $logs);
		}

		$this->db->trans_complete();

		return $inventory_id;
	}

	/**
		UPDATE inventories as i 
		LEFT JOIN storeitems as s on i.inventory_id = s.store_item_id
		LEFT JOIN products as p on p.product_id = s.product_id
		LEFT JOIN productsincategories as pc on pc.product_id = p.product_id
		LEFT JOIN categories as c on c.category_id = pc.category_id
		SET i.stock_at_hand = 0
		WHERE c.is_stockable = 0;
	*/
	public function reset_daily_menu()
	{
		$this->db->query('call reset_daily_menu()');

		return $this->db->affected_rows();
	}
}