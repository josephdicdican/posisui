<?php

class Supplier extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	private $table = 'suppliers';

	public function get_all_suppliers($search_str = null, $sort_column = null, $sort_by = null)
	{
		$this->db->select()->from($this->table);

		if($search_str) {
			$this->db->or_like('supplier_name', $search_str, '%', true);
			$this->db->or_like('supplier_contact', $search_str, '%', true);
			$this->db->or_like('supplier_email', $search_str, '%', true);
		}

		if($sort_column != null) {
			$this->db->order_by($sort_column, $sort_by);
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_supplier($supplier_id)
	{
		$this->db->from($this->table)->where('supplier_id', $supplier_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	public function add_supplier($supplier)
	{
		$this->db->trans_start();
		$flag = false;

		if($this->check_supplier_exist($supplier['supplier_name']) == 0) {
			$flag = true;

			$this->db->insert($this->table, $supplier);
			$supplier_id = $this->db->insert_id();
			$user_id = $this->session->userdata('user_id');

			if ($supplier_id != 0) {
				$logs = array(
						'user_id' => $user_id,
						'supplier_id' => $supplier_id,
						'remarks' => "Added",
						'is_unread' => 1
					);
				$this->db->insert('supplierlogs', $logs);
			}
		} else {
			$flag = -1;
		}

		$this->db->trans_complete();

		return $flag;
	}

	public function check_supplier_exist($supplier_name) {
		$this->db->select()->from($this->table)->where('supplier_name', $supplier_name);
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function update_supplier($supplier_id, $supplier)
	{
		$this->db->trans_start();
		$flag = false;

		if($this->check_supplier_exist($supplier['supplier_name']) == 0) {
			$flag = true;

			$this->db->where('supplier_id', $supplier_id);
			$this->db->update($this->table, $supplier);

			$user_id = $this->session->userdata('user_id');

			if ($supplier_id != 0) {
				$logs = array(
						'user_id' => $user_id,
						'supplier_id' => $supplier_id,
						'remarks' => "Updated",
						'is_unread' => 1
					);
				$this->db->insert('supplierlogs', $logs);
			}
		} else {
			$flag = -1;
		}

		$this->db->trans_complete();

		return $flag;
	}

	public function delete_supplier($supplier_id)
	{
		$this->db->where('supplier_id', $supplier_id);
		$this->db->delete('suppliers');

		return true;
	}
}