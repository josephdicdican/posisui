<?php

class User extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	private $table = 'users';

	/**
	* Gets all the user in record
	* @param none
	* @return array (array of users)
	*/
	public function get_all($search_str = null, $sort_column = null, $sort_by = null) 
	{
		$this->db->select()->from('users_vw');

		if($search_str) {
			$this->db->or_like('display_name', $search_str, '%', true);
			$this->db->or_like('username', $search_str, '%', true);
		}

		if($sort_column != null) {
			if($sort_column == 'display_name') {
				$this->db->order_by('display_name', $sort_by);
			} else {
				$this->db->order_by($sort_column, $sort_by);
			}
		}


		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	* Gets a user with the user_id passed
	* @param $user_id int
	* @return array (user instance)
	*/
	public function get($user_id)
	{
		$this->db->from('users_vw')->where('user_id', $user_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}

	/**
	* Add a user in users table and add userInRole to usersinrole table 
	* with the last inserted id
	* @param $user array
	* @return int
	*/
	public function add($user)
	{
		$this->db->trans_start();
		$flag = false;

		if($this->check_username_exist($user['user']['username']) == 0) {
			$flag = true;
			$this->db->insert($this->table, $user['user']);
			$user2log_id = $this->db->insert_id();

			$user['userInRole']['user_id'] = $this->db->insert_id();
			$this->db->insert('userinroles', $user['userInRole']);

			$user_id = $this->session->userdata('user_id');

			if ($user_id != 0) {
				$logs = array(
						'user_id' => $user_id,
						'user2log_id' => $user2log_id,
						'remarks' => "Added",
						'is_unread' => 1
					);
				$this->db->insert('userlogs', $logs);
			}
		} else {
			$flag = -1;
		}

		$this->db->trans_complete();

		return $flag;
	}

		public function check_username_exist($username) {
			$this->db->select()->from('users')->where('username', $username);
			$query = $this->db->get();

			return $query->num_rows();
		}

	public function update($id, $user)
	{
		$this->db->trans_start();

		$this->db->where('user_id', $id);
		$this->db->update('users', $user['user']);

		$this->db->where('user_id', $id);
		$this->db->update('userinroles', $user['userInRole']);

		$user_id = $this->session->userdata('user_id');

		if ($user_id != 0) {
			$logs = array(
					'user_id' => $user_id,
					'user2log_id' => $id,
					'remarks' => "Updated",
					'is_unread' => 1,
				);
			$this->db->insert('userlogs', $logs);
		}

		$this->db->trans_complete();

		return $id;
	}

	public function delete($user_id)
	{
		$tables = array('userinroles', 'users');
		$this->db->where('user_id', $user_id);
		$this->db->delete($tables);

		return $user_id;
	}
}