<?php

class UserRole extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	private $table = 'userroles';

	public function get_all() 
	{
		$query = $this->db->get($this->table);

		return $query->result_array();
	}

	public function get($role_id) 
	{
		$this->db->from($this->table)->where('role_id', $role_id);
		$query = $this->db->get();

		return $query->first_row('array');
	}
}