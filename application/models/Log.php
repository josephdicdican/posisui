<?php

class Log extends CI_Model {
	public function __construct() 
	{
		parent::__construct();
	}

	public function get_log($table, $condition, $id)
	{
		$this->db->from($table)->where($condition, $id);
		$this->db->order_by('date_created', 'DESC');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_inventory_logs($limit = 5) 
	{
		$fields = 'inventory_log_id, inventory_id, get_storeitem_name_by_id(inventory_id) as storeitemname, remarks, date_created, is_unread, get_username(user_id) as user';
		$this->db->select($fields)->from('inventorylogs');
		$this->db->order_by('date_created', 'DESC');
		$this->db->limit($limit);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_order_logs($limit = 5)
	{
		$fields = 'log_id, order_id, remarks, date_created, is_unread, get_username(user_id) as user';
		$this->db->select($fields)->from('orderlogs');
		$this->db->order_by('date_created', 'DESC');
		$this->db->limit($limit);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_customer_logs($limit = 5)
	{
		// $fields = 'customer_log_id, customer_id, remarks, date_created, is_unread, get_username(user_id) as user';

		$fields = 'customer_log_id, user_id, display_name, customer_id, firstname, lastname, remarks, date_created, is_unread';
		$this->db->select($fields)->from('customerlogs_list');
		$this->db->order_by('date_created', 'DESC');
		$this->db->limit($limit);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_user_logs($limit = 5)
	{
		$fields = 'user_log_id, get_username(user2log_id) as username, remarks, date_created, is_unread, get_username(user_id) as user';
		$this->db->select($fields)->from('userlogs_list');
		$this->db->order_by('date_created', 'DESC');
		$this->db->limit($limit);

		$query = $this->db->get();

		return $query->result_array();
	}
}