<?php
    /**
     *<pre>This is a helper method that
     * will convert an object to an array; which
     * is necessary on object/data passing to a 
     * webservice method.</pre> 
     * @author Belfry Ruiz <belfry.ruiz@softshore.net>
     */
    class CSerializable{
        
        public function __construct($data, array &$parent = array()) {
            //initialize object(s)
            $this->init();
            
            $this->analyze($data, $parent);
        }
        
        private $data;
        
        
        /**
         *<pre>This method retrieves the serialized
         * data to be passed in our webservice method.</pre>
         * @return Array 
         */
        public function getData() {
            return $this->data;
        }

                
        private function analyze($data, $parent){
            try{
                $arr = array();
                $this->data = $this->toArray($data, $arr);
                
                if(count($parent) > 0){
                    //append parent object(s)
                    foreach ($parent as $key => $value) {
                        $this->data[$key] = $value;
                    }
                }
            }catch(Exception $ex){
                $this->init();
            }
        }
        
        private function init(){
            $this->data = array();
        }
        
        private function toArray($data, &$arr){
            try{
                /*
                 * check if method passed is an object;
                 * and if it is, covert $data passed to an array
                 * by executing method get_object_vars();
                 */
                if(is_object($data)){
                    $arr = get_object_vars ($data);
                }
                else
                    $arr = $data;
                /*
                 * check if $arr is an array; and do
                 * the necessary things.
                 */
                if(is_array($arr)){
                    foreach ($arr as $key => $value) {
                        
                        //check if $value is an object
                        if(is_object($value)){
                            //check if $value has callable method toArray()                            
                            if(is_callable(array($value, 'toArray'), true) && method_exists($value, 'toArray')){
                                $arr[$key] = call_user_func(array($value, 'toArray'));
                                continue;
                            }
                        }
                        
                        if(is_object($value) || is_array($value)){
                            $arr[$key] = $this->toArray($value, $value);
                        }
                        
                    }
                }
                
            }catch(Exception $ex){
                $arr = array();
            }
            
            return $arr;
        }
    
    }