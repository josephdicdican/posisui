<?php
require_once('ReqSaveContactCallBack.php');

class ReqSaveContactMessage extends ReqSaveContactCallBack implements IObjectToArray {
	public function __construct() {
		parent::__construct();
	}

	public function __destruct() {
		unset($this);
	}

	private $message = "";

	public function getMessage() {
		return $this->image;
	}
	public function setMessage($message) {
		$this->message = $message;
	}

	public function toArray() {
		$parentItems = parent::toArray();
		$item = new CSerializable(get_object_vars($this));
		$arrObjVars = array_merge($parentItems, $item->getData());
		
		return $arrObjVars;
	}
}