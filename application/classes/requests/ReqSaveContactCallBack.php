<?php
class ReqSaveContactCallBack implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $contactType = -1;
	private $title = "";
	private $firstName = "";
	private $middleName = "";
	private $surname = "";
	private $email = "";
	private $phone = "";
	private $reachability = "";

	public function getContactType() {
		return $this->contactType;
	}
	public function setContactType($contactType) {
		$this->contactType = $contactType;
	}
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}
	public function getFirstName() {
		return $this->firstName;
	}
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}
	public function getMiddleName() {
		return $this->middleName;
	}
	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
	}
	public function getSurname() {
		return $this->surname;
	}
	public function setSurname($surname) {
		$this->surname = $surname;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
	}
	public function getPhone() {
		return $this->phone;
	}
	public function setPhone($phone) {
		$this->phone = $phone;
	}
	public function getReachability() {
		return $this->reachability;
	}
	public function setReachability($reachability) {
		$this->reachability = $reachability;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}