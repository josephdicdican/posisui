<?php
class ReqSaveContactPromo implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $contactType = -1;
	private $clinicName = "";
	private $title = "";
	private $firstName = "";
	private $middleName = "";
	private $surname = "";
	private $city = "";
	private $province = "";
	private $zipCode = "";
	private $street = "";
	private $email = "";
	private $phone = "";

	public function getContactType() {
		return $this->contactType;
	}
	public function setContactType($contactType) {
		$this->contactType = $contactType;
	}
	public function getClinicName() {
		return $this->clinicName;
	}
	public function setClinicName($clinicName) {
		$this->clinicName = $clinicName;
	}
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}
	public function getFirstName() {
		return $this->firstName;
	}
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}
	public function getMiddleName() {
		return $this->middleName;
	}
	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
	}
	public function getSurname() {
		return $this->surname;
	}
	public function setSurname($surname) {
		$this->surname = $surname;
	}
	public function getCity() {
		return $this->city;
	}
	public function setCity($city) {
		$this->city = $city;
	}
	public function getProvince() {
		return $this->province;
	}
	public function setProvince($province) {
		$this->province = $province;
	}
	public function getZipCode() {
		return $this->zipCode;
	}
	public function setZipCode($zipCode) {
		$this->zipCode = $zipCode;
	}
	public function getStreet() {
		return $this->street;
	}
	public function setStreet($street) {
		$this->street = $street;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
	}
	public function getPhone() {
		return $this->phone;
	}
	public function setPhone($phone) {
		$this->phone = $phone;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}