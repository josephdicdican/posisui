<?php

class DailyMenu extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->data['show_build'] = true;
		$this->posis_lib->check_session();
		$this->load->model('Category');
		$this->load->model('StoreItem');
	}

	private $consumable = 0;

	public function index()
	{
		if(is_post()) {
			$input = $this->input->post();
			$file = $_FILES['userfile'];
			$image = false;

			switch($input['process']) {
				case 'add_category':
					$category = array(
							'parent_id' => isset($input['nested']) ? $input['parent_id'] : 0,
							'category_name' => $input['category_name'],
							'is_stockable' => $this->consumable
						);

					if($file['tmp_name'] != null) {
						$flag = $this->upload($file);
						if($flag) {
							$image = array(
								'image' => $this->data['upload_data']['file_name'],
								'image_type' => 0
							);
						}
					}

					$this->Category->add_category($category, $image);
					break;

				case 'update_category':
					$category = array(
							'category_name' => $input['category_name']
						);

					if($file['tmp_name'] != null) {
						$flag = $this->upload($file);
						if($flag) {
							$image = array(
									'image' =>  $this->data['upload_data']['file_name']
								);
						}
					}

					$this->Category->update_category($input['category_id'], $category, $input['image_id'], $image);
					break;
			}
		}

		$this->posis_lib->admin_only();
		$this->data['view'] = 'dailymenu/index';
		$this->data['corejs'] = array('core/dailymenu.js');
		$this->data['title'] = 'Daily Menu';
		$this->load->view('shared/_layout', $this->data);
	}

	public function add_storeitem_image() 
	{
		if(is_post()) {
			$input = $this->input->post();
			$file = $_FILES['userfile'];
			$image = false;
			$id = 0;

			if($file['tmp_name'] != null) {
				$flag = $this->upload($file);
				if($flag) {
					$image = array(
						'image' => $this->data['upload_data']['file_name'],
						'image_type' => 1
					);

					$id = $this->StoreItem->add_storeitem_image($input['store_item_id'], $image);
				}
			}
		}

		redirect(base_url('dailymenu/index'));
	}

	public function edit_storeitem_image() 
	{
		if(is_post()) {
			$input = $this->input->post();
			$file = $_FILES['userfile'];
			$image = false;
			$id = 0;

			if($file['tmp_name'] != null) {
				$flag = $this->upload($file);
				if($flag) {
					$image = array(
						'image' => $this->data['upload_data']['file_name'],
						'image_type' => 1
					);

					$id = $this->StoreItem->edit_storeitem_image($input['store_item_id'], $image, $input['image_id']);
				}
			}
			//pre_print_r($this->data);
		}

		redirect(base_url('dailymenu/index'));
	}

	private function upload($file) 
	{
		$config = array(
			'upload_path' => "./uploads/images/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "4056000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "900",
			'max_width' => "1024",
			'file_name' => time() .'-'. strtolower($file['name'])
		);

		$this->load->library('upload', $config);
		if($this->upload->do_upload()) {
			$flag = true;
			$this->data['upload_data'] = $this->upload->data();
		} else {
			$flag = false;
			$this->data['error'] = $this->upload->display_errors();
		}

		return $flag;
	}
}