<?php

class Error extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->data['show_build'] = false;
	}

	private $data = array();

	public function unauthorized() 
	{
		$this->data['view'] = 'error/error_log';
		$this->load->view('shared/_layout', $this->data);
	}
}