<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->data['show_build'] = true;
	}

	private $data = array();

	public function index() 
	{
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/salesreports.js');
		$this->data['view'] = 'reports/daily_report';
		$this->data['title'] = 'Daily Report';
		$this->load->view('shared/_layout', $this->data);
	}

	public function inventory() 
	{
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/inventoryreport.js');
		$this->data['view'] = 'reports/inventory';
		$this->data['title'] = 'Inventory Reports';
		$this->load->view('shared/_layout', $this->data);
	}

	public function sales_reports() 
	{
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/sales.js');
		$this->data['view'] = 'reports/sales_reports';
		$this->data['title'] = 'Sales Reports';
		$this->load->view('shared/_layout', $this->data);
	}

	public function sales_forecast() 
	{
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/sales_forecast.js');
		$this->data['view'] = 'reports/sales_forecast';
		$this->data['title'] = 'Sales Forecast';
		$this->load->view('shared/_layout', $this->data);
	}
}
