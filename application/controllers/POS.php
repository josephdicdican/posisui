<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class POS extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->data['show_build'] = true;
	}

	private $data = array();

	public function index() 
	{
		$this->posis_lib->check_session();
		$this->data['view'] = 'POS/pos';
		$this->data['corejs'] = array('core/pos.js');
		$this->data['title'] = 'POS';
		$this->load->view('shared/_layout', $this->data);
	}

}
