<?php

class SalesForecast extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('SaleForecast');
	}

	public function sales($is_stockable = 1, $interval_type = 'week', $group_by = 'category_id')
	{
		$data = $this->SaleForecast->get_sales($is_stockable, $interval_type, $group_by);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function intervals($type = 'week') 
	{
		$data = $this->SaleForecast->get_distinct_intervals($type);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function distinct_items($is_stockable = 1, $item_type = 'store_item_id', $interval_type = 'week') {
		$data = $this->SaleForecast->get_distinct_items($is_stockable, $item_type);
		$i = 0;

		foreach($data as $row) {
			$id = $row[$item_type];
			$data[$i]['forecasts'] = $this->get_forecast($is_stockable, $interval_type, $item_type, $id);
			$i++;
		}

		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	} 

	public function get_forecast($is_stockable = 1, $interval_type = 'week', $group_by = 'category_id', $id = 0)
	{
		$data = $this->SaleForecast->get_sales($is_stockable, $interval_type, $group_by, $id);
		$sales = array();
		$i = 0;

		for($i = 0; $i < count($data); $i++) {
			$data[$i]['total_sales'] = number_format($data[$i]['total_sales']);
			$sales[$i] = $data[$i];
			$sales[$i]['growth'] = 0;
			$sales[$i]['decline'] = 0;
			$pre = $data[$i];
			$past = isset($data[$i+1]) ? $data[$i+1] : null ;

			if($past != null && $pre['category_id'] == $past['category_id'] && $group_by == 'category_id') {
				if($pre['total_sales'] >= $past['total_sales']) {
					$sales[$i]['growth'] = $this->calculate_growth_rate($pre['total_sales'], $past['total_sales']);
				} else {
					$sales[$i]['decline'] = $this->calculate_growth_rate($past['total_sales'], $pre['total_sales']);
				}
			} else if($past != null && @$pre['store_item_id'] == @$past['store_item_id'] && $group_by == 'store_item_id') {
				if($pre['total_sales'] >= $past['total_sales']) {
					$sales[$i]['growth'] = $this->calculate_growth_rate($pre['total_sales'], $past['total_sales']);
				} else {
					$sales[$i]['decline'] = $this->calculate_growth_rate($past['total_sales'], $pre['total_sales']);
				}
			}

			$sales[$i]['items_projection'] = $this->calculate_projection($pre['sold_items'], $sales[$i]['growth']);
			$sales[$i]['projection'] = $this->calculate_projection($pre['total_sales'], $sales[$i]['growth']);
		}

		return $sales;
	}

	public function forecast($is_stockable = 1, $interval_type = 'week', $group_by = 'category_id', $id = 0) {
		$data = $this->get_forecast($is_stockable, $interval_type, $group_by, $id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function forecast_tbl($is_stockable = 1, $interval_type = 'week', $group_by = 'category_id', $id = 0)
	{
		$sales = $this->get_forecast($is_stockable, $interval_type, $group_by, $id);

		echo $this->create_table($sales);
	}

	private function calculate_projection($pre, $rate)
	{
		$projection = $pre + ($pre * $rate);

		return $projection;
	}

	private function calculate_growth_rate($pre, $past) 
	{
		$growth_rate = (($pre - $past)) / $pre;

		return $growth_rate;
	}

	private function create_table($sales) 
	{
		$str_tbl = '<table style="border-collapse: collapse;">';
		$str_tbl .= '<tbody>';

		foreach($sales as $sale) {
			$str_tbl .= $this->create_tr($sale);
		}

		$str_tbl .= '</table></tbody>';

		return $str_tbl;
	}

	private function create_tr($sale) 
	{
		$str = '<tr style="border: 1px solid #333;">';
			foreach ($sale as $key => $value) {
				if($key != 'month' && $key != 'year') {
					$str .= $this->create_td($value);
				}
			}
		$str .= '</tr>';

		return $str;
	}

	private function create_td($data) 
	{
		return '<td style="padding: 5px;">' . $data . '</td>';
	}
}