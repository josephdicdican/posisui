<?php

class Suppliers extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Supplier');
	}

	/**
	* Get all records of suppliers in suppliers table
	*/
	public function get_all()
	{
		$search_str = null;
		$sort_column = null;
		$sort_by = null;

		if($_GET) {
			$input = $this->input->get();
			$search_str = $input['query'];
			$sort_column = $input['sort_column'];
			$sort_by = $input['sort_by'];
		}

		$data = $this->Supplier->get_all_suppliers($search_str, $sort_column, $sort_by);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	/**
	* Get a record of supplier by $supplier_id
	* @param $supplier_id int
	* @return json
	*/
	public function get($supplier_id)
	{
		$data = $this->Supplier->get_supplier($supplier_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	/**
	* Add supplier to suppliers table handling post
	* @return int
	*/
	public function add()
	{
		$flag = -1;
		$id = 0;

		if(is_post()) {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array(
					'supplier_name' => $arr->supplier_name,
					'supplier_contact' => $arr->supplier_contact,
					'supplier_email' => $arr->supplier_email
				);
			// $id = $this->Supplier->add_supplier($req);
			$flag = $this->Supplier->add_supplier($req);
		}

		return $this->posis_lib->to_json($flag);;
	}

	public function edit($supplier_id)
	{
		$flag = -1;
		$id = 0;

		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array(
					'supplier_name' => $arr->supplier_name,
					'supplier_contact' => $arr->supplier_contact,
					'supplier_email' => $arr->supplier_email
				);

			$flag = $this->Supplier->update_supplier($supplier_id, $req);
		}
		
		return $this->posis_lib->to_json($flag);
	}

	public function delete($supplier_id) {
		$id = 0;

		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$id = $this->Supplier->delete_supplier($supplier_id);
		}

		return $id;
	}
}