<?php

class UnitMeasures extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UnitMeasure');
	}

	public function get_all()
	{
		$data = $this->UnitMeasure->get_unit_measures();
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function get($unit_id)
	{
		$data = $this->UnitMeasure->get_unit_measure($unit_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function add()
	{
		$id = 0;
		if(is_post()) {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array(
					'measure_name' => $arr->measure_name
				);

			$id = $this->UnitMeasure->add_unit_measure($req);
		}

		$this->posis_lib->to_json($id);
	}

	public function edit() 
	{
		$row = 0;
		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array('measure_name' => $arr->measure_name);
			$row = $this->UnitMeasure->edit_unit_measure($arr->unit_id, $req);
		}

		$this->posis_lib->to_json($row);
	}
}