<?php

class StoreItems extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('StoreItem');
	}

	public function get_all()
	{
		$is_inventory = false;
		$str_search = null;
		$most_sold = (isset($_GET['most_sold'])) ? $this->input->get('most_sold') : 0;
		$is_stockable = (isset($_GET['is_stockable'])) ? $this->input->get('is_stockable') : 1;

		if($_GET) {
			$is_inventory = $this->input->get('is_inventory');
			$str_search = $this->input->get('query');
		}

		$data = $this->StoreItem->get_storeitems_list($is_inventory, $str_search, $most_sold, $is_stockable);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	public function get_all_storeitems()
	{
		$data = $this->StoreItem->get_storeitems();
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	public function get($item_id)
	{
		$data = $this->StoreItem->get_storeitem($item_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function get_by_category($category_id, $is_parent = false)
	{
		$is_inventory = false;
		$str_search = null;
		$most_sold = (isset($_GET['most_sold'])) ? $this->input->get('most_sold') : 0;

		if($_GET) {
			$is_inventory = $this->input->get('is_inventory');
			$str_search = $this->input->get('query');
		}

		$data = $this->StoreItem->get_storeitems_by_category($category_id, $is_parent, $is_inventory, $str_search, $most_sold);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	public function get_storeitem_sku($sku) 
	{
		$data = $this->StoreItem->get_storeitem_sku($sku);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	public function add()
	{
		if(is_post()) {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req['product'] = array('product_name' => $arr->product_name);
			$req['productInCategory'] = array('category_id' => $arr->category_id);
			$req['storeitems'] = array();

			if($arr->variants) {
				foreach($arr->variants as $variant) {
					$req['storeitems'][] = array(
							'sku' => $variant->sku,
							'unit_id' => $variant->unit_id,
							'price_per_unit' => $variant->price_per_unit
						);
				} // end foreach
			} // end if

			$flag = $this->StoreItem->add_store_items($req, $arr->is_stockable);
			$this->posis_lib->to_json($flag);
		}
	}

	public function delete($store_item_id, $product_id) 
	{
		$rows = 0;

		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$rows = $this->StoreItem->delete_store_item($store_item_id, $product_id);
		}

		$this->posis_lib->to_json($rows);
	}

	public function edit($store_item_id) 
	{
		$rows = 0;

		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array('price_per_unit' => $arr->price_per_unit);
			$rows = $this->StoreItem->edit_store_item($store_item_id, $req);
		}

		$this->posis_lib->to_json($rows);
	}

	public function remove_image($image_id)
	{
		$id = false;
		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$this->load->model('StoreItem');
			$id = $this->StoreItem->delete_image($image_id);
		}

		$this->posis_lib->to_json($id);
	}
}