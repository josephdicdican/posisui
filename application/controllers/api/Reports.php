<?php

class Reports extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Order');
		$this->load->model('Sale');
	}

	public function export_today_sale_csv()
	{
		$data_header = array('Transaction #', 'Date of Transaction', 'Checkout', 'Discounted', 'Total Amount', 'Total Items', 'Discount #', 'Discount Percentage', 'Discount Name');
		$data = $this->Order->get_orders(1,1);
		array_unshift($data, $data_header);
		$this->posis_lib->convert_to_csv($data, 'pos-is-sales-today_'.date('Y-m-d').'_'.date('h:i').'.csv', ',');
	}

	public function transaction_sales($interval_type = 0, $interval = 0, $is_report = true)
	{	$data_header = array('Order Id', 'Date of Transaction', 'Total Items', 'Total Sales');
		$data = $this->Sale->get_transaction_sales($interval_type, $interval, $is_report);
		array_unshift($data, $data_header);
		$this->posis_lib->convert_to_csv($data, 'pos-is-transaction-sales-'.$interval_type.'-'.$interval.'-'.time().'.csv', ',');
	}

	public function storeitems_sales($interval_type = 0, $interval = 0, $is_report = true)
	{	$data_header = array('Store Item #', 'SKU', 'Store Item', 'Total Items', 'Total Sales');
		$data = $this->Sale->get_storeitems_sales($interval_type, $interval, $is_report);
		array_unshift($data, $data_header);
		$this->posis_lib->convert_to_csv($data, 'pos-is-storeitems-sales-'.$interval_type.'-'.$interval.'-'.time().'.csv', ',');
	}

	public function inventories()
	{
		$this->load->model('Inventory');

		$search_str = null;
		$sort_column = null;
		$sort_by = null;
		$filter = 0;
		$category_id = 0;
		$is_parent = 1;
		$is_report = (isset($_GET['is_report']));

		if($_GET) {
			$input = $this->input->get();
			$search_str = $input['query'];
			$sort_column = $input['sort_column'];
			$sort_by = $input['sort_by'];
			$filter = $input['filter'];
			$category_id = $input['category_id'];
			$is_parent = $input['is_parent'];
		}

		$data_header = array('SKU', 'Store Item', 'Stock At Hand', 'Reordering Qty', 'Max Stock Qty', 'Minimum Stock Qty');
		$data = $this->Inventory->get_all_products($search_str, $sort_column, $sort_by, $filter, $category_id, $is_parent, $is_report);
		array_unshift($data, $data_header);
		$this->posis_lib->convert_to_csv($data, 'pos-is-inventory-report-'.time().'.csv', ',');
	}
}