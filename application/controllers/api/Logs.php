<?php

class Logs extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Log');
	}

	public function get_customerlogs($customer_id)
	{
		$data = $this->Log->get_log('customerlogs_list', 'customer_id', $customer_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function get_supplierlogs($supplier_id) 
	{
		$data = $this->Log->get_log('supplierlogs_list', 'supplier_id', $supplier_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function get_inventorylogs($inventory_id)
	{
		$data = $this->Log->get_log('inventorylogs_list', 'inventory_id', $inventory_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function get_userlogs($user_id)
	{
		$data = $this->Log->get_log('userlogs_list', 'user2log_id', $user_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function inventory_logs($limit = 5)
	{
		$data = $this->Log->get_inventory_logs($limit);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function order_logs($limit = 5)
	{
		$data = $this->Log->get_order_logs($limit);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function customer_logs($limit = 5)
	{
		$data = $this->Log->get_customer_logs($limit);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function user_logs($limit = 5)
	{
		$data = $this->Log->get_user_logs($limit);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}
}