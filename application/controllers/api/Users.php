<?php

class Users extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('User');
	}

	/**
	* Gets all user from users_list view
	* @return json
	*/
	public function get_all() 
	{
		$search_str = null;
		$sort_column = null;
		$sort_by = null;

		if($_GET) {
			$input = $this->input->get();
			$search_str = $input['query'];
			$sort_column = $input['sort_column'];
			$sort_by = $input['sort_by'];
		}

		$data = $this->User->get_all($search_str, $sort_column, $sort_by);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	/**
	* Get user by $user_id from users_vw
	* @param $user_id int
	* @return json
	*/
	public function get($user_id) 
	{
		$data = $this->User->get($user_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function add()
	{
		$flag = -1;
		$date = date('Y-m-d h:i:s a', time());
		if(is_post()) {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req['user'] = array(
					'display_name' => $arr->display_name,
					'username' => $arr->username,
					'password' => sha1($arr->password),
					'date_created' => $date,
					'date_modified' => ''
				);
			$req['userInRole']['role_id'] = $arr->role_id;
			$flag = $this->User->add($req);
		}
		
		$this->posis_lib->to_json($flag);
	}

	public function update($id) 
	{
		$date = date('Y-m-d h:i:s a', time());
		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req['user'] = array(
					'display_name' => $arr->display_name,
					'username' => $arr->username,
					'date_modified' => $date
				);
			$req['userInRole']['role_id'] = $arr->role_id;
			
			$this->User->update($id, $req);
			return $this->posis_lib->to_json($req);
		}

		//return $this->posis_lib->api_is_processed($id, 'update');
	}

	public function delete($user_id) {
		$id = $this->User->delete($user_id);
		return $this->posis_lib->api_is_deleted($id);
	}
}