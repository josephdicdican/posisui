<?php

class Categories extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Category');
	}

	/**
	* Gets list of categories with sub_categories in an array
	* @param $is_stockable int
	* @return json
	*/
	public function get_all($is_stockable = 1) 
	{
		$data = $this->Category->get_categories_sub($is_stockable);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	/**
	* Gets a category with sub_categories with the $category_id passed
	* @param $category_id int
	* @return json
	*/
	public function get($category_id)
	{
		$data = $this->Category->get_category($category_id);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	/**
	* Get all parent categories
	* @return json
	*/
	public function get_parents($parent_id = 0, $is_stockable = 1) 
	{
		$data = $this->Category->get_categories($parent_id, $is_stockable);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	public function delete($category_id) 
	{
		$rows_affected = 0;

		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$rows_affected = $this->Category->delete_category($category_id);
		}

		$this->posis_lib->to_json($rows_affected);
	}
}