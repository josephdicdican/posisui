<?php

class Inventories extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Inventory');
	}

	public function get_all()
	{
		$search_str = (isset($_GET['query'])) ? $this->input->get('query') : null;
		$sort_column = (isset($_GET['sort_column'])) ? $this->input->get('sort_column') : null;
		$sort_by = (isset($_GET['sort_by'])) ? $this->input->get('sort_by') : null;
		$filter = (isset($_GET['filter'])) ? $this->input->get('filter') : 0;
		$category_id = (isset($_GET['category_id'])) ? $this->input->get('category_id') : 0;
		$is_parent = (isset($_GET['is_parent'])) ? $this->input->get('is_parent') : 1;
		$is_report = (isset($_GET['is_report'])); 
		$is_stockable = (isset($_GET['is_stockable'])) ? $this->input->get('is_stockable') : 1;

		$data = $this->Inventory->get_all_products($search_str, $sort_column, $sort_by, $filter, $category_id, $is_parent, $is_report, $is_stockable);
		$data = $this->posis_lib->api_check($data);
		//pre_print_r($data);
		$this->posis_lib->to_json($data);
	}

	public function get($inventory_id)
	{
		$data = $this->Inventory->get_product($inventory_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function edit($inventory_id)
	{
		$id = 0;

		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array(
					'max_stock_qty' => $arr->max_stock_qty,
					'min_stock_qty' => $arr->min_stock_qty,
					'reordering_qty' => $arr->reordering_qty
				);

			$id = $this->Inventory->update_inventory($inventory_id, $req);
		}

		return $id;
	}

	public function edit_menu_item($inventory_id) 
	{
		$id = 0;

		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array(
					'stock_at_hand' => $arr->stock_at_hand
				);

			$id = $this->Inventory->update_inventory($inventory_id, $req);
		}

		return $id;
	}

	public function delete($inventory_id) 
	{
		$id = 0;

		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$id = $this->Inventory->delete_inventory($inventory_id);
		}

		return $id;
	}

	public function add()
	{
		$id = 0;

		if(is_post()) {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$req = array(
					'inventory_id' => $arr->inventory_id,
					'stock_at_hand' => $arr->stock_at_hand,
					'max_stock_qty' => $arr->max_stock_qty,
					'min_stock_qty' => $arr->min_stock_qty,
					'reordering_qty' => $arr->reordering_qty
				);
			$id = $this->Inventory->add_inventory($req);
		}

		return $id;
	}

	public function reset_daily_menu()
	{
		$rows = 0;

		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$rows = $this->Inventory->reset_daily_menu();
		}

		return $rows;	
	}
}