<?php

class Sales extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Sale');
	}

	public function get_totals()
	{
		$data = $this->Sale->get_totals();
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function get_intervals($interval_type = 'week')
	{
		$data = $this->Sale->get_intervals($interval_type);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function transaction_sales($interval_type = 0, $interval = 0, $is_report = false)
	{
		$data = $this->Sale->get_transaction_sales($interval_type, $interval, $is_report);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function storeitems_sales($interval_type = 0, $interval = 0, $is_report = false)
	{
		$data = $this->Sale->get_storeitems_sales($interval_type, $interval, $is_report);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}
}