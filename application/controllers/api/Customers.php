<?php

class Customers extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Customer');
	}

	/**
	* Gets all the customers from customers_vw
	*/
	public function get_all()
	{
		$search_str = null;
		$sort_column = null;
		$sort_by = null;

		if($_GET) {
			$input = $this->input->get();
			$search_str = $input['query'];
			$sort_column = $input['sort_column'];
			$sort_by = $input['sort_by'];
		}

		$data = $this->Customer->get_all_customers($search_str, $sort_column, $sort_by);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	/**
	* Get a record of customers by $customer_id
	* @param $supplier_id int
	* @return json
	*/
	public function get($customer_id)
	{
		$data = $this->Customer->get_customer($customer_id);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	/**
	* Add Customer to customers table handling post
	* @return int
	*/
	public function add()
	{
		$flag = -1;
		$id = 0;

		if (is_post()) {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$customer = array(
					'firstname' => $arr->firstname,
					'lastname' => $arr->lastname,
					'address' => $arr->address,
					'email' => $arr->email,
					'gender' => $arr->gender,
					'is_senior' => $arr->is_senior
				);
			$senior['card_no'] = isset($arr->card_no) ? $arr->card_no : null ;
			$flag = $this->Customer->add_customer($customer, $senior);
		}

		return $this->posis_lib->to_json($flag);
	}

	/**
	* Update a customer to the customers and seniorcitizens table
	* @param $customer_id
	*/
	public function edit($customer_id)
	{
		$flag = -1;
		$id = 0;

		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json);

			$customer = array(
					'customer_id' => $customer_id,
					'firstname' => $arr->firstname,
					'lastname' => $arr->lastname,
					'address' => $arr->address,
					'email' => $arr->email,
					'gender' => $arr->gender,
					'is_senior' => $arr->is_senior
				);
			$senior['card_no'] = '';
			if($arr->is_senior) {
				$senior['card_no'] = $arr->card_no;
			}

			// $id = $this->Customer->update_customer($customer, $senior);
			$flag = $this->Customer->add_customer($customer, $senior);
		}

		$this->posis_lib->to_json($flag);
	}

	/**
	* Delete a customer from customers table
	* @param $customer_id int
	*/
	public function delete($customer_id) 
	{
		$affected_rows = 0;

		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$affected_rows = $this->Customer->delete_customer($customer_id);
		}	

		$this->posis_lib->to_json($affected_rows);
	}
}