<?php

class SupplyPurchases extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('SupplyPurchase');
	}

	public function get_supply_purchases()
	{
		$is_confirmed = null;

		if($_GET) {
			$is_confirmed = $_GET['confirmed'];
		}

		$data = $this->SupplyPurchase->get_supply_purchases($is_confirmed);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function get_supply_purchase_lines($supply_purchase_id) 
	{
		$data = $this->SupplyPurchase->get_supply_purchase_lines($supply_purchase_id);
		$data = $this->posis_lib->api_check($data);

		$this->posis_lib->to_json($data);
	}

	public function add_supply_purchase() 
	{
		$data = array();

		if(is_post()) {
			$json = file_get_contents("php://input");
			$arr = $this->_remove_supply_purchase_id(json_decode($json, true));

			$data = $arr;
			$supply_purchase = array(
					'date_of_purchase' => $this->posis_lib->date_to_sql($arr['date_of_purchase']),
					'is_confirmed' => $arr['is_confirmed']
				);
			$this->SupplyPurchase->add_supply_purchase($supply_purchase, $arr['supplypurchaselines']);
		}

		$this->posis_lib->to_json($data);
	}

		private function _remove_supply_purchase_id($arr) 
		{
			unset($arr['supply_purchase_id']);

			for($i = 0; $i <= count($arr['supplypurchaselines']); $i++) {
				unset($arr['supplypurchaselines'][$i]['supply_purchase_id']);
			}

			return $arr;
		}

	public function edit_supply_purchase() 
	{
		$data = array();

		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json, true);

			$supply_purchase = array(
					'is_confirmed' => $arr['is_confirmed']
				);

			$this->SupplyPurchase->update_supply_purchase($arr['supply_purchase_id'], $supply_purchase, $arr['supplypurchaselines']);

			$data = $arr;
		}

		$this->posis_lib->to_json($data);
	}

	public function delete_supply_purchase() 
	{
		$data = false;

		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json, true);

			$user_id = $this->session->userdata('user_id');
			$password = sha1($arr['password']);
			$data = $this->SupplyPurchase->delete_supply_purchase($arr['supply_purchase_id'], $user_id, $password);
		}

		$this->posis_lib->to_json($data);
	}
}