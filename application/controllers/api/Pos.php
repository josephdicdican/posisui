<?php

class Pos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Order');
	}
	
	/**
	* Adds or Hold order
	*/
	public function add_order() 
	{
		$data = array();

		if(is_post()) {
			$json = file_get_contents("php://input");
			$arr = json_decode($json, true);

			$discount_id = 0;

			$customer_id = $arr['customer_id'];
			$order = array(
					'is_checkout' => $arr['is_checkout'],
					'is_discounted' => $arr['is_discounted']
				);

			if($order['is_discounted']) {
				$discount_id = $arr['discount_id'];
			}

			$orderlines = $this->_clean_orderlines_for_add($arr['orderlines']);

			$lines = $this->Order->add_order($order, $orderlines, $discount_id, $customer_id);

			$data = $this->posis_lib->api_check($lines, true);
		}

		$this->posis_lib->to_json($data);
	}

		private function _clean_orderlines_for_add($orderlines = array())
		{
			if($orderlines) {
				$i = 0;
				foreach($orderlines as $line) {
					unset($line['line_id']);
					unset($line['order_id']);

					$orderlines[$i] = $line;
					$i++;
				}
			}

			return $orderlines;
		}
	
	public function update_order()
	{
		if($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$json = file_get_contents("php://input");
			$arr = json_decode($json, true);

			$discount_id = 0;

			$order = array(
					'is_checkout' => $arr['is_checkout'],
					'is_discounted' => $arr['is_discounted']
				);

			if($order['is_discounted']) {
				$discount_id = $arr['discount_id'];
			}

			$orderlines = $arr['orderlines'];

			$lines = $this->Order->update_order($arr['order_id'], $order, $orderlines, $discount_id);

			$data = $this->posis_lib->api_check($lines, true);
		}

		$this->posis_lib->to_json($data);
	}

	/**
	* Get orders filtered by is_checkout 0 = hold orders, 1 = checked out orders
	* @param $is_checkout int
	* @param $range int 1 = TODAY, 2 = WEEK, 3 = MONTH
	*/
	public function get_orders($is_checkout = 0, $range = 1) 
	{
		$admin_access = isset($_GET['admin_access']);
		$data = $this->Order->get_orders($is_checkout, $range, $admin_access);
		$data = $this->posis_lib->api_check($data, true);
		
		$this->posis_lib->to_json($data);
	}

	public function get_all($limit = 0)
	{
		$data = $this->Order->get_all_transaction($limit);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	/**
	* Get orderlines by order_id being passed
	* @param $order_id int
	*/
	public function get_orderlines($order_id)
	{
		$data = $this->Order->get_orderlines($order_id);
		$data = $this->posis_lib->api_check($data, true);

		$this->posis_lib->to_json($data);
	}

	/**
	* Discard onhold order
	* @param $order_id int
	*/
	public function discard_order($order_id) 
	{
		$data = false;
		
		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$data = $this->Order->discard_order($order_id);
		}

		$this->posis_lib->to_json($data);
	}

	public function delete_expired_onhold_orders()
	{
		$rows = 0;
		if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
			$rows = $this->Order->delete_expired_onhold_orders();
		}

		$this->posis_lib->to_json($rows);
	}
}