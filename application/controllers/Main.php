<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
	}

	private $data = array();

	public function index() 
	{	
		if($this->session->userdata('user_id') != null) {
			if($this->session->userdata('is_admin')) {
				redirect(base_url('main/dashboard'));
			} else {
				redirect(base_url('POS/'));
			}
		}

		$this->data['show_build'] = false;
		$this->data['view'] = 'main/index';
		$this->data['title'] = 'Index';
		$this->load->view('shared/_layout', $this->data);
	}

	public function dashboard() 
	{
		$this->posis_lib->check_session();
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/dashboard.js');
		$this->data['show_build'] = true;
		$this->data['view'] = 'main/dashboard';
		$this->data['title'] = 'Admin Dashboard';
		$this->load->view('shared/_layout', $this->data);
	}
}
