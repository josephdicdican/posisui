<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->data['show_build'] = true;
	}

	private $data = array();

	public function index() 
	{
		$this->data['view'] = 'reports/forecast';
		$this->load->view('shared/_layout', $this->data);
	}
}
