<?php

class Auth extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Account');
	}

	public function login()
	{
		$this->logged_redirect(); 
		$this->data['error'] = '';
		if (is_post()) {
			$input = $this->input->post();
			$passwordEncrypted = sha1($input['password']);
			$user = $this->Account->login($input['username'], $passwordEncrypted);

			if ($user) {
				$sessData = array(
						'user_id' => $user['user_id'],
						'username' => $user['username'],
						'display_name' => $user['display_name'],
						'is_admin' => ($user['role_name'] == 'admin'),
						'role_name' => $user['role_name']
					);
				$this->session->set_userdata($sessData);

				$url = ($sessData['is_admin']) ? base_url('main/dashboard') : base_url('pos') ;
				redirect($url);
			} else {
				$this->data['error'] = 'Login failed';
			}
		}

		$this->data['show_build'] = false;
		$this->data['view'] = 'auth/index';
		$this->load->view('shared/_layout', $this->data);
	}

	public function forgot_password()
	{
		$this->data['show_build'] = false;
		$this->data['user'] = null;
		$this->data['error'] = null;
		$this->data['success'] = null;
		$this->data['alert'] = null;

		if(is_post()) {
			$input = $this->input->post();
			switch ($input['process']) {
				case 'forgotpassword':
					$user = $this->Account->check_user_exist($input['username']);
					
					if($user != null) {
						$this->data['user'] = $user;
						$this->data['view'] = 'auth/resetpassword';
					} else {
						$this->data['error'] = 'No User Found';
						$this->data['view'] = 'auth/forgotpassword';
					}	
					break;				
				case 'resetpassword':
					if($input['password'] != $input['confirm_password'])
					{
						$this->data['alert'] = "Password do not match";
						$this->data['view'] = 'auth/resetpassword';
					}else{
						$this->Account->change_password($input['password'], $input['user_id']);
						$this->data['success'] = 'Password was successfully reset';
						$this->data['view'] = 'auth/resetpassword';
						redirect(base_url('auth/login'));
					}				
					break;
			}			

		}else{
			$this->data['view'] = 'auth/forgotpassword';
		}

		$this->load->view('shared/_layout', $this->data);
	}

	public function reset_password()
	{
		$this->data['show_build'] = false;
		$this->data['view'] = 'auth/resetpassword';
		$this->load->view('shared/_layout', $this->data);
	}

	public function logout()
	{
		$this->session->unset_userdata('user_id', '');
		$this->session->unset_userdata('username', '');
		$this->session->unset_userdata('display_name','');
		$this->session->unset_userdata('is_admin','');
		$this->session->unset_userdata('role_name','');
		redirect(base_url('main/index'));
	}

	/**
	* Redirects to dashboard or pos if logged in already
	*/
	private function logged_redirect() 
	{
		if($this->session->userdata('user_id')) {
			$url = ($this->session->userdata['is_admin']) ? base_url('main/dashboard') : base_url('pos') ;
			redirect($url);
		}
	}

	public function encrypt($arg = 'hey')
	{
		$encrypted = sha1($arg);
		echo $arg . '<br/>';
		echo $encrypted . '<br/>';
	}
}