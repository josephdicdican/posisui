<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->data['show_build'] = true;
		$this->posis_lib->check_session();
	}

	private $data = array();

	public function index() 
	{
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/notification.js');
		$this->data['view'] = 'notification/index';
		$this->data['title'] = 'Notification';
		$this->load->view('shared/_layout', $this->data);
	}
}
