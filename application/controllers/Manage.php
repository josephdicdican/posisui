<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->data['show_build'] = true;
		$this->posis_lib->check_session();
	}

	private $data = array();

	/**
	* Show manage users interface
	*/
	public function users() 
	{
		$this->posis_lib->admin_only();
		$this->data['view'] = 'manage/users';
		$this->data['corejs'] = array('core/users.js');
		$this->data['title'] = 'Users';
		$this->load->view('shared/_layout', $this->data);
	}

	/**
	* Show manage customers interface 
	*/
	public function customers() 
	{
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/customers.js');
		$this->data['view'] = 'manage/customers';
		$this->data['title'] = 'Customers';
		$this->load->view('shared/_layout', $this->data);
	}

	/**
	* Show manage suppliers interface
	*/
	public function suppliers() 
	{
		$this->posis_lib->admin_only();
		$this->data['corejs'] = array('core/suppliers.js');
		$this->data['view'] = 'manage/suppliers';
		$this->data['title'] = 'Suppliers';
		$this->load->view('shared/_layout', $this->data);
	}

}

