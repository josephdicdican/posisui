<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
$route['api/users/(0)'] = 'api/users/get_all';
$route['api/users/(:num)'] = 'api/users/get/$1';
$route['api/suppliers/(0)'] = 'api/suppliers/get_all';
$route['api/suppliers/(:num)'] = 'api/suppliers/get/$1';
$route['api/customers/(0)'] = 'api/customers/get_all';
$route['api/customers/(:num)'] = 'api/customers/get/$1';
$route['api/categories/(0)'] = 'api/categories/get_all';
$route['api/categories/(:num)'] = 'api/categories/get/$1';
$route['api/inventories/(0)'] = 'api/inventories/get_all';
$route['api/inventories/(:num)'] = 'api/inventories/get/$1';
$route['api/supplypurchases/purchases'] = 'api/supplypurchases/get_supply_purchases';
$route['api/supplypurchases/purchaselines/(:num)'] = 'api/supplypurchases/get_supply_purchase_lines/$1';
$route['api/logs/customer/(:num)'] = 'api/logs/get_customerlogs/$1';
$route['api/logs/supplier/(:num)'] = 'api/logs/get_supplierlogs/$1';
$route['api/logs/inventory/(:num)'] = 'api/logs/get_inventorylogs/$1';
$route['api/logs/user/(:num)'] = 'api/logs/get_userlogs/$1';

