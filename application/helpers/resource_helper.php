<?php

    if (!function_exists('pre_print_r')) {

        /**
         * Method print_r enclosed with <pre></pre> tags.
         * @param Object $expression
         * @param String $extra is the attribute of the pre tag
         * @param String $return
         * @return String
         */
        function pre_print_r($expression, $extra = '', $return = false) {
            if(trim($extra) == '')
                $extra = 'style="color:#000;"';
            
            echo '<pre '.(($extra) ? $extra : '').'>';
            print_r($expression, $return);
            echo '</pre>';
        }

    }

    if (!function_exists('load_resource')) {

        /**
         * Loads resources (e.g js, css, images); and returns the right html statement.
         * @param $filename String
         * @param $attrs String
         * @return String
         */
        function load_resource($filename, $attrs = '') {
            $ext = '';
            $url = '';
            $itm = '';
            try {
                $ext = end(explode('.', $filename));
                $ext = strtolower($ext);

                $url = base_url('resources');
                switch ($ext) {
                    case 'js':
                        $url .= '/js/';
                        $itm = '<script type="text/javascript" src="_xxx_" ' . $attrs . '></script>';
                        break;
                    case 'css':
                        $url .= '/css/';
                        $itm = '<link rel="stylesheet" type="text/css" href="_xxx_" ' . $attrs . '/>';
                        break;
                    case 'jpg': case 'jpeg':
                    case 'gif': case 'png':
                    case 'tiff': case 'bmp':
                        $url .= '/images/';
                        $itm = '<img src="_xxx_" ' . $attrs . '/>';
                        break;
                }

                $url .= $filename;
                if (trim($itm) != '')
                    $itm = str_replace('_xxx_', $url, $itm);
            } catch (Exception $ex) {
                throw $ex->getMessage();
            }

            unset($ext, $url);
            return $itm;
        }

    }
    