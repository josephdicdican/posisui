<?php

if (!function_exists('check_loggin_rsp')) {


    function check_loggin_rsp($rsp) {
        
        if ($rsp->return) {
            $rsp = $rsp->return;
            if (!$rsp->OK) {
                $rsp->OK = 0;
            }
            return $rsp->OK;
        }        
    }

}