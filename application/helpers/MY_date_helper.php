<?php

if (!function_exists('time_to_milli')) {
    /**
     * coverts time() to milliseconds and returns the result.
     * @param $time BigInt
     * @return BigInt
     */
    function time_to_milli($time = null) {
        $str = '';
        $len = 0;

        try {
            if (!$time)
                $time = time();

            $str = $time . '';
            $len = strlen($str);
            
            if ($len < 11)
                $time *= 1000;
            
        } catch(Exception $ex) {
            throw $ex;
        }

        return $time;
    }

}

if (!function_exists('date_to_milli')) {
    /**
     * coverts a date to milliseconds and returns the result.
     * @param $date String
     * @return BigInt
     */
    function date_to_milli($date = null) {
        try {
            if ($date)
                $date = strtotime($date);
            
            $date = time_to_milli($date);
            
        } catch(Exception $ex) {
            throw $ex;
        }

        return $date;
    }

}

if (!function_exists('milli_to_date')) {
    /**
     * converts milliseconds to date and returns the result
     * @param $milli BigInt
     * @param $format String
     * @return String
     */
    function milli_to_date($milli, $format = 'Y/m/d') {
        try {
            if ($milli){
                $milli = $milli / 1000;
                return date($format, $milli);  
            }  
            else
                throw new Exception('milli_to_date requires milliseconds as input');
        } catch(Exception $ex) {
            throw $ex;
        }
    }
}

