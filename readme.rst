###############
Point of Sale Inventory System (PoSIS) 
###############

Thesis Project

A project of five members. A system made purposely for restuarant/small stores to manage daily transactions and do inventory automatically.

**Development**

* PHP (CodeIgniter framework)
* MySql
* Javascript/JQuery
* Bootstrap Admin Template

**Requirements:**

* Xamp (phpMyAdmin)
* Database: posis_db
* Path: posisui/db_schema/posis_db.sql

**Project Folder:**
Should be extracted inside the "htdocs" or "www" folder

To run type in browser: recommended chrome (optional mozilla)
default: 

* localhost/posisui/

if listening to specific port:

* localhost:85/posisui

**Application Access**

Admin User

* username: administrator
* password: welcome

Staff User

* username: jdicdican
* password: welcome