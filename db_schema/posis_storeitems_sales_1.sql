-- Sales
-- store_item_id, sku, storeitemname, items, sales, date_of_order, week, month, year
select store_item_id, sku, storeitemname, 
sum(quantity) as items, truncate(sum(quantity * price_per_unit),2) as sales,
date_of_order,
week, month, year 
from storeitems_sales 
group by store_item_id;

select 
count(DISTINCT order_id) as transactions, sum(quantity) as items, 
truncate(sum(quantity * price_per_unit),2) as sales
from storeitems_sales;

select distinct week, sum(quantity) as items, 
truncate(sum(quantity * price_per_unit),2) as sales
from storeitems_sales
group by week;

select distinct month, sum(quantity) as items, 
truncate(sum(quantity * price_per_unit),2) as sales
from storeitems_sales
group by month;

select distinct year, sum(quantity) as items, 
truncate(sum(quantity * price_per_unit),2) as sales
from storeitems_sales
group by year;

select distinct order_id from storeitems_sales;
