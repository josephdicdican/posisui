-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2016 at 09:43 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posis_db`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `count_products_in_category` (`category_id` INT(11)) RETURNS INT(11) BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(productsincategories.category_id) as categorizedproducts
    FROM 
    	productsincategories 
    LEFT JOIN
    	storeitems ON productsincategories.product_id = storeitems.product_id
    WHERE 
    	productsincategories.category_id = category_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_purchaselines_by_purchase_id` (`supply_purchase_id` INT(11)) RETURNS INT(11) BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(spl.supply_purchase_id) as storeitems_with_unit
    FROM supplypurchaselines as spl 
    WHERE spl.supply_purchase_id = supply_purchase_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_by_product_id` (`product_id` INT(11)) RETURNS INT(11) BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.product_id) as product_storeitems
    FROM storeitems WHERE storeitems.product_id = product_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_with_unit` (`unit_id` INT(11)) RETURNS INT(11) BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.unit_id) as storeitems_with_unit
    FROM storeitems 
    WHERE storeitems.unit_id = unit_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_order_total_amount` (`order_id` INT) RETURNS DOUBLE BEGIN
   declare total_amount DOUBLE(5,2);

   SELECT SUM(o.quantity * s.price_per_unit) INTO total_amount
   from orderlines as o
   LEFT JOIN storeitems as s ON s.store_item_id = o.store_item_id
   WHERE o.order_id = order_id;
  RETURN total_amount;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_order_total_items` (`order_id` INT) RETURNS INT(11) BEGIN
   declare total_items DOUBLE(5,2);

   SELECT SUM(o.quantity) INTO total_items
   from orderlines as o
   WHERE o.order_id = order_id;
  RETURN total_items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_purchase_total_qty` (`supply_purchase_id` INT(11)) RETURNS INT(11) BEGIN
	declare no_more_rows TINYINT(1);
    declare total_qty INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	SUM(spl.purchase_qty) as total
    FROM supplypurchaselines as spl 
    WHERE spl.supply_purchase_id = supply_purchase_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into total_qty;
    
    if no_more_rows then
    	set total_qty = 0;
    ELSE
    
    	if ISNULL(total_qty) THEN
        	set total_qty = 0;
        end if;
    end if;
    
    close cur;

  RETURN total_qty;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_storeitem_name` (`item_name` VARCHAR(100), `measure_name` VARCHAR(100)) RETURNS VARCHAR(200) CHARSET latin1 NO SQL
BEGIN declare storeitemname varchar(200); declare n1 varchar(100); declare n2 varchar(200); set storeitemname = ''; set n1 = TRIM(item_name); set n2 = TRIM(measure_name); set storeitemname = CONCAT(n1, ' ',n2); return storeitemname; END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `category_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_id`, `category_name`) VALUES
(4, 0, 'Foods'),
(6, 4, 'Desserts'),
(7, 4, 'Viands'),
(35, 0, 'Groceries'),
(53, 0, 'Drinks'),
(54, 53, 'Soft Drinks'),
(59, 53, 'Beverages'),
(61, 4, 'Appetizers'),
(62, 4, 'Noodles and Rice'),
(63, 35, 'Biscuits		'),
(64, 35, 'Chips and Chocolates		'),
(66, 35, 'Cup Noddles		');

-- --------------------------------------------------------

--
-- Stand-in structure for view `categories_list`
--
CREATE TABLE `categories_list` (
`category_id` int(11)
,`parent_id` int(11)
,`category_name` varchar(100)
,`image_id` int(11)
,`image` varchar(255)
,`image_type` tinyint(2)
,`products_count` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `categoryimages`
--

CREATE TABLE `categoryimages` (
  `category_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoryimages`
--

INSERT INTO `categoryimages` (`category_id`, `image_id`) VALUES
(53, 21),
(54, 22),
(4, 23),
(6, 24),
(7, 25),
(61, 26);

-- --------------------------------------------------------

--
-- Table structure for table `customerlogs`
--

CREATE TABLE `customerlogs` (
  `customer_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customerlogs`
--

INSERT INTO `customerlogs` (`customer_log_id`, `user_id`, `customer_id`, `remarks`, `date_created`, `is_unread`) VALUES
(1, 18, 7, 'Added', '2016-02-10 13:19:54', 1),
(2, 18, 7, 'Updated', '2016-02-10 13:20:09', 1),
(6, 18, 11, 'Added', '2016-03-04 17:11:36', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `customerlogs_list`
--
CREATE TABLE `customerlogs_list` (
`customer_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`customer_id` int(11)
,`firstname` varchar(100)
,`lastname` varchar(100)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` tinyint(2) NOT NULL,
  `is_senior` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `firstname`, `lastname`, `address`, `email`, `gender`, `is_senior`) VALUES
(7, 'Joseph', 'Dicdican', 'Pusok, Lapu-Lapu City, Cebu', 'joseph.dicdican@gmail.com', 1, 1),
(11, 'Helfe', 'Marquez', 'Cebu City', 'helfe.marquez@gmail.com', 0, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `customers_list`
--
CREATE TABLE `customers_list` (
`customer_id` int(11)
,`firstname` varchar(100)
,`lastname` varchar(100)
,`address` varchar(100)
,`email` varchar(100)
,`gender` tinyint(2)
,`is_senior` tinyint(2)
,`card_no` varchar(50)
);

-- --------------------------------------------------------

--
-- Table structure for table `discountedorders`
--

CREATE TABLE `discountedorders` (
  `order_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `discount_id` int(11) NOT NULL,
  `discount_percentage` double NOT NULL,
  `discount_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `image_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_type` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`image_id`, `image`, `image_type`) VALUES
(21, '1453823114-penguins.jpg', 0),
(22, '1453827003-jellyfish.jpg', 0),
(23, '1453827052-koala.jpg', 0),
(24, '1453827072-tulips.jpg', 0),
(25, '1453987920-hydrangeas.jpg', 0),
(26, '1457121362-1453827072-tulips.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `inventory_id` int(11) NOT NULL,
  `stock_at_hand` int(11) NOT NULL DEFAULT '0',
  `max_stock_qty` int(11) NOT NULL DEFAULT '0',
  `min_stock_qty` int(11) NOT NULL DEFAULT '0',
  `reordering_qty` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`inventory_id`, `stock_at_hand`, `max_stock_qty`, `min_stock_qty`, `reordering_qty`) VALUES
(23, 10, 10, 3, 5),
(24, 12, 15, 3, 5),
(25, 15, 15, 3, 5),
(26, 20, 18, 3, 5),
(60, 15, 15, 3, 5),
(61, 12, 12, 3, 5),
(62, 15, 15, 3, 5),
(64, 12, 12, 5, 8),
(65, 12, 12, 3, 5),
(66, 15, 15, 3, 5),
(69, 20, 20, 3, 5),
(70, 15, 15, 5, 3),
(75, 15, 10, 5, 8),
(80, 20, 20, 5, 8),
(81, 30, 15, 5, 7),
(82, 24, 12, 3, 5),
(83, 20, 10, 2, 3),
(84, 48, 25, 8, 10),
(85, 30, 15, 5, 7),
(86, 48, 25, 8, 10),
(87, 24, 12, 8, 5),
(88, 20, 10, 3, 5);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventories_list`
--
CREATE TABLE `inventories_list` (
`inventory_id` int(11)
,`stock_at_hand` int(11)
,`max_stock_qty` int(11)
,`min_stock_qty` int(11)
,`reordering_qty` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitemname` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `inventorylogs`
--

CREATE TABLE `inventorylogs` (
  `inventory_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventorylogs`
--

INSERT INTO `inventorylogs` (`inventory_log_id`, `user_id`, `inventory_id`, `remarks`, `date_created`, `is_unread`) VALUES
(18, 18, 85, 'Added', '2016-03-04 19:59:45', 1),
(19, 18, 83, 'Added', '2016-03-04 20:00:13', 1),
(21, 18, 81, 'Added', '2016-03-04 20:00:50', 1),
(22, 18, 80, 'Added', '2016-03-04 20:01:19', 1),
(23, 18, 61, 'Added', '2016-03-04 20:02:23', 1),
(24, 18, 62, 'Added', '2016-03-04 20:02:39', 1),
(25, 18, 60, 'Added', '2016-03-04 20:03:03', 1),
(26, 18, 64, 'Added', '2016-03-04 20:03:15', 1),
(27, 18, 65, 'Added', '2016-03-04 20:03:28', 1),
(28, 18, 66, 'Added', '2016-03-04 20:03:47', 1),
(29, 18, 25, 'Added', '2016-03-04 20:04:15', 1),
(30, 18, 83, 'Updated', '2016-03-04 20:07:03', 1),
(31, 18, 75, 'Added', '2016-03-04 20:08:45', 1),
(33, 18, 23, 'Added', '2016-03-04 20:17:10', 1),
(35, 18, 26, 'Added', '2016-03-04 20:19:17', 1),
(36, 18, 70, 'Added', '2016-03-04 20:21:30', 1),
(37, 18, 69, 'Added', '2016-03-04 20:22:11', 1),
(38, 18, 69, 'Updated', '2016-03-04 20:23:31', 1),
(39, 18, 69, 'Updated', '2016-03-04 20:24:55', 1),
(40, 18, 88, 'Added', '2016-03-04 20:27:30', 1),
(41, 18, 87, 'Added', '2016-03-04 20:28:11', 1),
(42, 18, 84, 'Added', '2016-03-04 20:28:32', 1),
(43, 18, 86, 'Added', '2016-03-04 20:29:06', 1),
(44, 18, 82, 'Added', '2016-03-04 20:29:25', 1),
(45, 18, 24, 'Added', '2016-03-04 20:37:07', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventorylogs_list`
--
CREATE TABLE `inventorylogs_list` (
`inventory_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`inventory_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);

-- --------------------------------------------------------

--
-- Table structure for table `ordercustomers`
--

CREATE TABLE `ordercustomers` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderlines`
--

CREATE TABLE `orderlines` (
  `line_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `orderlines_list`
--
CREATE TABLE `orderlines_list` (
`order_id` int(11)
,`date_of_order` timestamp
,`is_checkout` tinyint(2)
,`is_discounted` tinyint(2)
,`line_id` int(11)
,`store_item_id` int(11)
,`quantity` int(11)
,`sku` varchar(200)
,`storeitemname` varchar(200)
,`price_per_unit` double
,`stock_at_hand` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `orderlogs`
--

CREATE TABLE `orderlogs` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remarks` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `date_of_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_checkout` tinyint(2) NOT NULL,
  `is_discounted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `date_of_order`, `is_checkout`, `is_discounted`) VALUES
(3, '2016-03-03 04:22:35', 0, 0),
(6, '2016-03-03 13:00:32', 1, 0),
(7, '2016-03-03 13:05:29', 1, 0),
(8, '2016-03-04 06:07:35', 1, 0),
(9, '2016-03-04 06:02:08', 1, 0),
(10, '2016-03-04 06:05:14', 0, 0),
(11, '2016-03-04 08:20:47', 1, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `orders_transactions_list`
--
CREATE TABLE `orders_transactions_list` (
`order_id` int(11)
,`date_of_order` timestamp
,`is_checkout` tinyint(2)
,`is_discounted` tinyint(2)
,`total_amount` double
,`total_items` int(11)
,`discount_id` int(11)
,`discount_percentage` double
,`discount_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`) VALUES
(14, 'Leche Flan'),
(15, 'Mangga Cheesecake'),
(16, 'Halo-Halo'),
(17, 'Ice Cream'),
(18, 'Mango Pandan'),
(19, 'Lechon Kawali'),
(20, 'Kare-Kare'),
(22, 'Beef Caldereta'),
(23, 'Bicol Express'),
(24, 'Humba'),
(25, 'Crispy Dinuguan'),
(26, 'Adobong Pusit'),
(27, 'Binagoongang Baboy'),
(28, 'Griled Scallops'),
(29, 'Deep-Fried Tuna Belly'),
(30, 'Adobo'),
(31, 'Chicharon Bulaklak'),
(32, 'Lumpia Frito'),
(33, 'Crispy Shrimps'),
(34, 'Crispy Wings'),
(35, 'Chorizo Dinamitas'),
(36, 'Crusted Prawns'),
(37, 'Bam-i'),
(38, 'Palabok'),
(39, 'Sotanghon Guisado'),
(40, 'Pancit Canton'),
(41, 'Salted Egg Rice'),
(42, 'Sisig Rice Platter'),
(43, 'Tiger'),
(44, 'Butter Coconut'),
(45, 'Combi'),
(46, 'Voice'),
(47, 'Grahams'),
(48, 'Oreo'),
(49, 'Hansel'),
(50, 'Bingo'),
(51, 'Piattos'),
(52, 'Roller Coaster'),
(53, 'Nova'),
(55, 'Tortillos'),
(56, 'Chippy'),
(57, 'Chicharron ni Mang Juan'),
(58, 'Ricoa Curly Tops'),
(59, 'Wiggles'),
(60, 'Cloud 9'),
(61, 'V Cut'),
(62, 'Nissin Cup Noodles'),
(63, 'Nissin Yakisoba'),
(64, 'Payless Instant Mami'),
(65, 'Payless Extra Big'),
(66, 'C2 Cool and Clean'),
(67, 'Blue'),
(68, 'Nature'),
(69, 'Milk Tea'),
(70, 'Shake'),
(71, 'Coffee'),
(72, 'Coke'),
(73, 'Sprite'),
(74, 'Sprite');

-- --------------------------------------------------------

--
-- Table structure for table `productsincategories`
--

CREATE TABLE `productsincategories` (
  `pnc_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productsincategories`
--

INSERT INTO `productsincategories` (`pnc_id`, `category_id`, `product_id`) VALUES
(14, 6, 14),
(15, 6, 15),
(16, 6, 16),
(17, 6, 17),
(18, 6, 18),
(19, 7, 19),
(20, 7, 20),
(22, 7, 22),
(23, 7, 23),
(24, 7, 24),
(25, 7, 25),
(26, 7, 26),
(27, 7, 27),
(28, 7, 28),
(29, 7, 29),
(30, 7, 30),
(72, 54, 72),
(73, 54, 73),
(74, 54, 74),
(66, 59, 66),
(67, 59, 67),
(68, 59, 68),
(69, 59, 69),
(70, 59, 70),
(71, 59, 71),
(31, 61, 31),
(32, 61, 32),
(33, 61, 33),
(34, 61, 34),
(35, 61, 35),
(36, 61, 36),
(37, 62, 37),
(38, 62, 38),
(39, 62, 39),
(40, 62, 40),
(41, 62, 41),
(42, 62, 42),
(43, 63, 43),
(44, 63, 44),
(45, 63, 45),
(46, 63, 46),
(47, 63, 47),
(48, 63, 48),
(49, 63, 49),
(50, 63, 50),
(51, 64, 51),
(52, 64, 52),
(53, 64, 53),
(55, 64, 55),
(56, 64, 56),
(57, 64, 57),
(58, 64, 58),
(59, 64, 59),
(60, 64, 60),
(61, 64, 61),
(62, 66, 62),
(63, 66, 63),
(64, 66, 64),
(65, 66, 65);

-- --------------------------------------------------------

--
-- Table structure for table `seniorcitizens`
--

CREATE TABLE `seniorcitizens` (
  `customer_id` int(11) NOT NULL,
  `card_no` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seniorcitizens`
--

INSERT INTO `seniorcitizens` (`customer_id`, `card_no`) VALUES
(7, '123456576565');

-- --------------------------------------------------------

--
-- Table structure for table `storeitemimages`
--

CREATE TABLE `storeitemimages` (
  `store_item_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storeitems`
--

CREATE TABLE `storeitems` (
  `store_item_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `sku` varchar(200) NOT NULL,
  `price_per_unit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storeitems`
--

INSERT INTO `storeitems` (`store_item_id`, `product_id`, `unit_id`, `sku`, `price_per_unit`) VALUES
(23, 14, 4, '122113111', 30),
(24, 15, 4, '122113112', 45),
(25, 16, 4, '122113113', 35),
(26, 17, 4, '122113114', 15),
(27, 18, 4, '122113115', 25),
(28, 19, 4, '122113116', 60),
(29, 20, 4, '122113117', 55),
(31, 22, 4, '122113118', 50),
(32, 23, 4, '122113119', 40),
(33, 24, 4, '122113120', 40),
(34, 25, 4, '122113121', 30),
(35, 26, 4, '122113122', 35),
(36, 27, 4, '122113123', 40),
(37, 28, 4, '122113124', 40),
(38, 29, 4, '122113125', 45),
(39, 30, 4, '122113126', 30),
(40, 31, 4, '122113127', 45),
(41, 32, 4, '122113129', 40),
(42, 33, 4, '122113128', 35),
(43, 34, 4, '122113130', 65),
(44, 35, 4, '122113131', 40),
(45, 36, 4, '122113132', 35),
(46, 37, 4, '122113133', 25),
(47, 38, 4, '122113134', 40),
(48, 39, 4, '122113136', 35),
(49, 40, 4, '122113138', 20),
(50, 41, 4, '122113140', 40),
(51, 42, 4, '122113125', 35),
(52, 43, 1, '122113140', 6),
(53, 44, 1, '122113141', 10),
(54, 45, 1, '122113143', 6),
(55, 46, 1, '122113144', 6),
(56, 47, 1, '122113146', 8),
(57, 48, 1, '122113147', 15),
(58, 49, 1, '122113149', 6),
(59, 50, 1, '122113150', 6),
(60, 51, 1, '122113152', 15),
(61, 52, 1, '122113153', 10),
(62, 53, 1, '122113154', 15),
(64, 55, 1, '122113157', 7),
(65, 56, 1, '122113155', 7),
(66, 57, 1, '122113156', 13),
(67, 58, 1, '122113159', 2),
(68, 59, 1, '122113158', 1.5),
(69, 60, 1, '122113160', 8),
(70, 61, 1, '122113161', 13),
(71, 62, 1, '122113163', 14),
(72, 63, 1, '122113164', 18),
(73, 64, 1, '122113165', 9),
(74, 65, 1, '122113168', 13),
(75, 66, 12, '122113175', 23),
(76, 67, 12, '122113176', 20),
(77, 68, 12, '1221131777', 12),
(78, 69, 5, '122113178', 35),
(79, 70, 5, '122113179', 25),
(80, 71, 3, '122113180', 15),
(81, 72, 8, '122113190', 20),
(82, 72, 6, '122113192', 50),
(83, 72, 13, '122113193', 65),
(84, 72, 7, '122113194', 10),
(85, 73, 8, '122113210', 20),
(86, 73, 7, '122113211', 10),
(87, 73, 11, '122113212', 50),
(88, 74, 13, '122113215', 65);

-- --------------------------------------------------------

--
-- Stand-in structure for view `storeitems_list`
--
CREATE TABLE `storeitems_list` (
`store_item_id` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`category_id` int(11)
,`category_name` varchar(100)
,`parent_id` int(11)
,`storeitemname` varchar(200)
,`inventory_id` int(11)
,`stock_at_hand` int(11)
,`reordering_qty` int(11)
,`max_stock_qty` int(11)
,`min_stock_qty` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `supplierlogs`
--

CREATE TABLE `supplierlogs` (
  `supplier_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplierlogs`
--

INSERT INTO `supplierlogs` (`supplier_log_id`, `user_id`, `supplier_id`, `remarks`, `date_created`, `is_unread`) VALUES
(2, 18, 1, 'Updated', '2016-02-13 11:21:11', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplierlogs_list`
--
CREATE TABLE `supplierlogs_list` (
`supplier_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`supplier_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_contact` varchar(100) DEFAULT NULL,
  `supplier_email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `supplier_name`, `supplier_contact`, `supplier_email`) VALUES
(1, 'Aqua Soft', '09107728986', 'aquasoft@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchaselines`
--

CREATE TABLE `supplypurchaselines` (
  `purchase_line_id` int(11) NOT NULL,
  `supply_purchase_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `purchase_qty` int(11) NOT NULL,
  `purchase_price_per_unit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplypurchaselines`
--

INSERT INTO `supplypurchaselines` (`purchase_line_id`, `supply_purchase_id`, `store_item_id`, `purchase_qty`, `purchase_price_per_unit`) VALUES
(27, 12, 88, 10, 60),
(28, 12, 87, 12, 45),
(29, 12, 86, 24, 8),
(30, 12, 85, 15, 18),
(31, 12, 84, 24, 8),
(32, 12, 83, 10, 60),
(33, 12, 82, 12, 45),
(34, 12, 81, 15, 18);

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplypurchaselines_list`
--
CREATE TABLE `supplypurchaselines_list` (
`supply_purchase_id` int(11)
,`date_of_purchase` timestamp
,`is_confirmed` tinyint(2)
,`purchase_line_id` int(11)
,`store_item_id` int(11)
,`purchase_qty` int(11)
,`purchase_price_per_unit` double
,`storeitemname` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchases`
--

CREATE TABLE `supplypurchases` (
  `supply_purchase_id` int(11) NOT NULL,
  `date_of_purchase` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_confirmed` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplypurchases`
--

INSERT INTO `supplypurchases` (`supply_purchase_id`, `date_of_purchase`, `date_created`, `is_confirmed`) VALUES
(7, '2016-02-12 08:19:35', '2016-02-12 16:19:44', 1),
(8, '2016-02-12 08:24:52', '2016-02-12 16:26:50', 1),
(11, '2016-02-20 08:52:24', '2016-02-20 16:55:33', 1),
(12, '2016-03-04 12:25:33', '2016-03-04 20:29:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchasesuppliers`
--

CREATE TABLE `supplypurchasesuppliers` (
  `supply_purchase_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplypurchase_list`
--
CREATE TABLE `supplypurchase_list` (
`supply_purchase_id` int(11)
,`date_of_purchase` timestamp
,`date_created` timestamp
,`is_confirmed` tinyint(2)
,`total_qty` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `unitmeasures`
--

CREATE TABLE `unitmeasures` (
  `unit_id` int(11) NOT NULL,
  `measure_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `unitmeasures`
--

INSERT INTO `unitmeasures` (`unit_id`, `measure_name`) VALUES
(1, 'unit'),
(2, 'piece'),
(3, 'cup'),
(4, 'serve'),
(5, 'glass'),
(6, 'litre'),
(7, '8 ounce'),
(8, 'can'),
(9, '10 g'),
(10, 'box'),
(11, '1.5 litre'),
(12, '500 ml'),
(13, '2 litres');

-- --------------------------------------------------------

--
-- Stand-in structure for view `unitmeasure_storeitems`
--
CREATE TABLE `unitmeasure_storeitems` (
`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitems_count` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `userinroles`
--

CREATE TABLE `userinroles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userinroles`
--

INSERT INTO `userinroles` (`user_id`, `role_id`) VALUES
(18, 1),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2);

-- --------------------------------------------------------

--
-- Table structure for table `userlogs`
--

CREATE TABLE `userlogs` (
  `user_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user2log_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlogs`
--

INSERT INTO `userlogs` (`user_log_id`, `user_id`, `user2log_id`, `remarks`, `date_created`, `is_unread`) VALUES
(2, 18, 21, 'Added', '2016-03-04 17:00:30', 1),
(3, 18, 22, 'Added', '2016-03-04 17:01:13', 1),
(4, 18, 23, 'Added', '2016-03-04 17:02:17', 1),
(5, 18, 24, 'Added', '2016-03-04 17:02:43', 1),
(6, 18, 25, 'Added', '2016-03-04 17:03:17', 1),
(7, 18, 22, 'Updated', '2016-03-04 17:05:34', 1),
(8, 18, 22, 'Updated', '2016-03-04 17:05:39', 1),
(9, 18, 21, 'Updated', '2016-03-04 17:05:50', 1),
(10, 18, 23, 'Updated', '2016-03-04 17:06:25', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `userlogs_list`
--
CREATE TABLE `userlogs_list` (
`user_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`user2log_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE `userroles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `display_name`, `username`, `password`, `date_created`, `date_modified`) VALUES
(18, 'Admin', 'administrator', '81f344a7686a80b4c5293e8fdc0b0160c82c06a8', '2016-01-17 16:00:00', '2016-01-18 17:51:55'),
(21, 'Joseph Dicdican', 'jdicdican', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-03-03 22:00:30', '2016-03-03 22:05:50'),
(22, 'Elizabeth Rosales', 'erosales', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-03-03 22:01:12', '2016-03-03 22:05:39'),
(23, 'Anne Merlyn Martinez', 'ammartinez', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-03-03 22:02:17', '2016-03-03 22:06:25'),
(24, 'Ivy Grace Olasiman', 'igolasiman', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-03-03 22:02:42', '0000-00-00 00:00:00'),
(25, 'Helfe Marquez', 'hmarquez', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-03-03 22:03:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_list`
--
CREATE TABLE `users_list` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`role_id` int(11)
,`role_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_vw`
--
CREATE TABLE `users_vw` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`date_created` timestamp
,`date_modified` timestamp
,`role_id` int(11)
,`role_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure for view `categories_list`
--
DROP TABLE IF EXISTS `categories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `categories_list`  AS  select `c`.`category_id` AS `category_id`,`c`.`parent_id` AS `parent_id`,`c`.`category_name` AS `category_name`,`ci`.`image_id` AS `image_id`,`i`.`image` AS `image`,`i`.`image_type` AS `image_type`,`count_products_in_category`(`c`.`category_id`) AS `products_count` from ((`categories` `c` left join `categoryimages` `ci` on((`c`.`category_id` = `ci`.`category_id`))) left join `images` `i` on((`ci`.`image_id` = `i`.`image_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `customerlogs_list`
--
DROP TABLE IF EXISTS `customerlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customerlogs_list`  AS  select `cl`.`customer_log_id` AS `customer_log_id`,`cl`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`cl`.`customer_id` AS `customer_id`,`c`.`firstname` AS `firstname`,`c`.`lastname` AS `lastname`,`cl`.`remarks` AS `remarks`,`cl`.`date_created` AS `date_created`,`cl`.`is_unread` AS `is_unread` from ((`customerlogs` `cl` left join `customers` `c` on((`cl`.`customer_id` = `c`.`customer_id`))) left join `users` `u` on((`cl`.`user_id` = `u`.`user_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `customers_list`
--
DROP TABLE IF EXISTS `customers_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customers_list`  AS  select `c`.`customer_id` AS `customer_id`,`c`.`firstname` AS `firstname`,`c`.`lastname` AS `lastname`,`c`.`address` AS `address`,`c`.`email` AS `email`,`c`.`gender` AS `gender`,`c`.`is_senior` AS `is_senior`,`s`.`card_no` AS `card_no` from (`customers` `c` left join `seniorcitizens` `s` on((`c`.`customer_id` = `s`.`customer_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `inventories_list`
--
DROP TABLE IF EXISTS `inventories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventories_list`  AS  select `i`.`inventory_id` AS `inventory_id`,`i`.`stock_at_hand` AS `stock_at_hand`,`i`.`max_stock_qty` AS `max_stock_qty`,`i`.`min_stock_qty` AS `min_stock_qty`,`i`.`reordering_qty` AS `reordering_qty`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname` from (((`inventories` `i` left join `storeitems` `s` on((`i`.`inventory_id` = `s`.`store_item_id`))) left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `inventorylogs_list`
--
DROP TABLE IF EXISTS `inventorylogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventorylogs_list`  AS  select `i`.`inventory_log_id` AS `inventory_log_id`,`i`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`i`.`inventory_id` AS `inventory_id`,`i`.`remarks` AS `remarks`,`i`.`date_created` AS `date_created`,`i`.`is_unread` AS `is_unread` from (`inventorylogs` `i` left join `users` `u` on((`i`.`user_id` = `u`.`user_id`))) where 1 ;

-- --------------------------------------------------------

--
-- Structure for view `orderlines_list`
--
DROP TABLE IF EXISTS `orderlines_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orderlines_list`  AS  select `o`.`order_id` AS `order_id`,`o`.`date_of_order` AS `date_of_order`,`o`.`is_checkout` AS `is_checkout`,`o`.`is_discounted` AS `is_discounted`,`ol`.`line_id` AS `line_id`,`ol`.`store_item_id` AS `store_item_id`,`ol`.`quantity` AS `quantity`,`s`.`sku` AS `sku`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`s`.`price_per_unit` AS `price_per_unit`,`i`.`stock_at_hand` AS `stock_at_hand` from (((((`orderlines` `ol` left join `orders` `o` on((`o`.`order_id` = `ol`.`order_id`))) left join `storeitems` `s` on((`s`.`store_item_id` = `ol`.`store_item_id`))) left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`u`.`unit_id` = `s`.`unit_id`))) left join `inventories` `i` on((`i`.`inventory_id` = `s`.`store_item_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `orders_transactions_list`
--
DROP TABLE IF EXISTS `orders_transactions_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orders_transactions_list`  AS  select `o`.`order_id` AS `order_id`,`o`.`date_of_order` AS `date_of_order`,`o`.`is_checkout` AS `is_checkout`,`o`.`is_discounted` AS `is_discounted`,`get_order_total_amount`(`o`.`order_id`) AS `total_amount`,`get_order_total_items`(`o`.`order_id`) AS `total_items`,`do`.`discount_id` AS `discount_id`,`d`.`discount_percentage` AS `discount_percentage`,`d`.`discount_name` AS `discount_name` from ((`orders` `o` left join `discountedorders` `do` on((`do`.`order_id` = `o`.`order_id`))) left join `discounts` `d` on((`d`.`discount_id` = `do`.`discount_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `storeitems_list`
--
DROP TABLE IF EXISTS `storeitems_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storeitems_list`  AS  select `s`.`store_item_id` AS `store_item_id`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`pc`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`c`.`parent_id` AS `parent_id`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`i`.`inventory_id` AS `inventory_id`,`i`.`stock_at_hand` AS `stock_at_hand`,`i`.`reordering_qty` AS `reordering_qty`,`i`.`max_stock_qty` AS `max_stock_qty`,`i`.`min_stock_qty` AS `min_stock_qty` from (((((`storeitems` `s` left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`))) left join `productsincategories` `pc` on((`p`.`product_id` = `pc`.`product_id`))) left join `categories` `c` on((`c`.`category_id` = `pc`.`category_id`))) left join `inventories` `i` on((`s`.`store_item_id` = `i`.`inventory_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `supplierlogs_list`
--
DROP TABLE IF EXISTS `supplierlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplierlogs_list`  AS  select `s`.`supplier_log_id` AS `supplier_log_id`,`s`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`s`.`supplier_id` AS `supplier_id`,`s`.`remarks` AS `remarks`,`s`.`date_created` AS `date_created`,`s`.`is_unread` AS `is_unread` from (`supplierlogs` `s` left join `users` `u` on((`s`.`user_id` = `u`.`user_id`))) where 1 ;

-- --------------------------------------------------------

--
-- Structure for view `supplypurchaselines_list`
--
DROP TABLE IF EXISTS `supplypurchaselines_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplypurchaselines_list`  AS  select `supplypurchases`.`supply_purchase_id` AS `supply_purchase_id`,`supplypurchases`.`date_of_purchase` AS `date_of_purchase`,`supplypurchases`.`is_confirmed` AS `is_confirmed`,`supplypurchaselines`.`purchase_line_id` AS `purchase_line_id`,`supplypurchaselines`.`store_item_id` AS `store_item_id`,`supplypurchaselines`.`purchase_qty` AS `purchase_qty`,`supplypurchaselines`.`purchase_price_per_unit` AS `purchase_price_per_unit`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname` from ((((`supplypurchases` join `supplypurchaselines` on((`supplypurchases`.`supply_purchase_id` = `supplypurchaselines`.`supply_purchase_id`))) left join `storeitems` `s` on((`s`.`store_item_id` = `supplypurchaselines`.`store_item_id`))) left join `products` `p` on((`p`.`product_id` = `s`.`product_id`))) left join `unitmeasures` `u` on((`u`.`unit_id` = `s`.`unit_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `supplypurchase_list`
--
DROP TABLE IF EXISTS `supplypurchase_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplypurchase_list`  AS  select `supplypurchases`.`supply_purchase_id` AS `supply_purchase_id`,`supplypurchases`.`date_of_purchase` AS `date_of_purchase`,`supplypurchases`.`date_created` AS `date_created`,`supplypurchases`.`is_confirmed` AS `is_confirmed`,`get_purchase_total_qty`(`supplypurchases`.`supply_purchase_id`) AS `total_qty` from `supplypurchases` ;

-- --------------------------------------------------------

--
-- Structure for view `unitmeasure_storeitems`
--
DROP TABLE IF EXISTS `unitmeasure_storeitems`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `unitmeasure_storeitems`  AS  select `unitmeasures`.`unit_id` AS `unit_id`,`unitmeasures`.`measure_name` AS `measure_name`,`count_storeitems_with_unit`(`unitmeasures`.`unit_id`) AS `storeitems_count` from `unitmeasures` ;

-- --------------------------------------------------------

--
-- Structure for view `userlogs_list`
--
DROP TABLE IF EXISTS `userlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `userlogs_list`  AS  select `ul`.`user_log_id` AS `user_log_id`,`ul`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`ul`.`user2log_id` AS `user2log_id`,`ul`.`remarks` AS `remarks`,`ul`.`date_created` AS `date_created`,`ul`.`is_unread` AS `is_unread` from (`userlogs` `ul` left join `users` `u` on((`ul`.`user_id` = `u`.`user_id`))) where 1 ;

-- --------------------------------------------------------

--
-- Structure for view `users_list`
--
DROP TABLE IF EXISTS `users_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_list`  AS  select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `users_vw`
--
DROP TABLE IF EXISTS `users_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_vw`  AS  select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`date_created` AS `date_created`,`users`.`date_modified` AS `date_modified`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `categoryimages`
--
ALTER TABLE `categoryimages`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `image_id` (`image_id`);

--
-- Indexes for table `customerlogs`
--
ALTER TABLE `customerlogs`
  ADD PRIMARY KEY (`customer_log_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discountedorders`
--
ALTER TABLE `discountedorders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `discount_id` (`discount_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`inventory_id`);

--
-- Indexes for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
  ADD PRIMARY KEY (`inventory_log_id`),
  ADD KEY `user_id` (`user_id`,`inventory_id`),
  ADD KEY `inventory_id` (`inventory_id`);

--
-- Indexes for table `ordercustomers`
--
ALTER TABLE `ordercustomers`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `orderlines`
--
ALTER TABLE `orderlines`
  ADD PRIMARY KEY (`line_id`),
  ADD KEY `order_id` (`order_id`,`store_item_id`),
  ADD KEY `store_item_id` (`store_item_id`);

--
-- Indexes for table `orderlogs`
--
ALTER TABLE `orderlogs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `user_id` (`user_id`,`order_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `productsincategories`
--
ALTER TABLE `productsincategories`
  ADD PRIMARY KEY (`pnc_id`),
  ADD KEY `category_id` (`category_id`,`product_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `storeitemimages`
--
ALTER TABLE `storeitemimages`
  ADD PRIMARY KEY (`store_item_id`),
  ADD KEY `image_id` (`image_id`);

--
-- Indexes for table `storeitems`
--
ALTER TABLE `storeitems`
  ADD PRIMARY KEY (`store_item_id`),
  ADD KEY `product_id` (`product_id`,`unit_id`),
  ADD KEY `unit_id` (`unit_id`);

--
-- Indexes for table `supplierlogs`
--
ALTER TABLE `supplierlogs`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
  ADD PRIMARY KEY (`purchase_line_id`),
  ADD KEY `supply_purchase_id` (`supply_purchase_id`,`store_item_id`),
  ADD KEY `store_item_id` (`store_item_id`);

--
-- Indexes for table `supplypurchases`
--
ALTER TABLE `supplypurchases`
  ADD PRIMARY KEY (`supply_purchase_id`);

--
-- Indexes for table `supplypurchasesuppliers`
--
ALTER TABLE `supplypurchasesuppliers`
  ADD PRIMARY KEY (`supply_purchase_id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `unitmeasures`
--
ALTER TABLE `unitmeasures`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `userinroles`
--
ALTER TABLE `userinroles`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `userlogs`
--
ALTER TABLE `userlogs`
  ADD PRIMARY KEY (`user_log_id`),
  ADD KEY `user_id` (`user_id`,`user2log_id`),
  ADD KEY `user2log_id` (`user2log_id`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `customerlogs`
--
ALTER TABLE `customerlogs`
  MODIFY `customer_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
  MODIFY `inventory_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `orderlines`
--
ALTER TABLE `orderlines`
  MODIFY `line_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `orderlogs`
--
ALTER TABLE `orderlogs`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `productsincategories`
--
ALTER TABLE `productsincategories`
  MODIFY `pnc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `storeitems`
--
ALTER TABLE `storeitems`
  MODIFY `store_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
  MODIFY `purchase_line_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `supplypurchases`
--
ALTER TABLE `supplypurchases`
  MODIFY `supply_purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `unitmeasures`
--
ALTER TABLE `unitmeasures`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `userlogs`
--
ALTER TABLE `userlogs`
  MODIFY `user_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `userroles`
--
ALTER TABLE `userroles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categoryimages`
--
ALTER TABLE `categoryimages`
  ADD CONSTRAINT `categoryimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categoryimages_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customerlogs`
--
ALTER TABLE `customerlogs`
  ADD CONSTRAINT `customerlogs_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customerlogs_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discountedorders`
--
ALTER TABLE `discountedorders`
  ADD CONSTRAINT `discountedorders_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discountedorders_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`discount_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventories`
--
ALTER TABLE `inventories`
  ADD CONSTRAINT `inventories_ibfk_1` FOREIGN KEY (`inventory_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
  ADD CONSTRAINT `inventorylogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventorylogs_ibfk_2` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`inventory_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ordercustomers`
--
ALTER TABLE `ordercustomers`
  ADD CONSTRAINT `ordercustomers_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordercustomers_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlines`
--
ALTER TABLE `orderlines`
  ADD CONSTRAINT `orderlines_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderlines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlogs`
--
ALTER TABLE `orderlogs`
  ADD CONSTRAINT `orderlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderlogs_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productsincategories`
--
ALTER TABLE `productsincategories`
  ADD CONSTRAINT `productsincategories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productsincategories_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
  ADD CONSTRAINT `seniorcitizens_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeitemimages`
--
ALTER TABLE `storeitemimages`
  ADD CONSTRAINT `storeitemimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storeitemimages_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeitems`
--
ALTER TABLE `storeitems`
  ADD CONSTRAINT `storeitems_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `unitmeasures` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storeitems_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplierlogs`
--
ALTER TABLE `supplierlogs`
  ADD CONSTRAINT `supplierlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supplierlogs_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
  ADD CONSTRAINT `supplypurchaselines_ibfk_1` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supplypurchaselines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchasesuppliers`
--
ALTER TABLE `supplypurchasesuppliers`
  ADD CONSTRAINT `supplypurchasesuppliers_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supplypurchasesuppliers_ibfk_2` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userinroles`
--
ALTER TABLE `userinroles`
  ADD CONSTRAINT `userinroles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userinroles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `userroles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userlogs`
--
ALTER TABLE `userlogs`
  ADD CONSTRAINT `userlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userlogs_ibfk_2` FOREIGN KEY (`user2log_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
