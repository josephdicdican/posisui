SELECT 
order_id, date_of_order,
SUM(quantity) as items, TRUNCATE(SUM(quantity * price_per_unit),2) as sales, 
CONCAT(WEEK(date_of_order)) as week, CONCAT(MONTH(date_of_order)) as month, CONCAT(YEAR(date_of_order)) as year
from orderlines_list
where is_checkout = 1
group by order_id
order by date_of_order DESC