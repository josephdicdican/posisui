-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2016 at 09:25 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `posis_db`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `count_products_in_category`(`category_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(productsincategories.category_id) as categorizedproducts
    FROM 
    	productsincategories 
    LEFT JOIN
    	storeitems ON productsincategories.product_id = storeitems.product_id
    WHERE 
    	productsincategories.category_id = category_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_purchaselines_by_purchase_id`(`supply_purchase_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(spl.supply_purchase_id) as storeitems_with_unit
    FROM supplypurchaselines as spl 
    WHERE spl.supply_purchase_id = supply_purchase_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_by_product_id`(`product_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.product_id) as product_storeitems
    FROM storeitems WHERE storeitems.product_id = product_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_with_unit`(`unit_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.unit_id) as storeitems_with_unit
    FROM storeitems 
    WHERE storeitems.unit_id = unit_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_order_total_amount`(`order_id` INT) RETURNS double
    DETERMINISTIC
BEGIN
   declare total_amount DOUBLE(5,2);

   SELECT SUM(o.quantity * s.price_per_unit) INTO total_amount
   from orderlines as o
   LEFT JOIN storeitems as s ON s.store_item_id = o.store_item_id
   WHERE o.order_id = order_id;
  RETURN total_amount;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_order_total_items`(`order_id` INT) RETURNS int(11)
    DETERMINISTIC
BEGIN
   declare total_items DOUBLE(5,2);

   SELECT SUM(o.quantity) INTO total_items
   from orderlines as o
   WHERE o.order_id = order_id;
  RETURN total_items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_purchase_total_qty`(`supply_purchase_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare total_qty INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	SUM(spl.purchase_qty) as total
    FROM supplypurchaselines as spl 
    WHERE spl.supply_purchase_id = supply_purchase_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into total_qty;
    
    if no_more_rows then
    	set total_qty = 0;
    ELSE
    
    	if ISNULL(total_qty) THEN
        	set total_qty = 0;
        end if;
    end if;
    
    close cur;

  RETURN total_qty;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_storeitem_name`(`item_name` VARCHAR(100), `measure_name` VARCHAR(100)) RETURNS varchar(200) CHARSET latin1
    NO SQL
BEGIN declare storeitemname varchar(200); declare n1 varchar(100); declare n2 varchar(200); set storeitemname = ''; set n1 = TRIM(item_name); set n2 = TRIM(measure_name); set storeitemname = CONCAT(n1, ' ',n2); return storeitemname; END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `category_name` varchar(100) NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_id`, `category_name`) VALUES
(4, 0, 'Foods'),
(6, 4, 'Desserts'),
(7, 4, 'Viands'),
(35, 0, 'Groceries'),
(36, 35, 'Delata'),
(53, 0, 'Drinks'),
(54, 53, 'Soft Drinks'),
(59, 53, 'Beverages'),
(60, 53, 'Wine');

-- --------------------------------------------------------

--
-- Stand-in structure for view `categories_list`
--
CREATE TABLE IF NOT EXISTS `categories_list` (
`category_id` int(11)
,`parent_id` int(11)
,`category_name` varchar(100)
,`image_id` int(11)
,`image` varchar(255)
,`image_type` tinyint(2)
,`products_count` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `categoryimages`
--

CREATE TABLE IF NOT EXISTS `categoryimages` (
  `category_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `image_id` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoryimages`
--

INSERT INTO `categoryimages` (`category_id`, `image_id`) VALUES
(53, 21),
(54, 22),
(4, 23),
(6, 24),
(7, 25);

-- --------------------------------------------------------

--
-- Table structure for table `customerlogs`
--

CREATE TABLE IF NOT EXISTS `customerlogs` (
  `customer_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`customer_log_id`),
  KEY `user_id` (`user_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `customerlogs`
--

INSERT INTO `customerlogs` (`customer_log_id`, `user_id`, `customer_id`, `remarks`, `date_created`, `is_unread`) VALUES
(1, 18, 7, 'Added', '2016-02-10 13:19:54', 1),
(2, 18, 7, 'Updated', '2016-02-10 13:20:09', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `customerlogs_list`
--
CREATE TABLE IF NOT EXISTS `customerlogs_list` (
`customer_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`customer_id` int(11)
,`firstname` varchar(100)
,`lastname` varchar(100)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` tinyint(2) NOT NULL,
  `is_senior` tinyint(2) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `firstname`, `lastname`, `address`, `email`, `gender`, `is_senior`) VALUES
(7, 'Joseph', 'Dicdican', 'Pusok, Lapu-Lapu City, Cebu', 'joseph.dicdican@gmail.com', 1, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `customers_list`
--
CREATE TABLE IF NOT EXISTS `customers_list` (
`customer_id` int(11)
,`firstname` varchar(100)
,`lastname` varchar(100)
,`address` varchar(100)
,`email` varchar(100)
,`gender` tinyint(2)
,`is_senior` tinyint(2)
,`card_no` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `discountedorders`
--

CREATE TABLE IF NOT EXISTS `discountedorders` (
  `order_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `discount_id` (`discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_percentage` double NOT NULL,
  `discount_name` varchar(100) NOT NULL,
  PRIMARY KEY (`discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `image_type` tinyint(2) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`image_id`, `image`, `image_type`) VALUES
(21, '1453823114-penguins.jpg', 0),
(22, '1453827003-jellyfish.jpg', 0),
(23, '1453827052-koala.jpg', 0),
(24, '1453827072-tulips.jpg', 0),
(25, '1453987920-hydrangeas.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `inventory_id` int(11) NOT NULL,
  `stock_at_hand` int(11) NOT NULL DEFAULT '0',
  `max_stock_qty` int(11) NOT NULL DEFAULT '0',
  `min_stock_qty` int(11) NOT NULL DEFAULT '0',
  `reordering_qty` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`inventory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`inventory_id`, `stock_at_hand`, `max_stock_qty`, `min_stock_qty`, `reordering_qty`) VALUES
(10, 3, 15, 5, 10),
(11, 14, 15, 5, 7),
(12, 11, 20, 5, 7),
(13, 24, 50, 10, 25),
(14, 2, 0, 0, 0),
(15, 149, 200, 100, 120),
(16, 79, 100, 50, 70),
(17, 100, 100, 50, 70),
(18, 23, 24, 12, 16),
(19, 15, 15, 5, 8),
(20, 12, 12, 5, 7),
(21, 12, 12, 5, 8),
(22, 100, 150, 50, 70);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventories_list`
--
CREATE TABLE IF NOT EXISTS `inventories_list` (
`inventory_id` int(11)
,`stock_at_hand` int(11)
,`max_stock_qty` int(11)
,`min_stock_qty` int(11)
,`reordering_qty` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitemname` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `inventorylogs`
--

CREATE TABLE IF NOT EXISTS `inventorylogs` (
  `inventory_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`inventory_log_id`),
  KEY `user_id` (`user_id`,`inventory_id`),
  KEY `inventory_id` (`inventory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `inventorylogs`
--

INSERT INTO `inventorylogs` (`inventory_log_id`, `user_id`, `inventory_id`, `remarks`, `date_created`, `is_unread`) VALUES
(1, 18, 11, 'Added', '2016-02-12 16:12:10', 1),
(2, 18, 12, 'Updated', '2016-03-01 16:41:17', 1),
(4, 18, 16, 'Added', '2016-03-03 04:19:20', 1),
(5, 18, 17, 'Added', '2016-03-03 04:19:44', 1),
(6, 18, 18, 'Added', '2016-03-03 04:20:09', 1),
(7, 18, 19, 'Added', '2016-03-03 04:20:35', 1),
(8, 18, 20, 'Added', '2016-03-03 04:20:57', 1),
(9, 18, 21, 'Added', '2016-03-03 04:21:04', 1),
(11, 18, 13, 'Added', '2016-03-04 06:01:33', 1),
(12, 18, 22, 'Added', '2016-03-04 06:19:34', 1),
(13, 18, 15, 'Added', '2016-03-04 06:20:16', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventorylogs_list`
--
CREATE TABLE IF NOT EXISTS `inventorylogs_list` (
`inventory_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`inventory_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `ordercustomers`
--

CREATE TABLE IF NOT EXISTS `ordercustomers` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderlines`
--

CREATE TABLE IF NOT EXISTS `orderlines` (
  `line_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`line_id`),
  KEY `order_id` (`order_id`,`store_item_id`),
  KEY `store_item_id` (`store_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `orderlines`
--

INSERT INTO `orderlines` (`line_id`, `order_id`, `store_item_id`, `quantity`) VALUES
(9, 3, 17, 12),
(10, 3, 16, 12),
(16, 6, 10, 1),
(17, 6, 11, 1),
(18, 7, 17, 1),
(19, 7, 16, 1),
(21, 8, 16, 3),
(23, 8, 14, 1),
(24, 8, 13, 2),
(25, 9, 13, 1),
(26, 9, 14, 2),
(27, 9, 16, 1),
(28, 10, 10, 1),
(29, 10, 13, 1),
(30, 8, 14, 2),
(31, 8, 14, 3),
(32, 8, 18, 1),
(33, 11, 10, 1),
(34, 11, 15, 1),
(35, 11, 13, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `orderlines_list`
--
CREATE TABLE IF NOT EXISTS `orderlines_list` (
`order_id` int(11)
,`date_of_order` timestamp
,`is_checkout` tinyint(2)
,`is_discounted` tinyint(2)
,`line_id` int(11)
,`store_item_id` int(11)
,`quantity` int(11)
,`sku` varchar(200)
,`storeitemname` varchar(200)
,`price_per_unit` double
,`stock_at_hand` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `orderlogs`
--

CREATE TABLE IF NOT EXISTS `orderlogs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`,`order_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `orderlogs`
--

INSERT INTO `orderlogs` (`log_id`, `user_id`, `order_id`, `date_created`, `remarks`) VALUES
(1, 19, 3, '2016-03-03 04:22:35', 'Hold order 3'),
(4, 19, 6, '2016-03-03 13:00:32', 'Checked out order 6'),
(5, 19, 7, '2016-03-03 13:05:29', 'Checked out order 7'),
(6, 19, 8, '2016-03-03 17:51:31', 'Hold order 8'),
(7, 19, 8, '2016-03-03 20:20:26', 'Hold order 8'),
(8, 19, 8, '2016-03-04 05:53:51', 'Hold order 8'),
(9, 19, 8, '2016-03-04 05:59:33', 'Hold order 8'),
(10, 19, 9, '2016-03-04 06:02:08', 'Checked out order 9'),
(11, 19, 10, '2016-03-04 06:05:14', 'Hold order 10'),
(12, 19, 8, '2016-03-04 06:05:37', 'Hold order 8'),
(13, 19, 8, '2016-03-04 06:07:35', 'Checked out order 8'),
(14, 19, 11, '2016-03-04 08:20:47', 'Checked out order 11');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_checkout` tinyint(2) NOT NULL,
  `is_discounted` tinyint(2) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `date_of_order`, `is_checkout`, `is_discounted`) VALUES
(3, '2016-03-03 04:22:35', 0, 0),
(6, '2016-03-03 13:00:32', 1, 0),
(7, '2016-03-03 13:05:29', 1, 0),
(8, '2016-03-04 06:07:35', 1, 0),
(9, '2016-03-04 06:02:08', 1, 0),
(10, '2016-03-04 06:05:14', 0, 0),
(11, '2016-03-04 08:20:47', 1, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `orders_transactions_list`
--
CREATE TABLE IF NOT EXISTS `orders_transactions_list` (
`order_id` int(11)
,`date_of_order` timestamp
,`is_checkout` tinyint(2)
,`is_discounted` tinyint(2)
,`total_amount` double
,`total_items` int(11)
,`discount_id` int(11)
,`discount_percentage` double
,`discount_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`) VALUES
(8, 'Coke'),
(9, 'Sprite'),
(10, '555 Carne Norte'),
(11, 'Tanduay'),
(12, 'San Miguel Beer'),
(13, 'Señorita Sardines');

-- --------------------------------------------------------

--
-- Table structure for table `productsincategories`
--

CREATE TABLE IF NOT EXISTS `productsincategories` (
  `pnc_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`pnc_id`),
  KEY `category_id` (`category_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `productsincategories`
--

INSERT INTO `productsincategories` (`pnc_id`, `category_id`, `product_id`) VALUES
(10, 36, 10),
(13, 36, 13),
(8, 54, 8),
(9, 54, 9),
(11, 59, 11),
(12, 59, 12);

-- --------------------------------------------------------

--
-- Table structure for table `seniorcitizens`
--

CREATE TABLE IF NOT EXISTS `seniorcitizens` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_no` varchar(50) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `seniorcitizens`
--

INSERT INTO `seniorcitizens` (`customer_id`, `card_no`) VALUES
(7, '123456576565');

-- --------------------------------------------------------

--
-- Table structure for table `storeitemimages`
--

CREATE TABLE IF NOT EXISTS `storeitemimages` (
  `store_item_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`store_item_id`),
  KEY `image_id` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storeitems`
--

CREATE TABLE IF NOT EXISTS `storeitems` (
  `store_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `sku` varchar(200) NOT NULL,
  `price_per_unit` double NOT NULL,
  PRIMARY KEY (`store_item_id`),
  KEY `product_id` (`product_id`,`unit_id`),
  KEY `unit_id` (`unit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `storeitems`
--

INSERT INTO `storeitems` (`store_item_id`, `product_id`, `unit_id`, `sku`, `price_per_unit`) VALUES
(10, 8, 11, '8111454773144', 25),
(11, 8, 6, '861454773144', 20),
(12, 9, 6, '961454919237', 25),
(13, 9, 11, '9111454919237', 30),
(14, 9, 7, '971454919237', 8),
(15, 9, 8, '981454919237', 15),
(16, 10, 8, '1081455213217', 35),
(17, 10, 3, '1031455213217', 10),
(18, 11, 11, '11111455213754', 60),
(19, 11, 6, '1161455213754', 50),
(20, 12, 6, '400232349823450', 60),
(21, 12, 8, '43092018310380', 25),
(22, 13, 8, '40921308232434', 16);

-- --------------------------------------------------------

--
-- Stand-in structure for view `storeitems_list`
--
CREATE TABLE IF NOT EXISTS `storeitems_list` (
`store_item_id` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`category_id` int(11)
,`category_name` varchar(100)
,`parent_id` int(11)
,`storeitemname` varchar(200)
,`inventory_id` int(11)
,`stock_at_hand` int(11)
,`reordering_qty` int(11)
,`max_stock_qty` int(11)
,`min_stock_qty` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `supplierlogs`
--

CREATE TABLE IF NOT EXISTS `supplierlogs` (
  `supplier_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplierlogs`
--

INSERT INTO `supplierlogs` (`supplier_log_id`, `user_id`, `supplier_id`, `remarks`, `date_created`, `is_unread`) VALUES
(1, 18, 2, 'Updated', '2016-02-13 11:19:45', 1),
(2, 18, 1, 'Updated', '2016-02-13 11:21:11', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplierlogs_list`
--
CREATE TABLE IF NOT EXISTS `supplierlogs_list` (
`supplier_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`supplier_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_contact` varchar(100) DEFAULT NULL,
  `supplier_email` varchar(100) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `supplier_name`, `supplier_contact`, `supplier_email`) VALUES
(1, 'Aqua Soft', '09107728986', 'aquasoft@gmail.com'),
(2, 'Grand Mall', '09876543252', 'grandmall@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchaselines`
--

CREATE TABLE IF NOT EXISTS `supplypurchaselines` (
  `purchase_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `supply_purchase_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `purchase_qty` int(11) NOT NULL,
  `purchase_price_per_unit` double NOT NULL,
  PRIMARY KEY (`purchase_line_id`),
  KEY `supply_purchase_id` (`supply_purchase_id`,`store_item_id`),
  KEY `store_item_id` (`store_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `supplypurchaselines`
--

INSERT INTO `supplypurchaselines` (`purchase_line_id`, `supply_purchase_id`, `store_item_id`, `purchase_qty`, `purchase_price_per_unit`) VALUES
(10, 7, 11, 1, 20),
(11, 7, 10, 2, 25),
(12, 8, 12, 1, 25),
(13, 8, 11, 3, 20),
(14, 8, 10, 2, 25),
(23, 11, 14, 12, 8),
(24, 11, 13, 10, 30),
(25, 11, 12, 10, 25),
(26, 11, 11, 10, 20);

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplypurchaselines_list`
--
CREATE TABLE IF NOT EXISTS `supplypurchaselines_list` (
`supply_purchase_id` int(11)
,`date_of_purchase` timestamp
,`is_confirmed` tinyint(2)
,`purchase_line_id` int(11)
,`store_item_id` int(11)
,`purchase_qty` int(11)
,`purchase_price_per_unit` double
,`storeitemname` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `supplypurchases`
--

CREATE TABLE IF NOT EXISTS `supplypurchases` (
  `supply_purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_purchase` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_confirmed` tinyint(2) NOT NULL,
  PRIMARY KEY (`supply_purchase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `supplypurchases`
--

INSERT INTO `supplypurchases` (`supply_purchase_id`, `date_of_purchase`, `date_created`, `is_confirmed`) VALUES
(7, '2016-02-12 08:19:35', '2016-02-12 16:19:44', 1),
(8, '2016-02-12 08:24:52', '2016-02-12 16:26:50', 1),
(11, '2016-02-20 08:52:24', '2016-02-20 16:55:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchasesuppliers`
--

CREATE TABLE IF NOT EXISTS `supplypurchasesuppliers` (
  `supply_purchase_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  PRIMARY KEY (`supply_purchase_id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplypurchase_list`
--
CREATE TABLE IF NOT EXISTS `supplypurchase_list` (
`supply_purchase_id` int(11)
,`date_of_purchase` timestamp
,`date_created` timestamp
,`is_confirmed` tinyint(2)
,`total_qty` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `unitmeasures`
--

CREATE TABLE IF NOT EXISTS `unitmeasures` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `measure_name` varchar(100) NOT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `unitmeasures`
--

INSERT INTO `unitmeasures` (`unit_id`, `measure_name`) VALUES
(1, 'unit'),
(2, 'piece'),
(3, 'cup'),
(4, 'serve'),
(5, 'glass'),
(6, 'litre'),
(7, '8 ounce'),
(8, 'can'),
(9, '10 g'),
(10, 'box'),
(11, '1.5 litre');

-- --------------------------------------------------------

--
-- Stand-in structure for view `unitmeasure_storeitems`
--
CREATE TABLE IF NOT EXISTS `unitmeasure_storeitems` (
`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitems_count` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `userinroles`
--

CREATE TABLE IF NOT EXISTS `userinroles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userinroles`
--

INSERT INTO `userinroles` (`user_id`, `role_id`) VALUES
(18, 1),
(19, 2),
(20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `userlogs`
--

CREATE TABLE IF NOT EXISTS `userlogs` (
  `user_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user2log_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_log_id`),
  KEY `user_id` (`user_id`,`user2log_id`),
  KEY `user2log_id` (`user2log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `userlogs`
--

INSERT INTO `userlogs` (`user_log_id`, `user_id`, `user2log_id`, `remarks`, `date_created`, `is_unread`) VALUES
(1, 18, 19, 'Updated', '2016-02-27 04:46:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE IF NOT EXISTS `userroles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `display_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `display_name`, `username`, `password`, `date_created`, `date_modified`) VALUES
(18, 'Admin', 'administrator', '81f344a7686a80b4c5293e8fdc0b0160c82c06a8', '2016-01-17 16:00:00', '2016-01-18 17:51:55'),
(19, 'Joseph', 'josep@pos', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-01-18 17:53:19', '2016-02-26 21:46:20'),
(20, 'Jose', 'joseph@pos', '4a3487e57d90e2084654b6d23937e75af5c8ee55', '2016-01-21 20:00:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_list`
--
CREATE TABLE IF NOT EXISTS `users_list` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`role_id` int(11)
,`role_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `users_vw`
--
CREATE TABLE IF NOT EXISTS `users_vw` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`date_created` timestamp
,`date_modified` timestamp
,`role_id` int(11)
,`role_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Structure for view `categories_list`
--
DROP TABLE IF EXISTS `categories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `categories_list` AS select `c`.`category_id` AS `category_id`,`c`.`parent_id` AS `parent_id`,`c`.`category_name` AS `category_name`,`ci`.`image_id` AS `image_id`,`i`.`image` AS `image`,`i`.`image_type` AS `image_type`,`count_products_in_category`(`c`.`category_id`) AS `products_count` from ((`categories` `c` left join `categoryimages` `ci` on((`c`.`category_id` = `ci`.`category_id`))) left join `images` `i` on((`ci`.`image_id` = `i`.`image_id`)));

-- --------------------------------------------------------

--
-- Structure for view `customerlogs_list`
--
DROP TABLE IF EXISTS `customerlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customerlogs_list` AS select `cl`.`customer_log_id` AS `customer_log_id`,`cl`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`cl`.`customer_id` AS `customer_id`,`c`.`firstname` AS `firstname`,`c`.`lastname` AS `lastname`,`cl`.`remarks` AS `remarks`,`cl`.`date_created` AS `date_created`,`cl`.`is_unread` AS `is_unread` from ((`customerlogs` `cl` left join `customers` `c` on((`cl`.`customer_id` = `c`.`customer_id`))) left join `users` `u` on((`cl`.`user_id` = `u`.`user_id`)));

-- --------------------------------------------------------

--
-- Structure for view `customers_list`
--
DROP TABLE IF EXISTS `customers_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customers_list` AS select `c`.`customer_id` AS `customer_id`,`c`.`firstname` AS `firstname`,`c`.`lastname` AS `lastname`,`c`.`address` AS `address`,`c`.`email` AS `email`,`c`.`gender` AS `gender`,`c`.`is_senior` AS `is_senior`,`s`.`card_no` AS `card_no` from (`customers` `c` left join `seniorcitizens` `s` on((`c`.`customer_id` = `s`.`customer_id`)));

-- --------------------------------------------------------

--
-- Structure for view `inventories_list`
--
DROP TABLE IF EXISTS `inventories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventories_list` AS select `i`.`inventory_id` AS `inventory_id`,`i`.`stock_at_hand` AS `stock_at_hand`,`i`.`max_stock_qty` AS `max_stock_qty`,`i`.`min_stock_qty` AS `min_stock_qty`,`i`.`reordering_qty` AS `reordering_qty`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname` from (((`inventories` `i` left join `storeitems` `s` on((`i`.`inventory_id` = `s`.`store_item_id`))) left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`)));

-- --------------------------------------------------------

--
-- Structure for view `inventorylogs_list`
--
DROP TABLE IF EXISTS `inventorylogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventorylogs_list` AS select `i`.`inventory_log_id` AS `inventory_log_id`,`i`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`i`.`inventory_id` AS `inventory_id`,`i`.`remarks` AS `remarks`,`i`.`date_created` AS `date_created`,`i`.`is_unread` AS `is_unread` from (`inventorylogs` `i` left join `users` `u` on((`i`.`user_id` = `u`.`user_id`))) where 1;

-- --------------------------------------------------------

--
-- Structure for view `orderlines_list`
--
DROP TABLE IF EXISTS `orderlines_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orderlines_list` AS select `o`.`order_id` AS `order_id`,`o`.`date_of_order` AS `date_of_order`,`o`.`is_checkout` AS `is_checkout`,`o`.`is_discounted` AS `is_discounted`,`ol`.`line_id` AS `line_id`,`ol`.`store_item_id` AS `store_item_id`,`ol`.`quantity` AS `quantity`,`s`.`sku` AS `sku`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`s`.`price_per_unit` AS `price_per_unit`,`i`.`stock_at_hand` AS `stock_at_hand` from (((((`orderlines` `ol` left join `orders` `o` on((`o`.`order_id` = `ol`.`order_id`))) left join `storeitems` `s` on((`s`.`store_item_id` = `ol`.`store_item_id`))) left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`u`.`unit_id` = `s`.`unit_id`))) left join `inventories` `i` on((`i`.`inventory_id` = `s`.`store_item_id`)));

-- --------------------------------------------------------

--
-- Structure for view `orders_transactions_list`
--
DROP TABLE IF EXISTS `orders_transactions_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orders_transactions_list` AS select `o`.`order_id` AS `order_id`,`o`.`date_of_order` AS `date_of_order`,`o`.`is_checkout` AS `is_checkout`,`o`.`is_discounted` AS `is_discounted`,`get_order_total_amount`(`o`.`order_id`) AS `total_amount`,`get_order_total_items`(`o`.`order_id`) AS `total_items`,`do`.`discount_id` AS `discount_id`,`d`.`discount_percentage` AS `discount_percentage`,`d`.`discount_name` AS `discount_name` from ((`orders` `o` left join `discountedorders` `do` on((`do`.`order_id` = `o`.`order_id`))) left join `discounts` `d` on((`d`.`discount_id` = `do`.`discount_id`)));

-- --------------------------------------------------------

--
-- Structure for view `storeitems_list`
--
DROP TABLE IF EXISTS `storeitems_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storeitems_list` AS select `s`.`store_item_id` AS `store_item_id`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`pc`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`c`.`parent_id` AS `parent_id`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`i`.`inventory_id` AS `inventory_id`,`i`.`stock_at_hand` AS `stock_at_hand`,`i`.`reordering_qty` AS `reordering_qty`,`i`.`max_stock_qty` AS `max_stock_qty`,`i`.`min_stock_qty` AS `min_stock_qty` from (((((`storeitems` `s` left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`))) left join `productsincategories` `pc` on((`p`.`product_id` = `pc`.`product_id`))) left join `categories` `c` on((`c`.`category_id` = `pc`.`category_id`))) left join `inventories` `i` on((`s`.`store_item_id` = `i`.`inventory_id`)));

-- --------------------------------------------------------

--
-- Structure for view `supplierlogs_list`
--
DROP TABLE IF EXISTS `supplierlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplierlogs_list` AS select `s`.`supplier_log_id` AS `supplier_log_id`,`s`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`s`.`supplier_id` AS `supplier_id`,`s`.`remarks` AS `remarks`,`s`.`date_created` AS `date_created`,`s`.`is_unread` AS `is_unread` from (`supplierlogs` `s` left join `users` `u` on((`s`.`user_id` = `u`.`user_id`))) where 1;

-- --------------------------------------------------------

--
-- Structure for view `supplypurchaselines_list`
--
DROP TABLE IF EXISTS `supplypurchaselines_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplypurchaselines_list` AS select `supplypurchases`.`supply_purchase_id` AS `supply_purchase_id`,`supplypurchases`.`date_of_purchase` AS `date_of_purchase`,`supplypurchases`.`is_confirmed` AS `is_confirmed`,`supplypurchaselines`.`purchase_line_id` AS `purchase_line_id`,`supplypurchaselines`.`store_item_id` AS `store_item_id`,`supplypurchaselines`.`purchase_qty` AS `purchase_qty`,`supplypurchaselines`.`purchase_price_per_unit` AS `purchase_price_per_unit`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname` from ((((`supplypurchases` join `supplypurchaselines` on((`supplypurchases`.`supply_purchase_id` = `supplypurchaselines`.`supply_purchase_id`))) left join `storeitems` `s` on((`s`.`store_item_id` = `supplypurchaselines`.`store_item_id`))) left join `products` `p` on((`p`.`product_id` = `s`.`product_id`))) left join `unitmeasures` `u` on((`u`.`unit_id` = `s`.`unit_id`)));

-- --------------------------------------------------------

--
-- Structure for view `supplypurchase_list`
--
DROP TABLE IF EXISTS `supplypurchase_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplypurchase_list` AS select `supplypurchases`.`supply_purchase_id` AS `supply_purchase_id`,`supplypurchases`.`date_of_purchase` AS `date_of_purchase`,`supplypurchases`.`date_created` AS `date_created`,`supplypurchases`.`is_confirmed` AS `is_confirmed`,`get_purchase_total_qty`(`supplypurchases`.`supply_purchase_id`) AS `total_qty` from `supplypurchases`;

-- --------------------------------------------------------

--
-- Structure for view `unitmeasure_storeitems`
--
DROP TABLE IF EXISTS `unitmeasure_storeitems`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `unitmeasure_storeitems` AS select `unitmeasures`.`unit_id` AS `unit_id`,`unitmeasures`.`measure_name` AS `measure_name`,`count_storeitems_with_unit`(`unitmeasures`.`unit_id`) AS `storeitems_count` from `unitmeasures`;

-- --------------------------------------------------------

--
-- Structure for view `users_list`
--
DROP TABLE IF EXISTS `users_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_list` AS select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`)));

-- --------------------------------------------------------

--
-- Structure for view `users_vw`
--
DROP TABLE IF EXISTS `users_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_vw` AS select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`date_created` AS `date_created`,`users`.`date_modified` AS `date_modified`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categoryimages`
--
ALTER TABLE `categoryimages`
  ADD CONSTRAINT `categoryimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categoryimages_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customerlogs`
--
ALTER TABLE `customerlogs`
  ADD CONSTRAINT `customerlogs_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customerlogs_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discountedorders`
--
ALTER TABLE `discountedorders`
  ADD CONSTRAINT `discountedorders_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discountedorders_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`discount_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventories`
--
ALTER TABLE `inventories`
  ADD CONSTRAINT `inventories_ibfk_1` FOREIGN KEY (`inventory_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
  ADD CONSTRAINT `inventorylogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventorylogs_ibfk_2` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`inventory_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ordercustomers`
--
ALTER TABLE `ordercustomers`
  ADD CONSTRAINT `ordercustomers_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordercustomers_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlines`
--
ALTER TABLE `orderlines`
  ADD CONSTRAINT `orderlines_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderlines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlogs`
--
ALTER TABLE `orderlogs`
  ADD CONSTRAINT `orderlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderlogs_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productsincategories`
--
ALTER TABLE `productsincategories`
  ADD CONSTRAINT `productsincategories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productsincategories_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
  ADD CONSTRAINT `seniorcitizens_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeitemimages`
--
ALTER TABLE `storeitemimages`
  ADD CONSTRAINT `storeitemimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storeitemimages_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeitems`
--
ALTER TABLE `storeitems`
  ADD CONSTRAINT `storeitems_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `unitmeasures` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storeitems_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplierlogs`
--
ALTER TABLE `supplierlogs`
  ADD CONSTRAINT `supplierlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supplierlogs_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
  ADD CONSTRAINT `supplypurchaselines_ibfk_1` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supplypurchaselines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchasesuppliers`
--
ALTER TABLE `supplypurchasesuppliers`
  ADD CONSTRAINT `supplypurchasesuppliers_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supplypurchasesuppliers_ibfk_2` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userinroles`
--
ALTER TABLE `userinroles`
  ADD CONSTRAINT `userinroles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userinroles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `userroles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userlogs`
--
ALTER TABLE `userlogs`
  ADD CONSTRAINT `userlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userlogs_ibfk_2` FOREIGN KEY (`user2log_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
