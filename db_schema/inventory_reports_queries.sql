-- Selecting all items for reordering
SELECT * FROM inventories_list as i WHERE i.stock_at_hand < i.reordering_qty;
-- Selecting all items out of stock
SELECT * FROM inventories_list as i WHERE i.stock_at_hand = 0;
-- Selecting all overstock items
SELECT * FROM inventories_list as i WHERE i.stock_at_hand > i.max_stock_qty;
