select 
s.category_name,
s.sku,
s.storeitemname, sum(s.quantity) as sold_quantities,
sum(s.quantity * s.price_per_unit) as total_sales,
week
from storeitems_sales as s
where s.is_stockable = 0
group by store_item_id, week
order by store_item_id;