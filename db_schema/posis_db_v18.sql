-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2016 at 07:18 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `posis_db`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `count_products_in_category`(`category_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(productsincategories.category_id) as categorizedproducts
    FROM 
    	productsincategories 
    LEFT JOIN
    	storeitems ON productsincategories.product_id = storeitems.product_id
    WHERE 
    	productsincategories.category_id = category_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_purchaselines_by_purchase_id`(`supply_purchase_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(spl.supply_purchase_id) as storeitems_with_unit
    FROM supplypurchaselines as spl 
    WHERE spl.supply_purchase_id = supply_purchase_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_by_product_id`(`product_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.product_id) as product_storeitems
    FROM storeitems WHERE storeitems.product_id = product_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_with_unit`(`unit_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.unit_id) as storeitems_with_unit
    FROM storeitems 
    WHERE storeitems.unit_id = unit_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_order_total_amount`(`order_id` INT) RETURNS double
BEGIN
   declare total_amount DOUBLE(5,2);

   SELECT SUM(o.quantity * s.price_per_unit) INTO total_amount
   from orderlines as o
   LEFT JOIN storeitems as s ON s.store_item_id = o.store_item_id
   WHERE o.order_id = order_id;
  RETURN total_amount;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_order_total_items`(`order_id` INT) RETURNS int(11)
BEGIN
   declare total_items DOUBLE(5,2);

   SELECT SUM(o.quantity) INTO total_items
   from orderlines as o
   WHERE o.order_id = order_id;
  RETURN total_items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_purchase_total_qty`(`supply_purchase_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare total_qty INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	SUM(spl.purchase_qty) as total
    FROM supplypurchaselines as spl 
    WHERE spl.supply_purchase_id = supply_purchase_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into total_qty;
    
    if no_more_rows then
    	set total_qty = 0;
    ELSE
    
    	if ISNULL(total_qty) THEN
        	set total_qty = 0;
        end if;
    end if;
    
    close cur;

  RETURN total_qty;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_storeitem_name`(`item_name` VARCHAR(100), `measure_name` VARCHAR(100)) RETURNS varchar(200) CHARSET latin1
    NO SQL
BEGIN 
	declare storeitemname varchar(200); 
	declare n1 varchar(100); 
	declare n2 varchar(200); 
	
	set storeitemname = ''; 
	set n1 = TRIM(item_name); 
	set n2 = TRIM(measure_name); 
	set storeitemname = CONCAT(n1, ' (',n2); 
	set storeitemname = CONCAT(storeitemname, '', ')');
	
	return storeitemname; 
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_storeitem_orderlines`(`id` INT) RETURNS int(11)
BEGIN
	DECLARE storeitemlines INT(11);
	
	SELECT COUNT(o.line_id) INTO storeitemlines 
	FROM orderlines as o WHERE o.store_item_id = id;
	
	RETURN storeitemlines;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `category_name` varchar(100) NOT NULL,
  `is_stockable` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_id`, `category_name`, `is_stockable`) VALUES
(68, 0, 'Groceries', 1),
(69, 0, 'Drinks', 1),
(70, 69, 'Softdrinks', 1),
(71, 68, 'Snacks', 1),
(72, 0, 'Appetizers', 0),
(75, 0, 'Delights', 0),
(78, 75, 'Desserts', 0),
(82, 0, 'Breakfast ', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `categories_list`
--
CREATE TABLE IF NOT EXISTS `categories_list` (
`category_id` int(11)
,`parent_id` int(11)
,`category_name` varchar(100)
,`is_stockable` tinyint(2)
,`image_id` int(11)
,`image` varchar(255)
,`image_type` tinyint(2)
,`products_count` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `categoryimages`
--

CREATE TABLE IF NOT EXISTS `categoryimages` (
  `category_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoryimages`
--

INSERT INTO `categoryimages` (`category_id`, `image_id`) VALUES
(68, 40),
(69, 41),
(70, 42),
(71, 43),
(72, 44),
(75, 47),
(78, 50),
(82, 53);

-- --------------------------------------------------------

--
-- Table structure for table `customerlogs`
--

CREATE TABLE IF NOT EXISTS `customerlogs` (
`customer_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `customerlogs_list`
--
CREATE TABLE IF NOT EXISTS `customerlogs_list` (
`customer_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`customer_id` int(11)
,`firstname` varchar(100)
,`lastname` varchar(100)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
`customer_id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` tinyint(2) NOT NULL,
  `is_senior` tinyint(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `customers_list`
--
CREATE TABLE IF NOT EXISTS `customers_list` (
`customer_id` int(11)
,`firstname` varchar(100)
,`lastname` varchar(100)
,`address` varchar(100)
,`email` varchar(100)
,`gender` tinyint(2)
,`is_senior` tinyint(2)
,`card_no` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `discountedorders`
--

CREATE TABLE IF NOT EXISTS `discountedorders` (
  `order_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
`discount_id` int(11) NOT NULL,
  `discount_percentage` double NOT NULL,
  `discount_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`image_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_type` tinyint(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`image_id`, `image`, `image_type`) VALUES
(40, '1458205164-retail_1_.jpg', 0),
(41, '1458205229-softdrinks.jpg', 0),
(42, '1458205247-softdrinks.jpg', 0),
(43, '1458205680-coke1litre.jpg', 0),
(44, '1458210769-chrysanthemum.jpg', 0),
(47, '1458211017-hydrangeas.jpg', 0),
(50, '1458211964-tulips.jpg', 0),
(52, '1458213059-lighthouse.jpg', 1),
(53, '1458214202-jellyfish.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `inventory_id` int(11) NOT NULL,
  `stock_at_hand` int(11) NOT NULL DEFAULT '0',
  `max_stock_qty` int(11) NOT NULL DEFAULT '0',
  `min_stock_qty` int(11) NOT NULL DEFAULT '0',
  `reordering_qty` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`inventory_id`, `stock_at_hand`, `max_stock_qty`, `min_stock_qty`, `reordering_qty`) VALUES
(100, 20, 20, 10, 12),
(101, 20, 20, 10, 12),
(102, 20, 20, 10, 12),
(103, 20, 20, 10, 12),
(106, 0, 0, 0, 0),
(107, 0, 0, 0, 0),
(108, 0, 0, 0, 0),
(109, 10, 10, 5, 7);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventories_list`
--
CREATE TABLE IF NOT EXISTS `inventories_list` (
`inventory_id` int(11)
,`stock_at_hand` int(11)
,`max_stock_qty` int(11)
,`min_stock_qty` int(11)
,`reordering_qty` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitemname` varchar(200)
,`category_id` int(11)
,`category_name` varchar(100)
,`parent_id` int(11)
,`is_stockable` tinyint(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `inventorylogs`
--

CREATE TABLE IF NOT EXISTS `inventorylogs` (
`inventory_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `inventorylogs`
--

INSERT INTO `inventorylogs` (`inventory_log_id`, `user_id`, `inventory_id`, `remarks`, `date_created`, `is_unread`) VALUES
(1, 18, 100, 'Added', '2016-03-17 09:13:05', 1),
(2, 18, 101, 'Added', '2016-03-17 09:13:53', 1),
(3, 18, 102, 'Added', '2016-03-17 09:21:41', 1),
(4, 18, 103, 'Added', '2016-03-17 09:22:02', 1),
(5, 18, 109, 'Added', '2016-03-17 17:20:41', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventorylogs_list`
--
CREATE TABLE IF NOT EXISTS `inventorylogs_list` (
`inventory_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`inventory_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `ordercustomers`
--

CREATE TABLE IF NOT EXISTS `ordercustomers` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderlines`
--

CREATE TABLE IF NOT EXISTS `orderlines` (
`line_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `orderlines_list`
--
CREATE TABLE IF NOT EXISTS `orderlines_list` (
`order_id` int(11)
,`date_of_order` timestamp
,`is_checkout` tinyint(2)
,`is_discounted` tinyint(2)
,`line_id` int(11)
,`store_item_id` int(11)
,`quantity` int(11)
,`sku` varchar(200)
,`storeitemname` varchar(200)
,`price_per_unit` double
,`stock_at_hand` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `orderlogs`
--

CREATE TABLE IF NOT EXISTS `orderlogs` (
`log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remarks` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`order_id` int(11) NOT NULL,
  `date_of_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_checkout` tinyint(2) NOT NULL,
  `is_discounted` tinyint(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `orders_transactions_list`
--
CREATE TABLE IF NOT EXISTS `orders_transactions_list` (
`order_id` int(11)
,`date_of_order` timestamp
,`is_checkout` tinyint(2)
,`is_discounted` tinyint(2)
,`total_amount` double
,`total_items` int(11)
,`discount_id` int(11)
,`discount_percentage` double
,`discount_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`) VALUES
(14, 'Leche Flan'),
(15, 'Mangga Cheesecake'),
(16, 'Halo-Halo'),
(17, 'Ice Cream'),
(18, 'Mango Pandan'),
(19, 'Lechon Kawali'),
(20, 'Kare-Kare'),
(22, 'Beef Caldereta'),
(23, 'Bicol Express'),
(24, 'Humba'),
(25, 'Crispy Dinuguan'),
(26, 'Adobong Pusit'),
(27, 'Binagoongang Baboy'),
(28, 'Griled Scallops'),
(29, 'Deep-Fried Tuna Belly'),
(30, 'Adobo'),
(31, 'Chicharon Bulaklak'),
(32, 'Lumpia Frito'),
(33, 'Crispy Shrimps'),
(34, 'Crispy Wings'),
(35, 'Chorizo Dinamitas'),
(36, 'Crusted Prawns'),
(37, 'Bam-i'),
(38, 'Palabok'),
(39, 'Sotanghon Guisado'),
(40, 'Pancit Canton'),
(41, 'Salted Egg Rice'),
(42, 'Sisig Rice Platter'),
(43, 'Tiger'),
(44, 'Butter Coconut'),
(45, 'Combi'),
(46, 'Voice'),
(47, 'Grahams'),
(48, 'Oreo'),
(49, 'Hansel'),
(50, 'Bingo'),
(51, 'Piattos'),
(52, 'Roller Coaster'),
(53, 'Nova'),
(55, 'Tortillos'),
(56, 'Chippy'),
(57, 'Chicharron ni Mang Juan'),
(58, 'Ricoa Curly Tops'),
(59, 'Wiggles'),
(60, 'Cloud 9'),
(61, 'V Cut'),
(62, 'Nissin Cup Noodles'),
(63, 'Nissin Yakisoba'),
(64, 'Payless Instant Mami'),
(65, 'Payless Extra Big'),
(66, 'C2 Cool and Clean'),
(67, 'Blue'),
(69, 'Milk Tea'),
(70, 'Shake'),
(71, 'Coffee'),
(72, 'Coke'),
(73, 'Sprite'),
(74, 'Sprite'),
(75, 'Biko'),
(76, 'Utan Bisaya'),
(77, 'Royal'),
(78, 'C2 Cool & Clean'),
(79, 'Spicy Seafood Curls'),
(80, 'Sparkle'),
(83, 'Coke'),
(84, 'Sky Flakes'),
(85, 'Sky Flakes'),
(86, 'King Flakes'),
(87, 'Coke'),
(90, 'Mashed Potato'),
(91, 'Sunny Side Up'),
(92, 'Boiled Egg'),
(93, 'Presto Creams');

-- --------------------------------------------------------

--
-- Table structure for table `productsincategories`
--

CREATE TABLE IF NOT EXISTS `productsincategories` (
`pnc_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `productsincategories`
--

INSERT INTO `productsincategories` (`pnc_id`, `category_id`, `product_id`) VALUES
(3, 70, 87),
(1, 71, 85),
(2, 71, 86),
(9, 71, 93),
(6, 72, 90),
(7, 82, 91),
(8, 82, 92);

-- --------------------------------------------------------

--
-- Table structure for table `seniorcitizens`
--

CREATE TABLE IF NOT EXISTS `seniorcitizens` (
`customer_id` int(11) NOT NULL,
  `card_no` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `storeitemimages`
--

CREATE TABLE IF NOT EXISTS `storeitemimages` (
  `store_item_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storeitems`
--

CREATE TABLE IF NOT EXISTS `storeitems` (
`store_item_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `sku` varchar(200) NOT NULL,
  `price_per_unit` double NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `storeitems`
--

INSERT INTO `storeitems` (`store_item_id`, `product_id`, `unit_id`, `sku`, `price_per_unit`) VALUES
(100, 85, 16, '4301000100101', 5),
(101, 86, 16, '4301000100102', 5),
(102, 87, 17, '4301000100103', 20),
(103, 87, 18, '4301000100104', 25),
(106, 90, 16, '4302000200201', 15),
(107, 91, 19, '4302000200203', 8),
(108, 92, 19, '4302000200204', 8),
(109, 93, 16, '4301000100104', 11);

-- --------------------------------------------------------

--
-- Stand-in structure for view `storeitems_list`
--
CREATE TABLE IF NOT EXISTS `storeitems_list` (
`store_item_id` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`category_id` int(11)
,`category_name` varchar(100)
,`parent_id` int(11)
,`is_stockable` tinyint(2)
,`storeitemname` varchar(200)
,`inventory_id` int(11)
,`stock_at_hand` int(11)
,`reordering_qty` int(11)
,`max_stock_qty` int(11)
,`min_stock_qty` int(11)
,`image_id` int(11)
,`image` varchar(255)
,`image_type` tinyint(2)
,`lines` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `storeitems_sales`
--
CREATE TABLE IF NOT EXISTS `storeitems_sales` (
`order_id` int(11)
,`date_of_order` timestamp
,`store_item_id` int(11)
,`sku` varchar(200)
,`storeitemname` varchar(200)
,`quantity` int(11)
,`price_per_unit` double
,`week` varchar(2)
,`month` varchar(2)
,`year` varchar(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `supplierlogs`
--

CREATE TABLE IF NOT EXISTS `supplierlogs` (
  `supplier_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplierlogs_list`
--
CREATE TABLE IF NOT EXISTS `supplierlogs_list` (
`supplier_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`supplier_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
`supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_contact` varchar(100) DEFAULT NULL,
  `supplier_email` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchaselines`
--

CREATE TABLE IF NOT EXISTS `supplypurchaselines` (
`purchase_line_id` int(11) NOT NULL,
  `supply_purchase_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `purchase_qty` int(11) NOT NULL,
  `purchase_price_per_unit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplypurchaselines_list`
--
CREATE TABLE IF NOT EXISTS `supplypurchaselines_list` (
`supply_purchase_id` int(11)
,`date_of_purchase` timestamp
,`is_confirmed` tinyint(2)
,`purchase_line_id` int(11)
,`store_item_id` int(11)
,`purchase_qty` int(11)
,`purchase_price_per_unit` double
,`storeitemname` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `supplypurchases`
--

CREATE TABLE IF NOT EXISTS `supplypurchases` (
`supply_purchase_id` int(11) NOT NULL,
  `date_of_purchase` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_confirmed` tinyint(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchasesuppliers`
--

CREATE TABLE IF NOT EXISTS `supplypurchasesuppliers` (
  `supply_purchase_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `supplypurchase_list`
--
CREATE TABLE IF NOT EXISTS `supplypurchase_list` (
`supply_purchase_id` int(11)
,`date_of_purchase` timestamp
,`date_created` timestamp
,`is_confirmed` tinyint(2)
,`total_qty` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `unitmeasures`
--

CREATE TABLE IF NOT EXISTS `unitmeasures` (
`unit_id` int(11) NOT NULL,
  `measure_name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `unitmeasures`
--

INSERT INTO `unitmeasures` (`unit_id`, `measure_name`) VALUES
(16, 'piece'),
(17, 'litre'),
(18, '1.5 litre'),
(19, 'serve');

-- --------------------------------------------------------

--
-- Stand-in structure for view `unitmeasure_storeitems`
--
CREATE TABLE IF NOT EXISTS `unitmeasure_storeitems` (
`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitems_count` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `userinroles`
--

CREATE TABLE IF NOT EXISTS `userinroles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userinroles`
--

INSERT INTO `userinroles` (`user_id`, `role_id`) VALUES
(18, 1),
(21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `userlogs`
--

CREATE TABLE IF NOT EXISTS `userlogs` (
`user_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user2log_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_unread` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `userlogs`
--

INSERT INTO `userlogs` (`user_log_id`, `user_id`, `user2log_id`, `remarks`, `date_created`, `is_unread`) VALUES
(2, 18, 21, 'Added', '2016-03-04 17:00:30', 1),
(9, 18, 21, 'Updated', '2016-03-04 17:05:50', 1),
(13, 18, 18, 'Updated', '2016-03-08 15:00:22', 1),
(15, 18, 18, 'Updated', '2016-03-08 15:50:39', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `userlogs_list`
--
CREATE TABLE IF NOT EXISTS `userlogs_list` (
`user_log_id` int(11)
,`user_id` int(11)
,`display_name` varchar(100)
,`user2log_id` int(11)
,`remarks` varchar(255)
,`date_created` timestamp
,`is_unread` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE IF NOT EXISTS `userroles` (
`role_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `display_name`, `username`, `password`, `date_created`, `date_modified`) VALUES
(18, 'Admin', 'administrator', '81f344a7686a80b4c5293e8fdc0b0160c82c06a8', '2016-01-17 16:00:00', '2016-03-07 20:50:39'),
(21, 'Joseph Dicdican', 'jdicdican', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-03-03 22:00:30', '2016-03-03 22:05:50');

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_list`
--
CREATE TABLE IF NOT EXISTS `users_list` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`role_id` int(11)
,`role_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `users_vw`
--
CREATE TABLE IF NOT EXISTS `users_vw` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`date_created` timestamp
,`date_modified` timestamp
,`role_id` int(11)
,`role_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Structure for view `categories_list`
--
DROP TABLE IF EXISTS `categories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `categories_list` AS select `c`.`category_id` AS `category_id`,`c`.`parent_id` AS `parent_id`,`c`.`category_name` AS `category_name`,`c`.`is_stockable` AS `is_stockable`,`ci`.`image_id` AS `image_id`,`i`.`image` AS `image`,`i`.`image_type` AS `image_type`,`count_products_in_category`(`c`.`category_id`) AS `products_count` from ((`categories` `c` left join `categoryimages` `ci` on((`c`.`category_id` = `ci`.`category_id`))) left join `images` `i` on((`ci`.`image_id` = `i`.`image_id`)));

-- --------------------------------------------------------

--
-- Structure for view `customerlogs_list`
--
DROP TABLE IF EXISTS `customerlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customerlogs_list` AS select `cl`.`customer_log_id` AS `customer_log_id`,`cl`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`cl`.`customer_id` AS `customer_id`,`c`.`firstname` AS `firstname`,`c`.`lastname` AS `lastname`,`cl`.`remarks` AS `remarks`,`cl`.`date_created` AS `date_created`,`cl`.`is_unread` AS `is_unread` from ((`customerlogs` `cl` left join `customers` `c` on((`cl`.`customer_id` = `c`.`customer_id`))) left join `users` `u` on((`cl`.`user_id` = `u`.`user_id`)));

-- --------------------------------------------------------

--
-- Structure for view `customers_list`
--
DROP TABLE IF EXISTS `customers_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customers_list` AS select `c`.`customer_id` AS `customer_id`,`c`.`firstname` AS `firstname`,`c`.`lastname` AS `lastname`,`c`.`address` AS `address`,`c`.`email` AS `email`,`c`.`gender` AS `gender`,`c`.`is_senior` AS `is_senior`,`s`.`card_no` AS `card_no` from (`customers` `c` left join `seniorcitizens` `s` on((`c`.`customer_id` = `s`.`customer_id`)));

-- --------------------------------------------------------

--
-- Structure for view `inventories_list`
--
DROP TABLE IF EXISTS `inventories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventories_list` AS select `i`.`inventory_id` AS `inventory_id`,`i`.`stock_at_hand` AS `stock_at_hand`,`i`.`max_stock_qty` AS `max_stock_qty`,`i`.`min_stock_qty` AS `min_stock_qty`,`i`.`reordering_qty` AS `reordering_qty`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`pc`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`c`.`parent_id` AS `parent_id`,`c`.`is_stockable` AS `is_stockable` from (((((`inventories` `i` left join `storeitems` `s` on((`i`.`inventory_id` = `s`.`store_item_id`))) left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`))) left join `productsincategories` `pc` on((`p`.`product_id` = `pc`.`product_id`))) left join `categories` `c` on((`c`.`category_id` = `pc`.`category_id`)));

-- --------------------------------------------------------

--
-- Structure for view `inventorylogs_list`
--
DROP TABLE IF EXISTS `inventorylogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventorylogs_list` AS select `i`.`inventory_log_id` AS `inventory_log_id`,`i`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`i`.`inventory_id` AS `inventory_id`,`i`.`remarks` AS `remarks`,`i`.`date_created` AS `date_created`,`i`.`is_unread` AS `is_unread` from (`inventorylogs` `i` left join `users` `u` on((`i`.`user_id` = `u`.`user_id`))) where 1;

-- --------------------------------------------------------

--
-- Structure for view `orderlines_list`
--
DROP TABLE IF EXISTS `orderlines_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orderlines_list` AS select `o`.`order_id` AS `order_id`,`o`.`date_of_order` AS `date_of_order`,`o`.`is_checkout` AS `is_checkout`,`o`.`is_discounted` AS `is_discounted`,`ol`.`line_id` AS `line_id`,`ol`.`store_item_id` AS `store_item_id`,`ol`.`quantity` AS `quantity`,`s`.`sku` AS `sku`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`s`.`price_per_unit` AS `price_per_unit`,`i`.`stock_at_hand` AS `stock_at_hand` from (((((`orderlines` `ol` left join `orders` `o` on((`o`.`order_id` = `ol`.`order_id`))) left join `storeitems` `s` on((`s`.`store_item_id` = `ol`.`store_item_id`))) left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`u`.`unit_id` = `s`.`unit_id`))) left join `inventories` `i` on((`i`.`inventory_id` = `s`.`store_item_id`)));

-- --------------------------------------------------------

--
-- Structure for view `orders_transactions_list`
--
DROP TABLE IF EXISTS `orders_transactions_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orders_transactions_list` AS select `o`.`order_id` AS `order_id`,`o`.`date_of_order` AS `date_of_order`,`o`.`is_checkout` AS `is_checkout`,`o`.`is_discounted` AS `is_discounted`,`get_order_total_amount`(`o`.`order_id`) AS `total_amount`,`get_order_total_items`(`o`.`order_id`) AS `total_items`,`do`.`discount_id` AS `discount_id`,`d`.`discount_percentage` AS `discount_percentage`,`d`.`discount_name` AS `discount_name` from ((`orders` `o` left join `discountedorders` `do` on((`do`.`order_id` = `o`.`order_id`))) left join `discounts` `d` on((`d`.`discount_id` = `do`.`discount_id`)));

-- --------------------------------------------------------

--
-- Structure for view `storeitems_list`
--
DROP TABLE IF EXISTS `storeitems_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storeitems_list` AS select `s`.`store_item_id` AS `store_item_id`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`pc`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`c`.`parent_id` AS `parent_id`,`c`.`is_stockable` AS `is_stockable`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`i`.`inventory_id` AS `inventory_id`,`i`.`stock_at_hand` AS `stock_at_hand`,`i`.`reordering_qty` AS `reordering_qty`,`i`.`max_stock_qty` AS `max_stock_qty`,`i`.`min_stock_qty` AS `min_stock_qty`,`si`.`image_id` AS `image_id`,`im`.`image` AS `image`,`im`.`image_type` AS `image_type`,`get_storeitem_orderlines`(`s`.`store_item_id`) AS `lines` from (((((((`storeitems` `s` left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`))) left join `productsincategories` `pc` on((`p`.`product_id` = `pc`.`product_id`))) left join `categories` `c` on((`c`.`category_id` = `pc`.`category_id`))) left join `inventories` `i` on((`s`.`store_item_id` = `i`.`inventory_id`))) left join `storeitemimages` `si` on((`si`.`store_item_id` = `s`.`store_item_id`))) left join `images` `im` on((`im`.`image_id` = `si`.`image_id`)));

-- --------------------------------------------------------

--
-- Structure for view `storeitems_sales`
--
DROP TABLE IF EXISTS `storeitems_sales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storeitems_sales` AS select `orderlines_list`.`order_id` AS `order_id`,`orderlines_list`.`date_of_order` AS `date_of_order`,`orderlines_list`.`store_item_id` AS `store_item_id`,`orderlines_list`.`sku` AS `sku`,`orderlines_list`.`storeitemname` AS `storeitemname`,`orderlines_list`.`quantity` AS `quantity`,`orderlines_list`.`price_per_unit` AS `price_per_unit`,concat(week(`orderlines_list`.`date_of_order`,0)) AS `week`,concat(month(`orderlines_list`.`date_of_order`)) AS `month`,concat(year(`orderlines_list`.`date_of_order`)) AS `year` from `orderlines_list` where (`orderlines_list`.`is_checkout` = 1) order by `orderlines_list`.`date_of_order` desc;

-- --------------------------------------------------------

--
-- Structure for view `supplierlogs_list`
--
DROP TABLE IF EXISTS `supplierlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplierlogs_list` AS select `s`.`supplier_log_id` AS `supplier_log_id`,`s`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`s`.`supplier_id` AS `supplier_id`,`s`.`remarks` AS `remarks`,`s`.`date_created` AS `date_created`,`s`.`is_unread` AS `is_unread` from (`supplierlogs` `s` left join `users` `u` on((`s`.`user_id` = `u`.`user_id`))) where 1;

-- --------------------------------------------------------

--
-- Structure for view `supplypurchaselines_list`
--
DROP TABLE IF EXISTS `supplypurchaselines_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplypurchaselines_list` AS select `supplypurchases`.`supply_purchase_id` AS `supply_purchase_id`,`supplypurchases`.`date_of_purchase` AS `date_of_purchase`,`supplypurchases`.`is_confirmed` AS `is_confirmed`,`supplypurchaselines`.`purchase_line_id` AS `purchase_line_id`,`supplypurchaselines`.`store_item_id` AS `store_item_id`,`supplypurchaselines`.`purchase_qty` AS `purchase_qty`,`supplypurchaselines`.`purchase_price_per_unit` AS `purchase_price_per_unit`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname` from ((((`supplypurchases` join `supplypurchaselines` on((`supplypurchases`.`supply_purchase_id` = `supplypurchaselines`.`supply_purchase_id`))) left join `storeitems` `s` on((`s`.`store_item_id` = `supplypurchaselines`.`store_item_id`))) left join `products` `p` on((`p`.`product_id` = `s`.`product_id`))) left join `unitmeasures` `u` on((`u`.`unit_id` = `s`.`unit_id`)));

-- --------------------------------------------------------

--
-- Structure for view `supplypurchase_list`
--
DROP TABLE IF EXISTS `supplypurchase_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supplypurchase_list` AS select `supplypurchases`.`supply_purchase_id` AS `supply_purchase_id`,`supplypurchases`.`date_of_purchase` AS `date_of_purchase`,`supplypurchases`.`date_created` AS `date_created`,`supplypurchases`.`is_confirmed` AS `is_confirmed`,`get_purchase_total_qty`(`supplypurchases`.`supply_purchase_id`) AS `total_qty` from `supplypurchases`;

-- --------------------------------------------------------

--
-- Structure for view `unitmeasure_storeitems`
--
DROP TABLE IF EXISTS `unitmeasure_storeitems`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `unitmeasure_storeitems` AS select `unitmeasures`.`unit_id` AS `unit_id`,`unitmeasures`.`measure_name` AS `measure_name`,`count_storeitems_with_unit`(`unitmeasures`.`unit_id`) AS `storeitems_count` from `unitmeasures`;

-- --------------------------------------------------------

--
-- Structure for view `userlogs_list`
--
DROP TABLE IF EXISTS `userlogs_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `userlogs_list` AS select `ul`.`user_log_id` AS `user_log_id`,`ul`.`user_id` AS `user_id`,`u`.`display_name` AS `display_name`,`ul`.`user2log_id` AS `user2log_id`,`ul`.`remarks` AS `remarks`,`ul`.`date_created` AS `date_created`,`ul`.`is_unread` AS `is_unread` from (`userlogs` `ul` left join `users` `u` on((`ul`.`user_id` = `u`.`user_id`))) where 1;

-- --------------------------------------------------------

--
-- Structure for view `users_list`
--
DROP TABLE IF EXISTS `users_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_list` AS select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`)));

-- --------------------------------------------------------

--
-- Structure for view `users_vw`
--
DROP TABLE IF EXISTS `users_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_vw` AS select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`date_created` AS `date_created`,`users`.`date_modified` AS `date_modified`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`category_id`), ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `categoryimages`
--
ALTER TABLE `categoryimages`
 ADD PRIMARY KEY (`category_id`), ADD UNIQUE KEY `image_id` (`image_id`);

--
-- Indexes for table `customerlogs`
--
ALTER TABLE `customerlogs`
 ADD PRIMARY KEY (`customer_log_id`), ADD KEY `user_id` (`user_id`), ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discountedorders`
--
ALTER TABLE `discountedorders`
 ADD PRIMARY KEY (`order_id`), ADD KEY `discount_id` (`discount_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
 ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
 ADD PRIMARY KEY (`inventory_id`);

--
-- Indexes for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
 ADD PRIMARY KEY (`inventory_log_id`), ADD KEY `user_id` (`user_id`,`inventory_id`), ADD KEY `inventory_id` (`inventory_id`);

--
-- Indexes for table `ordercustomers`
--
ALTER TABLE `ordercustomers`
 ADD PRIMARY KEY (`order_id`), ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `orderlines`
--
ALTER TABLE `orderlines`
 ADD PRIMARY KEY (`line_id`), ADD KEY `order_id` (`order_id`,`store_item_id`), ADD KEY `store_item_id` (`store_item_id`);

--
-- Indexes for table `orderlogs`
--
ALTER TABLE `orderlogs`
 ADD PRIMARY KEY (`log_id`), ADD KEY `user_id` (`user_id`,`order_id`), ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `productsincategories`
--
ALTER TABLE `productsincategories`
 ADD PRIMARY KEY (`pnc_id`), ADD KEY `category_id` (`category_id`,`product_id`), ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `storeitemimages`
--
ALTER TABLE `storeitemimages`
 ADD PRIMARY KEY (`store_item_id`), ADD KEY `image_id` (`image_id`);

--
-- Indexes for table `storeitems`
--
ALTER TABLE `storeitems`
 ADD PRIMARY KEY (`store_item_id`), ADD KEY `product_id` (`product_id`,`unit_id`), ADD KEY `unit_id` (`unit_id`);

--
-- Indexes for table `supplierlogs`
--
ALTER TABLE `supplierlogs`
 ADD KEY `user_id` (`user_id`), ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
 ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
 ADD PRIMARY KEY (`purchase_line_id`), ADD KEY `supply_purchase_id` (`supply_purchase_id`,`store_item_id`), ADD KEY `store_item_id` (`store_item_id`);

--
-- Indexes for table `supplypurchases`
--
ALTER TABLE `supplypurchases`
 ADD PRIMARY KEY (`supply_purchase_id`);

--
-- Indexes for table `supplypurchasesuppliers`
--
ALTER TABLE `supplypurchasesuppliers`
 ADD PRIMARY KEY (`supply_purchase_id`), ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `unitmeasures`
--
ALTER TABLE `unitmeasures`
 ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `userinroles`
--
ALTER TABLE `userinroles`
 ADD PRIMARY KEY (`user_id`), ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `userlogs`
--
ALTER TABLE `userlogs`
 ADD PRIMARY KEY (`user_log_id`), ADD KEY `user_id` (`user_id`,`user2log_id`), ADD KEY `user2log_id` (`user2log_id`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
 ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `customerlogs`
--
ALTER TABLE `customerlogs`
MODIFY `customer_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
MODIFY `inventory_log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orderlines`
--
ALTER TABLE `orderlines`
MODIFY `line_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderlogs`
--
ALTER TABLE `orderlogs`
MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `productsincategories`
--
ALTER TABLE `productsincategories`
MODIFY `pnc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `storeitems`
--
ALTER TABLE `storeitems`
MODIFY `store_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
MODIFY `purchase_line_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplypurchases`
--
ALTER TABLE `supplypurchases`
MODIFY `supply_purchase_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `unitmeasures`
--
ALTER TABLE `unitmeasures`
MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `userlogs`
--
ALTER TABLE `userlogs`
MODIFY `user_log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `userroles`
--
ALTER TABLE `userroles`
MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categoryimages`
--
ALTER TABLE `categoryimages`
ADD CONSTRAINT `categoryimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `categoryimages_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customerlogs`
--
ALTER TABLE `customerlogs`
ADD CONSTRAINT `customerlogs_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `customerlogs_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discountedorders`
--
ALTER TABLE `discountedorders`
ADD CONSTRAINT `discountedorders_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `discountedorders_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`discount_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventories`
--
ALTER TABLE `inventories`
ADD CONSTRAINT `inventories_ibfk_1` FOREIGN KEY (`inventory_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
ADD CONSTRAINT `inventorylogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `inventorylogs_ibfk_2` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`inventory_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ordercustomers`
--
ALTER TABLE `ordercustomers`
ADD CONSTRAINT `ordercustomers_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ordercustomers_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlines`
--
ALTER TABLE `orderlines`
ADD CONSTRAINT `orderlines_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `orderlines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlogs`
--
ALTER TABLE `orderlogs`
ADD CONSTRAINT `orderlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `orderlogs_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productsincategories`
--
ALTER TABLE `productsincategories`
ADD CONSTRAINT `productsincategories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `productsincategories_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
ADD CONSTRAINT `seniorcitizens_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeitemimages`
--
ALTER TABLE `storeitemimages`
ADD CONSTRAINT `storeitemimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `storeitemimages_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeitems`
--
ALTER TABLE `storeitems`
ADD CONSTRAINT `storeitems_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `unitmeasures` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `storeitems_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplierlogs`
--
ALTER TABLE `supplierlogs`
ADD CONSTRAINT `supplierlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `supplierlogs_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
ADD CONSTRAINT `supplypurchaselines_ibfk_1` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `supplypurchaselines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchasesuppliers`
--
ALTER TABLE `supplypurchasesuppliers`
ADD CONSTRAINT `supplypurchasesuppliers_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `supplypurchasesuppliers_ibfk_2` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userinroles`
--
ALTER TABLE `userinroles`
ADD CONSTRAINT `userinroles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `userinroles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `userroles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userlogs`
--
ALTER TABLE `userlogs`
ADD CONSTRAINT `userlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `userlogs_ibfk_2` FOREIGN KEY (`user2log_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
