SELECT 
	SUM(quantity) as sold_items,
	SUM(quantity * price_per_unit) as total_sales
	from storeitems_sales 
	where week = 11 AND is_stockable = 0;

SELECT 
	category_name,
	SUM(quantity) as consumable_sold_items,
	SUM(quantity * price_per_unit) as total_sales,
	week, month, year
	from storeitems_sales 
	where is_stockable = 0 
	group by category_id, week
	order by category_id;

SELECT 
	category_name,
	SUM(quantity) as stockable_sold_items,
	SUM(quantity * price_per_unit) as total_sales,
	week, month, year
	from storeitems_sales 
	where is_stockable = 1
	group by category_id, week;