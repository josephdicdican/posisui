SELECT 
	storeitemname, week,
	FROM_DAYS(TO_DAYS(date_of_order) -MOD(TO_DAYS(date_of_order) -2, 7)) AS week_beginning,
	FROM_DAYS(TO_DAYS(date_of_order) -MOD(TO_DAYS(date_of_order) -1, 7)) AS week_end,
   SUM(quantity * price_per_unit) AS total,
   COUNT(*) AS transactions
  	FROM storeitems_sales
  	WHERE is_stockable = 0
 	GROUP BY store_item_id, FROM_DAYS(TO_DAYS(date_of_order) -MOD(TO_DAYS(date_of_order) -2, 7))
 	ORDER BY store_item_id, week DESC;
 	
SELECT DATE(DATE_FORMAT(date_of_order, '%Y-%m-01')) AS month_beginning,
       SUM(quantity * price_per_unit) AS total,
       COUNT(*) AS transactions
  FROM storeitems_sales
 GROUP BY DATE(DATE_FORMAT(date_of_order, '%Y-%m-01'))
 ORDER BY DATE(DATE_FORMAT(date_of_order, '%Y-%m-01'));

SELECT 
	storeitemname,
	SUM(quantity) as stockable_sold_items,
	SUM(quantity * price_per_unit) as total_sales,
	week, month, year
	from storeitems_sales 
	where is_stockable = 0
	group by store_item_id, week
	order by store_item_id;
	
SELECT 
	DISTINCT store_item_id, storeitemname
	from storeitems_sales
	where is_stockable = 0
	order by store_item_id;
	
SELECT
	DISTINCT week,
	FROM_DAYS(TO_DAYS(date_of_order) -MOD(TO_DAYS(date_of_order) -2, 7)) AS week_beginning,
	FROM_DAYS(TO_DAYS(date_of_order) -MOD(TO_DAYS(date_of_order) -1, 7)) AS week_end
	from storeitems_sales;
	
SELECT DATE(DATE_FORMAT(date_of_order, '%Y-%m-01')) AS month_beginning,
       SUM(quantity * price_per_unit) AS total,
       COUNT(*) AS transactions
  FROM storeitems_sales
 GROUP BY DATE(DATE_FORMAT(date_of_order, '%Y-%m-01'))
 ORDER BY DATE(DATE_FORMAT(date_of_order, '%Y-%m-01'));
 
 
 select 
 	max(week), 
	 storeitemname 
	 from storeitems_sales; 



