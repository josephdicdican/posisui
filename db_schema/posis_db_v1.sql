-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2016 at 04:12 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `posis_db`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `count_products_in_category`(`category_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(productsincategories.category_id) as categorizedproducts
    FROM 
    	productsincategories 
    LEFT JOIN
    	storeitems ON productsincategories.product_id = storeitems.product_id
    WHERE 
    	productsincategories.category_id = category_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_by_product_id`(`product_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.product_id) as product_storeitems
    FROM storeitems WHERE storeitems.product_id = product_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `count_storeitems_with_unit`(`unit_id` INT(11)) RETURNS int(11)
BEGIN
	declare no_more_rows TINYINT(1);
    declare items INTEGER(11);
    
    declare cur cursor for
    SELECT 
    	count(storeitems.unit_id) as storeitems_with_unit
    FROM storeitems 
    WHERE storeitems.unit_id = unit_id;
      
    
    declare continue handler for not found
    set no_more_rows = true;
    open cur;
    fetch cur into items;
    
    if no_more_rows then
    	set items = 0;
    ELSE
    
    	if ISNULL(items) THEN
        	set items = 0;
        end if;
    end if;
    
    close cur;

  RETURN items;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_storeitem_name`(`item_name` VARCHAR(100), `measure_name` VARCHAR(100)) RETURNS varchar(200) CHARSET latin1
    NO SQL
BEGIN declare storeitemname varchar(200); declare n1 varchar(100); declare n2 varchar(200); set storeitemname = ''; set n1 = TRIM(item_name); set n2 = TRIM(measure_name); set storeitemname = CONCAT(n1, ' ',n2); return storeitemname; END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `category_name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_id`, `category_name`) VALUES
(4, 0, 'Foods'),
(6, 4, 'Desserts'),
(7, 4, 'Viands'),
(35, 0, 'Groceries'),
(36, 35, 'Delata'),
(53, 0, 'Drinks'),
(54, 53, 'Soft Drinks'),
(59, 53, 'Beverages'),
(60, 53, 'Wine');

-- --------------------------------------------------------

--
-- Stand-in structure for view `categories_list`
--
CREATE TABLE IF NOT EXISTS `categories_list` (
`category_id` int(11)
,`parent_id` int(11)
,`category_name` varchar(100)
,`image_id` int(11)
,`image` varchar(255)
,`image_type` tinyint(2)
,`products_count` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `categoryimages`
--

CREATE TABLE IF NOT EXISTS `categoryimages` (
  `category_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoryimages`
--

INSERT INTO `categoryimages` (`category_id`, `image_id`) VALUES
(53, 21),
(54, 22),
(4, 23),
(6, 24),
(7, 25);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
`customer_id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` tinyint(2) NOT NULL,
  `is_senior` tinyint(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `firstname`, `lastname`, `address`, `email`, `gender`, `is_senior`) VALUES
(1, 'Juan', 'Dela Cruz', 'Sa amo.a', 'juan.delacruz@gmail.com', 1, 1),
(6, 'Joseph', 'Dicdican', 'Dalaguete, Cebu', 'joseph.dicdican@gmail.com', 1, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `customers_list`
--
CREATE TABLE IF NOT EXISTS `customers_list` (
`customer_id` int(11)
,`firstname` varchar(100)
,`lastname` varchar(100)
,`address` varchar(100)
,`email` varchar(100)
,`gender` tinyint(2)
,`is_senior` tinyint(2)
,`card_no` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
`discount_id` int(11) NOT NULL,
  `discount_percentage` double NOT NULL,
  `discount_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`image_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_type` tinyint(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`image_id`, `image`, `image_type`) VALUES
(21, '1453823114-penguins.jpg', 0),
(22, '1453827003-jellyfish.jpg', 0),
(23, '1453827052-koala.jpg', 0),
(24, '1453827072-tulips.jpg', 0),
(25, '1453987920-hydrangeas.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `inventory_id` int(11) NOT NULL,
  `stock_at_hand` int(11) NOT NULL DEFAULT '0',
  `max_stock_qty` int(11) NOT NULL DEFAULT '0',
  `min_stock_qty` int(11) NOT NULL DEFAULT '0',
  `reordering_qty` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`inventory_id`, `stock_at_hand`, `max_stock_qty`, `min_stock_qty`, `reordering_qty`) VALUES
(1, 0, 10, 5, 9),
(2, 0, 20, 10, 15);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventories_list`
--
CREATE TABLE IF NOT EXISTS `inventories_list` (
`inventory_id` int(11)
,`stock_at_hand` int(11)
,`max_stock_qty` int(11)
,`min_stock_qty` int(11)
,`reordering_qty` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitemname` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `inventorylogs`
--

CREATE TABLE IF NOT EXISTS `inventorylogs` (
`inventory_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderlines`
--

CREATE TABLE IF NOT EXISTS `orderlines` (
`line_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderlogs`
--

CREATE TABLE IF NOT EXISTS `orderlogs` (
`log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderpayments`
--

CREATE TABLE IF NOT EXISTS `orderpayments` (
  `order_id` int(11) NOT NULL,
  `payment` double NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date_of_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_checkout` tinyint(2) NOT NULL,
  `is_discounted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productimages`
--

CREATE TABLE IF NOT EXISTS `productimages` (
  `product_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`) VALUES
(1, 'Coke'),
(2, 'Sprite'),
(3, '555 Sardines'),
(4, '555 Sardines'),
(5, 'Carne Norte'),
(6, 'Mango Float'),
(7, 'Coke');

-- --------------------------------------------------------

--
-- Table structure for table `productsincategories`
--

CREATE TABLE IF NOT EXISTS `productsincategories` (
`pnc_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `productsincategories`
--

INSERT INTO `productsincategories` (`pnc_id`, `category_id`, `product_id`) VALUES
(6, 6, 6),
(3, 36, 3),
(4, 36, 4),
(5, 36, 5),
(1, 54, 1),
(2, 54, 2),
(7, 54, 7);

-- --------------------------------------------------------

--
-- Table structure for table `seniorcitizens`
--

CREATE TABLE IF NOT EXISTS `seniorcitizens` (
`customer_id` int(11) NOT NULL,
  `card_no` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `seniorcitizens`
--

INSERT INTO `seniorcitizens` (`customer_id`, `card_no`) VALUES
(1, 'SC-0099-01111');

-- --------------------------------------------------------

--
-- Table structure for table `storeitems`
--

CREATE TABLE IF NOT EXISTS `storeitems` (
`store_item_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `sku` varchar(200) NOT NULL,
  `price_per_unit` double NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `storeitems`
--

INSERT INTO `storeitems` (`store_item_id`, `product_id`, `unit_id`, `sku`, `price_per_unit`) VALUES
(1, 1, 6, '2147483647', 25),
(2, 2, 7, '0488723566545', 10),
(3, 3, 8, '213442524543', 18),
(4, 4, 9, '8937942343', 10),
(5, 5, 8, '21432542523', 35),
(6, 5, 9, '2354363454523', 18),
(7, 6, 3, '143254365475464', 10),
(8, 6, 10, '1234254656', 75.5),
(9, 7, 11, '21354678', 25);

-- --------------------------------------------------------

--
-- Stand-in structure for view `storeitems_list`
--
CREATE TABLE IF NOT EXISTS `storeitems_list` (
`store_item_id` int(11)
,`sku` varchar(200)
,`price_per_unit` double
,`product_id` int(11)
,`product_name` varchar(100)
,`unit_id` int(11)
,`measure_name` varchar(100)
,`category_id` int(11)
,`category_name` varchar(100)
,`parent_id` int(11)
,`storeitemname` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
`supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_contact` varchar(100) DEFAULT NULL,
  `supplier_email` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `supplier_name`, `supplier_contact`, `supplier_email`) VALUES
(1, 'Aqua Soft', '09107728986', 'aquasoft@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchaselines`
--

CREATE TABLE IF NOT EXISTS `supplypurchaselines` (
`purchase_line_id` int(11) NOT NULL,
  `supply_purchase_id` int(11) NOT NULL,
  `store_item_id` int(11) NOT NULL,
  `purchase_qty` int(11) NOT NULL,
  `purchase_price_per_unit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchases`
--

CREATE TABLE IF NOT EXISTS `supplypurchases` (
`supply_purchase_id` int(11) NOT NULL,
  `date_of_purchase` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_confirmed` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `supplypurchasesuppliers`
--

CREATE TABLE IF NOT EXISTS `supplypurchasesuppliers` (
  `supply_purchase_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unitmeasures`
--

CREATE TABLE IF NOT EXISTS `unitmeasures` (
`unit_id` int(11) NOT NULL,
  `measure_name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `unitmeasures`
--

INSERT INTO `unitmeasures` (`unit_id`, `measure_name`) VALUES
(1, 'unit'),
(2, 'piece'),
(3, 'cup'),
(4, 'serve'),
(5, 'glass'),
(6, 'litre'),
(7, '8 ounce'),
(8, 'can'),
(9, '10 g'),
(10, 'box'),
(11, '1.5 litre');

-- --------------------------------------------------------

--
-- Stand-in structure for view `unitmeasure_storeitems`
--
CREATE TABLE IF NOT EXISTS `unitmeasure_storeitems` (
`unit_id` int(11)
,`measure_name` varchar(100)
,`storeitems_count` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `userinroles`
--

CREATE TABLE IF NOT EXISTS `userinroles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userinroles`
--

INSERT INTO `userinroles` (`user_id`, `role_id`) VALUES
(18, 1),
(19, 2),
(20, 2),
(21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE IF NOT EXISTS `userroles` (
`role_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `display_name`, `username`, `password`, `date_created`, `date_modified`) VALUES
(18, 'Admin', 'administrator', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-01-17 16:00:00', '2016-01-18 17:51:55'),
(19, 'Joseph', 'jose@pos', 'c0b137fe2d792459f26ff763cce44574a5b5ab03', '2016-01-18 17:53:19', '2016-01-19 20:50:04'),
(20, 'Jose', 'joseph@pos', '4a3487e57d90e2084654b6d23937e75af5c8ee55', '2016-01-21 20:00:13', '0000-00-00 00:00:00'),
(21, 'HUHUHU', 'HAAHAHA', '3085296069762c17b36f0cb5db8110c654b4d669', '2016-02-04 19:14:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_list`
--
CREATE TABLE IF NOT EXISTS `users_list` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`role_id` int(11)
,`role_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `users_vw`
--
CREATE TABLE IF NOT EXISTS `users_vw` (
`user_id` int(11)
,`display_name` varchar(100)
,`username` varchar(100)
,`password` varchar(255)
,`date_created` timestamp
,`date_modified` timestamp
,`role_id` int(11)
,`role_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Structure for view `categories_list`
--
DROP TABLE IF EXISTS `categories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `categories_list` AS select `c`.`category_id` AS `category_id`,`c`.`parent_id` AS `parent_id`,`c`.`category_name` AS `category_name`,`ci`.`image_id` AS `image_id`,`i`.`image` AS `image`,`i`.`image_type` AS `image_type`,`count_products_in_category`(`c`.`category_id`) AS `products_count` from ((`categories` `c` left join `categoryimages` `ci` on((`c`.`category_id` = `ci`.`category_id`))) left join `images` `i` on((`ci`.`image_id` = `i`.`image_id`)));

-- --------------------------------------------------------

--
-- Structure for view `customers_list`
--
DROP TABLE IF EXISTS `customers_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customers_list` AS select `c`.`customer_id` AS `customer_id`,`c`.`firstname` AS `firstname`,`c`.`lastname` AS `lastname`,`c`.`address` AS `address`,`c`.`email` AS `email`,`c`.`gender` AS `gender`,`c`.`is_senior` AS `is_senior`,`s`.`card_no` AS `card_no` from (`customers` `c` left join `seniorcitizens` `s` on((`c`.`customer_id` = `s`.`customer_id`)));

-- --------------------------------------------------------

--
-- Structure for view `inventories_list`
--
DROP TABLE IF EXISTS `inventories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventories_list` AS select `i`.`inventory_id` AS `inventory_id`,`i`.`stock_at_hand` AS `stock_at_hand`,`i`.`max_stock_qty` AS `max_stock_qty`,`i`.`min_stock_qty` AS `min_stock_qty`,`i`.`reordering_qty` AS `reordering_qty`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname` from (((`inventories` `i` left join `storeitems` `s` on((`i`.`inventory_id` = `s`.`store_item_id`))) left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`)));

-- --------------------------------------------------------

--
-- Structure for view `storeitems_list`
--
DROP TABLE IF EXISTS `storeitems_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storeitems_list` AS select `s`.`store_item_id` AS `store_item_id`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`pc`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`c`.`parent_id` AS `parent_id`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname` from ((((`storeitems` `s` left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`))) left join `productsincategories` `pc` on((`p`.`product_id` = `pc`.`product_id`))) left join `categories` `c` on((`c`.`category_id` = `pc`.`category_id`)));

-- --------------------------------------------------------

--
-- Structure for view `unitmeasure_storeitems`
--
DROP TABLE IF EXISTS `unitmeasure_storeitems`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `unitmeasure_storeitems` AS select `unitmeasures`.`unit_id` AS `unit_id`,`unitmeasures`.`measure_name` AS `measure_name`,`count_storeitems_with_unit`(`unitmeasures`.`unit_id`) AS `storeitems_count` from `unitmeasures`;

-- --------------------------------------------------------

--
-- Structure for view `users_list`
--
DROP TABLE IF EXISTS `users_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_list` AS select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`)));

-- --------------------------------------------------------

--
-- Structure for view `users_vw`
--
DROP TABLE IF EXISTS `users_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_vw` AS select `users`.`user_id` AS `user_id`,`users`.`display_name` AS `display_name`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`date_created` AS `date_created`,`users`.`date_modified` AS `date_modified`,`userinroles`.`role_id` AS `role_id`,`userroles`.`role_name` AS `role_name` from ((`users` left join `userinroles` on((`users`.`user_id` = `userinroles`.`user_id`))) left join `userroles` on((`userinroles`.`role_id` = `userroles`.`role_id`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`category_id`), ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `categoryimages`
--
ALTER TABLE `categoryimages`
 ADD PRIMARY KEY (`category_id`), ADD UNIQUE KEY `image_id` (`image_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
 ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
 ADD PRIMARY KEY (`inventory_id`);

--
-- Indexes for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
 ADD PRIMARY KEY (`inventory_log_id`), ADD KEY `user_id` (`user_id`,`inventory_id`), ADD KEY `inventory_id` (`inventory_id`);

--
-- Indexes for table `orderlines`
--
ALTER TABLE `orderlines`
 ADD PRIMARY KEY (`line_id`), ADD KEY `order_id` (`order_id`,`store_item_id`), ADD KEY `store_item_id` (`store_item_id`);

--
-- Indexes for table `orderlogs`
--
ALTER TABLE `orderlogs`
 ADD PRIMARY KEY (`log_id`), ADD KEY `user_id` (`user_id`,`order_id`), ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `orderpayments`
--
ALTER TABLE `orderpayments`
 ADD PRIMARY KEY (`order_id`), ADD KEY `discount_id` (`discount_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`order_id`), ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `productimages`
--
ALTER TABLE `productimages`
 ADD PRIMARY KEY (`product_id`), ADD UNIQUE KEY `image_id` (`image_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `productsincategories`
--
ALTER TABLE `productsincategories`
 ADD PRIMARY KEY (`pnc_id`), ADD KEY `category_id` (`category_id`,`product_id`), ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `storeitems`
--
ALTER TABLE `storeitems`
 ADD PRIMARY KEY (`store_item_id`), ADD KEY `product_id` (`product_id`,`unit_id`), ADD KEY `unit_id` (`unit_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
 ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
 ADD PRIMARY KEY (`purchase_line_id`), ADD KEY `supply_purchase_id` (`supply_purchase_id`,`store_item_id`), ADD KEY `store_item_id` (`store_item_id`);

--
-- Indexes for table `supplypurchases`
--
ALTER TABLE `supplypurchases`
 ADD PRIMARY KEY (`supply_purchase_id`);

--
-- Indexes for table `supplypurchasesuppliers`
--
ALTER TABLE `supplypurchasesuppliers`
 ADD PRIMARY KEY (`supply_purchase_id`), ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `unitmeasures`
--
ALTER TABLE `unitmeasures`
 ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `userinroles`
--
ALTER TABLE `userinroles`
 ADD PRIMARY KEY (`user_id`), ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
 ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
MODIFY `inventory_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderlines`
--
ALTER TABLE `orderlines`
MODIFY `line_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderlogs`
--
ALTER TABLE `orderlogs`
MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `productsincategories`
--
ALTER TABLE `productsincategories`
MODIFY `pnc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `storeitems`
--
ALTER TABLE `storeitems`
MODIFY `store_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
MODIFY `purchase_line_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplypurchases`
--
ALTER TABLE `supplypurchases`
MODIFY `supply_purchase_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `unitmeasures`
--
ALTER TABLE `unitmeasures`
MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `userroles`
--
ALTER TABLE `userroles`
MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categoryimages`
--
ALTER TABLE `categoryimages`
ADD CONSTRAINT `categoryimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `categoryimages_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventories`
--
ALTER TABLE `inventories`
ADD CONSTRAINT `inventories_ibfk_1` FOREIGN KEY (`inventory_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventorylogs`
--
ALTER TABLE `inventorylogs`
ADD CONSTRAINT `inventorylogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `inventorylogs_ibfk_2` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`inventory_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlines`
--
ALTER TABLE `orderlines`
ADD CONSTRAINT `orderlines_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `orderlines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlogs`
--
ALTER TABLE `orderlogs`
ADD CONSTRAINT `orderlogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `orderlogs_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderpayments`
--
ALTER TABLE `orderpayments`
ADD CONSTRAINT `orderpayments_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `orderpayments_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`discount_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productimages`
--
ALTER TABLE `productimages`
ADD CONSTRAINT `productimages_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `productimages_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productsincategories`
--
ALTER TABLE `productsincategories`
ADD CONSTRAINT `productsincategories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `productsincategories_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seniorcitizens`
--
ALTER TABLE `seniorcitizens`
ADD CONSTRAINT `seniorcitizens_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeitems`
--
ALTER TABLE `storeitems`
ADD CONSTRAINT `storeitems_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `unitmeasures` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `storeitems_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchaselines`
--
ALTER TABLE `supplypurchaselines`
ADD CONSTRAINT `supplypurchaselines_ibfk_1` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `supplypurchaselines_ibfk_2` FOREIGN KEY (`store_item_id`) REFERENCES `storeitems` (`store_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplypurchasesuppliers`
--
ALTER TABLE `supplypurchasesuppliers`
ADD CONSTRAINT `supplypurchasesuppliers_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `supplypurchasesuppliers_ibfk_2` FOREIGN KEY (`supply_purchase_id`) REFERENCES `supplypurchases` (`supply_purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userinroles`
--
ALTER TABLE `userinroles`
ADD CONSTRAINT `userinroles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `userinroles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `userroles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
