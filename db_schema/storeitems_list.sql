-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2016 at 04:15 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `posis_db`
--

-- --------------------------------------------------------

--
-- Structure for view `storeitems_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storeitems_list` AS select `s`.`store_item_id` AS `store_item_id`,`s`.`sku` AS `sku`,`s`.`price_per_unit` AS `price_per_unit`,`p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`u`.`unit_id` AS `unit_id`,`u`.`measure_name` AS `measure_name`,`pc`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`c`.`parent_id` AS `parent_id`,`get_storeitem_name`(`p`.`product_name`,`u`.`measure_name`) AS `storeitemname`,`i`.`inventory_id` AS `inventory_id` from (((((`storeitems` `s` left join `products` `p` on((`s`.`product_id` = `p`.`product_id`))) left join `unitmeasures` `u` on((`s`.`unit_id` = `u`.`unit_id`))) left join `productsincategories` `pc` on((`p`.`product_id` = `pc`.`product_id`))) left join `categories` `c` on((`c`.`category_id` = `pc`.`category_id`))) left join `inventories` `i` on((`s`.`store_item_id` = `i`.`inventory_id`)));

--
-- VIEW  `storeitems_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
